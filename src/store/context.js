import React, { useEffect, useContext, useReducer } from 'react';
import reducer from './reducer'
import {
    ADD_USER,
    REGISTER_ALERT,
    GET_VALUES,
    LOG_OUT,
    DISPLAY_GAMES,
    GET_DAYS_IN_WEEK,
    GET_VOICE,
    SHOW_PROFILE,
    GIVE_LOGIN_ACCESS,
    GET_WHATSAPP,
    DISPLAY_PROMOTION,
    URL,
    SPINNER,
    GET_BETS,
    VALIDATE_SENT_TOKEN,
    LOGIN,
    NO_USER,
    CANCEL_GAME,
    ADMIN_PRIVILEDGE,
    USERS
} from './actions'

//http://localhost:5016

const url = "https://api.grandlotto.ng/api/v1/site-settings"
const urls = "https://api.grandlotto.ng/api/v1/promotions"

const AppContext = React.createContext()

const initialState = {
    isLoggedIn: '',
    adminToken: '',
    loading: false,
    value: [],
    voice: '',
    logedIn: false,
    admin: false,
    whatsapp: '',
    newALert: '',
    profile: null,
    alert: [],
    game: [],
    promo: [],
    days: [],
    urls: '',
    week: [],
    registered: [],
    loggedIn: [],
    grantAccess: [],
    bookedGames: 0,
    bets: [],
    canceledGames: 0,
    user: []
}

const AppProvider = ({ children }) => {
    const [state, dispatch] = useReducer(reducer, initialState)

    const addAlert = (user) => {
        dispatch({ type: ADD_USER, payload: user });
    }

    const RegisterAlert = (prop) => {
        dispatch({ type: REGISTER_ALERT, payload: prop });
    }

    const giveAdminAccess = (token) => {
        dispatch({type: ADMIN_PRIVILEDGE, payload: token});
    }

    const getUser = (e) => {
        dispatch({type: USERS, payload: e})
    }

    const getUrl = (u) => {
        dispatch({type: URL, payload: u })
    }

    const getValues = (values) => {
        dispatch({type: GET_VALUES, payload: values})
    }

    const logOut = (event) => {
        dispatch({type: LOG_OUT, payload: event})
    }

    
    const fetchData3 = async () => {
        let get = localStorage.getItem('token')
        var myHeaders = new Headers();
        myHeaders.append("signatures", "a0451a967f04a3ac7dc526086749599249a53b3d9e81b71afeb4f3efab8214d5");
        myHeaders.append("Authorization", `Bearer ${get}`);

        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };

        fetch('https://api.grandlotto.ng/api/v2/auth/betting-list', requestOptions)
            .then(response => response.json())
            .then(result => {
                const data = result.success.result.user.filter((bets) => {
                    return [bets.id, bets.bet_id, bets.winningAmount, bets.gameName, bets.kind, bets.status, bets.amounts, bets.results, bets.amount, bets.odd, bets.possibleWinning, bets.coupon, bets.staked, bets.stakes, bets.stakes1, bets.stakes2, bets.date]
                })
                
                dispatch({type: GET_BETS, payload: data})
            })
            .catch(error => console.log('error', error));
    }


    useEffect(() => {

        let isCancelled = false
        const get = localStorage.getItem('token')

        

        fetchData3()

        return () => {
            isCancelled = true
        }
   
    }, []);

    const fetchData = async () => {
        try {
            const res = await fetch(url)
            const data = await res.json()
            const arr = data.success.data
            dispatch({type: DISPLAY_GAMES, payload: arr})
        } catch (err) {
            console.log(`hello error from the api server:  ${err}`)
        }
    }

        const fetchData1 = async () => {
            try {
            const res = await fetch(urls)
            const data = await res.json()
            // console.log(data)
            const arr = data.success.data
            // console.log(arr)
            dispatch({type: DISPLAY_PROMOTION, payload: arr})
        } catch (err) {
            console.log(`hello error from the api server:  ${err}`)
        }
        
    }

    useEffect(() => {
        fetchData()
        fetchData1()
    }, [url, urls])

    const daysOfWeek = () => {
        function GetDates(startDate, daysToAdd) {
            var aryDates = [];

            for (var i = 0; i <= daysToAdd; i++) {
                var currentDate = new Date();
                currentDate.setDate(startDate.getDate() + i);
                aryDates.push(DayAsString(currentDate.getDay()) + ", " + currentDate.getDate() + " " + MonthAsString(currentDate.getMonth()));
            }

             return aryDates;
        }

        function MonthAsString(monthIndex) {
            var month = [];
            month[0] = "Jan";
            month[1] = "Feb";
            month[2] = "Mar";
            month[3] = "Apr";
            month[4] = "May";
            month[5] = "Jun";
            month[6] = "Jul";
            month[7] = "Aug";
            month[8] = "Sept";
            month[9] = "Oct";
            month[10] = "Nov";
            month[11] = "Dec";

            return month[monthIndex];
        }

        function DayAsString(dayIndex) {
            var weekdays = new Array(7);
            weekdays[0] = "Sunday";
            weekdays[1] = "Monday";
            weekdays[2] = "Tuesday";
            weekdays[3] = "Wednesday";
            weekdays[4] = "Thursday";
            weekdays[5] = "Friday";
            weekdays[6] = "Saturday";

            return weekdays[dayIndex];
        }


        var startDate = new Date();
        var aryDates = GetDates(startDate, 3);
        dispatch({type: GET_DAYS_IN_WEEK, payload: aryDates})
    }

   

    useEffect(() => {
        daysOfWeek()
    }, [])

    const showVoice = (event) => {
        dispatch({type: GET_VOICE, payload: event})
    }

    const showBoard = (event) => {
        dispatch({type: SHOW_PROFILE, payload: event})
    } 

    const giveAccess = (token) => {
        dispatch({type: GIVE_LOGIN_ACCESS, payload: token})
    }

    const showWhatsapp = (event) => {
        dispatch({ type: GET_WHATSAPP, payload: event });
    }
    
    const validateRegistration = () => {
        dispatch({type: VALIDATE_SENT_TOKEN})
    }
    
    const loggingIn = (userLogin) => {
        dispatch({type: LOGIN, payload: userLogin})
    }

    const noUser = () => {
        dispatch({ type: NO_USER });
    }

    const cancelGame = (gameId) => {
        dispatch({type: CANCEL_GAME, payload: gameId})
    }

    

    return (
        <AppContext.Provider value={{
            ...state,
            addAlert,
            getValues,
            validateRegistration,
            daysOfWeek,
            getUser,
            loggingIn,
            cancelGame,
            noUser,
            showVoice,
            getUrl,
            logOut,
            showBoard,
            showWhatsapp,
            giveAccess,
            RegisterAlert,
            giveAdminAccess
        }}>
            {children}
        </AppContext.Provider>
    )
}

export const useGlobalContext = () => {
    return useContext(AppContext)
}

export { AppContext, AppProvider };
