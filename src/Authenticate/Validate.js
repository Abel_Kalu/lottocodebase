import React, { useState, useEffect } from 'react'
import { Form, Button, Row, Col, Container } from 'react-bootstrap';
import { Formik } from 'formik';
import IntegrationNotistack from '../Fetch/IntegrationNotistack';
import { useHistory } from 'react-router';
import Layout from '../Layout'
import * as yup from 'yup';
 
const schema = yup.object().shape({
  otp: yup.string().required(),
});

const Validate = () => {
  const [success, setSuccess] = useState('')
  const url = 'https://api.grandlotto.ng'
  const [showAlert, setShowAlert] = useState(false)
  const history = useHistory()
  const [show, setShow] = useState(false)

  useEffect(() => {
    setTimeout(() => {
      setSuccess('')
    }, 3000)
  }, [success])
  
  return (
    <section className='register_section d-flex justify-content-center mt-5' >
      <Container fluid='md'>
        <Row>
          <Col className='mt-5' md={{ span: 12, offset: 1 }}>
            <Formik
              validationSchema={schema}
              onSubmit={values => {
                var myHeaders = new Headers();
                myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
                myHeaders.append("Content-Type", "application/json");

                var raw = JSON.stringify({
                  "otp": `${values.otp}`,
                });

                var requestOptions = {
                  method: 'POST',
                  headers: myHeaders,
                  body: raw,
                  redirect: 'follow'
                }

                //http://localhost:5016
                //https://lotto-api.firstbetng.com

                fetch(`${url}/api/v2/auth/password-validate`, requestOptions)
                  .then(response => response.json())
                  .then(result => {
                    if (result.success) {
                      const { message } = result.success;
                      setSuccess(message)
                      setShowAlert(true)

                      history.push('/validate/login')
                    } else if (result.error) {
                      const { message } = result.error;
                      setSuccess(message)
                    } else {
                      return;
                    }
                  },
                    (error) => {
                      console.log(error)
                    });

              }}
              initialValues={{
                otp: '',
              }}
            >
              {({
                handleSubmit,
                handleChange,
                handleBlur,
                values,
                touched,
                isValid,
                errors,
              }) => (
                <Form noValidate onSubmit={handleSubmit}>
                  <Form.Group as={Col} md="10" controlId="validationFormik04">
                    <Form.Label>Otp</Form.Label>
                    <Form.Control
                      value={values.otp}
                      type="text"
                      name="otp"
                      onChange={handleChange}
                      placeholder="otp"
                      isInvalid={!!errors.otp}
                    />
                    <Form.Control.Feedback type="invalid">
                      {errors.otp}
                    </Form.Control.Feedback>
                  </Form.Group>
                  <Button className='mt-2 ml-3 my-3' type='submit' variant="outline-success">Submit</Button>
                </Form>
              )}
            </Formik>
            <h6>Otp not sent ?</h6>
            <div className='d-flex'>
              <Button variant='success' onClick={() => history.push('/validate/whatsapp')} className='mb-3 mr-2' >Whatsapp</Button>
              <Button variant='success' onClick={() => history.push('/validate/voice')} className='mb-3' >Voice</Button>
            </div>
            
          </Col>
        </Row>
        {success && <IntegrationNotistack success={`${success}`} />}
      </Container>
    </section>
  )
}

export default Validate 


