import React, { useState, useEffect } from 'react';
import { Form, Button, Row, Container, Col, Tab, Tabs } from 'react-bootstrap';
import { useGlobalContext } from '../store/context';
import { Formik } from 'formik';
import Layout from '../Layout'
import { useHistory } from 'react-router';
import IntegrationNotistack from '../Fetch/IntegrationNotistack';
 import * as Yup from 'yup';

//  const schema2 = Yup.object().shape({
//   email: Yup.string()
//   .when('mobile', {
//     is: (mobile) => !mobile || mobile.length === 0,
//     then: Yup.string()
//       .required('At least one of the fields is required'),
//   }),
//   mobile: Yup.string()
//   .when('email', {
//     is: (email) => !email || email.length === 0,
//     then: Yup.string()
//       .required('At least one of the fields is required'),
//   }),
//   password: Yup.string().min(8, 'Must be 8 characters or more').required('Please provide a password'),
// }, ['email', 'mobile'])

const schema2 = Yup.object().shape({
  email: Yup.string().email('Email is required').required('Email is required'),
  // checked: Yup.string().email('Check box is required').required('Please accept out T/Cs'),
  password: Yup.string().min(8, 'Must be 8 characters or more').required('Pawword is required'),
  // confPassword: Yup.string().oneOf([Yup.ref('password'), null], 'Password Must Match').required('Confirm password')
});

const schema3 = Yup.object().shape({
  password: Yup.string().min(8, 'Must be 8 characters or more').required('Password is required'),
  // checked: Yup.string().email('Check box is required').required('Please accept out T/Cs'),
  mobile: Yup.string().min(11, 'Must be 11 characters long').required(),
  // confPassword: Yup.string().oneOf([Yup.ref('password'), null], 'Password Must Match').required('Confirm password')
});

// const schema = yup.object().shape({
//   mobile: yup.string().min(11, 'Must be 11 characters or more').required(),
//   password: yup.string().min(8, 'Must be 8 characters or more').required(),
// });

const Login = () => {
  const { giveAccess, showBoard, isLoggedIn } = useGlobalContext();
  let history = useHistory()
  const [success, setSuccess] = useState(false)
  const url = 'https://api.grandlotto.ng'
  const [showAlert, setShowAlert] = useState(false)

  useEffect(() => {
    let time = setTimeout(() => {
      setShowAlert(!showAlert)
      setSuccess('')
    }, 3000)

    return () => clearTimeout(time)
  }, [success]);
    
  return (
    <article>
      <div style={{ marginTop: '42px' }} className='d-flex justify-content-center'>
        <h5 className='green'>Login</h5>
      </div>
        
      <Tabs defaultActiveKey="Phone" className='d-flex justify-content-center' style={{ marginTop: '10px' }} transition={false} id="noanim-tab-example">
        <Tab eventKey="Phone" title="Phone">
          <section className='register_sections d-flex justify-content-center'>
            <Container fluid='md'>
              <Row>
                <Col className='mt-5' md={{ span: 12, offset: 1 }}>
                  <h5 className='ml-3 mb-1 green h'>Welcome to GrandLotto, Please Login Here</h5>
                  <Formik
                    validationSchema={schema3}
                    onSubmit={values => {
                      var myHeaders = new Headers();
                      myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
                      myHeaders.append("Content-Type", "application/json");


                      var raw = JSON.stringify({
                        "password": `${values.password}`,
                        "mobile": `${values.mobile}`
                      });


                      var requestOptions = {
                        method: 'POST',
                        headers: myHeaders,
                        body: raw,
                        redirect: 'follow'
                      };

                      // https://grandlotto.herokuapp.com/api/v2/auth/profile

                      fetch(`${url}/api/v1/login`, requestOptions)
                        .then(response => response.json())
                        .then(result => {
                          if (result.success) {
                            history.push('/games')
                            const { token } = result.success;
                            setShowAlert(true)
                            giveAccess(token)
                            localStorage.setItem('token', token)
                            var myHeaders = new Headers();
                            myHeaders.append("signatures", "5a1131f2eb747be50714281ec3e68b759476c6dc9e1faf5fc5d91c552cf8c230");
                            myHeaders.append("Authorization", `Bearer ${token}`);

                            var requestOptions = {
                              method: 'GET',
                              headers: myHeaders,
                              redirect: 'follow'
                            };

                            fetch(`${url}/api/v2/auth/profile`, requestOptions)
                              .then(response => response.json())
                              .then(result => {
                                if (result.success) {
                                  const { data } = result.success;
                                  setSuccess(true)
                                  showBoard(data)
                                } else {
                                  setSuccess('Please refresh and try again')
                                  return;
                                }
                              },
                                (error) => {
                                  console.log(error)
                                });
                          } else {
                            setSuccess('Mobile number and password incorrect')
                            return;
                          }
                        },
                          (error) => {
                            console.log(error)
                          }
                        )

                    }}
                    initialValues={{
                      email: '',
                      mobile: '',
                      // checked: '',
                      // referer: '',
                      password: '',
                      // confPassword: '',
                    }}
                  >
                    {({
                      handleSubmit,
                      handleChange,
                      handleBlur,
                      values,
                      touched,
                      isValid,
                      errors,
                    }) => (
                      <Form noValidate onSubmit={handleSubmit}>
                        <Form.Group as={Col} md="10" className='mt-2' controlId="validationFormik04">
                          <Form.Label>Mobile Number</Form.Label>
                          <Form.Control
                            type="text"
                            name="mobile"
                            onChange={handleChange}
                            placeholder="Mobile"
                            isInvalid={!!errors.mobile}
                            value={values.mobile}
                          />
                          <Form.Control.Feedback type="invalid">
                            {errors.mobile}
                          </Form.Control.Feedback>
                        </Form.Group>
                        <Form.Group as={Col} md="10" className='mt-2' controlId="validationFormik05">
                          <Form.Label>Password</Form.Label>
                          <Form.Control
                            type='password'
                            placeholder="Password"
                            name="password"
                            value={values.password}
                            onChange={handleChange}
                            isInvalid={!!errors.password}
                          />
                                            
                          <Form.Control.Feedback className='d-inline-block' type="invalid">
                            {errors.password}
                          </Form.Control.Feedback>


                        </Form.Group>
                        {/* <Form.Group as={Col} md="10" className='mt-2' controlId="validationFormik05">
                                            <Form.Label>Confirm Password</Form.Label>
                            <Form.Control
                                type='password'
                                placeholder="xxxxxxxxxxxxxxxx"
                                name="confPassword"
                                value={values.confPassword}
                                onChange={handleChange}
                                isInvalid={!!errors.confPassword}
                                    />
              <Form.Control.Feedback type="invalid">
                {errors.confPassword}
              </Form.Control.Feedback>
                          </Form.Group> */}
                        <Button className='mt-2 ml-3 my-3' type='submit' size='sm' variant="success">Submit</Button>
                      </Form>
        
                    )}
                  </Formik>
                </Col>
              </Row>
              {success && <IntegrationNotistack success={`${success}`} />}
            </Container>
          </section>
        </Tab>
        <Tab eventKey="Email" title="Email">
          <section className='register_sections d-flex justify-content-center'>
            <Container fluid='md'>
              <Row>
                <Col className='mt-5' md={{ span: 12, offset: 1 }}>
                  <h5 className='ml-3 mb-1 green h'>Welcome to GrandLotto, Please Login Here</h5>
                  <Formik
                    validationSchema={schema2}
                    onSubmit={values => {
                      var myHeaders = new Headers();
                      myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
                      myHeaders.append("Content-Type", "application/json");


                      var raw = JSON.stringify({
                        "email": `${values.email}`,
                        "password": `${values.password}`,
                      });


                      var requestOptions = {
                        method: 'POST',
                        headers: myHeaders,
                        body: raw,
                        redirect: 'follow'
                      };

                      // https://grandlotto.herokuapp.com/api/v2/auth/profile

                      fetch(`${url}/api/v1/login`, requestOptions)
                        .then(response => response.json())
                        .then(result => {
                          if (result.success) {
                            history.push('/games')
                            const { token } = result.success;
                            setShowAlert(true)
                            giveAccess(token)
                            localStorage.setItem('token', token)
                            var myHeaders = new Headers();
                            myHeaders.append("signatures", "5a1131f2eb747be50714281ec3e68b759476c6dc9e1faf5fc5d91c552cf8c230");
                            myHeaders.append("Authorization", `Bearer ${token}`);

                            var requestOptions = {
                              method: 'GET',
                              headers: myHeaders,
                              redirect: 'follow'
                            };

                            fetch("http://localhost:5016/api/v2/auth/profile", requestOptions)
                              .then(response => response.json())
                              .then(result => {
                                if (result.success) {
                                  const { data } = result.success;
                                  setSuccess(true)
                                  showBoard(data)
                                } else {
                                  setSuccess('Please refresh and try again')
                                  return;
                                }
                              },
                                (error) => {
                                  console.log(error)
                                });
                          } else {
                            setSuccess('Mobile number and password incorrect')
                            return;
                          }
                        },
                          (error) => {
                            console.log(error)
                          }
                        )

                    }}
                    initialValues={{
                      email: '',
                      mobile: '',
                      // checked: '',
                      password: '',
                      // referer: '',
                      // confPassword: '',
                    }}
                  >
                    {({
                      handleSubmit,
                      handleChange,
                      handleBlur,
                      values,
                      touched,
                      isValid,
                      errors,
                    }) => (
                      <Form noValidate onSubmit={handleSubmit}>
                        <Form.Group as={Col} md="10" className='mt-2' controlId="validationFormik04">
                          <Form.Label>Email</Form.Label>
                          <Form.Control
                            value={values.email}
                            type="email"
                            name="email"
                            onChange={handleChange}
                            placeholder="Email"
                            isInvalid={!!errors.email}
                          />
                          <Form.Control.Feedback type="invalid">
                            {errors.email}
                          </Form.Control.Feedback>
                        </Form.Group>
                        <Form.Group as={Col} md="10" className='mt-2' controlId="validationFormik05">
                          <Form.Label>Password</Form.Label>
                          <Form.Control
                            type='password'
                            placeholder="Password"
                            name="password"
                            value={values.password}
                            onChange={handleChange}
                            isInvalid={!!errors.password}
                          />
                                            
                          <Form.Control.Feedback className='d-inline-block' type="invalid">
                            {errors.password}
                          </Form.Control.Feedback>


                        </Form.Group>
                        <Button className='mt-2 ml-3 my-3' type='submit' size='sm' variant="success">Submit</Button>
                      </Form>
        
                    )}
                  </Formik>
                </Col>
              </Row>
              {success && <IntegrationNotistack success={`${success}`} />}
            </Container>
          </section>
        </Tab>
      </Tabs>
    </article>
  )
}

export default Login

