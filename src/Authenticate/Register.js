import React, { useState, useEffect } from 'react';
import { Form, Button, Row, Col, Container, Tab, Tabs } from 'react-bootstrap';
import { useHistory } from 'react-router';
import { Link } from 'react-router-dom';
import { Formik } from 'formik';
import Layout from '../Layout'
 import * as Yup from 'yup';
import IntegrationNotistack from '../Fetch/IntegrationNotistack';

const schema1 = Yup.object().shape({
  email: Yup.string().email('Email is required').required('Email is required'),
  checked: Yup.string().email('Check box is required').required('Please accept out T/Cs'),
  password: Yup.string().min(8, 'Must be 8 characters or more').required('Password is required'),
  mobile: Yup.string().min(11, 'Must be 11 characters long').required('Mobile is required'),
  confPassword: Yup.string().oneOf([Yup.ref('password'), null], 'Password Must Match').required('Confirm password')
});

const schema2 = Yup.object().shape({
  email: Yup.string().email('Email is required').required('Email is required'),
  // checked: Yup.string().email('Check box is required').required('Please accept out T/Cs'),
  password: Yup.string().min(8, 'Must be 8 characters or more').required('Pawword is required'),
  confPassword: Yup.string().oneOf([Yup.ref('password'), null], 'Password Must Match').required('Confirm password')
});

const schema3 = Yup.object().shape({
  password: Yup.string().min(8, 'Must be 8 characters or more').required('Password is required'),
  // checked: Yup.string().email('Check box is required').required('Please accept out T/Cs'),
  mobile: Yup.string().min(11, 'Must be 11 characters long').required(),
  confPassword: Yup.string().oneOf([Yup.ref('password'), null], 'Password Must Match').required('Confirm password')
});



const Register = () => {
    let history = useHistory()
    const [success, setSuccess] = useState(null)
    const url = 'https://api.grandlotto.ng'
    const [showAlert, setShowAlert] = useState(false)

    const handleLogin = (e) => {
        e.preventDefault()
        history.push('/validate/login')
    }

    useEffect(() => {
        setTimeout(() => {
          setShowAlert(!showAlert)
          setSuccess('')
        }, 3000)
    }, [success])

  return (

    <article>
      <div style={{ marginTop: '42px' }} className='d-flex justify-content-center h5_div'>
        <h5 className='green h5'>Register seamlessly with either your mobile number or email or both below </h5>
      </div>
        
      <Tabs defaultActiveKey="Phone" className='d-flex justify-content-center' style={{ marginTop: '10px' }} transition={false} id="noanim-tab-example">
        <Tab eventKey="Phone" title="Phone">
          <section className='register_sections d-flex justify-content-center'>
            <Container fluid='md'>
              <Row>
                <Col className='mt-5' md={{ span: 12, offset: 1 }}>
                  <h5 className='ml-3 mb-1 green h'>Welcome to GrandLotto, Please Register Here</h5>
                  <Formik
                    validationSchema={schema3}
                    onSubmit={values => {
                      if (values.checked) {
                        var myHeaders = new Headers();
                        myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
                        myHeaders.append("Content-Type", "application/json");


                        var raw = JSON.stringify({
                          "password": `${values.password}`,
                          "mobile": `${values.mobile}`
                        });


                        var requestOptions = {
                          method: 'POST',
                          headers: myHeaders,
                          body: raw,
                          redirect: 'follow'
                        };

                        // https://grandlotto.herokuapp.com/api/v2/auth/profile

                        fetch(`${url}/api/v1/register`, requestOptions)
                          .then(response => response.json())
                          .then(result => {
                            if (result.success) {
                              const { message } = result.success;
                              setSuccess(message)
                              history.push('/validate/login')
                            } else {
                              const { message } = result.error;
                              setSuccess(message)
                            }
                          },
                            (error) => {
                              console.log(error)
                            }
                          )
                      } else {
                        setSuccess('Please accept terms and conditions')
                        return;
                      }
                    }}
                    initialValues={{
                      email: '',
                      mobile: '',
                      checked: false,
                      // referer: '',
                      password: '',
                      confPassword: '',
                    }}
                  >
                    {({
                      handleSubmit,
                      handleChange,
                      handleBlur,
                      values,
                      touched,
                      isValid,
                      errors,
                    }) => (
                      <Form noValidate onSubmit={handleSubmit}>
                        <Form.Group as={Col} md="10" className='mt-2' controlId="validationFormik04">
                          <Form.Label>Mobile Number</Form.Label>
                          <Form.Control
                            type="text"
                            name="mobile"
                            onChange={handleChange}
                            placeholder="Mobile"
                            isInvalid={!!errors.mobile}
                            value={values.mobile}
                          />
                          <Form.Control.Feedback type="invalid">
                            {errors.mobile}
                          </Form.Control.Feedback>
                        </Form.Group>
                        <Form.Group as={Col} md="10" className='mt-2' controlId="validationFormik05">
                          <Form.Label>Password</Form.Label>
                          <Form.Control
                            type='password'
                            placeholder="Password"
                            name="password"
                            value={values.password}
                            onChange={handleChange}
                            isInvalid={!!errors.password}
                          />
                                            
                          <Form.Control.Feedback className='d-inline-block' type="invalid">
                            {errors.password}
                          </Form.Control.Feedback>


                        </Form.Group>
                        <Form.Group as={Col} md="10" className='mt-2' controlId="validationFormik05">
                          <Form.Label>Confirm Password</Form.Label>
                          <Form.Control
                            type='password'
                            placeholder="Confirm Password"
                            name="confPassword"
                            value={values.confPassword}
                            onChange={handleChange}
                            isInvalid={!!errors.confPassword}
                          />
                          <Form.Control.Feedback type="invalid">
                            {errors.confPassword}
                          </Form.Control.Feedback>
                        </Form.Group>
                        <Form.Group as={Col} md="10" className='mt-2' controlId="validationFormik05">
                          <Form.Label>Referal Code</Form.Label>
                          <Form.Control
                            type='password'
                            placeholder="Referral"
                            name="confPassword"
                            value={values.referer}
                            onChange={handleChange}
                          />
                        </Form.Group>
                        <Form.Group controlId="formBasicCheckbox" className='mt-3 ml-4'>
                          <Form.Check name='checked' type="radio" onChange={handleChange} isInvalid={!!errors.checked} className='ml-2' />
                          <Form.Label className='ml-4 mt-1'>I have read and agree <a className='register_link' onClick={() => history.push('/terms')}>terms and conditions</a> and confirm that I am at least 18 years old.</Form.Label>
                        </Form.Group>
                        <Button className='mt-2 ml-3 my-3' type='submit' size='sm' variant="success">Submit</Button>
                      </Form>
        
                    )}
                  </Formik>
                  <div className='ml-3'>
                    <h6>Already have an account ?</h6>
                    <Button variant='outline-success' onClick={handleLogin} className='mb-3' size='sm' >Login</Button>
                  </div>

                </Col>
              </Row>
              {success && <IntegrationNotistack success={`${success}`} />}
            </Container>
          </section>
        </Tab>
        <Tab eventKey="Email" title="Email">
          <section className='register_sections d-flex justify-content-center'>
            <Container fluid='md'>
              <Row>
                <Col className='mt-5' md={{ span: 12, offset: 1 }}>
                  <h5 className='ml-3 mb-1 green h'>Welcome to GrandLotto, Please Register Here</h5>
                  <Formik
                    validationSchema={schema2}
                    onSubmit={values => {
                      if (values.checked) {
                        var myHeaders = new Headers();
                        myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
                        myHeaders.append("Content-Type", "application/json");


                        var raw = JSON.stringify({
                          "email": `${values.email}`,
                          "password": `${values.password}`,
                        });


                        var requestOptions = {
                          method: 'POST',
                          headers: myHeaders,
                          body: raw,
                          redirect: 'follow'
                        };

                        // https://grandlotto.herokuapp.com/api/v2/auth/profile

                        fetch(`${url}/api/v1/register`, requestOptions)
                          .then(response => response.json())
                          .then(result => {
                            console.log(result)
                            if (result.success) {
                              const { message } = result.success;
                              setSuccess(message)
                              history.push('/validate/login')
                            } else {
                              const { message } = result.error;
                              setSuccess(message)
                            }
                          },
                            (error) => {
                              console.log(error)
                            }
                          )
                      } else {
                        setSuccess('Please accept terms and conditions')
                        return;
                      }
                    }}
                    initialValues={{
                      email: '',
                      mobile: '',
                      checked: false,
                      password: '',
                      // referer: '',
                      confPassword: '',
                    }}
                  >
                    {({
                      handleSubmit,
                      handleChange,
                      handleBlur,
                      values,
                      touched,
                      isValid,
                      errors,
                    }) => (
                      <Form noValidate onSubmit={handleSubmit}>
                        <Form.Group as={Col} md="10" className='mt-2' controlId="validationFormik04">
                          <Form.Label>Email</Form.Label>
                          <Form.Control
                            value={values.email}
                            type="email"
                            name="email"
                            onChange={handleChange}
                            placeholder="Email"
                            isInvalid={!!errors.email}
                          />
                          <Form.Control.Feedback type="invalid">
                            {errors.email}
                          </Form.Control.Feedback>
                        </Form.Group>
                        <Form.Group as={Col} md="10" className='mt-2' controlId="validationFormik05">
                          <Form.Label>Password</Form.Label>
                          <Form.Control
                            type='password'
                            placeholder="Password"
                            name="password"
                            value={values.password}
                            onChange={handleChange}
                            isInvalid={!!errors.password}
                          />
                                            
                          <Form.Control.Feedback className='d-inline-block' type="invalid">
                            {errors.password}
                          </Form.Control.Feedback>


                        </Form.Group>
                        <Form.Group as={Col} md="10" className='mt-2' controlId="validationFormik05">
                          <Form.Label>Confirm Password</Form.Label>
                          <Form.Control
                            type='password'
                            placeholder="Confirm Password"
                            name="confPassword"
                            value={values.confPassword}
                            onChange={handleChange}
                            isInvalid={!!errors.confPassword}
                          />
                          <Form.Control.Feedback type="invalid">
                            {errors.confPassword}
                          </Form.Control.Feedback>
                        </Form.Group>
                        <Form.Group as={Col} md="10" className='mt-2' controlId="validationFormik05">
                          <Form.Label>Referal Code</Form.Label>
                          <Form.Control
                            type='password'
                            placeholder="Referral"
                            name="confPassword"
                            value={values.referer}
                            onChange={handleChange}
                          />
                        </Form.Group>
                        <Form.Group controlId="formBasicCheckbox" className='mt-3 ml-4'>
                          <Form.Check name='checked' type="radio" onChange={handleChange} isInvalid={!!errors.checked} className='ml-2' />
                          <Form.Label className='ml-4 mt-1'>I have read and agree <a className='register_link' onClick={() => history.push('/terms')}>terms and conditions</a> and confirm that I am at least 18 years old.</Form.Label>
                          <Form.Control.Feedback type="invalid">
                            {errors.checked}
                          </Form.Control.Feedback>
                        </Form.Group>
                        <Button className='mt-2 ml-3 my-3' type='submit' size='sm' variant="success">Submit</Button>
                      </Form>
        
                    )}
                  </Formik>
                  <div className='ml-3'>
                    <h6>Already have an account ?</h6>
                    <Button variant='outline-success' onClick={handleLogin} className='mb-3' size='sm' >Login</Button>
                  </div>

                </Col>
              </Row>
              {success && <IntegrationNotistack success={`${success}`} />}
            </Container>
          </section>
        </Tab>
      </Tabs>
    </article>
  )
}

 
    


export default Register



























