import React, { useState, useEffect } from 'react';
import { Form, Button, Row, Container, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { useHistory } from 'react-router';
import Layout from '../Layout'
import moment from 'moment'
import IntegrationNotistack from '../Fetch/IntegrationNotistack';

const Voice = () => {
    const [value, setValue] = useState([])
    const [success, setSuccess] = useState(null)
    const url = 'https://api.grandlotto.ng'
    let history = useHistory()
    const [showAlert, setShowAlert] = useState(false)
    const [show, setShow] = useState(false)

    useEffect(() => {
        setTimeout(() => {
            setSuccess('')
        }, 3000)
    }, [success])

    const handleChange = (e) => {
        e.preventDefault();
        e.stopPropagation()
        let value = e.target.value;
        setValue(value);
    }

    // console.log(moment().format())

    const handleSubmit = (event) => {
        event.preventDefault()

        const form = event.currentTarget;
        if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
        }

        var myHeaders = new Headers();
        myHeaders.append("Authorization", "test_sk_DlzKdyZ6xHZLsA0WoS8fviJiC");
        myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
        myHeaders.append("Content-Type", "application/json");


        var raw = JSON.stringify({
            "mobile": `${value}`
        });


        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };


        fetch(`${url}/api/v1/password-reset-voice`, requestOptions)
            .then(response => response.json())
            .then(result => {
                console.log(result)
                if (result.success) {
                    const { message } = result.success;
                    setSuccess(message)
                    setShow(true)
                    // history.push('/validate')
                } else if (result.error) {
                    const { message } = result.error;
                    setSuccess(message)
                } else {
                    return;
                }
            },
                (error) => {
                    console.log(error)
                }
            )
    };

    // useEffect(() => {
    //     setTimeout(() => {
    //         setShowAlert(!showAlert)
    //     }, 3000)
    // }, [success]);


  
  

    return (
        <section className='register_section d-flex justify-content-center mt-5' style={{ marginTop: '90px', marginBottom: '150px' }}>
            <Container fluid='md'>
                <Row>
                    <Col className='mt-5' md={{ span: 12, offset: 1 }}>
                        <Form onSubmit={handleSubmit}>
                            <Form.Label htmlFor="inputPassword5">OTP Via voice Call</Form.Label>
                            <Form.Control
                                type="text"
                                name='otp'
                                className='input_width'
                                onChange={handleChange}
                                id="inputPassword5"
                                aria-describedby="passwordHelpBlock"
                            />
                            <Form.Text id="passwordHelpBlock" muted>
                                Please enter your mobile number.
                            </Form.Text>
                            <Button className='my-4 ' type='submit' variant="outline-success">Submit</Button>
                            {show && <Button className='mt-2 ml-3 my-3' size='sm' variant="outline-success" onClick={() => history.push('/validate')}>Verify Token</Button>}
                        </Form>
                    </Col>
                </Row>
                {success && <IntegrationNotistack success={`${success}`} />}
            </Container>
        </section>
    )
};

export default Voice

        