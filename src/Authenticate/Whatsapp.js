import React, { useState, useEffect } from 'react';
import { Form, Button, Row, Container, Col } from 'react-bootstrap';
import IntegrationNotistack from '../Fetch/IntegrationNotistack';
import { Link } from 'react-router-dom';
import Layout from '../Layout'
import { useHistory } from 'react-router';

const Whatsapp = () => {
  const [value, setValue] = useState([])
  const [success, setSuccess] = useState(null)
  const history = useHistory()
  const url = 'https://api.grandlotto.ng'
  const [error, setError] = useState(null)
  const [showAlert, setShowAlert] = useState(false)
  const [show, setShow] = useState(false)


  const handleChange = (e) => {
    e.preventDefault()
    setValue(e.target.value)
  }

  const handleSubmit = (e) => {
    e.preventDefault()
    var myHeaders = new Headers();
    myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
    myHeaders.append("timestamps", "1614848109");
    myHeaders.append("Content-Type", "application/json");

    var raw = JSON.stringify({
      "mobile": `${value}`
    });

    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: raw,
      redirect: 'follow'
    };

    fetch(`${url}/api/v1/password-reset-whatsapp`, requestOptions)
      .then(response => response.json())
      .then(result => {
        console.log(result)
        if (result.success) {
          const { message } = result.success;
          setSuccess(message)
          setShow(true)
        } else if (result.error) {
          setSuccess('Please Make sure your number is correct then refresh and try again')
        } else {
          return;
        }
      },
        (error) => {
          console.log(error)
        }
      );


  }

  useEffect(() => {
    setTimeout(() => {
      setSuccess('')
    }, 3000)
  }, [success])
  


  return (
    <section className='register_section d-flex justify-content-center mt-5' style={{ marginBottom: '150px' }}>
      <Container fluid='md'>
        <Row>
          <Col className='mt-5' md={{ span: 12, offset: 1 }}>
            <Form onSubmit={handleSubmit}>
              <Form.Label htmlFor="inputPassword5">OTP Via Whatsapp</Form.Label>
              <Form.Control
                type="text"
                name='otp'
                className='input_width'
                onChange={handleChange}
                id="inputPassword5"
                aria-describedby="passwordHelpBlock"
              />
              <Form.Text id="passwordHelpBlock" muted>
                Please enter your Whatsapp number.
              </Form.Text>
              <Button className='my-4 ' type='submit' variant="outline-success">Submit</Button>
              {show && <Button className='mt-2 ml-3 my-3' size='sm' variant="outline-success" onClick={() => history.push('/validate')}>Verify Token</Button>}
            </Form>
          </Col>
        </Row>
        {success && <IntegrationNotistack success={`${success}`} />}
      </Container>
    </section>
  )
}

export default Whatsapp
















