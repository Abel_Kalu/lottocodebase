import React, { useState, useEffect } from 'react'
import { Container, Form, Button } from 'react-bootstrap'
import IntegrationNotistack from '../../Fetch/IntegrationNotistack';
import Layout from '../../Layout'

const PostResult = () => {
    const [type, setType] = useState('NAP 1')
    const url = 'https://api.grandlotto.ng'
    const [odds, setOdds] = useState('')
    let token = localStorage.getItem('adminToken')
    const [arr, setArr] = useState([])
    const [success, setSuccess] = useState(null)
    const [showAlert, setShowAlert] = useState(false)

    const handleChange = e => {
        e.preventDefault()
        setOdds(e.target.value)
    }


    const handleChange2 = e => {
        e.preventDefault()
        setType(e.target.value)
    }

    useEffect(() => {
        const fetchData = async () => {
            try {
                const res = await fetch(`${url}/api/v1/allgames`)
                const data = await res.json()
                setArr(data.data)
            } catch (err) {
                console.log(`hello error from the api server:  ${err}`)
            }
        
        }

        fetchData()
    }, [url])

    const handleSubmit = e => {
        e.preventDefault()

        var myHeaders = new Headers();
        myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
        myHeaders.append("timestamps", "1614848109");
        myHeaders.append("Authorization", `Bearer ${token}`);
        myHeaders.append("Content-Type", "application/json");

        var raw = JSON.stringify({
            "gameId": `${type}`,
            "numbers": `${odds}`
        });

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };

        fetch(`${url}/api/v2/auth/postResult`, requestOptions)
            .then(response => response.json())
            .then(result => {
                console.log(result)
                if (result.success) {
                    console.log(result)
                    const { message } = result.success;
                    setSuccess(message)
                } else {
                    const { message } = result.error;
                    setSuccess(message)
                }
            },
                (error) => {
                    setSuccess('Please refresh your browser or try re-login to your admin account.')
                }
            )
    }

    useEffect(() => {
        setTimeout(() => {
            setSuccess('')
        }, 3000)
    }, [success]);

    const handleClick = uuid => {
        setSuccess('copied game id to clipboard');
        navigator.clipboard.writeText(uuid)
    }

    useEffect(() => {
        const timeout = setTimeout(() => {
            setSuccess(false)
        }, 3000)
        return () => clearTimeout(timeout)
    }, [alert]);


    return (
        <Container className='max_amount'>
            <section className='amount_section'>
                <div className='border-bttom'>
                    <div className='d-flex justify-content-center mb-4'>
                        <h6>Post results with game id and the odds (click on each game's id to copy and paste on form).</h6>
                    </div>
                    {arr?.length > 0 && arr.map((item) => {
                        const { uuid, name, day, id } = item;
                        return (
                            <>
                                <div key={id} className='d-flex mb-2 justify-content-between'>
                                    <p className='admin_p'>Day: {day}</p>
                                    <p className='admin_p'>Game Type: {name}</p>
                                    <p className='admin_p' onClick={() => handleClick(uuid)}>Game Id: <span className='p'>{uuid}</span></p>
                                </div>
                            </>
                        )
                    })}
                </div>
                <Form onSubmit={handleSubmit} autocomplete='off'>
                    <Form.Group className='mt-2' controlId="exampleForm.SelectCustom">
                        <Form.Label>Game Type:</Form.Label>
                        <Form.Control
                            required
                            type="text"
                            name="gameId"
                            autocomplete="off"
                            onChange={handleChange2}
                            placeholder="24vhbj56bh7ngg4"
                            required
                        />
                    </Form.Group>
                    <Form.Group className='mt-2 admin_form' controlId="validationCustom01">
                        <Form.Label>Numbers Seperated by Commas:</Form.Label>
                        <Form.Control
                            required
                            type="text"
                            name="odds"
                            onChange={handleChange}
                            placeholder="22,33,2,17,88"
                            required
                        />
                        <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
                    </Form.Group>
                    <Button className='mt-2 mb-3' type='submit' variant="outline-success">Submit</Button>
                    
                    <Form.Group className='mt-2' controlId="exampleForm.SelectCustom"></Form.Group>
                </Form>
            </section>
            {success && <IntegrationNotistack className='d' success={`${success}`} />}
        </Container>
    )
}

export default PostResult
