import React, { useState, useEffect } from 'react'
import { Container, Form, Button } from 'react-bootstrap'
import IntegrationNotistack from '../../Fetch/IntegrationNotistack';
import Layout from '../../Layout'

const LottoExpressOdds = () => {
    const [odds, setOdds] = useState('')
    let token = localStorage.getItem('adminToken')
    const [type, setType] = useState('NAP 1')
    const [success, setSuccess] = useState(null)
    const [showAlert, setShowAlert] = useState(false)
    const url = 'https://api.grandlotto.ng'

    const handleChange = e => {
        e.preventDefault()
        setOdds(e.target.value)
    }

    useEffect(() => {
        let time = setTimeout(() => {
            setShowAlert(!showAlert)
            setSuccess('')
        }, 3000)

        return () => clearTimeout(time)
    }, [success]);
            
    const handleSubmit = e => {
        e.preventDefault()

        var myHeaders = new Headers();
        myHeaders.append("timestamps", "1614848109");
        myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
        myHeaders.append("Authorization", `Bearer ${token}`);
        myHeaders.append("Content-Type", "application/json");

        var raw = JSON.stringify({
            "type": `${type}`,
            "odds": `${odds}`
        });

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };

        fetch(`${url}/api/v2/auth/postLottoExpressOdds`, requestOptions)
            .then(response => response.json())
            .then(result => {
                if (result.success) {
                    const { message } = result.success;
                    setSuccess(message)
                } else {
                    const { message } = result.error;
                    setSuccess(message)
                }
            },
                (error) => {
                    setSuccess('Please refresh your browser or try re-login to your admin account.')
                }
            )
    }

    useEffect(() => {
        setTimeout(() => {
            setShowAlert(!showAlert)
        }, 3000)
    }, [success]);

    const handleType = e => {
        e.preventDefault()
        let t = e.target.value
        setType(t)
    }


    return (
        <Container className='max_amount'>
            <section className='amount_section'>
                <Form onSubmit={handleSubmit}>
                   <Form.Group className='mt-2 admin_form' controlId="validationCustom01">
                        <Form.Label>Select Type</Form.Label>
                        <Form.Control required as="select" onChange={handleType} custom>
                            <option value='NAP 1'>NAP 1</option>
                            <option value='NAP 2'>NAP 2</option>
                            <option value='NAP 3'>NAP 3</option>
                            <option value='NAP 4'>NAP 4</option>
                            <option value='NAP 5'>NAP 5</option>
                            <option value='PERM 2'>PERM 2</option>
                            <option value='PERM 3'>PERM 3</option>
                            <option value='PERM 4'>PERM 4</option>
                            <option value='PERM 5'>PERM 5</option>
                            <option value='AGAINST'>AGAINST</option>
                            <option value='1 BANKER'>1 BANKER</option>
                        </Form.Control>
                        <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
                    </Form.Group>
                    <Form.Group className='mt-2 admin_form' controlId="validationCustom01">
                        <Form.Label>Lotto Express Odd</Form.Label>
                            <Form.Control
                                required
                                type="text"
                                name="Lotto Express"
                                onChange={handleChange}
                                placeholder="400"
                                required
                            />
                        <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
                    </Form.Group>
                    <Button className='mt-2 mb-3' type='submit' variant="outline-success">Submit</Button>
                </Form>
            </section>
            {success && <IntegrationNotistack success={`${success}`} />}
        </Container>
    )
}

export default LottoExpressOdds
