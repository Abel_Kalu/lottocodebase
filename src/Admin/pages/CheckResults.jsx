import React, { useState } from 'react';
import { Container, Form, Button } from 'react-bootstrap'
import Layout from '../../Layout'

const CheckResults = () => {
    const [odd, setOdd] = useState('')
    const get = localStorage.getItem('token')
    const [showCoupon, setShowCoupon] = useState(false)
    const [success, setSuccess] = useState('')
    const [list, setList] = useState([])
    const url = 'https://api.grandlotto.ng'

    const handleChange = e => {
        e.preventDefault()
        setOdd(e.target.value)
    }

    const handleSubmit = e => {
        e.preventDefault()
        console.log(odd)
        e.preventDefault()
        var myHeaders = new Headers();
        myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
        myHeaders.append("Authorization", `Bearer ${get}`);
        myHeaders.append("Content-Type", "application/json");

        var raw = JSON.stringify({
          "coupon": `${odd}`
        });

        var requestOptions = {
          method: 'POST',
          headers: myHeaders,
          body: raw,
          redirect: 'follow'
        };

        fetch(`${url}/api/v1/coupons`, requestOptions)
          .then(response => response.json())
            .then(result => {
              console.log(result)
            if (result.betCoupons) {
                  setList(result.betCoupons)
                  setShowCoupon(true)
              }else{
                  setSuccess(result.error.message)
              }
          })
          .catch(error => console.log('error', error));
    }

    return (
        <Container className='max_amount'>
            <section className='amount_sections'>
                <Form onSubmit={handleSubmit} >

               <Form.Group className='mt-2 admin_form' controlId="validationCustom01">
                            <Form.Label>Coupon Code</Form.Label>
                            <Form.Control
                                type="text"
                                name="odd"
                                onChange={handleChange}
                                placeholder="123456"
                                required
                            />
                            <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
            </Form.Group>
            <Button className='mt-2 mb-3' type='submit' variant="outline-success">Submit</Button>

        </Form>
            </section>
            <div className={`${list?.length > 1 && 'scrollersx'}`} >
                  <div className='show_coupon' style={{ height: '50vh' }}>
                  {list?.map((items) => {
                      const { amount, id, odd, stakes, type, user_id, stakes1, amounts, stakes2, status, staked, possibleWinning, game, gameId } = items;
                      return (
                          <>
                              <main className='get_line'>
                                  <div className=''>
                                      {status !== 'PENDING' && <p className='redz'>Expired ticket</p>}
                                      <p className='p_type'>possibleWinning: &#x20A6;{possibleWinning}</p>
                                      <p className='p_type'>Odd: {odd}</p>
                                      <p className='p_type'>Customer ID: {user_id}</p>
                                      <p className='p_type'>status: <span className={`${status.toLowerCase() === 'pending' && '', status.toLowerCase() === 'won' && 'yellow', status.toLowerCase() === 'lost' && 'red' }`}>{status.toLowerCase()}</span></p>
                                      <p className='p_type'>Game Name: {game}</p>
                                      <p className='p_type'>Stake Amount: &#x20A6;{staked}</p>
                                      <p className='p_type'>Stake Amount per Line: &#x20A6;{amounts}</p>
                                      {stakes && <p className='p_type'>Lines: {stakes}</p>}
                                      {stakes1 && <p className='p_type'>Against first: {stakes1}</p>}
                                      {stakes2 && <p className='p_type'>Against second: {stakes2}</p>}
                                      <p className='p_type'>game Type: {type}</p>
                                  </div>
                              </main>
                          </>
                      )
                  })}
              </div>
              </div>
        </Container>
    )
}

export default CheckResults
