import React, { useState, useEffect } from 'react'
import { Container, Form, Button } from 'react-bootstrap'
import IntegrationNotistack from '../../Fetch/IntegrationNotistack';

const PostResult = () => {
    const [type, setType] = useState('')
    const url = 'http://localhost:3018'
    const [reference, setReference] = useState('')
    let token = localStorage.getItem('adminToken')
    const [arr, setArr] = useState([])
    const [success, setSuccess] = useState(null)
    const [showAlert, setShowAlert] = useState(false)

    const handleChange = e => {
        e.preventDefault()
        setReference(e.target.value)
    }


    const handleChange2 = e => {
        e.preventDefault()
        setType(e.target.value)
    }

    // useEffect(() => {
    //     const fetchData = async () => {
    //         try {
    //             const res = await fetch(url)
    //             const data = await res.json()
    //             setArr(data.data)
    //         } catch (err) {
    //             console.log(`hello error from the api server:  ${err}`)
    //         }
        
    //     }

    //     fetchData()
    // }, [url])

    const handleSubmit = e => {
        e.preventDefault()

        var myHeaders = new Headers();
        myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
        myHeaders.append("Authorization", `Bearer ${token}`);
        myHeaders.append("Content-Type", "application/json");

        var raw = JSON.stringify({
            "type": `${type}`,
            "reference": `${reference}`
        });

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };

        fetch(`${url}/api/v2/auth/resolve-funding`, requestOptions)
            .then(response => response.json())
            .then(result => {
                console.log(result)
                if (result.success) {
                    const { message } = result.success;
                    setSuccess(message)
                } else {
                    setSuccess(result.error.message)
                }
            })
            .catch(error => console.log('error', error));
    }

    useEffect(() => {
        setTimeout(() => {
            setShowAlert(!showAlert)
            setSuccess('')
        }, 3000)
    }, [success]);

    // const handleClick = uuid => {
    //     setSuccess('copied game id to clipboard');
    //     navigator.clipboard.writeText(uuid)
    // }

    useEffect(() => {
        const timeout = setTimeout(() => {
            setSuccess(false)
        }, 3000)
        return () => clearTimeout(timeout)
    }, [alert]);


    return (
        <Container className='max_amount'>
            <section className='amount_section'>
                <Form onSubmit={handleSubmit}>
                                  <Form.Group className='mt-2' controlId="exampleForm.SelectCustom">
                <Form.Label>Type Of Payment</Form.Label>
                            <Form.Control
                                required
                                type="text"
                                name="type"
                                onChange={handleChange2}
                                placeholder="Paystack"
                                required
                            />
              </Form.Group>
               <Form.Group className='mt-2 admin_form' controlId="validationCustom01">
                            <Form.Label>Enter Payment Reference</Form.Label>
                            <Form.Control
                                required
                                type="text"
                                name="reference"
                                onChange={handleChange}
                                placeholder="Reference"
                                required
                            />
                            <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
                    </Form.Group>
            <Button className='mt-2 mb-3' type='submit' variant="outline-success">Submit</Button>
                    
              <Form.Group className='mt-2' controlId="exampleForm.SelectCustom"></Form.Group>
                </Form>
            </section>
            {success && <IntegrationNotistack className='d' success={`${success}`} />}
       </Container>
    )
}

export default PostResult
