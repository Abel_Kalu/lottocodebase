import React from 'react'
import {faArr} from 'react-icons'

const Sidebar = (props) => {
    const { result, lotto, soft, max, win, hideGame, suspendGame, promo, fund } = props;

    const handleLi1 = () => {
        max(true)
        result(false)
        hideGame(false)
        promo(false)
        fund(false)
        suspendGame(false)
        lotto(false)
        win(false)
        soft(false)
    }
    const handleLi2 = () => {
        max(false)
        win(false)
        result(true)
        promo(false)
        hideGame(false)
        suspendGame(false)
        fund(false)
        lotto(false)
        soft(false)
    }
    const handleLi3 = () => {
        max(false)
        win(false)
        suspendGame(false)
        result(false)
        fund(false)
        lotto(false)
        promo(false)
        hideGame(false)
        soft(true)
    }
    const handleLi4 = () => {
        max(false)
        result(false)
        lotto(true)
        suspendGame(false)
        promo(false)
        hideGame(false)
        fund(false)
        soft(false)
        win(false)
    }

    const handleLi5 = e => {
        max(false)
        result(false)
        lotto(false)
        hideGame(false)
        suspendGame(false)
        fund(false)
        promo(false)
        soft(false)
        win(true)
    }

    const handleLi6 = e => {
        max(false)
        result(false)
        lotto(false)
        hideGame(true)
        promo(false)
        suspendGame(false)
        fund(false)
        soft(false)
        win(false)
    }

    const handleLi7 = e => {
        max(false)
        result(false)
        lotto(false)
        hideGame(false)
        promo(false)
        fund(false)
        suspendGame(true)
        soft(false)
        win(false)
    }

    const handleLi8 = e => {
        max(false)
        result(false)
        lotto(false)
        hideGame(false)
        promo(true)
        suspendGame(false)
        fund(false)
        soft(false)
        win(false)
    }

    const handleLi9 = e => {
        max(false)
        result(false)
        lotto(false)
        hideGame(false)
        promo(false)
        suspendGame(false)
        fund(true)
        soft(false)
        win(false)
    }

    return (
        <div className='sidebar mt-5'>
            <ul className='sidebar_list'>
                <li onClick={handleLi1} className='sidebarList_items'>Max Amount for direct games
                <span></span>
                </li>
                <li onClick={handleLi2} className='sidebarList_items'>Post Regular Lotto Results <span></span>
                </li>
                <li onClick={handleLi3} className='sidebarList_items'>Post Soft Lotto Odds
                <span></span>
                </li>
                <li onClick={handleLi4} className='sidebarList_items'>Post Lotto Express Odds
                <span></span>
                </li>
                <li onClick={handleLi5} className='sidebarList_items'>View Coupons
                <span></span>
                </li>
                <li onClick={handleLi6} className='sidebarList_items'>Hide Games
                <span></span>
                </li>
                <li onClick={handleLi7} className='sidebarList_items'>Suspend Games
                <span></span>
                </li>
                <li onClick={handleLi8} className='sidebarList_items'>Post Promo Games Results
                <span></span>
                </li>
                <li onClick={handleLi9} className='sidebarList_items'>Reslove Wallet Fund Issues
                <span></span>
                </li>
           </ul>
        </div>
    )
}

export default Sidebar
