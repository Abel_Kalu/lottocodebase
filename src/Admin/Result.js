import React, { useState } from 'react'
import { Container, Col, Row } from 'react-bootstrap';
import Sidebar from './pages/Sidebar'
import MaxAmount from './pages/MaxAmount';
import PostResult from './pages/PostResult'
import SoftLottoOdds from './pages/SoftLottoOdds'
import PostPromoResult from './pages/PostPromoResult'
import ResolveFund from './pages/ResolveFund'
import LottoExpress from './pages/LottoExpressOdds';
import HideGame from './pages/HideGame';
import CheckResults from './pages/CheckResults';
import SuspendGame from './pages/SuspendGame';

const Result = () => {
    const [max, setMax] = useState(true)
    const [result, setResult] = useState(false)
    const [lotto, setLotto] = useState(false)
    const [promo, setPromo] = useState(false)
    const [hideGame, setHideGame] = useState(false)
    const [suspendGame, setSuspendGame] = useState(false)
    const [soft, setSoft] = useState(false)
    const [fund, setFund] = useState(false)
    const [win, setWin] = useState(false)


    return (
        <Container className='d-flex mt-5' fluid>
            <Row>
                <Col md={4}>
                    <Sidebar fund={setFund} result={setResult} hideGame={setHideGame} promo={setPromo} suspendGame={setSuspendGame} lotto={setLotto} max={setMax} soft={setSoft} win={setWin} />
                </Col>
                <Col md={8}>
                    {hideGame && <HideGame />}
                    {promo && <PostPromoResult />}
                    {suspendGame && < SuspendGame />}
                    {max && <MaxAmount />}
                    {result && <PostResult />}
                    {lotto && <LottoExpress />}
                    {fund && <ResolveFund />}
                    {soft && <SoftLottoOdds />}
                    {win && <CheckResults />}
                </Col>
           </Row>
        </Container>
    )
}

export default Result
