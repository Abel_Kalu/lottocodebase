import React, { useState, useEffect } from 'react';
import { Row, Col, Container, Button, Form, InputGroup } from 'react-bootstrap'
import { Link } from 'react-router-dom';
import Countdown from "react-countdown";
import { useGlobalContext } from '../store/context';
import { useHistory } from 'react-router';
import { FaTimes } from 'react-icons/fa';
import Layout from '../Layout'
import moment from 'moment'
import MobileCoupon from '../Fetch/MobileCoupon'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faApple, faGooglePlay, faFacebook, faYoutube, faTwitter, faInstagram } from '@fortawesome/free-brands-svg-icons'
import axios from 'axios';
import FundModal from '../Fetch/FundModal'
import GetWhatsapp from '../Fetch/GetWhatsapp'
import play from '../svg/play.svg'
import Bet from '../Fetch/Bet'
import { RiBankLine, RiHome2Line, RiSdCardMiniLine, RiUserAddLine } from 'react-icons/ri';
import { IoIosFootball, IoMdHelpCircleOutline } from 'react-icons/io';
import { IoOptionsOutline } from "react-icons/io5";
import IntegrationNotistack from '../Fetch/IntegrationNotistack';

let date = new Date().getHours(0, 0, 0, 0);

const LottoExpress = () => {
    const { game, logedIn, days, user, getUser, showBoard } = useGlobalContext();
    const [activeNums, setActiveNums] = useState(false)
    const [value, setValue] = useState(0);
    const [showAlert, setShowAlert] = useState(false)
    const [number, setNumber] = useState(0)
    const history = useHistory()
    var currentUrl = window.location.href.replace(/(.+\w\/)(.+)/, "/$2")
    const [mode, setMode] = useState('')
    const [open, setOpen] = useState(false)
    const [showMobile, setShowMobile] = useState(false)
    let [array, setArray] = useState([])
    let [day, setDay] = useState(date)
    const [betSlip, setBetSlip] = useState([])
    const [listV, setListV] = useState(0)
    let [arr, setArr] = useState([])
    const [getBet, setGetBet] = useState(false)
    const [success, setSuccess] = useState('')
    const [geteNums, setGetNums] = useState(false)
    const [general, setGeneral] = useState(null)
    const [timer, setTimer] = useState(moment().format('LTS'))
    const [time, setTime] = useState(null)
    const [times, setTimes] = useState(null)
    const [gameType, setGameType] = useState('Regular')
    const [showModal, setShowModal] = useState(false)
    const [softMax, setSoftMax] = useState(null)
    const [subValue, setSubValue] = useState(0)
    const [showGameModal, setShowGameModal] = useState(false)
    var [count, setCount] = useState(0)
    const url = 'https://api.grandlotto.ng'
    const [how, setHow] = useState(true)
    const [slip, setSlip] = useState(false)
    const [showCoupon, setShowCoupon] = useState(false)
    const [coupon, setCoupon] = useState('')
    const [list, setList] = useState([])
    const [id, setId] = useState(Math.floor(Math.random() * 10000000000) + 1)
    const [fundModal, setFundModal] = useState(false)
    const [alerts, setAlerts] = useState('')

    let nums = []

    for (let i = 1; i < 11; i++) {
        nums.push(i)
    }

    const CouponSubmit = e => {
        e.preventDefault()
        console.log(coupon)
        var myHeaders = new Headers();
        myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
        myHeaders.append("Authorization", `Bearer ${get}`);
        myHeaders.append("Content-Type", "application/json");

        var raw = JSON.stringify({
            "coupon": `${coupon}`
        });

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };

        //http://localhost:5016
        //https://lotto-api.firstbetng.com
            
        fetch(`${url}/api/v1/coupons`, requestOptions)
            .then(response => response.json())
            .then(result => {
                if (result.betCoupons) {
                    setList(result.betCoupons)
                    setShowCoupon(true)
                }
            })
            .catch(error => console.log('error', error));
    }

    const handleCoupon = e => {
        e.preventDefault()
        if (e.target.value.includes('softlotto')) {
            setCoupon(e.target.value)
        } else {
            setSuccess('You can only view Soft Lotto Coupons here')
            return;
        }
    }

    const get = localStorage.getItem('token')

    const handleSubmit = (e) => {
        e.preventDefault()
        var myHeaders = new Headers();
        myHeaders.append("signatures", "95631547ca07a9ca16e1116e577199003e96bf55fb110b3ccbc9ed1c1b2092e8");
        myHeaders.append("Authorization", `Bearer ${get}`);
        myHeaders.append("Content-Type", "application/json");

        const val = betSlip.reduce((total, money) => {
            total += parseInt(money.amount)
            return total;
        }, 0);

        if (user.activated >= 0) {
            if (user.balance > val) {
                if (val < 1) {
                    setSuccess(`Kindly add an amount`)
                    return;
                } else {
                    betSlip.filter((a, i) => {
                        const { amount, amounts, stakeList, kind, type, code } = a;
                        if (amount < 1) {
                            setSuccess(`cannot place bet for ticket number ${i + 1} please add an amount`)
                            return;
                        } else {
                            var raw = JSON.stringify({
                                "stakes": [
                                    {
                                        "coupon": `${id + 'softlotto'}`,
                                        "amounts": `${amounts}`,
                                        "value": `${amount}`,
                                        "kind": `${kind}`,
                                        "numbers": `${stakeList}`,
                                        "type": `${type}`,
                                        "game": "Soft Lotto"
                                    }
                                ]
                            });

                            var requestOptions = {
                                method: 'POST',
                                headers: myHeaders,
                                body: raw,
                                redirect: 'follow'
                            };

                            fetch(`${url}/api/v1/placeSoftLotto`, requestOptions)
                                .then(response => response.json())
                                .then(result => {
                                    console.log(result)
                                    let show = result.result.map((res) => res)
                                    console.log(show[0])
                                    const { type, odd, staked, date, amounts, possibleWinning, stakes, kind, amount, coupon } = show[0]
                                    let response = stakes.toString()
                                    var myHeaders = new Headers();
                                    myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
                                    myHeaders.append("Authorization", `Bearer ${get}`);
                                    myHeaders.append("Content-Type", "application/json");

                                    
                                    var raw = JSON.stringify({
                                        "game": 'Soft Lotto',
                                        "amount": `${amount}`,
                                        "amounts": `${amounts}`,
                                        "kind": `${kind}`,
                                        "line": `3`,
                                        "type": `${type}`,
                                        "odd": `${odd}`,
                                        "possibleWinning": `${possibleWinning}`,
                                        "staked": `${amount}`,
                                        "stakes": `${response}`,
                                        "date": `${date}`,
                                        "coupon": `${coupon}`
                                    });

                                    var requestOptions = {
                                        method: 'POST',
                                        headers: myHeaders,
                                        body: raw,
                                        redirect: 'follow'
                                    };

                                    fetch(`${url}/api/v2/auth/betHistory`, requestOptions)
                                        .then(response => response.json())
                                        .then(result => {
                                            const { message } = result.success;
                                            setId(Math.floor(Math.random() * 10000000000) + 1)
                                            setMode(message)
                                            setOpen(!open)
                                            var myHeaders = new Headers();
                                            myHeaders.append("signatures", "5a1131f2eb747be50714281ec3e68b759476c6dc9e1faf5fc5d91c552cf8c230");
                                            myHeaders.append("Authorization", `Bearer ${get}`);

                                            var requestOptions = {
                                                method: 'GET',
                                                headers: myHeaders,
                                                redirect: 'follow'
                                            };

                                            fetch(`${url}/api/v2/auth/profile`, requestOptions)
                                                .then(response => response.json())
                                                .then(result => {
                                                    if (result.success) {
                                                        getUser(result.success.data)
                                                        const { data } = result.success;
                                                        showBoard(data)
                                                        //   localStorage.setItem('user', JSON.stringify(data))
                                                    } else {
                                                        return;
                                                    }
                                                },
                                                    (error) => {
                                                        console.log(error)
                                                    });

                                        })
                                        .catch(error => console.log('error', error));
                                })
                                .catch(error => console.log('error', error));
                        }
                    });
                }
            } else {
                setFundModal(!fundModal)
                setAlerts('Please kindly note that you currently do not have sufficient balance to place this bet')
                return;
            }
        } else {
            if (val < 1) {
                setSuccess(`Kindly add an amount`)
                return;
            } else {
                betSlip.filter((a, i) => {
                    const { amount, amounts, stakeList, type, code, kind, } = a;
                    if (amount < 1) {
                        setSuccess(`cannot place bet for ticket number ${i + 1} please add an amount`)
                        return;
                    } else {
                        var raw = JSON.stringify({
                            "stakes": [
                                {
                                    "coupon": `${id + 'softlotto'}`,
                                    "value": `${amount}`,
                                    "amounts": `${amounts}`,
                                    "kind": `${kind}`,
                                    "numbers": `${stakeList}`,
                                    "type": `${type}`,
                                    "game": "Soft Lotto"
                                }
                            ]
                        });

                        var requestOptions = {
                            method: 'POST',
                            headers: myHeaders,
                            body: raw,
                            redirect: 'follow'
                        };

                        fetch(`${url}/api/v1/placeSoftLotto`, requestOptions)
                            .then(response => response.json())
                            .then(result => {
                                console.log(result)
                                let show = result.result.map((res) => res)
                                setId(Math.floor(Math.random() * 10000000000) + 1)
                                const { type, odd, staked, date, possibleWinning, stakes, kind, amount, amounts, coupon } = show[0]
                                let response = stakes.toString()
                                var myHeaders = new Headers();
                                myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
                                myHeaders.append("Authorization", `Bearer ${get}`);
                                myHeaders.append("Content-Type", "application/json");

                                var raw = JSON.stringify({
                                    "game": 'Soft Lotto',
                                    "amount": `${amount}`,
                                    "amounts": `${amounts}`,
                                    "type": `${type}`,
                                    "kind": `${kind}`,
                                    "odd": `${odd}`,
                                    "possibleWinning": `${possibleWinning}`,
                                    "staked": `${amount}`,
                                    "stakes": `${response}`,
                                    "date": `${date}`,
                                    "coupon": `${coupon}`
                                });

                                var requestOptions = {
                                    method: 'POST',
                                    headers: myHeaders,
                                    body: raw,
                                    redirect: 'follow'
                                };

                                fetch(`${url}/api/v2/auth/betHistory`, requestOptions)
                                    .then(response => response.json())
                                    .then(result => {
                                        const { message } = result.success;
                                        setMode(message)
                                        setOpen(!open)
                                        var myHeaders = new Headers();
                                        myHeaders.append("signatures", "5a1131f2eb747be50714281ec3e68b759476c6dc9e1faf5fc5d91c552cf8c230");
                                        myHeaders.append("Authorization", `Bearer ${get}`);

                                        var requestOptions = {
                                            method: 'GET',
                                            headers: myHeaders,
                                            redirect: 'follow'
                                        };

                                        fetch(`${url}/api/v2/auth/profile`, requestOptions)
                                            .then(response => response.json())
                                            .then(result => {
                                                if (result.success) {
                                                    getUser(result.success.data)
                                                    const { data } = result.success;
                                                    showBoard(data)
                                                    //   localStorage.setItem('user', JSON.stringify(data))
                                                } else {
                                                    return;
                                                }
                                            },
                                                (error) => {
                                                    console.log(error)
                                                });

                                    })
                                    .catch(error => console.log('error', error));
                            })
                            .catch(error => console.log('error', error));
                    }
                });
            }
        }
        setActiveNums(false)
    }

    useEffect(() => {
        let v = JSON.parse(localStorage.getItem('SoftLotto'))
        if (v?.length > 0) {
            setSlip(true)
            setShowGameModal(!showGameModal)
            setHow(false)
            setCount(v.length)
            setBetSlip(v)
        }
    }, []);

    useEffect(() => {
        let time = setTimeout(() => {
            setShowAlert(!showAlert)
            setSuccess('')
        }, 3000)

        return () => clearTimeout(time)
    }, [success]);

    const createSlip = () => {
        let data = {
            id: new Date().getTime().toString(),
            code: id + 'softlotto',
            stakeList: array,
            kind: gameType,
            amount: subValue * 3,
            amounts: subValue,
            type: gameType
        }
        betSlip.unshift(data)
        calculateTotalStake();
        setShowGameModal(!showGameModal)
        setArr([]);
    }

    const calculateTotalStake = () => {
        setValue(0)
        const val = betSlip.reduce((total, money) => {
            total += parseInt(money.amount)
            return total;
        }, 0);
        setNumber(val)
        setCount(betSlip.length)
        localStorage.setItem('SoftLotto', JSON.stringify(betSlip))
    }
    // http://www.v2nmobile.com/api/httpsms.php?u=${email}&p=${pass}&m=${'abelkelly}&r=${09047597017}&s=${senderID}&t=1`

    const calculateTotalStake1 = (newItem) => {
        setValue(0)
        const val = newItem.reduce((total, money) => {
            total += parseInt(money.amount)
            return total;
        }, 0);
        setNumber(val)
        localStorage.setItem('SoftLotto', JSON.stringify(newItem))
    }

    useEffect(() => {
        let hours = new Date().getHours()
        let mins = new Date().getMinutes()
        let startTime = `${hours}:${mins}`

        if (startTime === '23:59') {
            localStorage.setItem('SoftLotto', [])
        }
    }, []);

    const handleBet = (e) => {
        e.preventDefault()
        if (array.length === 3) {
            setActiveNums(false)
            setGetBet(true)
            setGetNums(false)
            setSlip(true);
            setHow(false);
            createSlip()
            setArray([])
        } else {
            setSuccess('Please Select 3 Numbers')
            return;
        }
        
    }

    const removeItem = (id) => {
        let newItem = betSlip.filter((item) => item.id !== id)
        if (newItem.length < 1) {
            setBetSlip([])
            setHow(true)
            setSlip(false)
            setCount(0)
            setShowGameModal(false)
            setNumber(0)
            calculateTotalStake1([])
        } else {
            setBetSlip(newItem)
            setCount(count -= 1)
            // console.log(betSlip)
            calculateTotalStake1(newItem)
        }
        // setBetSlip(newItem)
    }

    const Completionist = ({ setDay }) => {
        // setDay(Date.now())
        return <p>Games Drawn</p>
    }

    useEffect(() => {
        const loggedInUser = localStorage.getItem('time')
        if (loggedInUser) {
            const foundTime = JSON.parse(loggedInUser)
            setDay(foundTime)
        }
    }, [])

    useEffect(() => {
        const timeInterval = setInterval(() => {
            setTimer(moment().format('LTS'))
        }, 500)

        return () => clearInterval(timeInterval)
    })

    const handleClass = (i) => {
        setActiveNums((state) => {
            return {
                ...state,
                [i]: !state[i],
            };
        });

        if (array.includes(i)) {
            const index = array.indexOf(i)
            if (index > -1) {
                array.splice(index, 1)
            }
        } else {
            if (array.length < 3) {
                array.push(i)
            } else {
                setSuccess('Please unselect a number to select new one')
                setGetNums((state) => {
                    return {
                        ...state,
                        [i]: !state[i],
                    };
                });
            }
        }

        
    }

    useEffect(() => {
        var myHeaders = new Headers();
        myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");

        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };

        fetch(`${url}/api/v1/gamemaxamount`, requestOptions)
            .then(response => response.json())
            .then(result => {
                result.showMax.map((value) => {
                    if (value.type === 'Soft Lotto') {
                        setSoftMax(value.value)
                    } else {
                        return;
                    }
                })
            })
            .catch(error => console.log('error', error));
        
    }, [])
    
    const handleInputChange = (e) => {
        e.preventDefault()
        let value = e.target.value
        if (value > parseInt(softMax)) {
            setSuccess(`Cannot place bet with more than ${softMax} naira`)
            return
        } else {
            setValue(value)
        }
    }

    const handleSelect = (e) => {
        e.preventDefault()
        setGameType(e.target.value)
    }

    useEffect(() => {
        if (window.innerWidth > 770) {
            setShowGameModal(false)
        }
    }, [])

    const handleInputSubmit = (data) => {
        data.amount = parseInt(value) * data.stakeList.length;
        data.amounts = parseInt(value)
        calculateTotalStake()
    }
    
    
    const handleRandom = e => {
        e.preventDefault()
        let ar = []
        const number = Math.floor(Math.random() * 10) + 1
        const number1 = Math.floor(Math.random() * 10) + 1
        const number2 = Math.floor(Math.random() * 10) + 1
        const num3 = Math.floor(Math.random() * 10) + 1
        const num4 = Math.floor(Math.random() * 10) + 1
        ar = [number, number1, number2]
        let numberSet = new Set(ar)
        let a = [...numberSet]
        if (a.length !== 3) {
            a.push(num3)
            let x = new Set(a)
            let b = [...x]
            if (b.length !== 3) {
                b.push(num4)
                let y = new Set(b)
                let m = [...y]
                setArray(m)
            }
            setArray(b)
            setGetNums(true);
        } else {
            setArray(a)
            setGetNums(true);
        }
    }

    const removeItems = id => {
        let newItems = list.filter((item) => item.id !== id)
        if (newItems.length < 1) {
            setList([])
            setShowCoupon(false)
        } else {
            setList(newItems)
        }
    }

    useEffect(() => {
        var myHeaders = new Headers();
        myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");

        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };

        fetch(`${url}/api/v1/gamemaxamount`, requestOptions)
            .then(response => response.json())
            .then(result => {
                result.showMax.map((value) => {
                    if (value.type === 'General') {
                        setGeneral(value.value)
                    } else {
                        return;
                    }
                })
            })
            .catch(error => console.log('error', error));
        
    }, [])

    const handleCouponBet = e => {
        e.preventDefault()
        const val = list.reduce((total, money) => {
            total += parseInt(money.amounts)
            return total;
        }, 0);

        if (user.balance > val) {
            if (val < 1) {
                setSuccess(`Kindly add an amount`)
                return;
            } else {
                console.log(list)
                var myHeaders = new Headers();
                myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
                myHeaders.append("Authorization", `Bearer ${get}`);
                myHeaders.append("Content-Type", "application/json");
                myHeaders.append("Cookie", "connect.sid=s%3Ae0Zu5esOyG3kUqFKLpyqNN4lka0d4o3F.WngMLxSIx3GfvGfOlQDWPwr%2BeLG2ABZ6sABSEo4Hwkc");
          
                list.filter((bet, i) => {
                    const { coupon, type, gameId, stakes, stakes1, stakes2, kind, amount, amounts } = bet;

                    if (amount < 1) {
                        setSuccess(`cannot place bet for ticket number ${i + 1} please add an amount`)
                        return;
                    } else {

                        //                         var raw = JSON.stringify({
                        //     "stakes": [
                        //         {
                        //             "coupon": `${code}`,
                        //             "amounts": `${amounts}`,
                        //             "value": `${amount}`,
                        //             "kind": `${kind}`,
                        //             "numbers": `${stakeList}`,
                        //             "type": `${type}`,
                        //             "game": "Soft Lotto"
                        //         }
                        //     ]
                        // });
                            
                        var raw = JSON.stringify({
                            "totalStake": `${number}`,
                            "stakes": [
                                {
                                    "coupon": `${id + 'softlotto'}`,
                                    "value": `${amount}`,
                                    "amounts": `${amounts}`,
                                    "type": `${type}`,
                                    "kind": `${kind}`,
                                    "numbers": `${stakes}`,
                                    "game": "Soft Lotto"
                                },
                            ]
                        });

                        var requestOptions = {
                            method: 'POST',
                            headers: myHeaders,
                            body: raw,
                            redirect: 'follow'
                        };

                        fetch(`${url}/api/v1/placeSoftLotto`, requestOptions)
                            .then(response => response.json())
                            .then(result => {
                                console.log(result)
                                if (result.result) {
                                    let show = result.result.map((res) => res)
                                    const { type, amount, odd, staked, coupon, date, amounts, kind, possibleWinning, gameId, stakes1, stakes2 } = show[0];
                                    let response1 = stakes.toString()
                                    var myHeaders = new Headers();
                                    myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
                                    myHeaders.append("Authorization", `Bearer ${get}`);
                                    myHeaders.append("Content-Type", "application/json");

                                    var raw = JSON.stringify({
                                        "game": 'Soft Lotto',
                                        "amount": `${amount}`,
                                        "amounts": `${amounts}`,
                                        "type": `${type}`,
                                        "kind": `${kind}`,
                                        "odd": `${odd}`,
                                        "possibleWinning": `${possibleWinning}`,
                                        "staked": `${amount}`,
                                        "stakes": `${response1}`,
                                        "line": `3`,
                                        "date": `${date}`,
                                        "coupon": `${coupon}`,
                                        "gameId": `${gameId}`
                                    });

                                    var requestOptions = {
                                        method: 'POST',
                                        headers: myHeaders,
                                        body: raw,
                                        redirect: 'follow'
                                    };

                                    fetch(`${url}/api/v2/auth/betHistory`, requestOptions)
                                        .then(response => response.json())
                                        .then(result => {
                                            console.log(result)
                                            if (result.success) {
                                                setId(Math.floor(Math.random() * 10000000000) + 1)
                                                const { message } = result.success;
                                                setMode(message)
                                                setOpen(!open)
                                                var myHeaders = new Headers();
                                                myHeaders.append("signatures", "5a1131f2eb747be50714281ec3e68b759476c6dc9e1faf5fc5d91c552cf8c230");
                                                myHeaders.append("Authorization", `Bearer ${get}`);

                                                var requestOptions = {
                                                    method: 'GET',
                                                    headers: myHeaders,
                                                    redirect: 'follow'
                                                };

                                                fetch(`${url}/api/v2/auth/profile`, requestOptions)
                                                    .then(response => response.json())
                                                    .then(result => {
                                                        if (result.success) {
                                                            getUser(result.success.data)
                                                            const { data } = result.success;
                                                            showBoard(data)
                                                            //   localStorage.setItem('user', JSON.stringify(data))
                                                        } else {
                                                            return;
                                                        }
                                                    },
                                                        (error) => {
                                                            console.log(error)
                                                        });

                                            } else {
                                                setSuccess('Check your internet, refresh and try again. Thanks.')
                                                return;
                                            }
                                        })
                                        .catch(error => console.log('error', error));

                                } else {
                                    return
                                }
                
                            },
                                (error) => {
                                    console.log(error)
                                }
                            );
                    }
                })
    
            }
        } else {
            setSuccess('Please kindly note that you currently do not have sufficient balance to place this bet')
            return;
        }
    }

    const handleCat = (e, data) => {
        if (isNaN(e.target.value)) {
            return;
        } else {
            e.target.value * data.stakeList.length > parseInt(general) && setSuccess(`for games whose staking total amount is more than ${general} naira the odds would be made dynamic`)
            data.amounts = e.target.value;
            data.amount = e.target.value * data.stakeList.length;
            calculateTotalStake()
        }
    }

    const handleV = (e, items) => {
        e.preventDefault()
        if (isNaN(e.target.value) || e.target.value < 1) {
            return;
        } else {
            setListV(e.target.value)
            items.amounts = e.target.value
            items.amount = e.target.value * items.line
            items.staked = e.target.value * items.line
        }
    }

    useEffect(() => {
        let betSlip = JSON.parse(localStorage.getItem('SoftLotto'))
        console.log(betSlip)

        if (betSlip) {
            const val = betSlip.reduce((total, money) => {
                total += parseInt(money.amount)
                return total;
            }, 0);
            
            setNumber(val)
        }


    }, []);


    useEffect(() => {
        const timeInterval = setInterval(() => {
            if ((new Date().getMinutes()) <= 9) {
                setTime(`${new Date().getHours()}:10`)
            }else if ((new Date().getMinutes()) >= 10 && (new Date().getMinutes()) < 21) {
                setTime(`${new Date().getHours()}:20`)
            
            } else if ((new Date().getMinutes()) >= 21 && (new Date().getMinutes()) < 31) {
                setTime(`${new Date().getHours()}:30`)
            
            } else if ((new Date().getMinutes()) >= 31 && (new Date().getMinutes()) < 41) {
                setTime(`${new Date().getHours()}:40`)
            
            } else if ((new Date().getMinutes()) >= 41 && (new Date().getMinutes()) < 51) {
                setTime(`${new Date().getHours()}:50`)
            
            } else if ((new Date().getMinutes()) >= 51 && (new Date().getMinutes()) <= 59) {
                setTime(`${new Date().getHours() + 1}:00`)
            }
        }, 1000)
        return () => clearInterval(timeInterval)
    });

    return (
        <section className='black_bg soft'>
            <div className='news pl-1 pl-lg-5 p_white game_head d-flex justify-content-between'>
                <p className="timer pt-2">{timer}</p>
                <Link className='game_links backgs first pt-2' to='/lottoexpress'>Lotto Express</Link>
                <Link className='game_links backgs pt-2' to='/games'>Original Lotto</Link>
                <Link className='game_links backgs first pt-2'  to='/promotion'>7/90</Link>
                <Link className='first activated ml-3 pt-2' to='/softlotto'>Soft Lotto</Link>
                <Link className='pt-2' to='#'>
                    <p className='game_links d-none d-lg-inline backgs first'>How to Play</p>
                </Link>
                <Link className='pt-2' to='/fundwallet'>
                    <p className='game_links backgs'>Deposit Fund</p>
                </Link>
                <Link className='pt-2' to='#'>
                    <p className='game_links d-none d-lg-inline backgs'>First Bet</p>
                </Link>
                <div className="pt-2 d-none d-lg-flex flex-right mr-2">
                    <Link to='https://www.facebook.com'>
                        <FontAwesomeIcon className=' backg backgs color3' size-mode='1x' icon={faFacebook} />
                    </Link>
                    <Link to='https://www.twitter.com'>
                        <FontAwesomeIcon className='ml-1 mr-1 backgs backg color4' size-md='1x' icon={faTwitter} />
                    </Link>
                    <Link to='https://www.instagram.com'>
                        <FontAwesomeIcon className='mr-1 backg backgs color5' size-md='1x' icon={faInstagram} />
                    </Link>
                    <Link to='https://www.youtube.com'>
                        <FontAwesomeIcon className=' backg backgs color6' size-md='1x' icon={faYoutube} />
                    </Link>
                </div>
            </div>
            <Container>
                <Row>
                    <Col className='d-none d-lg-inline' lg={3}>
                        <section className='mt-2 ml-2 ml-lg-0 mb-2 mb-lg-0 pt-5'>
                            <img src={play} alt="" className='game_section_svg' />
                        </section>
                    
                    </Col>
                    <Col lg={5} className={` boxplay soft-lotto`}>
                        <div className='d-flex justify-content-center'>
                            <h5 className='yellow game_head'>Soft Lotto</h5>
                        <Form.Control className='yellow choose mt-3 mb-3' style={{width: '150px', marginTop: '10px'}} as="select" required onChange={(e) => {history.push(e.target.value)}} custom>
                            <option className='yellow' name='softlotto' value='/softlotto'>Soft Lotto (3/10)</option>
                            <option className='yellow' name='games' value='/games'>Original Lotto (5/90)</option>
                            <option className='yellow' name='lottoexpress' value='/lottoexpress'>Lotto Express (5/90)</option>
                            <option className='yellow' name='promotions' value='/promotion'>Extra Lotto (7/90)</option>
                        </Form.Control>
                        </div>
                                               <div className='d-flex justify-content-center'>
                            {/* <h4 className='h4_font'>
                                            </h4> */}
                            {/* {gameShow.name && <p className='p_width pr-2' style={{ fontWeight: '800' }}> <span className='span_h4'>{gameShow.name}</span></p>} */}
                            <p className='p_width' style={{ fontWeight: '500' }}> <span className='span_h4'>Results will be drawn: {time} WAT</span></p>
                        </div>
                        <Form.Control as="select" required onChange={handleSelect} className='mb-3' custom>
                            <option name='Regular' value='Regular'>Regular</option>
                            <option name='Perfect Order' value='Perfect Order'>Perfect Order</option>
                        </Form.Control>
                        <div className='mt-2 text-md-center ml-2 ml-md-0'>
                            {nums.map((i) => {
                                return <button key={i} name={!activeNums[i] && 'ready'} onClick={() => handleClass(i)} className={`${array.includes(i) ? 'game_clicked' : geteNums && 'red'} balx`} >{i}</button>
                            })}
                        </div>
                        <Row className="clear instant_pay mt-2">
                            <Button variant='outline-primary' className='thew' onClick={handleRandom}>Quick Pick</Button>
                            <Button variant='success' className='thew' onClick={handleBet}>Play</Button>
                            <Button variant='outline-danger' className='thew' onClick={() => { setBetSlip([]); calculateTotalStake1([]); setHow(true); setSlip(false); setCount(0); setShowGameModal(false) }}>Clear</Button>
                        </Row>
                    </Col>
                    <Col lg={3} className='show1'>
                        <Row className='mt-2'>
                            <div className={`col-md-6 col-sm-6 mbox1 text-center ${betSlip.length < 1 && 'disabled'}`} onClick={() => { setSlip(true); setHow(false); }}>BetSlip <span class="badge bg-secondary">{count}</span></div>
                            <div className='col-md-5 ml-1 col-sm-6 mbox1 text-center' onClick={() => { setSlip(false); setHow(true); }}><small>Search Coupon</small></div>
                        </Row>
                        <Row>
                            {
                                how ?
                                    <div className={`${list.length > 0 && 'scroller'} bet_section col-sm-12`} id="howto">
                                        <form className="d-flex flex-column" onSubmit={CouponSubmit}>
                                            <input className="form-control m-2" type="text" name='coupon' placeholder="Coupon Code" onChange={handleCoupon} />
                                            <button className="btn size=sm btn-outline-success ml-2 mr-2 mb-2" type="submit" >Load Coupon</button>
          
                                        </form>
                                        {showCoupon && <div className='show_coupon' >
                                            {list.map((items) => {
                                                var { amount, id, odd, results, stakes, line, type, stakes1, amounts, stakes2, status, staked, possibleWinning, game, gameId } = items;
                                                return (
                                                    <>
                                                        <main className='get_line'>
                                                            <div className='d-flex justify-content-end'>
                                                                <FaTimes onClick={() => {
                                                                    removeItems(id)
                                                                }}
                                                                    className='cancel_game'
                                                                />
                                                            </div>
                                                            <div className=''>
                                                                {status !== 'PENDING' && <p className='redz'>Expired ticket</p>}
                                                                <p className='p_type'>possibleWinning: &#x20A6;{possibleWinning}</p>
                                                                <p className='p_type'>Odd: {odd}</p>
                                                                {user.wallet && <p className='p_type'>status: {status.toLowerCase()}</p>}
                                                                <p className='p_type'>Game Name: {game}</p>
                                                                <p className='p_type'>Lines: {line}</p>
                                                                <p className='p_type'>Stake Amount: &#x20A6;{staked}</p>
                                                                <p className='p_type'>Stake Amount per Line: &#x20A6;{amounts}</p>
                                                                {stakes && <p className='p_type'>Bet Numbers: {stakes}</p>}
                                                                {stakes1 && <p className='p_type'>Against first: {stakes1}</p>}
                                                                {stakes2 && <p className='p_type'>Against second: {stakes2}</p>}
                                                                {results && <p className='p_type'>Numbers Drawn {results}</p>}
                                                                <p className='p_type'>game Type: {type}</p>
                                                                {status === 'PENDING' && <div>
                                                                    <Form.Control className='form_input' pattern='[0-9]' onChange={(e) => handleV(e, items)} value={items.amounts} size='sm' placeholder={amounts} />
                                                                    <div className='mt-2 d-flex justify-content-lg-between'>
                                                                        <Button className='mr-2 mr-lg-0 games game' value='50' size='sm' onClick={() => {
                                                                            setListV(50)
                                                                            items.amounts = 50;
                                                                            items.amount = 50 * items.line
                                                                            items.staked = 50 * items.line
                                                                        }}>50
                                                                        </Button>
                                                                        <Button className='mr-2 mr-lg-0 games game ' size='sm' value='100' size='sm' onClick={() => {
                                                                            setListV(100)
                                                                            items.amounts = 100;
                                                                            items.amount = 100 * items.line
                                                                            items.staked = 100 * items.line
                                                                        }}>100</Button>
                                                                        <Button className='mr-2 mr-lg-0 games game' size='sm' value='500' size='sm' onClick={() => {
                                                                            setListV(500)
                                                                            items.amounts = 500;
                                                                            items.amount = 500 * items.line
                                                                            items.staked = 500 * items.line
                                                                        }}>500</Button>
                                                                        <Button className='mr-2 mr-lg-0 games game' size='sm' value='1000' size='sm' onClick={() => {
                                                                            setListV(1000)
                                                                            items.amounts = 2500;
                                                                            items.amount = 2500 * items.line
                                                                            items.staked = 2500 * items.line
                                                                        }}>1000</Button>
                                                                        <Button className='mr-2 mr-lg-0 games game' size='sm' value='2500' size='sm' onClick={() => {
                                                                            setListV(2500)
                                                                            items.amounts = 2500;
                                                                            items.amount = 2500 * items.line
                                                                            items.staked = 2500 * items.line
                                                                        }}>2500</Button>
                                                                        <Button className='mr-2 mr-lg-0 games game' size='sm' value='5000' size='sm' onClick={() => {
                                                                            setListV(5000)
                                                                            items.amounts = 5000;
                                                                            items.amount = 5000 * items.line
                                                                            items.staked = 5000 * items.line
                                                                        }}>5000</Button>
                                                                    </div>
                                                                </div>
                                                                }
                                                            </div>
                                                        </main>
                                                    </>
                                                )
                                            })}
                                            <Button size='sm' autoFocus className={`align-item-center mb-2 game `} variant='success' onClick={handleCouponBet}>Bet Ticket</Button>
                                        </div>}
                                    </div> :
                                    <section className={`${betSlip.length > 1 && 'scroller'} bet_section mt-2`}>
                                        <div className='d-flex justify-content-between game_back'>
                                        </div>
                                        <div>
                                            {betSlip.map((data) => {
                                                const { type, id, stakeList, stakeList2, amount, kind } = data;
                                                return (
                                                    <main key={id} className='get_line'>
                                                        <div className='d-flex justify-content-end'>
                                                            <FaTimes onClick={() => {
                                                                removeItem(id)
                                                            }}
                                                                className='cancel_game'
                                                            />
                                                        </div>
                                                        <div>
                                                            <p className='p_type'>Game Name: Soft Lotto </p>
                                                            <p className='p_type'>Type: {type} </p>
                                                            <p className='p_type'>Stakes: {stakeList.toString()} </p>
                                                            <p className='p_type'>Betslip total Amount: {amount}</p>
                                                            <Form.Control className='form_input' onChange={(e) => handleCat(e, data)} value={data.amounts > 0 ? data.amounts : ''} size='sm' placeholder={data.amounts > 0 ? `${data.amounts}` : 'Amount'} />
                                                            <div className='mt-2 d-flex justify-content-lg-between'>
                                                                <Button className='mr-2 mr-lg-0 games game' value='50' size='sm' onClick={() => {
                                                                    data.amount = 50;
                                                                    data.amounts = 50
                                                                    calculateTotalStake()
                                                                }}>50</Button>
                                                                <Button className='mr-2 mr-lg-0 games game ' size='sm' value='100' size='sm' onClick={() => {
                                                                    data.amount = 100;
                                                                    data.amounts = 100
                                                                    calculateTotalStake()
                                                                }}>100</Button>
                                                                <Button className='mr-2 mr-lg-0 games game' size='sm' value='500' size='sm' onClick={() => {
                                                                    data.amount = 500;
                                                                    data.amounts = 500
                                                                    calculateTotalStake()
                                                                }}>500</Button>
                                                                <Button className='mr-2 mr-lg-0 games game' size='sm' value='1000' size='sm' onClick={() => {
                                                                    data.amount = 1000;
                                                                    data.amounts = 1000
                                                                    calculateTotalStake()
                                                                }}>1000</Button>
                                                                <Button className='mr-2 mr-lg-0 games game' size='sm' value='400' size='sm' onClick={() => {
                                                                    data.amount = 2500;
                                                                    data.amounts = 2500
                                                                    calculateTotalStake()
                                                                }}>2500</Button>
                                                                <Button className='mr-2 mr-lg-0 games game' size='sm' value='500' size='sm' onClick={() => {
                                                                    data.amount = 5000;
                                                                    data.amounts = 5000
                                                                    calculateTotalStake()
                                                                }}>5000</Button>
                                                            </div>
                                                        </div>
                                                    </main>
                                        
                                                )
                                            })}
                                        </div>
                                        <section className='mt-2'>
                                            <div className='d-flex justify-content-between'>
                                                <p className='p_type'>Number of Bets: </p>
                                                <p className='p_type'>{betSlip.length}</p>
                                            </div>
                                            <div className='d-flex justify-content-between'>
                                                <p className='p_type'>Total Stake: </p>
                                                <p className='p_type'>&#x20A6;{number}</p>
                                            </div>
                                        </section>
                                        <div className='d-flex justify-content-center'>
                                            {!logedIn && <Button size='sm' autoFocus className={`align-item-center mb-2 game`} variant='success' onClick={() => { setShowModal(!showModal); showGameModal && setShowGameModal(false) }}>Login To Place Bet</Button>}
                                            {logedIn &&
                                                <Button size='sm' autoFocus className={`align-item-center mb-2 game`} variant='success' onClick={handleSubmit}>Place Bet</Button>
                                            }
                                        </div>
                            
                                    </section>
                            }
                        </Row>
                    </Col>
                   
                    <div className={`${!showGameModal ? 'display' : 'c-sidebar --bet-slip is-open'} ${betSlip.length > 0 ? 'd_none scroll_game' : 'display'}`}>
                        <section className={`${betSlip.length > 2 && 'scroller'} bet_section mt-2`}>
                            <div className='d-flex justify-content-between game_back'>
                                    <h6 className='game_h6'>
                                      Soft Lotto
                                </h6>
                                <p className='palce_bet' onClick={() =>  setShowGameModal(false)}>Place Another Bet</p>
                                <button className="game_slip_btn mb-1" onClick={() => { setBetSlip([]); calculateTotalStake1([]); setCount(0); setShowGameModal(false) }}>Clear Slip</button>
                            </div>
                            <div>
                                {betSlip.map((data) => {
                                    const { id, stakeList, amount, type } = data;
                                    return (
                                        <main key={id} className='get_line'>
                                            <div className='d-flex justify-content-end'>
                                                <FaTimes onClick={() => {
                                                    removeItem(id)
                                                }}
                                                    className='cancel_game'
                                                />
                                            </div>
                                            <div>
                                                <p className='p_type'>Game Name: Soft Lotto </p>
                                                <p className='p_type'>Type: {type}</p>
                                                <p className='p_type'>Stakes: {stakeList.toString()} </p>
                                                <p className='p_type'>Betslip total Amount: {amount}</p>
                                                <Form.Control className='form_input' onChange={(e) => handleCat(e, data)} value={data.amounts > 0 ? data.amounts : ''} size='sm' placeholder={data.amounts > 0 ? `${data.amounts}` : 'Amount'} />
                                                <div className='mt-2 d-flex justify-content-lg-between'>
                                                    <Button className='mr-1 mr-lg-0 games game' value='50' size='sm' onClick={() => {
                                                        data.amount = 50;
                                                        data.amounts = 50
                                                        calculateTotalStake()
                                                    }}>50</Button>
                                                    <Button className='mr-2 mr-lg-0 games game ' size='sm' value='100' size='sm' onClick={() => {
                                                        data.amount = 100;
                                                        data.amounts = 100
                                                        calculateTotalStake()
                                                    }}>100</Button>
                                                    <Button className='mr-2 mr-lg-0 games game' size='sm' value='500' size='sm' onClick={() => {
                                                        data.amount = 500;
                                                        data.amounts = 500
                                                        calculateTotalStake()
                                                    }}>500</Button>
                                                    <Button className='mr-1 mr-lg-0 games game' size='sm' value='1000' size='sm' onClick={() => {
                                                        data.amount = 1000;
                                                        data.amounts = 1000
                                                        calculateTotalStake()
                                                    }}>1000</Button>
                                                    <Button className='mr-2 mr-lg-0 games game' size='sm' value='400' size='sm' onClick={() => {
                                                        data.amount = 2500;
                                                        data.amounts = 2500
                                                        calculateTotalStake()
                                                    }}>2500</Button>
                                                    <Button className='mr-1 mr-lg-0 games game' size='sm' value='500' size='sm' onClick={() => {
                                                        data.amount = 5000;
                                                        data.amounts = 5000
                                                        calculateTotalStake()
                                                    }}>5000</Button>
                                                    <Button className='mr-2 mr-lg-0 games game' size='sm' value='500' size='sm' onClick={() => {
                                                        data.amount = 10000;
                                                        data.amounts = 10000
                                                        calculateTotalStake()
                                                    }}>10000</Button>
                                                </div>
                                            </div>
                                        </main>
                                        
                                    )
                                })}
                            </div>
                            <section className='mt-2'>
                                <div className='d-flex justify-content-between'>
                                    <p className='p_type'>Number of Bets: </p>
                                    <p className='p_type'>{betSlip.length}</p>
                                </div>
                                <div className='d-flex justify-content-between'>
                                    <p className='p_type'>Total Stake: </p>
                                    <p className='p_type'>&#x20A6;{number}</p>
                                </div>
                            </section>
                            <div className='d-flex justify-content-center'>
                                {!logedIn && <Button size='sm' autoFocus className={`align-item-center mb-2 game`} variant='success' onClick={() => { setShowModal(!showModal); showGameModal && setShowGameModal(false) }}>Login To Place Bet</Button>}
                                {logedIn &&
                                    <Button size='sm' autoFocus className={`align-item-center mb-2 game`} variant='success' onClick={handleSubmit}>Place Bet</Button>
                                }
                            </div>
                            
                        </section>
                    </div>
                </Row>
                {showModal && <GetWhatsapp />}
                {showMobile && <MobileCoupon value='soft lotto' showMobile={() => setShowMobile(false)} />}
                {fundModal && <FundModal currentUrl={currentUrl} setFundModal={setFundModal} mode={alerts} />}
                {open && <Bet mode={mode} setOpen={() => { setOpen(false) }} betSlip={setBetSlip} type='softlotto' calculateTotalStake1={() => calculateTotalStake1([])} setMode={setMode} number={setNumber} count={setCount} />}
                {success && <IntegrationNotistack success={`${success}`} />}
            <section className='bottom'>
                <div className='d-flex'>
                    <div className='game_item' style={{background: '#122d4b', fontWeight: '800'}}>
                        {/* <Button variant='outline-primary' className='thew' onClick={handleRandom}>Quick Pick</Button> */}
                        <p onClick={handleRandom} className='pt-2 pb-2 mb-0 b play_sec'>Quick Pick</p>
                    </div>
                    <div className='game_item' style={{background: '#007bff', fontWeight: '800'}}>
                                            
                            <p onClick={handleBet} className='pt-2 pb-2 mb-0 play_sec'>Play</p>
                    </div>
                    <div className='game_item' style={{background: '#6c757d', fontWeight: '800'}}>
                            <p onClick={() => { setArr([]); setGetNums(false) }} className='pt-2 pb-2 mb-0 play_sec'>Clear</p>
                                            {/* <Button variant='outline-danger' className='thew' onClick={() => { setArr([]); setHideButton(false); setFirstAgainst([]); setGetNums(false) }}>Clear</Button> */}
                    </div>
                </div>
                <div className='d-flex smaller'>
                   
                    <div className='game_item' onClick={() => { showGameModal && setShowGameModal(false); history.push('/') }} style={{fontWeight: '700'}}>
                        <RiHome2Line className='select_icon' />
                        <span className='select_item'>Home</span>
                    </div>
                    <div className='game_item' onClick={() => { showGameModal && setShowGameModal(false); setShowMobile(!showMobile) }} style={{fontWeight: '700'}}>
                        <IoIosFootball className='select_icon' />
                        <span className='select_item'>Load Coupon</span>
                    </div>
                    <div className={`${betSlip.length > 0 && 'real'} game_item`} onClick={() => { setShowGameModal(!showGameModal) }} style={{fontWeight: '700'}}>
                        {/* <span className='bet_count'>{count}</span> */}
                        <IoOptionsOutline className='select_icon' />
                        <span className='select_item'>BetSlip <span className={`${betSlip.length > 0 && 'zoom-in-zoom-out'}`}>({count})</span> </span>
                    </div>
                    <div className={`game_item ${!logedIn && 'disabled getLogged'}`} onClick={() => { showGameModal && setShowGameModal(false); history.push('/profile') }} style={{fontWeight: '700'}}>
                        <IoMdHelpCircleOutline className='select_icon' />
                        <span className='select_item' >FAQ & Support</span>
                    </div>
                </div>
            </section>
            </Container>
            <div class="row win">
                 <h1 className='instant_pay' style={{marginLeft: '26rem'}}>INSTANT PAYMENT, NO STORY</h1>
                <Col className='pr-0'>
                  <p style={{display: 'flex', marginBottom: '0px', flexWrap: 'wrap'}}>{array.map((a) => <span className='num_span'>{a }</span> )}</p>
                </Col>
                {/* <div class="col-md-12 col-sm-12 col-xs-12">
                      
                  </div> */}
            </div>
        </section>
        
    )
}

export default LottoExpress