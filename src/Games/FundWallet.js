import React, { useState, useEffect } from 'react';
import { Form, Button } from 'react-bootstrap';
import { PaystackButton } from "react-paystack"
import IntegrationNotistack from '../Fetch/IntegrationNotistack';
import { Redirect } from 'react-router-dom'
import Layout from '../Layout'
import { useHistory } from 'react-router';
import { useGlobalContext } from '../store/context';
import { SignalCellularNullRounded } from '@material-ui/icons';

const FundWallet = (props) => {
    let { giveAccess, isLoggedIn, urls, getUser, showBoard, user, refreshCanvas } = useGlobalContext();
    const [amount, setAmount] = useState(null)
    const [opay, setOpay] = useState(false)
    const [opayAmount, setOpayAmount] = useState('')
    const [paystack, setPaystack] = useState(true)
    const publicKey = process.env.REACT_APP_PAYSTACK_PUBLIC_KEY
    const [show, setShow] = useState(false)
    const [urlz, setUrl] = useState(null)
    const history = useHistory()
    const get = localStorage.getItem('token')
    const url = 'https://api.grandlotto.ng'
    const [success, setSuccess] = useState('')
    const [showAlert, setShowAlert] = useState(false)
  
    // console.log(urls.indexOf('/'))

    // const getU = () => {
    //     return 
    // }
    // arr.slice(arr.indexOf(0) + 1, arr[arr.length - 1])

    const handleSubmit2 = e => {
        e.preventDefault()
        // console.log(opayAmount)

        var myHeaders = new Headers();
        myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
        myHeaders.append("Authorization", `Bearer ${get}`);
        myHeaders.append("Content-Type", "application/json");

        var raw = JSON.stringify({
            "amount": `${opayAmount}`,
            "url": `${'https://www.grandlotto.ng/fundwallet'}`
        });

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };

        fetch("https://api.grandlotto.ng/api/v2/auth/opay/initialise", requestOptions)
            .then(response => response.json())
            .then(result => {
                // console.log(result)
                if (result.success) {
                    const { data } = result.success.data;
                    window.location.href = data.cashierUrl
                    setUrl(data.cashierUrl)
                    // console.log(window.location.href)
                    // history.push(`/${data.cashierUrl}`)
                    // var myHeaders = new Headers();
                    // myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
                    // myHeaders.append("Authorization", `Bearer ${get}`);
                    // myHeaders.append("Content-Type", "application/json");

                    // var raw = JSON.stringify({
                    //     "reference": `${data.reference}`
                    // });

                    // var requestOptions = {
                    //     method: 'POST',
                    //     headers: myHeaders,
                    //     body: raw,
                    //     redirect: 'follow'
                    // };

                    // fetch("https://api.grandlotto.ng/api/v2/auth/opay/validate", requestOptions)
                    //     .then(response => response.json())
                    //     .then(result => {
                    //         console.log(result)
                    //         if (result.success) {
                    //             const { message } = result.success;
                    //             setSuccess(message)
                    //         } else {
                    //             setSuccess(result.error.message)
                    //         }
                    //     })
                    //     .catch(error => console.log('error', error));
                }
            })
            .catch(error => console.log('error', error));
    }

    const handleChange1 = e => {
        e.preventDefault()
        setOpayAmount(e.target.value)
    }

    useEffect(() => {
        var myHeaders = new Headers();
        myHeaders.append("signatures", "3b55227b019105b2f8550792916ee41321b53fb2104fd0149e81c360811ef027");
        myHeaders.append("Authorization", `Bearer ${get}`);


        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };

        fetch(`${url}/api/v2/auth/profile`, requestOptions)
            .then(response => response.json())
            .then(result => {
                getUser(result.success.data)
                giveAccess(isLoggedIn)
            })
            .catch(error => console.log('error', error));
    }, []);

        
    useEffect(() => {
        
        var url_string = window.location.href
        var url = new URL(url_string)
        var opayRef = url.searchParams.get('opayref')
            
        // console.log(currentUrl)
        if (opayRef) {
            // console.log('heeeee')
            var myHeaders = new Headers();
            myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
            myHeaders.append("Authorization", `Bearer ${get}`);
            myHeaders.append("Content-Type", "application/json");

            var raw = JSON.stringify({
                "reference": `${opayRef}`
            });

            var requestOptions = {
                method: 'POST',
                headers: myHeaders,
                body: raw,
                redirect: 'follow'
            };

            fetch("https://api.grandlotto.ng/api/v2/auth/opay/validate", requestOptions)
                .then(response => response.json())
                .then(result => {
                    if (result.success) {
                        const { message } = result.success;
                        setSuccess(message)
                        setUrl = ('https://www.grandlotto.ng/fundwallet')
                        // windo = 'https://www.grandlotto.ng/fundwallet'
                    } else {
                        setSuccess(result.error.message)
                    }
                })
                .catch(error => console.log('error', error));
        }
        // }
    }, []);


    
    // useEffect(() => {
    //     let windo = window.location.href;
    //     if (windo.includes('=')) {
    //         const currentUrl = windo?.substring(windo?.indexOf('=') + 1)
    //         // console.log(currentUrl)

    //         // console.log(currentUrl)
    //         if (currentUrl) {
    //             // console.log('heeeee')
    //             var myHeaders = new Headers();
    //             myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
    //             myHeaders.append("Authorization", `Bearer ${get}`);
    //             myHeaders.append("Content-Type", "application/json");

    //             var raw = JSON.stringify({
    //                 "reference": `${currentUrl}`
    //             });

    //             var requestOptions = {
    //                 method: 'POST',
    //                 headers: myHeaders,
    //                 body: raw,
    //                 redirect: 'follow'
    //             };

    //             fetch("https://api.grandlotto.ng/api/v2/auth/opay/validate", requestOptions)
    //                 .then(response => response.json())
    //                 .then(result => {
    //                     if (result.success) {
    //                         const { message } = result.success;
    //                         setSuccess(message)
    //                         setUrl = ('https://www.grandlotto.ng/fundwallet')
    //                         windo = 'https://www.grandlotto.ng/fundwallet'
    //                     } else {
    //                         setSuccess(result.error.message)
    //                     }
    //                 })
    //                 .catch(error => console.log('error', error));
    //         }
    //     }
    // }, []);




    // useEffect(() => {
    //     // let windo = window.location.href;
    //     const currentUrl = urlz?.substr(urlz?.indexOf('=') + 1)
    //     // console.log(currentUrl)

    //     // console.log(urlz)
    //     if (currentUrl) {
    //         var myHeaders = new Headers();
    //         myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
    //         myHeaders.append("Authorization", `Bearer ${get}`);
    //         myHeaders.append("Content-Type", "application/json");

    //         var raw = JSON.stringify({
    //             "reference": `${currentUrl}`
    //         });

    //         var requestOptions = {
    //             method: 'POST',
    //             headers: myHeaders,
    //             body: raw,
    //             redirect: 'follow'
    //         };

    //         fetch("https://api.grandlotto.ng/api/v2/auth/opay/validate", requestOptions)
    //             .then(response => response.json())
    //             .then(result => {
    //                 if (result.success) {
    //                     const { message } = result.success;
    //                     setSuccess(message)
    //                     setUrl=('https://www.grandlotto.ng/fundwallet')
    //                 } else {
    //                     setSuccess(result.error.message)
    //                 }
    //             })
    //             .catch(error => console.log('error', error));
    //     }
    // }, []);



    let reference = new Date().getTime().toString()

    let componentProps;

    if (user) {
        
        let val = user.mobile + '@grandlotto.ng';
        
        componentProps = {
            email: user.email !== null ? user.email : val,
            reference,
            amount: parseInt(amount) * 100,
            metadata: {
                name: `${user.firstname} ${user.lastname}`,
                phone: user.mobile
            },
            publicKey,
            text: "Paystack",
            onSuccess: () => {
                var myHeaders = new Headers();
                myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
                myHeaders.append("Authorization", `Bearer ${get}`);
                myHeaders.append("Content-Type", "application/json");

                var raw = JSON.stringify({
                    "reference": `${reference}`
                });

                var requestOptions = {
                    method: 'POST',
                    headers: myHeaders,
                    body: raw,
                    redirect: 'follow'
                };

                //http://localhost:5016
                //https://lotto-api.firstbetng.com/                 

                fetch(`${url}/api/v2/auth/fund-wallet`, requestOptions)
                    .then(response => response.json())
                    .then(result => {
                        if (result.success) {
                            const { message } = result.success;
                            setSuccess(message)
                            var myHeaders = new Headers();
                            myHeaders.append("signatures", "5a1131f2eb747be50714281ec3e68b759476c6dc9e1faf5fc5d91c552cf8c230");
                            myHeaders.append("Authorization", `Bearer ${get}`);

                            var requestOptions = {
                                method: 'GET',
                                headers: myHeaders,
                                redirect: 'follow'
                            };

                            fetch(`${url}/api/v2/auth/profile`, requestOptions)
                                .then(response => response.json())
                                .then(result => {
                                    if (result.success) {
                                        getUser(result.success.data)
                                        const { data } = result.success;
                                        showBoard(data)
                                        return history.push(`${urls}`)
                                        //   localStorage.setItem('user', JSON.stringify(data))
                                    } else {
                                        return;
                                    }
                                },
                                    (error) => {
                                        console.log(error)
                                    });

                        } else {
                            if (result.error) {
                                setSuccess(result.error.message);
                            }
                        }
                    })
                    .catch(error => console.log('error', error));
                setAmount("")
                props.updateProfile()
                setSuccess(`Successfully Credited your account ${refreshCanvas()}`)
            },
            onClose: () => alert("Plase come back soon"),
        };
    }

    const handleSubmit = (e) => {
        e.preventDefault()
    }

    const handleChange = (e) => {
        e.preventDefault()
        setAmount(e.target.value)
    }

    useEffect(() => {
        setTimeout(() => {
            setShowAlert(!showAlert)
        }, 3000)
    }, [success])

    const handleClick = (e) => {
        e.preventDefault()
        history.push(`/games`)
    }

    return (
        <section>
   <h5 className='payment_h5'>Choose A Payment Method</h5>
        <section className='d-flex justify-content-start wide'>
            <div className='choose_pay' onClick={() => { setPaystack(true); setOpay(false)}}>
                <h5>Paystack</h5>
            </div>
            <div className='choose_pay' onClick={() => { setPaystack(false); setOpay(true)}}>
                <h5>Opay</h5>
            </div>
        </section>
        {
            paystack &&
            <section className='widths'>
                <span className='span_stack'>Payment Via Paystack</span>
               <Form onSubmit={handleSubmit}>
                    <Form.Label htmlFor="inputPassword5">Amount:</Form.Label>
                    <Form.Control
                      type="text"
                        name='amount'
                        value={amount}
                        placeholder='Amount'
                      className='input_widths'
                      onChange={handleChange}
                      id="inputPassword5"
                      aria-describedby="passwordHelpBlock"
                    />
                    </Form>
                {/* <Button className="paystack-button" variant='success' {...componentProps} type='submit'>paystack</Button> */}
                 <PaystackButton className="paystack-button" {...componentProps} />
        </section>
        }
        {
            opay &&
            <section className='widths'>
                <span className='span_stack'>Payment Via Opay</span>
               <Form onSubmit={handleSubmit2}>
                    <Form.Label htmlFor="inputPassword5">Amount:</Form.Label>
                    <Form.Control
                      type="text"
                        name='amount'
                        value={opayAmount}
                        placeholder='Amount'
                      className='input_widths'
                      onChange={handleChange1}
                      id="inputPassword5"
                      aria-describedby="passwordHelpBlock"
                    />
                <Button className="paystack-button" variant='success' type='submit'>Opay</Button>
                </Form>
                 {/* <PaystackButton className="paystack-button" {...componentProps} /> */}
        </section>
            }
            {success && <IntegrationNotistack className='d' success={`${success}`} />}
            </section>
    )
}

export default FundWallet



// 1


