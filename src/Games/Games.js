import React, { useState, useEffect, useRef } from 'react'
import {  Row, Col, Container, Form, Button, InputGroup, SplitButton } from 'react-bootstrap'
import { useGlobalContext } from '../store/context'
import { FaTimes } from 'react-icons/fa';
import { Link } from 'react-router-dom';
import play from '../svg/play.svg'
import hamburger from '../svg/hanmburger.svg'
import moment from 'moment'
import { useHistory } from 'react-router';
import FriendModal from '../Fetch/FriendModal'
import Types from '../Fetch/Types'
import GetWhatsapp from '../Fetch/GetWhatsapp'
// import ArrowDownwardIcon from '@mui/icons-material/ArrowDownward';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import MobileCoupon from '../Fetch/MobileCoupon'
import { faLock } from '@fortawesome/free-solid-svg-icons'
import FundModal from '../Fetch/FundModal'
import GetVoice from '../Fetch/GetVoice'
import Bet from '../Fetch/Bet'
import { RiBankLine, RiHome2Line, RiSdCardMiniLine, RiUserAddLine } from 'react-icons/ri';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faApple, faGooglePlay, faFacebook, faYoutube, faTwitter, faInstagram } from '@fortawesome/free-brands-svg-icons'
import IntegrationNotistack from '../Fetch/IntegrationNotistack';
import { array } from 'yup';
import Layout from '../Layout'
import { IoIosFootball, IoMdHelpCircleOutline } from 'react-icons/io';
import { IoOptionsOutline } from "react-icons/io5";

const Games = (props) => {
    const { game, logedIn, days, user, getUser, showBoard } = useGlobalContext();
    let history = useHistory()
    const [sec, setSec] = useState([])
    const [a, setA] = useState([0])
    const [showSlip, setShowSlip] = useState(false)
    const [showGames, setShowGames] = useState(false)
    const [currentDay, setCurrentDay] = useState(days)
    const [showMobile, setShowMobile] = useState(false)
    const [activeNums, setActiveNums] = useState(false)
    const [geteNums, setGetNums] = useState(false)
    let [gameType, setGameType] = useState('PERM 2')
    const [games, setGames] = useState('EMBASSY')
    const [subValue, setSubValue] = useState(0)
    const [value, setValue] = useState(0);
    let [arr, setArr] = useState([])
    const url = 'https://api.grandlotto.ng'
    const [success, setSuccess] = useState('')
    const [showGameType, setShowGameType] = useState(false)
    const [coupon, setCoupon] = useState('')
    const [uuid, setUuid] = useState([])
    let [betSlip, setBetSlip] = useState([])
    const [showAlert, setShowAlert] = useState(false)
    const [hideButton, setHideButton] = useState(false)
    const [timer, setTimer] = useState(null)
    const [show, setShow] = useState(true)
    const [data, setDate] = useState([])
    const [data2, setDate2] = useState([])
    const [daysShow, setDaysShow] = useState('')
    const [showModal, setShowModal] = useState(false)
    const [gameShow, setGameShow] = useState({ name: '', endTime: '', day: '', image: '', start: '', end: '', id: '' })
    const [clicked, setClicked] = useState(false)
    const [showSvg, setShowSvg] = useState(false)
    const [showVoice, setShowVoice] = useState(false)
    var [list, setList] = useState([])
    const [firstAgainst, setFirstAgainst] = useState([])
    const [submited, setSubmited] = useState(false)
    const [nap1Max, setNap1Max] = useState(null)
    const [nap2Max, setNap2Max] = useState(null)
    const [general, setGeneral] = useState(null)
    const [nap3Max, setNap3Max] = useState(null)
    const [nap4Max, setNap4Max] = useState(null)
    const get = localStorage.getItem('token')
    const [nap5Max, setNap5Max] = useState(null)
    const [number, setNumber] = useState(0)
    const [secArr, setSecArr] = useState([])
    const [amount, setAmount] = useState(null)
    var [maxArr, setMaxArr] = useState([])
    const [listV, setListV] = useState(0)
    const [showAll, setShowAll] = useState([])
    const [message, showMessage] = useState(false)
    const [showGameModal, setShowGameModal] = useState(false)
    var [count, setCount] = useState(0)
    const [how, setHow] = useState(true)
    const [slip, setSlip] = useState(false)
    const [mode, setMode] = useState('')
    const [open, setOpen] = useState(false)
    var currentUrl = window.location.href.replace(/(.+\w\/)(.+)/, "/$2")
    const [showCoupon, setShowCoupon] = useState(false)
    const [id, setId] = useState(Math.floor(Math.random() * 10000000000) + 1)
    const [fundModal, setFundModal] = useState(false)
    const [alerts, setAlerts] = useState('')
    var [betCount, setBetCount] = useState(0)
    const [friend, setFriend] = useState(false)
    // const [currentDay, setCurrentDay] = useState('')
    const [currentTime, setCurrentTime] = useState(null)
    let c = new Date().toLocaleDateString();

    // const categories = [...new Set(game.map((item) => {
    //     return [item.name, item.startTime, item.endTime]
    // }))]

    useEffect(() => {
        game.map((gam, index) => {
            a.includes(index) && setGameShow({ ...gameShow, name: gam.name, day: gam.day, endTime: gam.endTime, image: gam.image, start: gam.startTime, end: gam.endTime, id: gam.uuid });
        });
    }, [game]);

    const CouponSubmit = e => {
        e.preventDefault()
        var myHeaders = new Headers();
        myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
        myHeaders.append("Authorization", `Bearer ${get}`);
        myHeaders.append("Content-Type", "application/json");

        var raw = JSON.stringify({
            "coupon": `${coupon}`
        });

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };

        fetch(`${url}/api/v1/coupons`, requestOptions)
            .then(response => response.json())
            .then(result => {
                if (result.betCoupons) {
                    setList(result.betCoupons)
                    setShowCoupon(true)
                } else {
                    setSuccess(result.error.message)
                }
            })
            .catch(error => console.log('error', error));
    }

    // console.log(game)
    
    const handleCoupon = e => {
        e.preventDefault()
        if (e.target.value.includes('regularlotto')) {
            setCoupon(e.target.value)
        } else {
            setSuccess('You can only view regular lotto coupons here')
            return;
        }
    }
    
    let nums = []

    for (let i = 1; i < 91; i++) {
        nums.push(i)
    }
    useEffect(() => {
        if (parseInt(user.dailybet_num) > 3 && user.activated < 1) {
            setSuccess('Hello Customer, This is to remind you to validate your account')
        }
    }, []);


    const handleChange = (e) => {
        e.preventDefault()
        setGetNums(false)
        setHideButton(false);
        setFirstAgainst([]);
        setArr([])
        setActiveNums(false)
        let value = e.target.value;
        if (value === 'AGAINST') {
            setHideButton(false);
            setFirstAgainst([]);
            setHideButton(false)
        }
        setGameType(value)
    }
  

    const factorial = (num) => {
        if (num === 0 || num === 1) return 1
        for (var i = num - 1; i >= 1; i--) {
            num *= i
        }
        return num
    }


    useEffect(() => {
        let v = JSON.parse(localStorage.getItem('RegularLotto'))
        if (v?.length > 0) {
            setSlip(true)
            setShowGames(true)
            setHow(false)
            setCount(v.length)
            setBetSlip(v)
        }
    }, []);

    useEffect(() => {
        let hours = new Date().getHours()
        let mins = new Date().getMinutes()
        let startTime = `${hours}:${mins}`

        if (startTime === '23:59') {
            localStorage.setItem('RegularLotto', [])
        }
    }, []);

        useEffect(() => {
        var v = JSON.parse(localStorage.getItem('RegularLotto'))

        function GetDates(startDate, daysToAdd) {
            var aryDates = [];

            for (var i = 0; i <= daysToAdd; i++) {
                var currentDate = new Date();
                currentDate.setDate(startDate.getDate() + i);
                aryDates.push(DayAsString(currentDate.getDay()));
            }

            return aryDates;
        }

        function DayAsString(dayIndex) {
            var weekdays = new Array(7);
            weekdays[0] = "Sunday";
            weekdays[1] = "Monday";
            weekdays[2] = "Tuesday";
            weekdays[3] = "Wednesday";
            weekdays[4] = "Thursday";
            weekdays[5] = "Friday";
            weekdays[6] = "Saturday";

            return weekdays[dayIndex];
        }


        var startDate = new Date();
        var aryDates = GetDates(startDate, 0);
        // console.log(aryDates)

        let hours = new Date().getHours()
        let mins = new Date().getMinutes()
        var regExp = /(\d{1,2})\:(\d{1,2})\:(\d{1,2})/;
        let startTime = `${hours}:${mins}`
        let str1 = startTime.split(':');

        // console.log(currentDay)

        let totalSeconds1 = parseInt(str1[0] * 3600 + str1[1] * 60 + str1[0]);
        // console.log(totalSeconds1)
            

        let current = aryDates.toString().toLowerCase();
        setCurrentDay(current)
        setCurrentTime(totalSeconds1)
    }, []);


    const perm2 = (n) => {
        return (n * n - n) / 2
    }

    const perm3 = (n) => {
        return factorial(n) / factorial(n - 3) / 6
    }

    const perm4 = (n) => {
        return factorial(n) / factorial(n - 4) / 24
    }

    const perm5 = (n) => {
        return factorial(n) / factorial(n - 5) / 120
    }

    const createSlip = () => {
        if (arr.length > 0) {
            var n = arr.length
        }
        var lines
        if (gameType == 'PERM 2' || gameType == 'NAP 2') {
            lines = perm2(n)
        } else if (gameType == 'PERM 3') {
            lines = perm3(n)
        } else if (gameType == 'PERM 4') {
            lines = perm4(n)
        } else if (gameType == 'PERM 5') {
            lines = perm5(n)
        } else if (gameType === '1 BANKER') {
            lines = 89
        } else if (gameType === 'AGAINST') {
            const first_against = arr.slice(0, arr.indexOf(0))
            const second_against = arr.slice(arr.indexOf(0) + 1, arr[arr.length - 1])
            lines = first_against.length * second_against.length
        }
        else {
            lines = 1
        }

        if (gameType === 'AGAINST') {
            let data = {
                id: new Date().getTime().toString(),
                gameId: gameShow.id,
                date: c,
                lines: lines,
                gameName: gameShow.name,
                endTime: gameShow.endTime,
                day: gameShow.day,
                expired: false,
                type: gameType,
                stakeList: arr.slice(0, arr.indexOf(0)),
                stakeList2: arr.slice(arr.indexOf(0) + 1, arr[arr.length - 1]),
                amount: 0 || subValue * lines,
                amounts: subValue
            }
            betSlip.unshift(data)
            calculateTotalStake();
            setShowGameModal(!showGameModal)
            setArr([]);
            setShowSlip(true);
        } else {
            let data = {
                id: new Date().getTime().toString(),
                gameId: gameShow.id,
                lines: lines,
                date: c,
                gameName: gameShow.name,
                endTime: gameShow.endTime,
                day: gameShow.day,
                expired: false,
                type: gameType,
                stakeList: arr,
                amount: 0 || subValue * lines,
                amounts: subValue
            }
            betSlip.unshift(data)
            calculateTotalStake();
            setShowGameModal(!showGameModal)
            setArr([]);
            setShowSlip(true);
        }
    }

    const calculateTotalStake = () => {
        setValue(0)
        setSubValue(0)
        const val = betSlip.reduce((total, money) => {
            total += parseInt(money.amount)
            return total;
        }, 0);
        setNumber(val)
        setCount(betSlip.length)
        localStorage.setItem('RegularLotto', JSON.stringify(betSlip))
    }
    // http://www.v2nmobile.com/api/httpsms.php?u=${email}&p=${pass}&m=${'abelkelly}&r=${09047597017}&s=${senderID}&t=1`

    const calculateTotalStake1 = (newItem) => {
        setValue(0)
        setSubValue(0)
        const val = newItem.reduce((total, money) => {
            total += parseInt(money.amount)
            return total;
        }, 0);
        setNumber(val)
        localStorage.setItem('RegularLotto', JSON.stringify(newItem))
    }


    const handleGameBet = (e) => {
        e.preventDefault()

        const val = betSlip.reduce((total, money) => {
            total += parseInt(money.amount)
            return total;
        }, 0);

        // console.log(betSlip)


        if (user.activated >= 0) {
            if (user.balance > val) {
                if (val < 1) {
                    setSuccess(`Kindly add an amount`)
                    return;
                } else {
                    var myHeaders = new Headers();
                    myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
                    myHeaders.append("Authorization", `Bearer ${get}`);
                    myHeaders.append("Content-Type", "application/json");
                    myHeaders.append("Cookie", "connect.sid=s%3Ae0Zu5esOyG3kUqFKLpyqNN4lka0d4o3F.WngMLxSIx3GfvGfOlQDWPwr%2BeLG2ABZ6sABSEo4Hwkc");
          
                    betSlip.filter((bet, i) => {
                        const { type, gameId, gameName, expired, stakeList, stakeList2, amount, amounts } = bet;

                        if (expired) {
                            setSuccess(`You cannot place bet on ticket ${i + 1}, it is expired`)
                            return;
                        }else if (amount < 1) {
                            setSuccess(`cannot place bet for ticket number ${i + 1} please add an amount`)
                            return;
                        } else {

                            var raw = JSON.stringify({
                                "totalStake": `${number}`,
                                "stakes": [
                                    {
                                        "coupon": `${id + 'regularlotto'}`,
                                        "amount": `${amount}`,
                                        "amounts": `${amounts}`,
                                        "type": `${type}`,
                                        "gameName": `${gameName}`,
                                        "stakeList": `${stakeList}`,
                                        "stakeList2": `${stakeList2}`,
                                        "gameId": `${gameId}`,
                                        "game": "Regular Lotto"
                                    },
                                ]
                            });

                            var requestOptions = {
                                method: 'POST',
                                headers: myHeaders,
                                body: raw,
                                redirect: 'follow'
                            };

                            fetch(`${url}/api/v1/placeStake`, requestOptions)
                                .then(response => response.json())
                                .then(result => {
                                    console.log(result)
                                    if (result.result) {
                                        setSubmited(true)
                                        let show = result.result.map((res) => res)
                                        if (show[0].type === 'AGAINST') {
                                            const { type, amount, odd, staked, line, coupon, date, gameName, amounts, possibleWinning, gameId, stakes1, stakes2 } = show[0];
                                            let response1 = stakes1.toString()
                                            let response2 = stakes2.toString()
                                            var myHeaders = new Headers();
                                            myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
                                            myHeaders.append("Authorization", `Bearer ${get}`);
                                            myHeaders.append("Content-Type", "application/json");

                                            var raw = JSON.stringify({
                                                "line": `${line}`,
                                                "game": 'Regular Lotto',
                                                "amount": `${amount}`,
                                                "amounts": `${amounts}`,
                                                "type": `${type}`,
                                                "odd": `${odd}`,
                                                "possibleWinning": `${possibleWinning}`,
                                                "staked": `${amount}`,
                                                "stakes1": `${response1}`,
                                                "gameName": `${coupon}`,
                                                "stakes2": `${response2}`,
                                                "date": `${date}`,
                                                "gameId": `${gameId}`,
                                                "coupon": `${gameName}`
                                            });

                                            // var raw = JSON.stringify({
                                            //     "game": 'Regular Lotto',
                                            //     "amount": `${amount}`,
                                            //     "amounts": `${amounts}`,
                                            //     "type": `${type}`,
                                            //     "line": `${lines}`,
                                            //     "odd": `${odd}`,
                                            //     "possibleWinning": `${possibleWinning}`,
                                            //     "staked": `${amount}`,
                                            //     "stakes1": `${response1}`,
                                            //     "gameName": `${gameName}`,
                                            //     "stakes2": `${response2}`,
                                            //     "date": `${date}`,
                                            //     "coupon": `${coupon}`,
                                            //     "gameId": `${gameId}`
                                            // });

                                            var requestOptions = {
                                                method: 'POST',
                                                headers: myHeaders,
                                                body: raw,
                                                redirect: 'follow'
                                            };

                                            fetch(`${url}/api/v2/auth/betHistory`, requestOptions)
                                                .then(response => response.json())
                                                .then(result => {
                                                    console.log(result)
                                                    if (result.success) {
                                                        setId(Math.floor(Math.random() * 10000000000) + 1)
                                                        const { message } = result.success;
                                                        setMode(message)
                                                        setOpen(!open)
                                                        var myHeaders = new Headers();
                                                        myHeaders.append("signatures", "5a1131f2eb747be50714281ec3e68b759476c6dc9e1faf5fc5d91c552cf8c230");
                                                        myHeaders.append("Authorization", `Bearer ${get}`);

                                                        var requestOptions = {
                                                            method: 'GET',
                                                            headers: myHeaders,
                                                            redirect: 'follow'
                                                        };

                                                        fetch(`${url}/api/v2/auth/profile`, requestOptions)
                                                            .then(response => response.json())
                                                            .then(result => {
                                                                if (result.success) {
                                                                    getUser(result.success.data)
                                                                    const { data } = result.success;
                                                                    showBoard(data)
                                                                    //   localStorage.setItem('user', JSON.stringify(data))
                                                                } else {
                                                                    return;
                                                                }
                                                            },
                                                                (error) => {
                                                                    console.log(error)
                                                                });

                                                    } else {
                                                        console.log(result)
                                                    }
                                                })
                                                .catch(error => console.log('error', error));

                    
                                        } else if (show[0].type === '1 BANKER') {
                                            const { type, amount, coupon, odd, staked, line, gameName, amounts, date, possibleWinning, gameId, stakes } = show[0];
                                            let response = stakes.toString()
                                            var myHeaders = new Headers();
                                            myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
                                            myHeaders.append("Authorization", `Bearer ${get}`);
                                            myHeaders.append("Content-Type", "application/json");

                                            var raw = JSON.stringify({
                                                "game": 'Regular Lotto',
                                                "amount": `${amount}`,
                                                "amounts": `${amounts}`,
                                                "type": `${type}`,
                                                "line": `${line}`,
                                                "odd": `${odd}`,
                                                "amounts": `${amount}`,
                                                "possibleWinning": `${possibleWinning}`,
                                                "staked": `${amount}`,
                                                "stakes": `${response}`,
                                                "gameName": `${gameName}`,
                                                "date": `${date}`,
                                                "coupon": `${coupon}`,
                                                "gameId": `${gameId}`
                                            });

                                            var requestOptions = {
                                                method: 'POST',
                                                headers: myHeaders,
                                                body: raw,
                                                redirect: 'follow'
                                            };

                                            fetch(`${url}/api/v2/auth/betHistory`, requestOptions)
                                                .then(response => response.json())
                                                .then(result => {
                                                    console.log(result)
                                                    if (result.success) {
                                                        setId(Math.floor(Math.random() * 10000000000) + 1)
                                                        const { message } = result.success;
                                                        setMode(message)
                                                        setOpen(!open)
                                                        var myHeaders = new Headers();
                                                        myHeaders.append("signatures", "5a1131f2eb747be50714281ec3e68b759476c6dc9e1faf5fc5d91c552cf8c230");
                                                        myHeaders.append("Authorization", `Bearer ${get}`);

                                                        var requestOptions = {
                                                            method: 'GET',
                                                            headers: myHeaders,
                                                            redirect: 'follow'
                                                        };

                                                        fetch(`${url}/api/v2/auth/profile`, requestOptions)
                                                            .then(response => response.json())
                                                            .then(result => {
                                                                if (result.success) {
                                                                    getUser(result.success.data)
                                                                    const { data } = result.success;
                                                                    showBoard(data)
                                                                    //   localStorage.setItem('user', JSON.stringify(data))
                                                                } else {
                                                                    return;
                                                                }
                                                            },
                                                                (error) => {
                                                                    console.log(error)
                                                                });

                                                    } else {
                                                        setSuccess('Check your internet, refresh and try again. Thanks.')
                                                        return;
                                                    }
                                                })
                                                .catch(error => console.log('error', error));
                                        } else {
                                            const { type, amount, coupon, odd, line, staked, date, gameName, amounts, possibleWinning, stakes, gameId } = show[0];
                                            let response = stakes.toString()
                                            var myHeaders = new Headers();
                                            myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
                                            myHeaders.append("Authorization", `Bearer ${get}`);
                                            myHeaders.append("Content-Type", "application/json");

                                            var raw = JSON.stringify({
                                                "game": 'Regular Lotto',
                                                "amount": `${amount}`,
                                                "type": `${type}`,
                                                "gameName": `${gameName}`,
                                                "odd": `${odd}`,
                                                "line": `${line}`,
                                                "amounts": `${amounts}`,
                                                "possibleWinning": `${possibleWinning}`,
                                                "staked": `${amount}`,
                                                "stakes": `${response}`,
                                                "date": `${date}`,
                                                "coupon": `${coupon}`,
                                                "gameId": `${gameId}`
                                            });


                                            var requestOptions = {
                                                method: 'POST',
                                                headers: myHeaders,
                                                body: raw,
                                                redirect: 'follow'
                                            };

                                            fetch(`${url}/api/v2/auth/betHistory`, requestOptions)
                                                .then(response => response.json())
                                                .then(result => {
                                                    console.log(result)
                                                    if (result.success) {
                                                        setId(Math.floor(Math.random() * 10000000000) + 1)
                                                        const { message } = result.success;
                                                        setMode(message)
                                                        setOpen(!open)
                                                        var myHeaders = new Headers();
                                                        myHeaders.append("signatures", "5a1131f2eb747be50714281ec3e68b759476c6dc9e1faf5fc5d91c552cf8c230");
                                                        myHeaders.append("Authorization", `Bearer ${get}`);

                                                        var requestOptions = {
                                                            method: 'GET',
                                                            headers: myHeaders,
                                                            redirect: 'follow'
                                                        };

                                                        fetch(`${url}/api/v2/auth/profile`, requestOptions)
                                                            .then(response => response.json())
                                                            .then(result => {
                                                                if (result.success) {
                                                                    getUser(result.success.data)
                                                                    const { data } = result.success;
                                                                    showBoard(data)
                                                                    //   localStorage.setItem('user', JSON.stringify(data))
                                                                } else {
                                                                    return;
                                                                }
                                                            },
                                                                (error) => {
                                                                    console.log(error)
                                                                });

                                                    } else {
                                                        setSuccess('Check your internet, refresh and try again. Thanks.')
                                                        return;
                                                    }
                                                })
                                                .catch(error => console.log('error', error));
                                        }
                                    } else {
                                        return
                                    }
                
                                },
                                    (error) => {
                                        console.log(error)
                                    }
                                );
                        }
                    })
    
                }
            } else {
                setFundModal(!fundModal)
                setAlerts('Please kindly note that you currently do not have sufficient balance to place this bet')
                return;
            }
        } else {
            if (val < 1) {
                setSuccess(`Kindly add an amount`)
                return;
            } else {
                var myHeaders = new Headers();
                myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
                myHeaders.append("Authorization", `Bearer ${get}`);
                myHeaders.append("Content-Type", "application/json");
                myHeaders.append("Cookie", "connect.sid=s%3Ae0Zu5esOyG3kUqFKLpyqNN4lka0d4o3F.WngMLxSIx3GfvGfOlQDWPwr%2BeLG2ABZ6sABSEo4Hwkc");
          
                betSlip.filter((bet, i) => {
                    const { id, type, gameId, gameName, stakeList, stakeList2, amount, amounts } = bet;

                    if (amount < 1) {
                        setSuccess(`cannot place bet for ticket number ${i + 1} please add an amount`)
                        return;
                    } else {

                        

                        var raw = JSON.stringify({
                            "totalStake": `${number}`,
                            "stakes": [
                                {
                                    "coupon": `${id + 'regularlotto'}`,
                                    "amount": `${amount}`,
                                    "amounts": `${amounts}`,
                                    "type": `${type}`,
                                    "gameName": `${gameName}`,
                                    "stakeList": `${stakeList}`,
                                    "stakeList2": `${stakeList2}`,
                                    "game": 'Regular Lotto',
                                    "gameId": `${gameId}`
                                },
                            ]
                        });

                        var requestOptions = {
                            method: 'POST',
                            headers: myHeaders,
                            body: raw,
                            redirect: 'follow'
                        };

                        fetch(`${url}/api/v1/placeStake`, requestOptions)
                            .then(response => response.json())
                            .then(result => {
                                if (result.result) {
                                    console.log(amounts)
                                    setSubmited(true)
                                    let show = result.result.map((res) => res)
                                    if (show[0].type === 'AGAINST') {
                                        const { type, amount, coupon, odd, staked, line, date, possibleWinning, gameName, stakes1, stakes2, gameId } = show[0];
                                        let response1 = stakes1.toString()
                                        let response2 = stakes2.toString()
                                        var myHeaders = new Headers();
                                        myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
                                        myHeaders.append("Authorization", `Bearer ${get}`);
                                        myHeaders.append("Content-Type", "application/json");

                                        var raw = JSON.stringify({
                                            "game": 'Regular Lotto',
                                            "amount": `${amount}`,
                                            "amounts": `${amounts}`,
                                            "type": `${type}`,
                                            "odd": `${odd}`,
                                            "line": `${line}`,
                                            "possibleWinning": `${possibleWinning}`,
                                            "staked": `${amount}`,
                                            "stakes1": `${response1}`,
                                            "gameName": `${gameName}`,
                                            "stakes2": `${response2}`,
                                            "date": `${date}`,
                                            "gameId": `${gameId}`,
                                            "coupon": `${coupon}`
                                        });

                                        var requestOptions = {
                                            method: 'POST',
                                            headers: myHeaders,
                                            body: raw,
                                            redirect: 'follow'
                                        };

                                        fetch(`${url}/api/v2/auth/betHistory`, requestOptions)
                                            .then(response => response.json())
                                            .then(result => {
                                                console.log(result)
                                                if (result.success) {
                                                    setId(Math.floor(Math.random() * 10000000000) + 1)
                                                    const { message } = result.success;
                                                    setMode(message)
                                                    setOpen(!open)
                                                    var myHeaders = new Headers();
                                                    myHeaders.append("signatures", "5a1131f2eb747be50714281ec3e68b759476c6dc9e1faf5fc5d91c552cf8c230");
                                                    myHeaders.append("Authorization", `Bearer ${get}`);

                                                    var requestOptions = {
                                                        method: 'GET',
                                                        headers: myHeaders,
                                                        redirect: 'follow'
                                                    };

                                                    fetch(`${url}/api/v2/auth/profile`, requestOptions)
                                                        .then(response => response.json())
                                                        .then(result => {
                                                            if (result.success) {
                                                                getUser(result.success.data)
                                                                const { data } = result.success;
                                                                showBoard(data)
                                                                //   localStorage.setItem('user', JSON.stringify(data))
                                                            } else {
                                                                return;
                                                            }
                                                        },
                                                            (error) => {
                                                                console.log(error)
                                                            });

                                                } else {
                                                    console.log(result)
                                                }
                                            })
                                            .catch(error => console.log('error', error));

                    
                                    } else if (show[0].type === '1 BANKER') {
                                        const { type, amount, coupon, odd, line, staked, gameName, amounts, date, possibleWinning, stakes } = show[0];
                                        let response = stakes.toString()
                                        var myHeaders = new Headers();
                                        myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
                                        myHeaders.append("Authorization", `Bearer ${get}`);
                                        myHeaders.append("Content-Type", "application/json");

                                        var raw = JSON.stringify({
                                            "game": 'Regular Lotto',
                                            "amount": `${amount}`,
                                            "amounts": `${amounts}`,
                                            "type": `${type}`,
                                            "line": `${line}`,
                                            "gameName": `${gameName}`,
                                            "odd": `${odd}`,
                                            "possibleWinning": `${possibleWinning}`,
                                            "staked": `${amount}`,
                                            "stakes": `${response}`,
                                            "date": `${date}`,
                                            "coupon": `${coupon}`
                                        });

                                        var requestOptions = {
                                            method: 'POST',
                                            headers: myHeaders,
                                            body: raw,
                                            redirect: 'follow'
                                        };

                                        fetch(`${url}/api/v2/auth/betHistory`, requestOptions)
                                            .then(response => response.json())
                                            .then(result => {
                                                console.log(result)
                                                if (result.success) {
                                                    setId(Math.floor(Math.random() * 10000000000) + 1)
                                                    const { message } = result.success;
                                                    setMode(message)
                                                    setOpen(!open)
                                                    var myHeaders = new Headers();
                                                    myHeaders.append("signatures", "5a1131f2eb747be50714281ec3e68b759476c6dc9e1faf5fc5d91c552cf8c230");
                                                    myHeaders.append("Authorization", `Bearer ${get}`);

                                                    var requestOptions = {
                                                        method: 'GET',
                                                        headers: myHeaders,
                                                        redirect: 'follow'
                                                    };

                                                    fetch(`${url}/api/v2/auth/profile`, requestOptions)
                                                        .then(response => response.json())
                                                        .then(result => {
                                                            if (result.success) {
                                                                getUser(result.success.data)
                                                                const { data } = result.success;
                                                                showBoard(data)
                                                                //   localStorage.setItem('user', JSON.stringify(data))
                                                            } else {
                                                                return;
                                                            }
                                                        },
                                                            (error) => {
                                                                console.log(error)
                                                            });

                                                } else {
                                                    setSuccess('Check your internet, refresh and try again. Thanks.')
                                                    return;
                                                }
                                            })
                                            .catch(error => console.log('error', error));
                                    } else {
                                        const { type, amount, odd, staked, date, line, gameName, amounts, possibleWinning, stakes, coupon } = show[0];
                                        let response = stakes.toString()
                                        var myHeaders = new Headers();
                                        myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
                                        myHeaders.append("Authorization", `Bearer ${get}`);
                                        myHeaders.append("Content-Type", "application/json");

                                        var raw = JSON.stringify({
                                            "game": 'Regular Lotto',
                                            "amount": `${amount}`,
                                            "amounts": `${amounts}`,
                                            "type": `${type}`,
                                            "line": `${line}`,
                                            "odd": `${odd}`,
                                            "possibleWinning": `${possibleWinning}`,
                                            "staked": `${amount}`,
                                            "stakes": `${response}`,
                                            "gameName": `${gameName}`,
                                            "date": `${date}`,
                                            "coupon": `${coupon}`
                                        });

                                        var requestOptions = {
                                            method: 'POST',
                                            headers: myHeaders,
                                            body: raw,
                                            redirect: 'follow'
                                        };

                                        fetch(`${url}/api/v2/auth/betHistory`, requestOptions)
                                            .then(response => response.json())
                                            .then(result => {
                                                if (result.success) {
                                                    setId(Math.floor(Math.random() * 10000000000) + 1)
                                                    const { message } = result.success;
                                                    setMode(message)
                                                    setOpen(!open)
                                                    var myHeaders = new Headers();
                                                    myHeaders.append("signatures", "5a1131f2eb747be50714281ec3e68b759476c6dc9e1faf5fc5d91c552cf8c230");
                                                    myHeaders.append("Authorization", `Bearer ${get}`);

                                                    var requestOptions = {
                                                        method: 'GET',
                                                        headers: myHeaders,
                                                        redirect: 'follow'
                                                    };

                                                    fetch(`${url}/api/v2/auth/profile`, requestOptions)
                                                        .then(response => response.json())
                                                        .then(result => {
                                                            if (result.success) {
                                                                getUser(result.success.data)
                                                                const { data } = result.success;
                                                                showBoard(data)
                                                                //   localStorage.setItem('user', JSON.stringify(data))
                                                            } else {
                                                                return;
                                                            }
                                                        },
                                                            (error) => {
                                                                console.log(error)
                                                            });

                                                } else {
                                                    setSuccess('Check your internet, refresh and try again. Thanks.')
                                                    return;
                                                }
                                            })
                                            .catch(error => console.log('error', error));
                                    }
                                } else {
                                    return
                                }
                
                            },
                                (error) => {
                                    console.log(error)
                                }
                            );
                    }
                })
    
            }
        }
    }

    

    let v = [];

    if (arr.includes(0)) {
        v.push(arr.slice(arr.indexOf(0) + 1, arr[arr.length - 1]))
    }

    // useEffect(() => {
    //     let betSlip = JSON.parse(localStorage.getItem('RegularLotto'))
    //     // console.log(betSlip)

    //     if (betSlip) {
    //         const val = betSlip.reduce((total, money) => {
    //             total += parseInt(money.amount)
    //             return total;
    //         }, 0);
            
    //         setNumber(val)
    //     }


    // }, []);
    
    
    // console.log(arr.slice(0, arr.indexOf(0)))


    const handleBets = (e) => {
        e.preventDefault()
        setActiveNums(false)
        setGetNums(false)
        // console.log(array)
        if (arr.length < 1) {
            setSuccess('Please Select numbers to play')
            return;
        } else {
            if (gameType === 'NAP 1') {
                if (arr.length < 1) {
                    return;
                } else if (arr.length > 1 && arr.length < 3) {
                    gameType = 'NAP 2'
                    setGameType(gameType)
                    setSuccess('Your game type has been automatically changed to NAP 2 because the numbers picked are greater than 1; kindly choose a single number only for NAP 1 and proceed or continue with the NAP 2 game type')
                    setSlip(true);
                    setHow(false)
                    createSlip()
                    // betCount < 1 && window.innerWidth < 766 && setSuccess('Please Click on betSlip to see games')
                    // setBetCount(betCount += 1)
                    setShowGames(true)
                } else if (arr.length > 2) {
                    gameType = 'PERM 2'
                    setSuccess('Your game type has been automatically changed to PERM 2 because the numbers picked are greater than 2; kindly choose 2 numbers only for NAP 2 and proceed or continue with the PERM 2 game type')
                    setSlip(true);
                    setHow(false)
                    createSlip()
                    // betCount < 1 && window.innerWidth < 766 && setSuccess('Please Click on betSlip to see games')
                    // setBetCount(betCount += 1)
                    setShowGames(true)
                } else if (arr.length > 15) {
                    setSuccess('Please Select atmost 15 numbers')
                    return;
                }
                else {
                    setSlip(true);
                    setHow(false)
                    createSlip()
                    // betCount < 1 && window.innerWidth < 766 && setSuccess('Please Click on betSlip to see games')
                    // setBetCount(betCount += 1)
                    setShowGames(true)
                }
            }
            else if (gameType === 'NAP 2') {
                if (arr.length < 2) {
                    setSuccess('Please select atleast 2 numbers')
                    return;
                } else if (arr.length > 2) {
                    gameType = 'PERM 2'
                    setGameType(gameType)
                    setSuccess('Your game type has been automatically changed to PERM 2 because the numbers picked are greater than 2; kindly choose 2 numbers only for NAP 2 and proceed or continue with the PERM 2 game type')
                    setSlip(true);
                    setHow(false)
                    createSlip()
                    // betCount < 1 && window.innerWidth < 766 && setSuccess('Please Click on betSlip to see games')
                    // setBetCount(betCount += 1)
                    setShowGames(true)
                } else if (arr.length > 15) {
                    setSuccess('Please Select atmost 15 numbers')
                    return;
                } else {
                    setSlip(true);
                    setHow(false)
                    createSlip()
                    // betCount < 1 && window.innerWidth < 766 && setSuccess('Please Click on betSlip to see games')
                    // setBetCount(betCount += 1)
                    setShowGames(true)
                }
            } else if (gameType === 'NAP 3') {
                if (arr.length < 3) {
                    setSuccess('Please select atleast 3 numbers')
                    return;
                } else if (arr.length > 3) {
                    gameType = 'PERM 2'
                    setGameType(gameType)
                    setSuccess('Your game type has been automatically changed to PERM 2 because the numbers picked are greater than 3; kindly choose 3 numbers only for NAP 2 and proceed or continue with the PERM 2 game type')
                    setSlip(true);
                    setHow(false)
                    createSlip()
                    // betCount < 1 && window.innerWidth < 766 && setSuccess('Please Click on betSlip to see games')
                    // setBetCount(betCount += 1)
                    setShowGames(true)
                } else if (arr.length > 15) {
                    setSuccess('Please Select atmost 15 numbers')
                    return;
                } else {
                    setSlip(true);
                    setHow(false)
                    createSlip()
                    // betCount < 1 && window.innerWidth < 766 && setSuccess('Please Click on betSlip to see games')
                    // setBetCount(betCount += 1)
                    setShowGames(true)
                }
            } else if (gameType === 'NAP 4') {
                if (arr.length < 4) {
                    setSuccess('Please select atleast 4 numbers')
                    return;
                } else if (arr.length > 4) {
                    gameType = 'PERM 2'
                    setGameType(gameType)
                    setSuccess('Your game type has been automatically changed to PERM 2 because the numbers picked are greater than 4; kindly choose 4 numbers only for NAP 2 and proceed or continue with the PERM 2 game type')
                    setSlip(true);
                    setHow(false)
                    createSlip()
                    // betCount < 1 && window.innerWidth < 766 && setSuccess('Please Click on betSlip to see games')
                    // setBetCount(betCount += 1)
                    setShowGames(true)
                } else if (arr.length > 15) {
                    setSuccess('Please Select atmost 15 numbers')
                    return;
                } else {
                    setSlip(true);
                    setHow(false)
                    createSlip()
                    // betCount < 1 && window.innerWidth < 766 && setSuccess('Please Click on betSlip to see games')
                    // setBetCount(betCount += 1)
                    setShowGames(true)
                }
            } else if (gameType === 'NAP 5') {
                if (arr.length < 5) {
                    setSuccess('Please select atleast 5 numbers')
                    return;
                } else if (arr.length > 5) {
                    gameType = 'PERM 2'
                    setGameType(gameType)
                    setSuccess('Your game type has been automatically changed to PERM 2 because the numbers picked are greater than 5; kindly choose 5 numbers only for NAP 2 and proceed or continue with the PERM 2 game type')
                    setSlip(true);
                    setHow(false)
                    createSlip()
                    // betCount < 1 && window.innerWidth < 766 && setSuccess('Please Click on betSlip to see games')
                    // setBetCount(betCount += 1)
                    setShowGames(true)
                } else if (arr.length > 15) {
                    setSuccess('Please Select atmost 15 numbers')
                    return;
                } else {
                    setSlip(true);
                    setHow(false)
                    createSlip()
                    // betCount < 1 && window.innerWidth < 766 && setSuccess('Please Click on betSlip to see games')
                    // setBetCount(betCount += 1)
                    setShowGames(true)
                }
            } else if (gameType === 'PERM 2') {
                if (arr.length < 3) {
                    setSuccess('Please Select atleast 3 numbers')
                    return
                } else if (arr.length > 15) {
                    setSuccess('Please Select atmost 15 numbers')
                    return;
                } else {
                    setSlip(true);
                    setHow(false)
                    createSlip()
                    // betCount < 1 && window.innerWidth < 766 && setSuccess('Please Click on betSlip to see games')
                    // setBetCount(betCount += 1)
                    setShowGames(true)
                }
            } else if (gameType === 'PERM 3') {
                if (arr.length < 4) {
                    setSuccess('Please Select atleast 4 numbers')
                    return;
                } else if (arr.length > 15) {
                    setSuccess('Please Select atmost 15 numbers')
                    return;
                } else {
                    setSlip(true);
                    setHow(false)
                    createSlip()
                    // betCount < 1 && window.innerWidth < 766 && setSuccess('Please Click on betSlip to see games')
                    // setBetCount(betCount += 1)
                    setShowGames(true)
                }
            } else if (gameType === 'PERM 4') {
                if (arr.length < 5) {
                    setSuccess('Please Select atleast 5 numbers')
                    return;
                } else if (arr.length > 15) {
                    setSuccess('Please Select atmost 15 numbers')
                    return;
                } else {
                    setSlip(true);
                    setHow(false)
                    createSlip()
                    // betCount < 1 && window.innerWidth < 766 && setSuccess('Please Click on betSlip to see games')
                    // setBetCount(betCount += 1)
                    setShowGames(true)
                }
            } else if (gameType === 'PERM 5') {
                if (arr.length < 6) {
                    setSuccess('Please Select atleast 6 numbers')
                    return;
                } else if (arr.length > 15) {
                    setSuccess('Please Select atmost 15 numbers')
                    return;
                } else {
                    setSlip(true);
                    setHow(false)
                    createSlip()
                    // betCount < 1 && window.innerWidth < 766 && setSuccess('Please Click on betSlip to see games')
                    // setBetCount(betCount += 1)
                    setShowGames(true)
                }
            } else if (gameType === '1 BANKER') {
                if (!arr.length) {
                    setSuccess('Please Select a single number')
                    return
                }
                let y = arr[arr.length - 1]
                arr = y
                setShowGames(true)
                setSlip(true);
                setHow(false)
                setFirstAgainst([])
                createSlip()
                // betCount < 1 && window.innerWidth < 766 && setSuccess('Please Click on betSlip to see games')
                // setBetCount(betCount += 1)
            } else if (gameType === 'AGAINST') {
                let max = 20;
                let min = 1
                const first_against = arr.slice(0, arr.indexOf(0))
                const second_against = arr.slice(arr.indexOf(0) + 1, arr[arr.length - 1])
                setSec(second_against)
                if ((first_against.length === 0 || second_against.length === 0)) {
                    setSuccess('please you need to choose two set of numbers for AGAINST games')
                    return;
                } else if (first_against.length === min && second_against.length === min) {
                    setSuccess('please choose atleast one 1 number for either side of the games and more than one number for the other side')
                    return;
                } else if ((first_against.length + second_against.length) > max) {
                    setSuccess('please total number of selected numbers should not exceed 20')
                    return;
                } else {
                    setSlip(true);
                    setHow(false)
                    setFirstAgainst([])
                    createSlip()
                    // betCount < 1 && window.innerWidth < 766 && setSuccess('Please Click on betSlip to see games')
                    // setBetCount(betCount += 1)
                    setShowGames(true)
                }
            } else {
                setSlip(true);
                setHow(false)
                createSlip()
                // betCount < 1 && window.innerWidth < 766 && setSuccess('Please Click on betSlip to see games')
                // setBetCount(betCount += 1)
                setShowGames(true)
            }
        }
    }



    useEffect(() => {
        let time = setTimeout(() => {
            setShowAlert(!showAlert)
            setSuccess('')
        }, 3000)

        return () => clearTimeout(time)
    }, [success]);

    const removeItem = (id) => {
        let newItem = betSlip.filter((item) => item.id !== id)
        if (newItem.length < 1) {
            setBetSlip([])
            setHow(true)
            setSlip(false)
            setCount(0)
            setShowGameModal(false)
            setNumber(0)
            calculateTotalStake1([])
        } else {
            setBetSlip(newItem)
            setCount(count -= 1)
            // console.log(betSlip)
            calculateTotalStake1(newItem)
        }
    }

    const removeItems = (id) => {
        let newItems = list.filter((item) => item.id !== id)
        if (newItems.length < 1) {
            setList([])
            setShowCoupon(false)
        } else {
            setList(newItems)
        }
    }
    

    useEffect(() => {
        const timeInterval = setInterval(() => {
            setTimer(moment().format('LTS'))
        }, 1000)

        return () => clearInterval(timeInterval)
    })

    const handleCategory = (e, i) => {
        e.preventDefault()

        setShow((state) => {
            return {
                ...state,
                [i]: !state[i],
            };
        });

        if (show) {
            setDaysShow(e.target.textContent)
            setSecArr([i])
        } else {
            setDaysShow('')
            setSecArr([])
        }
    }

    useEffect(() => {
        var myHeaders = new Headers();
        myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");

        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };

        fetch(`${url}/api/v1/gamemaxamount`, requestOptions)
            .then(response => response.json())
            .then(result => {
                result.showMax.map((value) => {
                    if (value.type === 'NAP 1') {
                        setNap1Max(value.value)
                    } else if (value.type === 'NAP 2') {
                        setNap2Max(value.value)
                    } else if (value.type === 'NAP 3') {
                        setNap3Max(value.value)
                    } else if (value.type === 'NAP 4') {
                        setNap4Max(value.value)
                    } else if (value.type === 'NAP 5') {
                        setNap5Max(value.value)
                    } else if (value.type === 'General') {
                        setGeneral(value.value)
                    } else {
                        return;
                    }
                })
            })
            .catch(error => console.log('error', error));
        
    }, [])

    const handleRandom = e => {
        e.preventDefault()
        if (gameType === 'NAP 1' || gameType === '1 BANKER') {
            let ar = []
            if (showAll.length > 0) {
                let num = generateRandom(90, showAll);
                function generateRandom(max, except) {
                    const exceptSet = new Set(except);
                    let result;

                    do {
                        result = Math.floor(Math.random() * max) + 1;
                    } while (exceptSet.has(result));

                    return result;
                }
                setArr([num])
                setGetNums(true);
            } else {
                const number = Math.floor(Math.random() * 90) + 1
                setArr([number])
                setGetNums(true);
            }
        } else if (gameType === 'NAP 2') {
            let ar = []
            if (showAll.length > 0) {
                let num = generateRandom(90, showAll);
                let num1 = generateRandom(90, showAll);
                let num2 = generateRandom(90, showAll);
                function generateRandom(max, except) {
                    const exceptSet = new Set(except);
                    let result;

                    do {
                        result = Math.floor(Math.random() * max) + 1;
                    } while (exceptSet.has(result));

                    return result;
                }
                ar = [num, num1]
                let a = new Set(ar)
                let b = [...a]
                if (b.length !== 2) {
                    b.push(num2)
                    setArr(b)
                    setGetNums(true);
                } else {
                    setArr(b)
                    setGetNums(true);
                }
            } else {
                const number = Math.floor(Math.random() * 90) + 1
                const number1 = Math.floor(Math.random() * 90) + 1
                const number2 = Math.floor(Math.random() * 90) + 1
                ar = [number, number1]
                let a = new Set(ar)
                let b = [...a]
                if (b.length !== 2) {
                    b.push(number2)
                    setArr(b)
                    setGetNums(true);
                } else {
                    setArr(b)
                    setGetNums(true);
                }
            }
        } else if (gameType === 'NAP 3') {
            let ar = []
            if (showAll.length > 0) {
                let num = generateRandom(90, showAll);
                let num1 = generateRandom(90, showAll);
                let num2 = generateRandom(90, showAll);
                let num3 = generateRandom(90, showAll);
                function generateRandom(max, except) {
                    const exceptSet = new Set(except);
                    let result;

                    do {
                        result = Math.floor(Math.random() * max) + 1;
                    } while (exceptSet.has(result));

                    return result;
                }
                ar = [num, num1, num2]
                let a = new Set(ar)
                let b = [...a]
                if (b.length !== 3) {
                    b.push(num3)
                    setArr(b)
                    setGetNums(true);
                } else {
                    setArr(b)
                    setGetNums(true);
                }
            } else {
                const number = Math.floor(Math.random() * 90) + 1
                const number1 = Math.floor(Math.random() * 90) + 1
                const number2 = Math.floor(Math.random() * 90) + 1
                const number3 = Math.floor(Math.random() * 90) + 1
                ar = [number, number1, number2]
                let a = new Set(ar)
                let b = [...a]
                if (b.length !== 3) {
                    b.push(number3)
                    setArr(b)
                    setGetNums(true);
                } else {
                    setArr(b)
                    setGetNums(true);
                }
            }
        } else if (gameType === 'NAP 4') {
            let ar = []
            if (showAll.length > 0) {
                let num = generateRandom(90, showAll);
                let num1 = generateRandom(90, showAll);
                let num2 = generateRandom(90, showAll);
                let num3 = generateRandom(90, showAll);
                let num4 = generateRandom(90, showAll);

                function generateRandom(max, except) {
                    const exceptSet = new Set(except);
                    let result;

                    do {
                        result = Math.floor(Math.random() * max) + 1;
                    } while (exceptSet.has(result));

                    return result;
                }
                ar = [num, num1, num2, num3]
                let numberSet = new Set(ar)
                let a = [...numberSet]
                if (a.length !== 4) {
                    a.push(num4)
                    setArr(a)
                    setGetNums(true);
                } else {
                    setArr(a)
                    setGetNums(true);
                }
            } else {
                const number = Math.floor(Math.random() * 90) + 1
                const number1 = Math.floor(Math.random() * 90) + 1
                const number2 = Math.floor(Math.random() * 90) + 1
                const number3 = Math.floor(Math.random() * 90) + 1
                const number4 = Math.floor(Math.random() * 90) + 1
                ar = [number, number1, number2, number3]
                let numberSet = new Set(ar)
                let a = [...numberSet]
                if (a.length !== 4) {
                    a.push(number4)
                    setArr(a)
                    setGetNums(true);
                } else {
                    setArr(a)
                    setGetNums(true);
                }
            }
        } else if (gameType === 'NAP 5') {
            let ar = []
            if (showAll.length > 0) {
                let num = generateRandom(90, showAll);
                let num1 = generateRandom(90, showAll);
                let num2 = generateRandom(90, showAll);
                let num3 = generateRandom(90, showAll);
                let num4 = generateRandom(90, showAll);
                let num5 = generateRandom(90, showAll);

                function generateRandom(max, except) {
                    const exceptSet = new Set(except);
                    let result;

                    do {
                        result = Math.floor(Math.random() * max) + 1;
                    } while (exceptSet.has(result));

                    return result;
                }
                ar = [num, num1, num2, num3, num4]
                let numberSet = new Set(ar)
                let a = [...numberSet]
                if (a.length !== 5) {
                    a.push(num5)
                    setArr(a)
                    setGetNums(true);
                } else {
                    setArr(a)
                    setGetNums(true);
                }
            } else {
                const number = Math.floor(Math.random() * 90) + 1
                const number1 = Math.floor(Math.random() * 90) + 1
                const number2 = Math.floor(Math.random() * 90) + 1
                const number3 = Math.floor(Math.random() * 90) + 1
                const number4 = Math.floor(Math.random() * 90) + 1
                const num5 = Math.floor(Math.random() * 90) + 1
                ar = [number, number1, number2, number3, number4]
                let numberSet = new Set(ar)
                let a = [...numberSet]
                if (a.length !== 5) {
                    a.push(num5)
                    setArr(a)
                    setGetNums(true);
                } else {
                    setArr(a)
                    setGetNums(true);
                }
            }
        } else if (gameType === 'PERM 2' || gameType === 'PERM 3' || gameType === 'PERM 4') {
            let ar = []
            if (showAll.length > 0) {
                let num = generateRandom(90, showAll);
                let num1 = generateRandom(90, showAll);
                let num2 = generateRandom(90, showAll);
                let num3 = generateRandom(90, showAll);
                let num4 = generateRandom(90, showAll);
                let num5 = generateRandom(90, showAll);

                function generateRandom(max, except) {
                    const exceptSet = new Set(except);
                    let result;

                    do {
                        result = Math.floor(Math.random() * max) + 1;
                    } while (exceptSet.has(result));

                    return result;
                }
                ar = [num, num1, num2, num3, num4]
                let numberSet = new Set(ar)
                let a = [...numberSet]
                if (a.length !== 5) {
                    a.push(num5)
                    setArr(a)
                } else {
                    setArr(a)
                }
            } else {
                const number = Math.floor(Math.random() * 90) + 1
                const number1 = Math.floor(Math.random() * 90) + 1
                const number2 = Math.floor(Math.random() * 90) + 1
                const number3 = Math.floor(Math.random() * 90) + 1
                const number4 = Math.floor(Math.random() * 90) + 1
                const num5 = Math.floor(Math.random() * 90) + 1
                ar = [number, number1, number2, number3, number4]
                let numberSet = new Set(ar)
                let a = [...numberSet]
                if (a.length !== 5) {
                    a.push(num5)
                    setArr(a)
                } else {
                    setArr(a)
                }
            }
        } else if (gameType === 'PERM 5') {
            let ar = []
            if (showAll.length > 0) {
                let num = generateRandom(90, showAll);
                let num1 = generateRandom(90, showAll);
                let num2 = generateRandom(90, showAll);
                let num3 = generateRandom(90, showAll);
                let num4 = generateRandom(90, showAll);
                let num5 = generateRandom(90, showAll);
                let num6 = generateRandom(90, showAll);

                function generateRandom(max, except) {
                    const exceptSet = new Set(except);
                    let result;

                    do {
                        result = Math.floor(Math.random() * max) + 1;
                    } while (exceptSet.has(result));

                    return result;
                }
                ar = [num, num1, num2, num3, num4, num5]
                let numberSet = new Set(ar)
                let a = [...numberSet]
                if (a.length !== 6) {
                    a.push(num6)
                    setArr(a)
                } else {
                    setArr(a)
                }
            } else {
                const number = Math.floor(Math.random() * 90) + 1
                const number1 = Math.floor(Math.random() * 90) + 1
                const number2 = Math.floor(Math.random() * 90) + 1
                const number3 = Math.floor(Math.random() * 90) + 1
                const number4 = Math.floor(Math.random() * 90) + 1
                const num5 = Math.floor(Math.random() * 90) + 1
                const num6 = Math.floor(Math.random() * 90) + 1
                ar = [number, number1, number2, number3, number4, num5]
                let numberSet = new Set(ar)
                let a = [...numberSet]
                if (a.length !== 6) {
                    a.push(num6)
                    setArr(a)
                } else {
                    setArr(a)
                }
            }
        } else {
            setSuccess('Please Pick games manually for this game type')
            return;
        }
    }

    const handleClass = (e, i) => {
        if (gameShow.suspend === 1) {
            setSuccess('Current game has been temporarily suspended suspended please choose another game type')
            return;
        } else if (showAll.includes(i)) {
            setSuccess('You cannot pick this number as you have exceeded the amount of picks for this number per day')
            return;
        } else {
            if (gameType === 'NAP 1' || gameType === '1 BANKER') {
                setActiveNums((state) => {
                    return {
                        ...state,
                        [i]: !state[i],
                    };
                });

                if (arr.includes(i)) {
                    const index = arr.indexOf(i)
                    if (index > -1) {
                        arr.splice(index, 1)
                    }
                } else {
                    setArr([i])
                    setGetNums((state) => {
                        return {
                            ...state,
                            [i]: !state[i],
                        };
                    });

                }
            } else if (gameType === 'NAP 2') {
                setActiveNums((state) => {
                    return {
                        ...state,
                        [i]: !state[i],
                    };
                });

                if (arr.includes(i)) {
                    const index = arr.indexOf(i)
                    if (index > -1) {
                        arr.splice(index, 1)
                    }
                } else {
                    if (arr.length < 2) {
                        arr.push(i)
                    } else {
                        setSuccess('Please unselect a number to select new one')
                        setGetNums((state) => {
                            return {
                                ...state,
                                [i]: !state[i],
                            };
                        });
                    }
                }
            } else if (gameType === 'NAP 3') {
                setActiveNums((state) => {
                    return {
                        ...state,
                        [i]: !state[i],
                    };
                });

                if (arr.includes(i)) {
                    const index = arr.indexOf(i)
                    if (index > -1) {
                        arr.splice(index, 1)
                    }
                } else {
                    if (arr.length < 3) {
                        arr.push(i)
                    } else {
                        setSuccess('Please unselect a number to select new one')
                        setGetNums((state) => {
                            return {
                                ...state,
                                [i]: !state[i],
                            };
                        });
                    }
                }
            } else if (gameType === 'NAP 4') {
                setActiveNums((state) => {
                    return {
                        ...state,
                        [i]: !state[i],
                    };
                });

                if (arr.includes(i)) {
                    const index = arr.indexOf(i)
                    if (index > -1) {
                        arr.splice(index, 1)
                    }
                } else {
                    if (arr.length < 4) {
                        arr.push(i)
                    } else {
                        setSuccess('Please unselect a number to select new one')
                        setGetNums((state) => {
                            return {
                                ...state,
                                [i]: !state[i],
                            };
                        });
                    }
                }
            } else if (gameType === 'NAP 5') {
                setActiveNums((state) => {
                    return {
                        ...state,
                        [i]: !state[i],
                    };
                });

                if (arr.includes(i)) {
                    const index = arr.indexOf(i)
                    if (index > -1) {
                        arr.splice(index, 1)
                    }
                } else {
                    if (arr.length < 5) {
                        arr.push(i)
                    } else {
                        setSuccess('Please unselect a number to select new one')
                        setGetNums((state) => {
                            return {
                                ...state,
                                [i]: !state[i],
                            };
                        });
                    }
                }
            } else if (gameType === 'PERM 2' || gameType === 'PERM 3' || gameType === 'PERM 4' || gameType === 'PERM 5') {
                setActiveNums((state) => {
                    return {
                        ...state,
                        [i]: !state[i],
                    };
                });

                if (arr.includes(i)) {
                    const index = arr.indexOf(i)
                    if (index > -1) {
                        arr.splice(index, 1)
                    }
                } else {
                    if (arr.length < 15) {
                        arr.push(i)
                    } else {
                        setSuccess('Please unselect a number to select new one')
                        setGetNums((state) => {
                            return {
                                ...state,
                                [i]: !state[i],
                            };
                        });
                    }
                }
            } else if (gameType === 'AGAINST') {
                setActiveNums((state) => {
                    return {
                        ...state,
                        [i]: !state[i],
                    };
                });

                if (arr.includes(i)) {
                    const index = arr.indexOf(i)
                    if (index > -1) {
                        arr.splice(index, 1)
                    }
                } else {
                    const first_against = arr.slice(0, arr.indexOf(0))
                    const second_against = arr.slice(arr.indexOf(0) + 1, arr[arr.length - 1])
                    if ((second_against.length + first_against.length) < 20) {
                        arr.push(i)
                    } else {
                        setSuccess('Please unselect a number to select new one')
                        setGetNums((state) => {
                            return {
                                ...state,
                                [i]: !state[i],
                            };
                        });
                    }
                }
            } else {
                setSuccess('Please Choose a valid game type')
                return;
            }
        }

    }

    const clickeds = (e, index) => {
        setA([index])
    }

    useEffect(() => {
        
        function GetDates(startDate, daysToAdd) {
            var aryDates = [];

            for (var i = 0; i <= daysToAdd; i++) {
                var currentDate = new Date();
                currentDate.setDate(startDate.getDate() + i);
                aryDates.push(DayAsString(currentDate.getDay()) + ", " + currentDate.getDate() + " " + MonthAsString(currentDate.getMonth()));
            }

            return aryDates;
        }

        function MonthAsString(monthIndex) {
            var month = [];
            month[0] = "Jan";
            month[1] = "Feb";
            month[2] = "Mar";
            month[3] = "Apr";
            month[4] = "May";
            month[5] = "Jun";
            month[6] = "Jul";
            month[7] = "Aug";
            month[8] = "Sept";
            month[9] = "Oct";
            month[10] = "Nov";
            month[11] = "Dec";

            return month[monthIndex];
        }

        function DayAsString(dayIndex) {
            var weekdays = new Array(7);
            weekdays[0] = "Sunday";
            weekdays[1] = "Monday";
            weekdays[2] = "Tuesday";
            weekdays[3] = "Wednesday";
            weekdays[4] = "Thursday";
            weekdays[5] = "Friday";
            weekdays[6] = "Saturday";

            return weekdays[dayIndex];
        }


        var startDate = new Date();
        var aryDates = GetDates(startDate, 0);
        setDaysShow(aryDates.toString())
        setSecArr([0])
        setShow(0)

        game.map((gam) => {
            if (daysShow.includes(gam.day.charAt(0).toUpperCase() + gam.day.slice(1))) {
                setClicked(true)
                setGameShow({ ...gameShow, name: gam.name, start: gam.startTime, end: gam.endTime, id: gam.uuid })
            }
        });

    }, [])

    useEffect(() => {
        if (gameShow.name) {
            setShowSvg(false)
        }
    }, [])


    useEffect(() => {
        var myHeaders = new Headers();
        myHeaders.append("signatures", "");
        myHeaders.append("Authorization", `Bearer ${get}`);
        myHeaders.append("Content-Type", "application/json");


        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };

        fetch(`${url}/api/v2/auth/gettotal`, requestOptions)
            .then(response => response.json())
            .then(result => {
                console.log(result)
                if (result.success) {
                    let a = []
                    let arr = result.success.total.bet_numbers
                    a = Array.from(arr.split(','), Number)
                    // setMaxArr([result.success.total.bet_numbers])
                    setMaxArr(a)
                }
            })
            .catch(error => console.log('error', error));

        const countDuplicate = (a) => {
            let count = {}
            for (let i = 0; i < maxArr.length; i++) {
                if (count[maxArr[i]]) {
                    count[maxArr[i]] += 1;
                } else {
                    count[maxArr[i]] = 1
                }
            }

            let x = []

            for (let prop in count) {
                if (count[prop] >= 2) {
                    console.log(prop + " counted" + " " + count[prop] + " times")
                    x.push(parseInt(prop))
                }
            }

            setShowAll(x)
        }

        countDuplicate(maxArr)
        
    }, []);


    // useEffect(() => {
    //     if (value > parseInt(general)) {
    //         setSuccess(`for games whose staking total amount is more than ${general} naira the odds would be made dynamic`)
    //         return;
    //     }
    // }, [general])

    const handleCouponBet = e => {
        e.preventDefault()
        const val = list.reduce((total, money) => {
            total += parseInt(money.amounts)
            return total;
        }, 0);
        if (user.balance > val) {
            if (val < 1) {
                setSuccess(`Kindly add an amount`)
                return;
            } else {
                var myHeaders = new Headers();
                myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
                myHeaders.append("Authorization", `Bearer ${get}`);
                myHeaders.append("Content-Type", "application/json");
                myHeaders.append("Cookie", "connect.sid=s%3Ae0Zu5esOyG3kUqFKLpyqNN4lka0d4o3F.WngMLxSIx3GfvGfOlQDWPwr%2BeLG2ABZ6sABSEo4Hwkc");

                list.filter((bet, i) => {
                    const { coupon, type, gameId, stakes, stakes1, stakes2, amount, line, gameName, amounts, status } = bet;


                    if (amount < 1) {
                        setSuccess(`cannot place bet for ticket number ${i + 1} please add an amount`)
                        return;
                    } else if (status !== 'PENDING') {
                        setSuccess(`Cannot place bet on expired coupons`)
                        return;
                    } else {
                        // console.log(listV)
                        // console.log(amount)
                        // console.log(amounts)
                        // console.log(coupon)
                        if (listV > 1) {
                            if (listV > user.wallet && listV > user.withdrawable) {
                                setFundModal(true)
                                return;
                            } else {
                                var raw = JSON.stringify({
                                    "totalStake": `${number}`,
                                    "stakes": [
                                        {
                                            "coupon": `${id + 'regularlotto'}`,
                                            "amount": `${listV * line}`,
                                            "amounts": `${listV}`,
                                            "type": `${type}`,
                                            "stakeList": `${stakes}`,
                                            "gameName": `${gameName}`,
                                            "stakeList1": `${stakes1}`,
                                            "stakeList2": `${stakes2}`,
                                            "gameId": `${gameId}`,
                                            "game": "Regular Lotto"
                                        },
                                    ]
                                });

                                var requestOptions = {
                                    method: 'POST',
                                    headers: myHeaders,
                                    body: raw,
                                    redirect: 'follow'
                                };

                                fetch(`${url}/api/v1/placeStake`, requestOptions)
                                    .then(response => response.json())
                                    .then(result => {
                                        console.log('place stake result')
                                        console.log(result)
                                        if (result.result) {
                                            setSubmited(true)
                                            let show = result.result.map((res) => res)
                                            if (show[0].type === 'AGAINST') {
                                                const { type, amount, odd, staked, line, gameName, coupon, date, amounts, possibleWinning, gameId, stakes1, stakes2 } = show[0];
                                                let response1 = stakes1.toString()
                                                let response2 = stakes2.toString()
                                                var myHeaders = new Headers();
                                                myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
                                                myHeaders.append("Authorization", `Bearer ${get}`);
                                                myHeaders.append("Content-Type", "application/json");

                                                var raw = JSON.stringify({
                                                    "game": 'Regular Lotto',
                                                    "amount": `${amount}`,
                                                    "amounts": `${amounts}`,
                                                    "type": `${type}`,
                                                    "odd": `${odd}`,
                                                    "line": `${line}`,
                                                    "gameName": `${gameName}`,
                                                    "possibleWinning": `${possibleWinning}`,
                                                    "staked": `${amount}`,
                                                    "stakes1": `${response1}`,
                                                    "stakes2": `${response2}`,
                                                    "date": `${date}`,
                                                    "coupon": `${coupon}`,
                                                    "gameId": `${gameId}`
                                                });

                                                var requestOptions = {
                                                    method: 'POST',
                                                    headers: myHeaders,
                                                    body: raw,
                                                    redirect: 'follow'
                                                };

                                                fetch(`${url}/api/v2/auth/betHistory`, requestOptions)
                                                    .then(response => response.json())
                                                    .then(result => {
                                                        if (result.success) {
                                                            setId(Math.floor(Math.random() * 10000000000) + 1)
                                                            const { message } = result.success;
                                                            setMode(message)
                                                            setOpen(!open)
                                                            var myHeaders = new Headers();
                                                            myHeaders.append("signatures", "5a1131f2eb747be50714281ec3e68b759476c6dc9e1faf5fc5d91c552cf8c230");
                                                            myHeaders.append("Authorization", `Bearer ${get}`);

                                                            var requestOptions = {
                                                                method: 'GET',
                                                                headers: myHeaders,
                                                                redirect: 'follow'
                                                            };

                                                            fetch(`${url}/api/v2/auth/profile`, requestOptions)
                                                                .then(response => response.json())
                                                                .then(result => {
                                                                    if (result.success) {
                                                                        getUser(result.success.data)
                                                                        const { data } = result.success;
                                                                        showBoard(data)
                                                                        //   localStorage.setItem('user', JSON.stringify(data))
                                                                    } else {
                                                                        return;
                                                                    }
                                                                },
                                                                    (error) => {
                                                                        console.log(error)
                                                                    });

                                                        } else {
                                                            console.log(result)
                                                        }
                                                    })
                                                    .catch(error => console.log('error', error));

                    
                                            } else if (show[0].type === '1 BANKER') {
                                                const { type, amount, coupon, odd, staked, gameName, line, amounts, date, possibleWinning, gameId, stakes } = show[0];
                                                let response = stakes.toString()
                                                var myHeaders = new Headers();
                                                myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
                                                myHeaders.append("Authorization", `Bearer ${get}`);
                                                myHeaders.append("Content-Type", "application/json");

                                                var raw = JSON.stringify({
                                                    "game": 'Regular Lotto',
                                                    "amount": `${amount}`,
                                                    "amounts": `${amounts}`,
                                                    "type": `${type}`,
                                                    "odd": `${odd}`,
                                                    "line": `${line}`,
                                                    "gameName": `${gameName}`,
                                                    "amounts": `${amount}`,
                                                    "possibleWinning": `${possibleWinning}`,
                                                    "staked": `${staked}`,
                                                    "stakes": `${response}`,
                                                    "date": `${date}`,
                                                    "coupon": `${coupon}`,
                                                    "gameId": `${gameId}`
                                                });

                                                var requestOptions = {
                                                    method: 'POST',
                                                    headers: myHeaders,
                                                    body: raw,
                                                    redirect: 'follow'
                                                };

                                                fetch(`${url}/api/v2/auth/betHistory`, requestOptions)
                                                    .then(response => response.json())
                                                    .then(result => {
                                                        console.log(result)
                                                        if (result.success) {
                                                            const { message } = result.success;
                                                            setMode(message)
                                                            setOpen(!open)
                                                            var myHeaders = new Headers();
                                                            myHeaders.append("signatures", "5a1131f2eb747be50714281ec3e68b759476c6dc9e1faf5fc5d91c552cf8c230");
                                                            myHeaders.append("Authorization", `Bearer ${get}`);

                                                            var requestOptions = {
                                                                method: 'GET',
                                                                headers: myHeaders,
                                                                redirect: 'follow'
                                                            };

                                                            fetch(`${url}/api/v2/auth/profile`, requestOptions)
                                                                .then(response => response.json())
                                                                .then(result => {
                                                                    if (result.success) {
                                                                        getUser(result.success.data)
                                                                        const { data } = result.success;
                                                                        showBoard(data)
                                                                        //   localStorage.setItem('user', JSON.stringify(data))
                                                                    } else {
                                                                        return;
                                                                    }
                                                                },
                                                                    (error) => {
                                                                        console.log(error)
                                                                    });

                                                        } else {
                                                            setSuccess('refresh and try again. Thanks.')
                                                            return;
                                                        }
                                                    })
                                                    .catch(error => console.log('error', error));
                                            } else {
                                                const { type, amount, coupon, odd, gameName, staked, line, date, amounts, possibleWinning, stakes, gameId } = show[0];
                                                let response = stakes.toString()
                                                var myHeaders = new Headers();
                                                myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
                                                myHeaders.append("Authorization", `Bearer ${get}`);
                                                myHeaders.append("Content-Type", "application/json");

                                                var raw = JSON.stringify({
                                                    "game": 'Regular Lotto',
                                                    "amount": `${amount}`,
                                                    "type": `${type}`,
                                                    "line": `${line}`,
                                                    "odd": `${odd}`,
                                                    "gameName": `${gameName}`,
                                                    "amounts": `${amounts}`,
                                                    "possibleWinning": `${possibleWinning}`,
                                                    "staked": `${staked}`,
                                                    "stakes": `${response}`,
                                                    "date": `${date}`,
                                                    "coupon": `${coupon}`,
                                                    "gameId": `${gameId}`
                                                });

                                                var requestOptions = {
                                                    method: 'POST',
                                                    headers: myHeaders,
                                                    body: raw,
                                                    redirect: 'follow'
                                                };

                                                fetch(`${url}/api/v2/auth/betHistory`, requestOptions)
                                                    .then(response => response.json())
                                                    .then(result => {
                                                        console.log(result)
                                                        if (result.success) {
                                                            const { message } = result.success;
                                                            setMode(message)
                                                            setOpen(!open)
                                                            var myHeaders = new Headers();
                                                            myHeaders.append("signatures", "5a1131f2eb747be50714281ec3e68b759476c6dc9e1faf5fc5d91c552cf8c230");
                                                            myHeaders.append("Authorization", `Bearer ${get}`);

                                                            var requestOptions = {
                                                                method: 'GET',
                                                                headers: myHeaders,
                                                                redirect: 'follow'
                                                            };

                                                            fetch(`${url}/api/v2/auth/profile`, requestOptions)
                                                                .then(response => response.json())
                                                                .then(result => {
                                                                    if (result.success) {
                                                                        getUser(result.success.data)
                                                                        const { data } = result.success;
                                                                        showBoard(data)
                                                                        //   localStorage.setItem('user', JSON.stringify(data))
                                                                    } else {
                                                                        return;
                                                                    }
                                                                },
                                                                    (error) => {
                                                                        console.log(error)
                                                                    });

                                                        } else {
                                                            setSuccess('refresh and try again. Thanks.')
                                                            return;
                                                        }
                                                    })
                                                    .catch(error => console.log('error', error));
                                            }
                                        } else {
                                            return;
                                        }
                
                                    },
                                        (error) => {
                                            console.log(error)
                                        }
                                    );
                            }
                        } else if (listV === 0 && amount > 1) {
                            if (amount > user.wallet && amount > user.withdrawable) {
                                setFundModal(true)
                                return;
                            } else {
                                var raw = JSON.stringify({
                                    "totalStake": `${number}`,
                                    "stakes": [
                                        {
                                            "coupon": `${id + 'regularlotto'}`,
                                            "amount": `${amount}`,
                                            "amounts": `${amounts}`,
                                            "type": `${type}`,
                                            "stakeList": `${stakes}`,
                                            "gameName": `${gameName}`,
                                            "stakeList1": `${stakes1}`,
                                            "stakeList2": `${stakes2}`,
                                            "gameId": `${gameId}`,
                                            "game": "Regular Lotto"
                                        },
                                    ]
                                });

                                var requestOptions = {
                                    method: 'POST',
                                    headers: myHeaders,
                                    body: raw,
                                    redirect: 'follow'
                                };

                                fetch(`${url}/api/v1/placeStake`, requestOptions)
                                    .then(response => response.json())
                                    .then(result => {
                                        console.log('place stake result')
                                        console.log(result)
                                        if (result.result) {
                                            setSubmited(true)
                                            setId(Math.floor(Math.random() * 10000000000) + 1)
                                            let show = result.result.map((res) => res)
                                            if (show[0].type === 'AGAINST') {
                                                const { type, amount, odd, line, staked, gameName, coupon, date, amounts, possibleWinning, gameId, stakes1, stakes2 } = show[0];
                                                let response1 = stakes1.toString()
                                                let response2 = stakes2.toString()
                                                var myHeaders = new Headers();
                                                myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
                                                myHeaders.append("Authorization", `Bearer ${get}`);
                                                myHeaders.append("Content-Type", "application/json");

                                                var raw = JSON.stringify({
                                                    "game": 'Regular Lotto',
                                                    "amount": `${amount}`,
                                                    "amounts": `${amounts}`,
                                                    "type": `${type}`,
                                                    "line": `${line}`,
                                                    "gameName": `${gameName}`,
                                                    "odd": `${odd}`,
                                                    "possibleWinning": `${possibleWinning}`,
                                                    "staked": `${amount}`,
                                                    "stakes1": `${response1}`,
                                                    "stakes2": `${response2}`,
                                                    "date": `${date}`,
                                                    "coupon": `${coupon}`,
                                                    "gameId": `${gameId}`
                                                });

                                                var requestOptions = {
                                                    method: 'POST',
                                                    headers: myHeaders,
                                                    body: raw,
                                                    redirect: 'follow'
                                                };

                                                fetch(`${url}/api/v2/auth/betHistory`, requestOptions)
                                                    .then(response => response.json())
                                                    .then(result => {
                                                        console.log(result)
                                                        if (result.success) {
                                                            const { message } = result.success;
                                                            setMode(message)
                                                            setOpen(!open)
                                                            var myHeaders = new Headers();
                                                            myHeaders.append("signatures", "5a1131f2eb747be50714281ec3e68b759476c6dc9e1faf5fc5d91c552cf8c230");
                                                            myHeaders.append("Authorization", `Bearer ${get}`);

                                                            var requestOptions = {
                                                                method: 'GET',
                                                                headers: myHeaders,
                                                                redirect: 'follow'
                                                            };

                                                            fetch(`${url}/api/v2/auth/profile`, requestOptions)
                                                                .then(response => response.json())
                                                                .then(result => {
                                                                    if (result.success) {
                                                                        getUser(result.success.data)
                                                                        const { data } = result.success;
                                                                        showBoard(data)
                                                                        //   localStorage.setItem('user', JSON.stringify(data))
                                                                    } else {
                                                                        return;
                                                                    }
                                                                },
                                                                    (error) => {
                                                                        console.log(error)
                                                                    });

                                                        } else {
                                                            console.log(result)
                                                        }
                                                    })
                                                    .catch(error => console.log('error', error));

                    
                                            } else if (show[0].type === '1 BANKER') {
                                                const { type, amount, coupon, odd, gameName, line, staked, amounts, date, possibleWinning, gameId, stakes } = show[0];
                                                let response = stakes.toString()
                                                var myHeaders = new Headers();
                                                myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
                                                myHeaders.append("Authorization", `Bearer ${get}`);
                                                myHeaders.append("Content-Type", "application/json");

                                                var raw = JSON.stringify({
                                                    "game": 'Regular Lotto',
                                                    "amount": `${amount}`,
                                                    "amounts": `${amounts}`,
                                                    "type": `${type}`,
                                                    "odd": `${odd}`,
                                                    "line": `${line}`,
                                                    "amounts": `${amount}`,
                                                    "gameName": `${gameName}`,
                                                    "possibleWinning": `${possibleWinning}`,
                                                    "staked": `${staked}`,
                                                    "stakes": `${response}`,
                                                    "date": `${date}`,
                                                    "coupon": `${coupon}`,
                                                    "gameId": `${gameId}`
                                                });

                                                var requestOptions = {
                                                    method: 'POST',
                                                    headers: myHeaders,
                                                    body: raw,
                                                    redirect: 'follow'
                                                };

                                                fetch(`${url}/api/v2/auth/betHistory`, requestOptions)
                                                    .then(response => response.json())
                                                    .then(result => {
                                                        console.log(result)
                                                        if (result.success) {
                                                            const { message } = result.success;
                                                            setMode(message)
                                                            setOpen(!open)
                                                            var myHeaders = new Headers();
                                                            myHeaders.append("signatures", "5a1131f2eb747be50714281ec3e68b759476c6dc9e1faf5fc5d91c552cf8c230");
                                                            myHeaders.append("Authorization", `Bearer ${get}`);

                                                            var requestOptions = {
                                                                method: 'GET',
                                                                headers: myHeaders,
                                                                redirect: 'follow'
                                                            };

                                                            fetch(`${url}/api/v2/auth/profile`, requestOptions)
                                                                .then(response => response.json())
                                                                .then(result => {
                                                                    if (result.success) {
                                                                        getUser(result.success.data)
                                                                        const { data } = result.success;
                                                                        showBoard(data)
                                                                        //   localStorage.setItem('user', JSON.stringify(data))
                                                                    } else {
                                                                        return;
                                                                    }
                                                                },
                                                                    (error) => {
                                                                        console.log(error)
                                                                    });

                                                        } else {
                                                            setSuccess('refresh and try again. Thanks.')
                                                            return;
                                                        }
                                                    })
                                                    .catch(error => console.log('error', error));
                                            } else {
                                                const { type, amount, coupon, line, odd, gameName, staked, date, amounts, possibleWinning, stakes, gameId } = show[0];
                                                let response = stakes.toString()
                                                var myHeaders = new Headers();
                                                myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
                                                myHeaders.append("Authorization", `Bearer ${get}`);
                                                myHeaders.append("Content-Type", "application/json");

                                                var raw = JSON.stringify({
                                                    "game": 'Regular Lotto',
                                                    "amount": `${amount}`,
                                                    "type": `${type}`,
                                                    "odd": `${odd}`,
                                                    "amounts": `${amounts}`,
                                                    "possibleWinning": `${possibleWinning}`,
                                                    "staked": `${staked}`,
                                                    "stakes": `${response}`,
                                                    "line": `${line}`,
                                                    "gameName": `${gameName}`,
                                                    "date": `${date}`,
                                                    "coupon": `${coupon}`,
                                                    "gameId": `${gameId}`
                                                });

                                                var requestOptions = {
                                                    method: 'POST',
                                                    headers: myHeaders,
                                                    body: raw,
                                                    redirect: 'follow'
                                                };

                                                fetch(`${url}/api/v2/auth/betHistory`, requestOptions)
                                                    .then(response => response.json())
                                                    .then(result => {
                                                        console.log(result)
                                                        if (result.success) {
                                                            const { message } = result.success;
                                                            setMode(message)
                                                            setOpen(!open)
                                                            var myHeaders = new Headers();
                                                            myHeaders.append("signatures", "5a1131f2eb747be50714281ec3e68b759476c6dc9e1faf5fc5d91c552cf8c230");
                                                            myHeaders.append("Authorization", `Bearer ${get}`);

                                                            var requestOptions = {
                                                                method: 'GET',
                                                                headers: myHeaders,
                                                                redirect: 'follow'
                                                            };

                                                            fetch(`${url}/api/v2/auth/profile`, requestOptions)
                                                                .then(response => response.json())
                                                                .then(result => {
                                                                    if (result.success) {
                                                                        getUser(result.success.data)
                                                                        const { data } = result.success;
                                                                        showBoard(data)
                                                                        //   localStorage.setItem('user', JSON.stringify(data))
                                                                    } else {
                                                                        return;
                                                                    }
                                                                },
                                                                    (error) => {
                                                                        console.log(error)
                                                                    });

                                                        } else {
                                                            setSuccess('refresh and try again. Thanks.')
                                                            return;
                                                        }
                                                    })
                                                    .catch(error => console.log('error', error));
                                            }
                                        } else {
                                            return;
                                        }
                
                                    },
                                        (error) => {
                                            console.log(error)
                                        }
                                    );
                            }
                        }
                    }
                })
    
            }
        } else {
            setSuccess('Please kindly note that you currently do not have sufficient balance to place this bet')
            return;
        }
    }

    const handleCat = (e, data) => {
        if (isNaN(e.target.value)) {
            return;
        } else {
            e.target.value * data.lines > parseInt(general) && setSuccess(`for games whose staking total amount is more than ${general} naira the odds would be made dynamic`)
            data.amounts = e.target.value;
            data.amount = e.target.value * data.lines;
            calculateTotalStake()
        }
    }

    const handleV = (e, items) => {
        e.preventDefault()
        if (isNaN(e.target.value) || e.target.value < 1) {
            return;
        } else {
            setListV(e.target.value)
            items.amounts = e.target.value
            items.amount = e.target.value * items.line
            items.staked = e.target.value * items.line
        }
    }

    const handleAgainst = (e) => {
        e.preventDefault()
        arr.push(0)
        const first_against = arr.slice(0, arr.indexOf(0))
        setFirstAgainst(first_against)
        setActiveNums(false)
        setHideButton(true)
    };

    const handleSuspend = (gam, e, index) => {
        if (gam.suspend === 1) {
            setSuccess('Game has been temporarily suspended')
            return;
        } else {
            clickeds(e, index)
            setClicked(true)
            setGameShow({ ...gameShow, name: gam.name, endTime: gam.endTime, day: gam.day, suspend: gam.suspend, image: gam.image, start: gam.startTime, end: gam.endTime, id: gam.uuid })
        }
    }


    let str = 'Original Lotto (5/90)'
    let str1 = 'Lotto Express (5/90)'
    let str2 = 'Soft Lotto (3/10)'


    useEffect(() => {

        for (let j = 0; j < game.length; j++) {
            data2.push(game[j].uuid)
        }
        
    }, [game]);


    const handleLoad = e => {
        if (data2) {
            if (!(data2.includes(e.gameId))|| (parseInt(e.date) < parseInt(c)) || (e.day === currentDay && parseInt(e.endTime[0] * 3600 + e.endTime[1] * 60 + e.endTime[0]) > currentTime) ) {
                e.expired = true;
                return;
            } else {
                e.expired = false;
            }
        }
    };

    return (
        <section className='black_bg'>
            <div className='news pl-1 mt-2 pl-lg-5 game_head p_white d-flex justify-content-between'>
                <p className="timer pb-2 pt-2">{timer}</p>
                <Link className='game_links backgs first pt-2' to='/lottoexpress'>Lotto Express</Link>
                <Link className='first activated pb-2 pt-2' to='/games'>Original Lotto</Link>
                <Link className='game_links backgs first pt-2' to='/promotion'>7/90</Link>
                <Link className='game_links backgs ml-3 pt-2' to='/softlotto'>Soft Lotto</Link>
                <Link className='pt-2' to='#'>
                    <p className='game_links d-none d-lg-inline backgs first '>How to Play</p>
                </Link>
                <Link to='/fundwallet'>
                    <p className='game_links backgs pt-2'>Deposit Fund</p>
                </Link>
                <Link to='#' className='pt-2'>
                    <p className='game_links d-none d-lg-inline backgs'>First Bet</p>
                </Link>
                <div class="d-none d-lg-flex flex-right pt-2 mr-2">
                    <Link to='https://www.facebook.com'>
                        <FontAwesomeIcon className=' backg backgs color3' size-mode='1x' icon={faFacebook} />
                    </Link>
                    <Link to='https://www.twitter.com'>
                        <FontAwesomeIcon className='ml-1 mr-1 backgs backg color4' size-md='1x' icon={faTwitter} />
                    </Link>
                    <Link to='https://www.instagram.com'>
                        <FontAwesomeIcon className='mr-1 backg backgs color5' size-md='1x' icon={faInstagram} />
                    </Link>
                    <Link to='https://www.youtube.com'>
                        <FontAwesomeIcon className=' backg backgs color6' size-md='1x' icon={faYoutube} />
                    </Link>
                </div>
            </div>
            <Container fluid='md'>
                <Row>
                    <Col className='' md={3}>
                        {days.map((day, i) => {
                            return (
                                <section className='d-none d-lg-inline'>
                                    <div className='htext' key={i} onClick={(e) => handleCategory(e, i)}>
                                        <span className=''>{day}</span>
                                    </div>
                                    {
                                        <div className='game_types'>
                                            {game.map((gam, index) => {
                                                if (secArr.includes(i) && daysShow.includes(gam.day.charAt(0).toUpperCase() + gam.day.slice(1))) {
                                                    return <img src={gam.image} onClick={(e) => {
                                                        e.preventDefault()
                                                        handleSuspend(gam, e, index)
                                                        // clickeds(e, index)
                                                        // setClicked(true)
                                                        // setGameShow({ ...gameShow, name: gam.name, image: gam.image, start: gam.startTime, end: gam.endTime, id: gam.uuid })
                                                    }}
                                                        className={`${a.includes(index) && gam.suspend !== 1 ? 'actives' : 'types'} `} style={{ width: '83px' }} />
                                                }
                                            })}
                                        </div>
                                    }
                                </section>
                            )
                        })}
                    </Col>
                    <Col md={5} className='boxplay'>
                        {/* <Row>
                            <Col className=''> */}
                        <div className='d-flex d-lg-inline pt-3 pt-lg-0 justify-content-start'>
                            <Form.Control className='choose mt-1 mb-3' style={{ width: '50%', color: '#fff', fontSize: '12px', background: 'black', marginTop: '10px' }} as="select" required onChange={(e) => { history.push(e.target.value) }} custom>
                                <option name='games' value='/games'>{str}</option>
                                <option name='lottoexpress' value='/lottoexpress'>{str1}</option>
                                <option name='softlotto' value='/softlotto'>{str2}</option>
                                <option name='promotions' value='/promotion'>Extra Lotto (7/90)</option>
                            </Form.Control>
                            <div className='gameShowName d-inline d-lg-none mt-1 d-flex justify-content-between' style={{ height: '32px', background: '#28a745', fontSize: '13px', width: '50%', fontWeight: '800' }} onClick={() => setShowGameType(!showGameType)}>
                                <p>Games Basket</p> <ExpandMoreIcon /></div>
                            <h5 className='game_head yellow'>{str.toUpperCase()}</h5>
                        </div>
                        <div className='d-flex justify-content-between'>
                            {/* <h4 className='h4_font'>
                                            </h4> */}
                            {gameShow.name && <p className='p_width pr-2' style={{ fontWeight: '800' }}> <span className='span_h4'>{gameShow.name}</span></p>}
                            {gameShow.end && <p className='p_width' style={{ fontWeight: '500' }}> <span className='span_h4'>Closes: {gameShow.end}</span></p>}
                        </div>
                        {gameType === 'AGAINST' && <Button variant='danger' onClick={handleAgainst} className={`small_class ml-2 mb-2 ml-lg-0 ${arr.length > 0 ? '' : 'disabled'}`}>Against</Button>}
                                    
                        <div className='d-flex justify-content-between d-lg-none'>
                            <Form className='form-select form-select-sm align-center select_game' onChange={handleChange}>
                                <Form.Group controlId="exampleForm.SelectCustom">
                                    <Form.Control as="select" custom>
                                        <option value='PERM 2'>PERM 2</option>
                                        <option value='NAP 1'>NAP 1</option>
                                        <option value='NAP 2'>NAP 2</option>
                                        <option value='NAP 3'>NAP 3</option>
                                        <option value='NAP 4'>NAP 4</option>
                                        <option value='NAP 5'>NAP 5</option>
                                        <option value='PERM 3'>PERM 3</option>
                                        <option value='PERM 4'>PERM 4</option>
                                        <option value='PERM 5'>PERM 5</option>
                                        <option value='1 BANKER'>1 BANKER</option>
                                        <option value='AGAINST'>AGAINST</option>
                                    </Form.Control>
                                </Form.Group>
                            </Form>
                            <div>
                                {gameType === 'NAP 1' && <p style={{color: '#fff', fontSize: '8px', marginLeft: '11px', marginTop: '7px'}}>Pick a single number And If They Are Drawn You Win Assigns Odds X Your Single Stake Amount.</p>}
                                {gameType === 'NAP 2' && <p style={{color: '#fff', fontSize: '8px', marginLeft: '11px', marginTop: '7px'}}>Pick 2 Numbers And If They Are Drawn You Win Assigns Odds X Your Single Stake Amount.</p>}
                                {gameType === 'PERM 2' && <p style={{ color: '#fff', fontSize: '8px', marginLeft: '11px', marginTop: '7px' }}>Perm Games Are Very Exciting, Please Read Our Guide On This.</p>}
                                {gameType === 'PERM 3' && <p style={{ color: '#fff', fontSize: '8px', marginLeft: '11px', marginTop: '7px' }}>Perm Games Are Very Exciting, Please Read Our Guide On This.</p>}
                                {gameType === 'PERM 4' && <p style={{ color: '#fff', fontSize: '8px', marginLeft: '11px', marginTop: '7px' }}>Perm Games Are Very Exciting, Please Read Our Guide On This.</p>}
                                {gameType === 'PERM 5' && <p style={{ color: '#fff', fontSize: '8px', marginLeft: '11px', marginTop: '7px' }}>Perm Games Are Very Exciting, Please Read Our Guide On This.</p>}
                                {gameType === 'NAP 3' && <p style={{color: '#fff', fontSize: '8px', marginLeft: '11px', marginTop: '7px'}}>Pick 3 Numbers And If They Are Drawn You Win Assigns Odds X Your Single Stake Amount.</p>}
                                {gameType === 'AGAINST' && <p style={{color: '#fff', fontSize: '8px', marginLeft: '11px', marginTop: '7px'}}> Permutations Of Numbers Against Each Other.</p>}
                                {gameType === 'NAP 4' && <p style={{color: '#fff', fontSize: '8px', marginLeft: '11px', marginTop: '7px'}}>Pick 4 Numbers And If They Are Drawn You Win Assigns Odds X Your Single Stake Amount.</p>}
                                {gameType === 'NAP 5' && <p style={{color: '#fff', fontSize: '8px', marginLeft: '11px', marginTop: '7px'}}>Pick 5 Numbers And If They Are Drawn You Win Assigns Odds X Your Single Stake Amount.</p>}
                                {gameType === '1 BANKER' && <p style={{color: '#fff', fontSize: '8px', marginLeft: '11px', marginTop: '7px'}}>1 BANKER: Pick A Number And If Its Drawn You Win Assigns Odds X Your Single Stake Amount.</p> }
                            </div>
                        </div>
                        <Form className='form-select form-select-sm game_head align-center ' onChange={handleChange}>
                            <Form.Group controlId="exampleForm.SelectCustom">
                                <Form.Control as="select" custom>
                                    <option value='PERM 2'>PERM 2</option>
                                    <option value='NAP 1'>NAP 1</option>
                                    <option value='NAP 2'>NAP 2</option>
                                    <option value='NAP 3'>NAP 3</option>
                                    <option value='NAP 4'>NAP 4</option>
                                    <option value='NAP 5'>NAP 5</option>
                                    <option value='PERM 3'>PERM 3</option>
                                    <option value='PERM 4'>PERM 4</option>
                                    <option value='PERM 5'>PERM 5</option>
                                    <option value='1 BANKER'>1 BANKER</option>
                                    <option value='AGAINST'>AGAINST</option>
                                </Form.Control>
                            </Form.Group>
                        </Form>
                        <div className='mt-2 text-md-center ml-md-0'>
                            {gameType !== 'AGAINST' && nums.map((i) => {
                                return <button key={i} name={!activeNums[i] && 'ready'} onClick={(e) => handleClass(e, i)} className={`${arr.includes(i) && !v[0]?.includes(i) ? 'game_clicked' : geteNums && !v[0]?.includes(i) && 'red'} balx ${showAll.includes(i) && 'reds'} ${v[0]?.includes(i) && 'games_b'}`} >{i}</button>
                            })}
                            {gameType === 'AGAINST' && nums.map((i) => {
                                return <button key={i} name={!activeNums[i] && 'ready'} onClick={(e) => handleClass(e, i)} className=
                                    {
                                        `${arr.includes(i) && !v[0]?.includes(i) ? 'game_clicked' : geteNums && !v[0]?.includes(i) && 'red'}
                                     balx ${showAll.includes(i) && 'reds'}
                                    ${v[0]?.includes(i) && 'games_b'}
                                    ${firstAgainst.includes(i) && 'disabled'}`
                                    }>{firstAgainst.includes(i) && <FontAwesomeIcon size='1x' icon={faLock} /> || i}</button>
                            })}
                        </div>
                        <Row className="clear instant_pay mt-2">
                            <Button variant='outline-primary' className='thew' onClick={handleRandom}>Quick Pick</Button>
                            {
                                hideButton && gameType === "AGAINST" && sec &&
                                <Button variant='success' className='thew' onClick={handleBets}>Play</Button>
                            }
                            {
                                gameType !== "AGAINST" &&
                                <Button variant='success' className='thew' onClick={handleBets}>Play</Button>
                            }
                            <Button variant='outline-danger' className='thew' onClick={() => { setArr([]); setHideButton(false); setFirstAgainst([]); setGetNums(false) }}>Clear</Button>
                        </Row>
                    </Col>
                    <Col md={3} className='show1'>
                        <Row className='mt-2'>
                            <div className={`col-md-6 col-sm-6 mbox1 text-center ${betSlip.length < 1 && 'disabled'}`} onClick={() => { setSlip(true); setHow(false); }}>BetSlip <span class="badge bg-secondary">{count}</span></div>
                            <div className='col-md-5 ml-1 col-sm-6 mbox1 text-center' onClick={() => { setSlip(false); setHow(true); }}><small>Search Coupon</small></div>
                        </Row>
                        <Row>
                            {
                                how ?
                                    <div className={`${list.length > 1 && 'scroller'} bet_section col-sm-12  margin_bottom`} id="howto">
                                        <form className="d-flex flex-column" onSubmit={CouponSubmit}>
                                            <input className="form-control m-2" type="text" name='coupon' placeholder="Coupon Code" onChange={handleCoupon} />
                                            <button className="btn size=sm btn-outline-success ml-2 mr-2 mb-2" type="submit" >Load Coupon</button>
          
                                        </form>
                                        {showCoupon && <div className='show_coupon' >
                                            {list.map((items) => {
                                                var { amount, id, odd, results, stakes, line, gameName, type, stakes1, amounts, stakes2, status, staked, possibleWinning, game, gameId } = items;
                                                return (
                                                    <>
                                                        <main className='get_line'>
                                                            <div className='d-flex justify-content-end'>
                                                                <FaTimes onClick={() => {
                                                                    removeItems(id)
                                                                }}
                                                                    className='cancel_game'
                                                                />
                                                            </div>
                                                            <div className=''>
                                                                {status !== 'PENDING' && <p className='redz'>Expired ticket</p>}
                                                                <p className='p_type'>Game Name: {gameName}</p>
                                                                <p className='p_type'>possibleWinning: &#x20A6;{possibleWinning}</p>
                                                                <p className='p_type'>Odd: {odd}</p>
                                                                {user.wallet && <p className='p_type'>status: {status.toLowerCase()}</p>}
                                                                <p className='p_type'>Game Name: {game}</p>
                                                                <p className='p_type'>Lines: {line}</p>
                                                                <p className='p_type'>Stake Amount: &#x20A6;{amount}</p>
                                                                <p className='p_type'>Stake Amount per Line: &#x20A6;{amounts}</p>
                                                                {stakes && <p className='p_type'>Bet Numbers: {stakes}</p>}
                                                                {stakes1 && <p className='p_type'>Against first: {stakes1}</p>}
                                                                {stakes2 && <p className='p_type'>Against second: {stakes2}</p>}
                                                                {results && <p className='p_type'>Numbers Drawn {results}</p>}
                                                                <p className='p_type'>game Type: {type}</p>
                                                                {status === 'PENDING' && <div>
                                                                    <Form.Control className='form_input' pattern='[0-9]' onChange={(e) => handleV(e, items)} value={items.amounts} size='sm' placeholder={amounts} />
                                                                    <div className='mt-2 d-flex justify-content-lg-between'>
                                                                        <Button className='mr-2 mr-lg-0 games game' value='50' size='sm' onClick={() => {
                                                                            setListV(50)
                                                                            items.amounts = 50;
                                                                            items.amount = 50 * items.line
                                                                            items.staked = 50 * items.line
                                                                        }}>50
                                                                        </Button>
                                                                        <Button className='mr-2 mr-lg-0 games game ' size='sm' value='100' size='sm' onClick={() => {
                                                                            setListV(100)
                                                                            items.amounts = 100;
                                                                            items.amount = 100 * items.line
                                                                            items.staked = 100 * items.line
                                                                        }}>100</Button>
                                                                        <Button className='mr-2 mr-lg-0 games game' size='sm' value='500' size='sm' onClick={() => {
                                                                            setListV(500)
                                                                            items.amounts = 500;
                                                                            items.amount = 500 * items.line
                                                                            items.staked = 500 * items.line
                                                                        }}>500</Button>
                                                                        <Button className='mr-2 mr-lg-0 games game' size='sm' value='1000' size='sm' onClick={() => {
                                                                            setListV(1000)
                                                                            items.amounts = 2500;
                                                                            items.amount = 2500 * items.line
                                                                            items.staked = 2500 * items.line
                                                                        }}>1000</Button>
                                                                        <Button className='mr-2 mr-lg-0 games game' size='sm' value='2500' size='sm' onClick={() => {
                                                                            setListV(2500)
                                                                            items.amounts = 2500;
                                                                            items.amount = 2500 * items.line
                                                                            items.staked = 2500 * items.line
                                                                        }}>2500</Button>
                                                                        <Button className='mr-2 mr-lg-0 games game' size='sm' value='5000' size='sm' onClick={() => {
                                                                            setListV(5000)
                                                                            items.amounts = 5000;
                                                                            items.amount = 5000 * items.line
                                                                            items.staked = 5000 * items.line
                                                                        }}>5000</Button>
                                                                    </div>
                                                                </div>
                                                                }
                                                            </div>
                                                        </main>
                                                    </>
                                                )
                                            })}
                                            <Button size='sm' autoFocus className={`align-item-center mb-2 game `} variant='success' onClick={handleCouponBet}>Bet Ticket</Button>
                                        </div>}
                                    </div> :
                                    <section className={`${betSlip.length > 1 && 'scroller'} bet_section mt-2`}>
                                        <div className='d-flex justify-content-end game_back'>
                                            <button className="game_slip_btn" onClick={() => { setBetSlip([]); calculateTotalStake1([]); setCount(0); setNumber(0); setShowGameModal(false) }}>Clear Slip</button>
                                        </div>
                                        <div>
                                            {betSlip.map((data, index) => {
                                                const { type, lines, id, gameId, expired, gameName, stakeList, stakeList2, amount, amounts } = data;
                                                handleLoad(data)
                                                return (
                                                    <main key={id} className='get_line'>
                                                        <div className='d-flex justify-content-end'>
                                                            <FaTimes onClick={() => {
                                                                removeItem(id)
                                                            }}
                                                                className='cancel_game'
                                                            />
                                                        </div>
                                                        <div>
                                                            {expired && <p className={`${expired && 'redz'}`}>Expired ticket</p>}
                                                            <p className='p_type'>Game Name: {gameName}</p>
                                                            <p className='p_type'>Lines: {lines}</p>
                                                            <p className='p_type'>Type: {type}</p>
                                                            {stakeList && stakeList2 && <p className='p_type'>Numbers: {`${stakeList.toString()}`}</p>}
                                                            {type === 'AGAINST' && stakeList && <p className='p_type'>Against: {stakeList2.toString()} </p>}
                                                            {type !== 'AGAINST' && stakeList && <p className='p_type'>Stakes: {stakeList.toString()} </p>}
                                                            <p className='p_type'>Amount per line: &#x20A6;{amounts}</p>
                                                            <p className='p_type'>Betslip total Amount: &#x20A6;{amount}</p>
                                                            <Form.Control className='form_input' pattern='[0-9]' onChange={(e) => handleCat(e, data)} value={data.amounts > 0 ? data.amounts : ''} size='sm' placeholder={data.amounts > 0 ? `${data.amounts}` : 'Amount'} />
                                                            <div className='mt-2 d-flex justify-content-lg-between'>
                                                                <Button className='mr-2 mr-lg-0 games game' value='50' size='sm' onClick={() => {
                                                                    data.amount = 50 * lines;
                                                                    data.amounts = 50;
                                                                    calculateTotalStake()
                                                                }}>50</Button>
                                                                <Button className='mr-2 mr-lg-0 games game ' size='sm' value='100' size='sm' onClick={() => {
                                                                    data.amount = 100 * lines;
                                                                    data.amounts = 100
                                                                    calculateTotalStake()
                                                                }}>100</Button>
                                                                <Button className='mr-2 mr-lg-0 games game' size='sm' value='500' size='sm' onClick={() => {
                                                                    data.amount = 500 * lines;
                                                                    data.amounts = 500
                                                                    calculateTotalStake()
                                                                }}>500</Button>
                                                                <Button className='mr-2 mr-lg-0 games game' size='sm' value='1000' size='sm' onClick={() => {
                                                                    data.amount = 1000 * lines;
                                                                    data.amounts = 1000
                                                                    calculateTotalStake()
                                                                }}>1000</Button>
                                                                <Button className='mr-2 mr-lg-0 games game' size='sm' value='2500' size='sm' onClick={() => {
                                                                    data.amount = 2500 * lines;
                                                                    data.amounts = 2500
                                                                    calculateTotalStake()
                                                                }}>2500</Button>
                                                                <Button className='mr-2 mr-lg-0 games game' size='sm' value='5000' size='sm' onClick={() => {
                                                                    data.amount = 5000 * lines;
                                                                    data.amounts = 5000
                                                                    calculateTotalStake()
                                                                }}>5000</Button>
                                                            </div>
                                                        </div>
                                                    </main>
                                        
                                                )
                                            })}
                                        </div>
                                        <section className='mt-2'>
                                            <div className='d-flex justify-content-between'>
                                                <p className='p_type'>Number of Bets: </p>
                                                <p className='p_type'>{betSlip.length}</p>
                                            </div>
                                            <div className='d-flex justify-content-between'>
                                                <p className='p_type'>Total Stake: </p>
                                                <p className='p_type'>&#x20A6;{number}</p>
                                            </div>
                                        </section>
                                        <div className='d-flex justify-content-center'>
                                            {!logedIn && <div className='d-flex flex-column'>
                                                <Button size='sm' className={`align-item-center mb-5 game`} variant='success' onClick={() => { setShowModal(!showModal); showGameModal && setShowGameModal(false) }}>Login To Place Bet</Button>
                                                
                                                {/* <Button size='sm' className={`align-item-center mb-2 game`} variant='success' onClick={() => setFriend(true)}>Play for a friend</Button> */}
                                                
                                            </div>}
                                            {logedIn &&
                                                <div className='d-flex flex-column'>
                                                    <Button size='sm' className={`align-item-center mb-5 game`} variant='success' onClick={handleGameBet}>Place Bet</Button>
                                                    {/* <Button size='sm' className={`align-item-center mb-2 game`} variant='success' onClick={() => setFriend(true)}>Play for a friend</Button> */}
                                                </div>
                                            }
                                        </div>
                            
                                    </section>
                            }
                        </Row>
                    </Col>
                    {showGames &&
                   
                        <div className={`${!showGameModal ? 'display' : 'c-sidebar --bet-slip is-open'} ${betSlip.length > 0 ? 'd_none scroll_game' : 'display'}`} >
                            <section className={`${betSlip.length > 2 && 'scroller'} bet_section mt-2`}>
                                <div className='d-flex justify-content-between game_back'>
                                    <h6 className='game_h6'>
                                      Regular Lotto
                                </h6>
                                <p className='palce_bet' onClick={() =>  setShowGameModal(false)}>Place Another Bet</p>
                                    <button className="game_slip_btn mb-1" onClick={() => { setBetSlip([]); calculateTotalStake1([]); setCount(0); setShowGameModal(false) }}>Clear Slip</button>
                                </div>
                                <div>
                                    {betSlip.map((data) => {
                                        const { type, id, lines, expired, gameId, gameName, stakeList, stakeList2, amount, amounts } = data;
                                        handleLoad(data)
                                        return (
                                            <main key={id} className='get_line'>
                                                <div className='d-flex justify-content-end'>
                                                    <FaTimes onClick={() => {
                                                        removeItem(id)
                                                    }}
                                                        className='cancel_game'
                                                    />
                                                </div>
                                                <div>
                                                    {expired && <p className='redz'>Expired ticket</p>}
                                                    <p className='p_type'>Game Name: {gameName}</p>
                                                    <p className='p_type'>Lines: {lines}</p>
                                                    <p className='p_type'>Type: {type}</p>
                                                    {stakeList && stakeList2 && <p className='p_type'>Numbers: {`${stakeList.toString()}`}</p>}
                                                    {type === 'AGAINST' && <p className='p_type'>Against: {`${stakeList2.toString()}`}</p>}
                                                    {type !== 'AGAINST' && stakeList && <p className='p_type'>Stakes: {stakeList.toString()} </p>}
                                                    <p className='p_type'>Amount per Line: &#x20A6;{amounts}</p>
                                                    <p className='p_type'>Betslip total Amount: &#x20A6;{amount}</p>
                                                    <Form.Control className='form_input' pattern='[0-9]' onChange={(e) => handleCat(e, data)} value={data.amounts > 0 ? data.amounts : ''} size='sm' placeholder={data.amounts > 0 ? `${data.amounts}` : 'Amount'} />
                                                    <div className='mt-2 d-flex justify-content-lg-between'>
                                                        <Button className='mr-1 mr-lg-0 games game' value='50' size='sm' onClick={() => {
                                                            data.amount = 50 * lines;
                                                            data.amounts = 50
                                                            calculateTotalStake()
                                                        }}>50</Button>
                                                        <Button className='mr-2 mr-lg-0 games game ' size='sm' value='100' size='sm' onClick={() => {
                                                            data.amount = 100 * lines;
                                                            data.amounts = 100
                                                            calculateTotalStake()
                                                        }}>100</Button>
                                                        <Button className='mr-2 mr-lg-0 games game' size='sm' value='500' size='sm' onClick={() => {
                                                            data.amount = 500 * lines;
                                                            data.amounts = 500
                                                            calculateTotalStake()
                                                        }}>500</Button>
                                                        <Button className='mr-1 mr-lg-0 games game' size='sm' value='1000' size='sm' onClick={() => {
                                                            data.amount = 1000 * lines;
                                                            data.amounts = 1000
                                                            calculateTotalStake()
                                                        }}>1000</Button>
                                                        <Button className='mr-2 mr-lg-0 games game' size='sm' value='25000' size='sm' onClick={() => {
                                                            data.amount = 2500 * lines;
                                                            data.amounts = 2500
                                                            calculateTotalStake()
                                                        }}>2500</Button>
                                                        <Button className='mr-1 mr-lg-0 games game' size='sm' value='5000' size='sm' onClick={() => {
                                                            data.amount = 5000 * lines;
                                                            data.amounts = 5000
                                                            calculateTotalStake()
                                                        }}>5000</Button>
                                                        <Button className='mr-2 mr-lg-0 games game' size='sm' value='10000' size='sm' onClick={() => {
                                                            data.amount = 10000 * lines;
                                                            data.amounts = 10000
                                                            calculateTotalStake()
                                                        }}>10000</Button>
                                                    </div>
                                                </div>
                                            </main>
                                        
                                        )
                                    })}
                                </div>
                                <section className='mt-2'>
                                    <div className='d-flex justify-content-between'>
                                        <p className='p_type'>Number of Bets: </p>
                                        <p className='p_type'>{betSlip.length}</p>
                                    </div>
                                    <div className='d-flex justify-content-between'>
                                        <p className='p_type'>Total Stake: </p>
                                        <p className='p_type'>&#x20A6;{number}</p>
                                    </div>
                                </section>
                                <div autoFocus className='d-flex justify-content-center'>
                                    {!logedIn && <div className='d-flex flex-column'>
                                        <Button size='sm' className={`align-item-center mb-2 game`} variant='success' onClick={() => { setShowModal(!showModal); showGameModal && setShowGameModal(false) }}>Login To Place Bet</Button>
                                                
                                        {/* <Button size='sm' className={`align-item-center mb-2 game`} variant='success' onClick={() => { setShowModal(!showModal); showGameModal && setShowGameModal(false) }}>Play for a friend</Button> */}
                                                
                                    </div>}
                                    {logedIn &&
                                        <div className='d-flex flex-column'>
                                            <Button size='sm' className={`align-item-center mb-2 game`} variant='success' onClick={handleGameBet}>Place Bet</Button>
                                            {/* <Button size='sm' className={`align-item-center mb-2 game`} variant='success' onClick={() => setFriend(true)}>Play for a friend</Button> */}
                                        </div>
                                    }
                                </div>
                            
                            </section>
                        </div>
                    }
                </Row>
            </Container>
            {showModal && <GetWhatsapp />}
            <div class="row win">
                <div className="d-flex instant_pay d-none d-lg-inline justify-content-center text-center">
                 <h1 style={{marginLeft: '26rem'}} className='pb-2'>INSTANT PAYMENT, NO STORY</h1>
                </div>
                <Col className='pr-0'>
                  {gameType !== 'AGAINST' && <p style={{display: 'flex', marginBottom: '0px', flexWrap: 'wrap'}} className='pb-2'>{arr.map((a) => <span className='num_span'>{a }</span> )}</p>}
                  {gameType === 'AGAINST' && <p style={{display: 'flex', marginBottom: '0px', flexWrap: 'wrap'}} className='pb-2'>{arr.map((a) => firstAgainst.includes(a) && <span className='num_span'>{a }</span> || <span className='num_span1'>{a }</span>)}</p>}
                </Col>
            </div>
            {friend && <FriendModal friend={() => setFriend(false)}/>}
            {showGameType && <Types days={days} a={a} gameShow={gameShow} secArr={secArr} setGameShow={setGameShow} setClicked={() => setClicked(true)} clickeds={clickeds} daysShow={daysShow} handleCategory={handleCategory} game={game} showGameType={() => setShowGameType(false)} />}
            {success && <IntegrationNotistack className='d' success={`${success}`} />}
            {open && <Bet mode={mode} setOpen={() => { setOpen(false) }} betSlip={setBetSlip} type='regular' calculateTotalStake1={() => calculateTotalStake1([])} setMode={setMode} number={setNumber} count={setCount} />}
            {showMobile && <MobileCoupon value='regular lotto' showMobile={() => setShowMobile(false)} />}
            {fundModal && <FundModal currentUrl={currentUrl} setFundModal={setFundModal} mode={alerts} />}
            {showVoice && <GetVoice
                setGameShow={setGameShow}
                clicked={setClicked}
                gameShow={gameShow}
            />}
            <section className='bottom'>
                <div className='d-flex'>
                    <div className='game_item' style={{background: '#122d4b', fontWeight: '800'}}>
                        {/* <Button variant='outline-primary' className='thew' onClick={handleRandom}>Quick Pick</Button> */}
                        <p onClick={handleRandom} className='pt-2 mb-0 pb-2 play_sec'>Quick Pick</p>
                    </div>
                    <div className='game_item' style={{background: '#007bff', fontWeight: '800'}}>
                                            {
                            hideButton && gameType === "AGAINST" && sec &&
                            <p onClick={handleBets} className='pt-2 mb-0 pb-2 play_sec'>Play</p>
                        // <Button variant='success' className='thew' onClick={handleBets}>Play</Button>
                    }
                    {
                        gameType !== "AGAINST" &&
                            <p onClick={handleBets} className='pt-2 mb-0 pb-2 play_sec'>Play</p>
                        // <Button variant='success' className='thew' onClick={handleBets}>Play</Button>
                    }
                    </div>
                    <div className='game_item' style={{background: '#6c757d', fontWeight: '800'}}>
                            <p onClick={() => { setArr([]); setHideButton(false); setFirstAgainst([]); setGetNums(false) }} className='pt-2 mb-0 pb-2 play_sec'>Clear</p>
                                            {/* <Button variant='outline-danger' className='thew' onClick={() => { setArr([]); setHideButton(false); setFirstAgainst([]); setGetNums(false) }}>Clear</Button> */}
                    </div>
                </div>
                <div className='d-flex smaller'>
                   
                    <div className='game_item' onClick={() => { showGameModal && setShowGameModal(false); history.push('/') }} style={{fontWeight: '700'}}>
                        <RiHome2Line className='select_icon' />
                        <span className='select_item'>Home</span>
                    </div>
                    <div className='game_item' onClick={() => { showGameModal && setShowGameModal(false); setShowMobile(!showMobile) }} style={{fontWeight: '700'}}>
                        <IoIosFootball className='select_icon' />
                        <span className='select_item'>Load Coupon</span>
                    </div>
                    <div className={`${betSlip.length > 0 && 'real'} game_item`} onClick={() => { setShowGameModal(!showGameModal) }} style={{fontWeight: '700'}}>
                        {/* <span className='bet_count'>{count}</span> */}
                        <IoOptionsOutline className='select_icon' />
                        <span className='select_item'>BetSlip <span className={`${betSlip.length > 0 && 'zoom-in-zoom-out'}`}>({count})</span> </span>
                    </div>
                    <div className={`game_item ${!logedIn && 'disabled getLogged'}`} onClick={() => { showGameModal && setShowGameModal(false); history.push('/profile') }} style={{fontWeight: '700'}}>
                        <IoMdHelpCircleOutline className='select_icon' />
                        <span className='select_item'>FAQ & Support</span>
                    </div>
                </div>
            </section>
        </section>
    )
}

export default Games