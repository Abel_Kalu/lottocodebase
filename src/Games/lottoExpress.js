import React, { useState, useEffect } from 'react';
import { Row, Col, Container, Button, Form, InputGroup } from 'react-bootstrap'
import { Link } from 'react-router-dom';
import Countdown from "react-countdown";
import { useGlobalContext } from '../store/context';
import { useHistory } from 'react-router';
import { FaTimes } from 'react-icons/fa';
import moment from 'moment'
import MobileCoupon from '../Fetch/MobileCoupon'
import Bet from '../Fetch/Bet'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faApple, faGooglePlay, faFacebook, faYoutube, faTwitter, faInstagram } from '@fortawesome/free-brands-svg-icons'
import axios from 'axios';
import Layout from '../Layout'
import GetWhatsapp from '../Fetch/GetWhatsapp'
import play from '../svg/play.svg'
import FundModal from '../Fetch/FundModal'
import { RiBankLine, RiHome2Line, RiSdCardMiniLine, RiUserAddLine } from 'react-icons/ri';
import { IoIosFootball, IoMdHelpCircleOutline } from 'react-icons/io';
import { IoOptionsOutline } from "react-icons/io5";
import { faLock } from '@fortawesome/free-solid-svg-icons'
import IntegrationNotistack from '../Fetch/IntegrationNotistack';

let date = new Date().getHours(0, 0, 0, 0);

const LottoExpress = () => {
    const { game, logedIn, days, user, getUser, showBoard } = useGlobalContext();
    const [hideButton, setHideButton] = useState(false)
    const [activeNums, setActiveNums] = useState(false)
    const [showMobile, setShowMobile] = useState(false)
    const [value, setValue] = useState(0);
    const [sec, setSec] = useState([])
    const [general, setGeneral] = useState(null)
    const [showAlert, setShowAlert] = useState(false)
    const [number, setNumber] = useState(0)
    const [time, setTime] = useState(null)
    const history = useHistory()
    const url = 'https://api.grandlotto.ng'
    const [showAll, setShowAll] = useState([])
    let [array, setArray] = useState([])
    const [listV, setListV] = useState(0)
    let [day, setDay] = useState(date)
    let [gameType, setGameType] = useState('PERM 2')
    const [betSlip, setBetSlip] = useState([])
    let [arr, setArr] = useState([])
    const [getBet, setGetBet] = useState(false)
    const [success, setSuccess] = useState('')
    const [geteNums, setGetNums] = useState(false)
    const [timer, setTimer] = useState(moment().format('LTS'))
    const [showModal, setShowModal] = useState(false)
    const [expressMax, setExpressMax] = useState(null)
    const [subValue, setSubValue] = useState(0)
    const [showGameModal, setShowGameModal] = useState(false)
    const [firstAgainst, setFirstAgainst] = useState([])
    var [count, setCount] = useState(0)
    var currentUrl = window.location.href.replace(/(.+\w\/)(.+)/,"/$2")
    const [how, setHow] = useState(true)
    const [showCoupon, setShowCoupon] = useState(false)
    const [coupon, setCoupon] = useState('')
    const [list, setList] = useState([])
    const [slip, setSlip] = useState(false)
    const [mode, setMode] = useState('')
    const [open, setOpen] = useState(false)
    const [id, setId] = useState(Math.floor(Math.random() * 10000000000) + 1)
    const [fundModal, setFundModal] = useState(false)
    const [alerts, setAlerts] = useState('')


    let nums = []

    for (let i = 1; i < 91; i++) {
        nums.push(i)
    }

        const CouponSubmit = e => {
        e.preventDefault()
        console.log(coupon)
        var myHeaders = new Headers();
        myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
        myHeaders.append("Authorization", `Bearer ${get}`);
        myHeaders.append("Content-Type", "application/json");

        var raw = JSON.stringify({
          "coupon": `${coupon}`
        });

        var requestOptions = {
          method: 'POST',
          headers: myHeaders,
          body: raw,
          redirect: 'follow'
        };

        //http://localhost:5016
            //https://lotto-api.firstbetng.com

        fetch(`${url}/api/v1/coupons`, requestOptions)
          .then(response => response.json())
          .then(result => {
              if(result.betCoupons){
                setList(result.betCoupons)
                  setShowCoupon(true)
              }
          })
          .catch(error => console.log('error', error));
    }

    const handleCoupon = e => {
        e.preventDefault()
        if(e.target.value.includes('lottoexpress')){
            setCoupon(e.target.value)
        }else{
            setSuccess('You can only view Lotto Express coupons here')
            return;
        }

    }
    

    const get = localStorage.getItem('token')

    const handleSubmit = (e) => {
        e.preventDefault()
        var myHeaders = new Headers();
        myHeaders.append("signatures", "95631547ca07a9ca16e1116e577199003e96bf55fb110b3ccbc9ed1c1b2092e8");
        myHeaders.append("Authorization", `Bearer ${get}`);
        myHeaders.append("Content-Type", "application/json");

        const val = betSlip.reduce((total, money) => {
            total += parseInt(money.amount)
            return total;
        }, 0);

        if (user.activated >= 0) {
            if (user.balance > val) {
                if (val < 1) {
                    setSuccess(`Kindly add an amount`)
                    return;
                } else {
                    betSlip.filter((a, i) => {
                        const { amount, amounts, stakeList, stakeList2, type } = a;
                
                        if (amount < 1) {
                            setSuccess(`cannot place bet for ticket number ${i + 1} please add an amount`)
                            return;
                        } else {
                            var raw = JSON.stringify({
                                "stakes": [
                                    {
                                        "coupon": `${id + 'lottoexpress'}`,
                                        "type": `${type}`,
                                        "amounts": `${amounts}`,
                                        "value": `${amount}`,
                                        "numbers1": `${stakeList}`,
                                        "numbers2": `${stakeList2}`
                                    }
                                ]
                            });

                            var requestOptions = {
                                method: 'POST',
                                headers: myHeaders,
                                body: raw,
                                redirect: 'follow'
                            };

                            fetch(`${url}/api/v1/placeLottoExpressStake`, requestOptions)
                                .then(response => response.json())
                                .then(result => {
                                    console.log('lotto express')
                                    console.log(result)
                                    let show = result.result.map((res) => res)
                                    const { type, odd, staked, date, amounts, line, possibleWinning, stakes, stakes1, stakes2, amount, coupon } = show[0];
                                    let response = stakes?.toString()
                                    let response1 = stakes1?.toString()
                                    let response2 = stakes2?.toString()
                                    var myHeaders = new Headers();
                                    myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
                                    myHeaders.append("Authorization", `Bearer ${get}`);
                                    myHeaders.append("Content-Type", "application/json");

                                    var raw = JSON.stringify({
                                        "game": 'Lotto Express',
                                        "amounts": `${amount}`,
                                        "amount": `${amounts}`,
                                        "type": `${type}`,
                                        "odd": `${odd}`,
                                        "line": `${line}`,
                                        "possibleWinning": `${possibleWinning}`,
                                        "staked": `${staked}`,
                                        "stakes": `${response}`,
                                        "stakes1": `${response1}`,
                                        "stakes2": `${response2}`,
                                        "date": `${date}`,
                                        "coupon": `${coupon}`
                                    });

                                    var requestOptions = {
                                        method: 'POST',
                                        headers: myHeaders,
                                        body: raw,
                                        redirect: 'follow'
                                    };

                                    fetch(`${url}/api/v2/auth/betHistory`, requestOptions)
                                        .then(response => response.json())
                                        .then(result => {
                                        console.log(result)
                                            setId(Math.floor(Math.random() * 10000000000) + 1)
                                            const { message } = result.success;
                                            setMode(message)
                                            setOpen(!open)
                                            var myHeaders = new Headers();
                                            myHeaders.append("signatures", "5a1131f2eb747be50714281ec3e68b759476c6dc9e1faf5fc5d91c552cf8c230");
                                            myHeaders.append("Authorization", `Bearer ${get}`);

                                            var requestOptions = {
                                                method: 'GET',
                                                headers: myHeaders,
                                                redirect: 'follow'
                                            };

                                            fetch(`${url}/api/v2/auth/profile`, requestOptions)
                                                .then(response => response.json())
                                                .then(result => {
                                                    if (result.success) {
                                                        getUser(result.success.data)
                                                        const { data } = result.success;
                                                        showBoard(data)
                                                        //   localStorage.setItem('user', JSON.stringify(data))
                                                    } else {
                                                        return;
                                                    }
                                                },
                                                    (error) => {
                                                        console.log(error)
                                                    });

                                        })
                                        .catch(error => console.log('error', error));
                                })
                                .catch(error => console.log('error', error));
                        }
                    });
                }
            } else {
                setFundModal(!fundModal)
                setAlerts('Please kindly note that you currently do not have sufficient balance to place this bet')
                return;
            }
        } else {
            if (val < 1) {
                setSuccess(`Kindly add an amount`)
                return;
            } else {
                betSlip.filter((a, i) => {
                    const { amount, amounts, stakeList, stakeList2, type } = a;
                    
                    if (amount < 1) {
                        setSuccess(`cannot place bet for ticket number ${i + 1} please add an amount`)
                        return;
                    } else {
                        var raw = JSON.stringify({
                            "stakes": [
                                {
                                    "coupon": `${id + 'lottoexpress'}`,
                                    "type": `${type}`,
                                    "amounts": `${amounts}`,
                                    "value": `${amount}`,
                                    "numbers1": `${stakeList}`,
                                    "numbers2": `${stakeList2}`,
                                    "game": 'Lotto Express'
                                }
                            ]
                        });

                        var requestOptions = {
                            method: 'POST',
                            headers: myHeaders,
                            body: raw,
                            redirect: 'follow'
                        };

                        fetch(`${url}/api/v1/placeLottoExpressStake`, requestOptions)
                            .then(response => response.json())
                            .then(result => {
                                    console.log('lotto express')
                                    console.log(result)
                                let show = result.result.map((res) => res)
                                const { type, odd, staked, date, amounts, line, possibleWinning, stakes, stakes1, stakes2, amount, coupon } = show[0];
                                let response = stakes?.toString()
                                let response1 = stakes1?.toString()
                                let response2 = stakes2?.toString()
                                var myHeaders = new Headers();
                                myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
                                myHeaders.append("Authorization", `Bearer ${get}`);
                                myHeaders.append("Content-Type", "application/json");

                                var raw = JSON.stringify({
                                    "game": 'Lotto Express',
                                    "amount": `${amount}`,
                                    "amounts": `${amounts}`,
                                    "type": `${type}`,
                                    "odd": `${odd}`,
                                    "possibleWinning": `${possibleWinning}`,
                                    "staked": `${staked}`,
                                    "line": `${line}`,
                                    "stakes": `${response}`,
                                    "stakes1": `${response1}`,
                                    "stakes2": `${response2}`,
                                    "date": `${date}`,
                                    "coupon": `${coupon}`
                                });

                                var requestOptions = {
                                    method: 'POST',
                                    headers: myHeaders,
                                    body: raw,
                                    redirect: 'follow'
                                };

                                fetch(`${url}/api/v2/auth/betHistory`, requestOptions)
                                    .then(response => response.json())
                                    .then(result => {
                                        console.log(result)
                                        setId(Math.floor(Math.random() * 10000000000) + 1)
                                        const { message } = result.success;
                                        setMode(message)
                                        setOpen(!open)
                                        var myHeaders = new Headers();
                                        myHeaders.append("signatures", "5a1131f2eb747be50714281ec3e68b759476c6dc9e1faf5fc5d91c552cf8c230");
                                        myHeaders.append("Authorization", `Bearer ${get}`);

                                        var requestOptions = {
                                            method: 'GET',
                                            headers: myHeaders,
                                            redirect: 'follow'
                                        };

                                        fetch(`${url}/api/v2/auth/profile`, requestOptions)
                                            .then(response => response.json())
                                            .then(result => {
                                                if (result.success) {
                                                    getUser(result.success.data)
                                                    const { data } = result.success;
                                                    showBoard(data)
                                                    //   localStorage.setItem('user', JSON.stringify(data))
                                                } else {
                                                    return;
                                                }
                                            },
                                                (error) => {
                                                    console.log(error)
                                                });

                                    })
                                    .catch(error => console.log('error', error));
                            })
                            .catch(error => console.log('error', error));
                    }
                });
            }
        }

        setActiveNums(false)
    }

    useEffect(() => {
        let time = setTimeout(() => {
            setShowAlert(!showAlert)
            setSuccess('')
        }, 3000)

        return () => clearTimeout(time)
    }, [success]);

    useEffect(() => {
        let v = JSON.parse(localStorage.getItem('LottoExpress'))
        if (v?.length > 0) {
            setSlip(true)
            setShowGameModal(!showGameModal)
            setHow(false)
            setCount(v.length)
            setBetSlip(v)
        }
    }, []);

    useEffect(() => {
        let hours = new Date().getHours()
        let mins = new Date().getMinutes()
        let startTime = `${hours}:${mins}`

        if (startTime === '23:59') {
            localStorage.setItem('LottoExpress', [])
        }
    }, []);
    
    const factorial = (num) => {
        if (num === 0 || num === 1) return 1
        for (var i = num - 1; i >= 1; i--) {
            num *= i
        }
        return num
    }

    const perm2 = (n)  => {
      return (n * n - n) / 2
    }

    const perm3 = (n)  => {
      return factorial(n) / factorial(n - 3) / 6
    }

    const perm4 = (n)  => {
      return factorial(n) / factorial(n - 4) / 24
    }

    const perm5 = (n)  => {
      return factorial(n) / factorial(n - 5) / 120
    }


    const createSlip = () => {
        if (array.length > 0) {
            var n = array.length
        }
      var lines
      if (gameType == 'PERM 2' || gameType == 'NAP 2') {
        lines = perm2(n)
      } else if (gameType == 'PERM 3') {
        lines = perm3(n)
      } else if (gameType == 'PERM 4') {
        lines = perm4(n)
      } else if (gameType == 'PERM 5') {
        lines = perm5(n)
      } else if (gameType === '1 BANKER'){
        lines = 89
      } else if (gameType === 'AGAINST') {
        const first_against = array.slice(0, array.indexOf(0))
          const second_against = array.slice(array.indexOf(0) + 1, array[array.length - 1])
          lines = first_against.length * second_against.length
       }
      else {
          lines = 1
        }

        if (gameType === 'AGAINST') {
            let data = {
                id: new Date().getTime().toString(),
                lines: lines,
                type: gameType,
                stakeList: array.slice(0, array.indexOf(0)),
                stakeList2:  array.slice(array.indexOf(0) + 1, array[array.length - 1]),
                amount: 0 || subValue * lines,
                amounts: subValue
            }
            betSlip.unshift(data)
            calculateTotalStake();
            setShowGameModal(!showGameModal)
            setArr([]);
        } else {
            let data = {
                id: new Date().getTime().toString(),
                lines: lines,
                type: gameType,
                stakeList: array,
                amount: 0 || subValue * lines,
                amounts: subValue
            }
            betSlip.unshift(data)
            calculateTotalStake();
            setShowGameModal(!showGameModal)
            setArr([]);
        }
    }

    const calculateTotalStake = () => {
        setValue(0)
        const val = betSlip.reduce((total, money) => {
            total += parseInt(money.amount)
            return total;
        }, 0);
        setNumber(val)
        setCount(betSlip.length)
        localStorage.setItem('LottoExpress', JSON.stringify(betSlip))
    }
    // http://www.v2nmobile.com/api/httpsms.php?u=${email}&p=${pass}&m=${'abelkelly}&r=${09047597017}&s=${senderID}&t=1`

    const calculateTotalStake1 = (newItem) => {
        setValue(0)
        const val = newItem.reduce((total, money) => {
            total += parseInt(money.amount)
            return total;
        }, 0);
        setNumber(val)
        localStorage.setItem('LottoExpress', JSON.stringify(newItem))
    }

    const handleBet = (e) => {
        e.preventDefault()
        setActiveNums(false)
        setGetNums(false)
        if (array.length < 1) {
            setSuccess('Please Select numbers to play')
            return;
        }else{
            if (gameType === 'NAP 1') {
            if (array.length < 1) {
                return;
            } else if (array.length > 1 && array.length < 3) {
                gameType = 'NAP 2'
                setGameType(gameType)
                setSuccess('Your game type has been automatically changed to NAP 2 because the numbers picked are greater than 1; kindly choose a single number only for NAP 1 and proceed or continue with the NAP 2 game type')
                setSlip(true); 
                setHow(false);
                setGetNums(false)
                createSlip()
            } else if (array.length > 2) {
                gameType = 'PERM 2'
                setSuccess('Your game type has been automatically changed to PERM 2 because the numbers picked are greater than 2; kindly choose 2 numbers only for NAP 2 and proceed or continue with the PERM 2 game type')
                setSlip(true); 
                setHow(false);
                setGetNums(false)
                createSlip()
            } else if (array.length > 15) {
                setSuccess('Please Select atmost 15 numbers')
                return;
            }
            else {
                setSlip(true); 
                setHow(false);
                setGetNums(false)
                createSlip()
            }
        }
        else if (gameType === 'NAP 2') {
            if (array.length < 2) {
                setSuccess('Please select atleast 2 numbers')
                return;
            } else if (array.length > 2) {
                gameType = 'PERM 2'
                setGameType(gameType)
                setSuccess('Your game type has been automatically changed to PERM 2 because the numbers picked are greater than 2; kindly choose 2 numbers only for NAP 2 and proceed or continue with the PERM 2 game type')
                setSlip(true); 
                setHow(false);
                setGetNums(false)
                createSlip()
            } else if (array.length > 15) {
                setSuccess('Please Select atmost 15 numbers')
                return;
            } else {
                setSlip(true); 
                setHow(false);
                setGetNums(false)
                createSlip()
            }
        } else if (gameType === 'NAP 3') {
            if (array.length < 3) {
                setSuccess('Please select atleast 3 numbers')
                return;
            } else if (array.length > 3) {
                gameType = 'PERM 2'
                setGameType(gameType)
                setSuccess('Your game type has been automatically changed to PERM 2 because the numbers picked are greater than 3; kindly choose 3 numbers only for NAP 2 and proceed or continue with the PERM 2 game type')
                setSlip(true); 
                setHow(false);
                setGetNums(false)
                createSlip()
            } else if (array.length > 15) {
                setSuccess('Please Select atmost 15 numbers')
                return;
            } else {
                setSlip(true); 
                setHow(false);
                setGetNums(false)
                createSlip()
            }
        } else if (gameType === 'NAP 4') {
            if (array.length < 4) {
                setSuccess('Please select atleast 4 numbers')
                return;
            } else if (array.length > 4) {
                gameType = 'PERM 2'
                setGameType(gameType)
                setSuccess('Your game type has been automatically changed to PERM 2 because the numbers picked are greater than 4; kindly choose 4 numbers only for NAP 2 and proceed or continue with the PERM 2 game type')
                setSlip(true); 
                setHow(false);
                setGetNums(false)
                createSlip()
            } else if (array.length > 15) {
                setSuccess('Please Select atmost 15 numbers')
                return;
            } else {
                setSlip(true); 
                setHow(false);
                setGetNums(false)
                createSlip()
            }
        } else if (gameType === 'NAP 5') {
            if (array.length < 5) {
                setSuccess('Please select atleast 5 numbers')
                return;
            } else if (array.length > 5) {
                gameType = 'PERM 2'
                setGameType(gameType)
                setSuccess('Your game type has been automatically changed to PERM 2 because the numbers picked are greater than 5; kindly choose 5 numbers only for NAP 2 and proceed or continue with the PERM 2 game type')
                setSlip(true); 
                setHow(false);
                setGetNums(false)
                createSlip()
            } else if (array.length > 15) {
                setSuccess('Please Select atmost 15 numbers')
                return;
            } else {
                setSlip(true); 
                setHow(false);
                setGetNums(false);
                createSlip();
            }
        } else if (gameType === 'PERM 2') {
            if (array.length < 3) {
                setSuccess('Please Select atleast 3 numbers')
                return
            } else if (array.length > 15) {
                setSuccess('Please Select atmost 15 numbers')
                return;
            } else {
                setSlip(true); 
                setHow(false);
                setGetNums(false)
                createSlip()
            }
        } else if (gameType === 'PERM 3') {
            if (array.length < 4) {
                setSuccess('Please Select atleast 4 numbers')
                return;
            } else if (array.length > 15) {
                setSuccess('Please Select atmost 15 numbers')
                return;
            } else {
                setSlip(true); 
                setHow(false);
                setGetNums(false)
                createSlip()
            }
        } else if (gameType === 'PERM 4') {
            if (array.length < 5) {
                setSuccess('Please Select atleast 5 numbers')
                return;
            } else if (array.length > 15) {
                setSuccess('Please Select atmost 15 numbers')
                return;
            } else {
                setSlip(true); 
                setHow(false);
                setGetNums(false)
                createSlip()
            }
        } else if (gameType === 'PERM 5') {
            if (array.length < 6) {
                setSuccess('Please Select atleast 6 numbers')
                return;
            } else if (array.length > 15) {
                setSuccess('Please Select atmost 15 numbers')
                return;
            } else {
                setSlip(true); 
                setHow(false);
                setGetNums(false);
                createSlip()
            }
        } else if (gameType === '1 BANKER') {
            if (!array.length) {
                setSuccess('Please Select a single number')
                return
            }
            let y = array[array.length - 1]
            array = y
            setSlip(true); 
            setHow(false);
            setGetNums(false)
            createSlip()
        } else if (gameType === 'AGAINST') {
                    let max = 20
                    let min = 1
                    const first_against = array.slice(0, array.indexOf(0))
                    const second_against = array.slice(array.indexOf(0) + 1, arr[arr.length - 1])
                if ((first_against.length === 0 || second_against.length === 0)) {
                    setSuccess('please you need to choose two set of numbers for AGAINST games')
                    return;
                } else if (first_against.length === min && second_against.length === min) {
                    setSuccess('please choose atleast one 1 number for either side of the games and more than one number for the other side')
                    return;
                } else if ((first_against.length + second_against.length) > max) {
                    setSuccess('please total number of selected numbers should not exceed 20')
                    return;
                } else {
                    setSlip(true);
                    setHow(false);
                    setFirstAgainst([])
                    setGetNums(false)
                    createSlip()
                }
        } else {
            setSlip(true); 
            setHow(false);
            setGetNums(false);
            createSlip()
        }
        }
    }


    const removeItem = (id) => {
        let newItem = betSlip.filter((item) => item.id !== id)
        if (newItem.length < 1) {
            setBetSlip([])
            setHow(true)
            setSlip(false)
            setCount(0)
            setShowGameModal(false)
            setNumber(0)
            calculateTotalStake1([])
        } else {
            setBetSlip(newItem)
            setCount(count -= 1)
            // console.log(betSlip)
            calculateTotalStake1(newItem)
        }
        // setBetSlip(newItem)
    }

    const removeItems = id => {
        let newItems = list.filter((item) => item.id !== id)
        if(newItems.length < 1){
            setList([])
            setShowCoupon(false)
        }else{
            setList(newItems)
        }
    }

    const Completionist = ({setDay}) => {
        // setDay(Date.now())
        return <p>Games Drawn</p>
    }

    useEffect(() => {
        const loggedInUser = localStorage.getItem('time')
        if (loggedInUser ) {
            const foundTime = JSON.parse(loggedInUser)
            setDay(foundTime)
        }
    }, [])

    useEffect(() => {
        const timeInterval = setInterval(() => {
          setTimer(moment().format('LTS'))
        }, 500)

        return () => clearInterval(timeInterval)
    })

    const handleClass = (i) => {

        if (gameType === 'NAP 1' || gameType === '1 BANKER') {
            setActiveNums((state) => {
                return {
                    ...state,
                    [i]: !state[i],
                };
            });

            if (array.includes(i)) {
                const index = array.indexOf(i)
                if (index > -1) {
                    array.splice(index, 1)
                }
            } else {
                setArray([i])
                setGetNums((state) => {
                    return {
                        ...state,
                        [i]: !state[i],
                    };
                });

            }
        } else if (gameType === 'NAP 2') {
            setActiveNums((state) => {
                return {
                    ...state,
                    [i]: !state[i],
                };
            });

            if (array.includes(i)) {
                const index = array.indexOf(i)
                if (index > -1) {
                    array.splice(index, 1)
                }
            } else {
                if (array.length < 2) {
                    array.push(i)
                } else {
                    setSuccess('Please unselect a number to select new one')
                    setGetNums((state) => {
                        return {
                            ...state,
                            [i]: !state[i],
                        };
                    });
                }
            }
        } else if (gameType === 'NAP 3') {
            setActiveNums((state) => {
                return {
                    ...state,
                    [i]: !state[i],
                };
            });

            if (array.includes(i)) {
                const index = array.indexOf(i)
                if (index > -1) {
                    array.splice(index, 1)
                }
            } else {
                if (array.length < 3) {
                    array.push(i)
                } else {
                    setSuccess('Please unselect a number to select new one')
                    setGetNums((state) => {
                        return {
                            ...state,
                            [i]: !state[i],
                        };
                    });
                }
            }
        } else if (gameType === 'NAP 4') {
            setActiveNums((state) => {
                return {
                    ...state,
                    [i]: !state[i],
                };
            });

            if (array.includes(i)) {
                const index = arr.indexOf(i)
                if (index > -1) {
                    array.splice(index, 1)
                }
            } else {
                if (array.length < 4) {
                    array.push(i)
                } else {
                    setSuccess('Please unselect a number to select new one')
                    setGetNums((state) => {
                        return {
                            ...state,
                            [i]: !state[i],
                        };
                    });
                }
            }
        } else if (gameType === 'NAP 5') {
            setActiveNums((state) => {
                return {
                    ...state,
                    [i]: !state[i],
                };
            });

            if (array.includes(i)) {
                const index = array.indexOf(i)
                if (index > -1) {
                    array.splice(index, 1)
                }
            } else {
                if (array.length < 5) {
                    array.push(i)
                } else {
                    setSuccess('Please unselect a number to select new one')
                    setGetNums((state) => {
                        return {
                            ...state,
                            [i]: !state[i],
                        };
                    });
                }
            }
        } else if (gameType === 'PERM 2' || gameType === 'PERM 3' || gameType === 'PERM 4' || gameType === 'PERM 5') {
            setActiveNums((state) => {
                return {
                    ...state,
                    [i]: !state[i],
                };
            });

            if (array.includes(i)) {
                const index = array.indexOf(i)
                if (index > -1) {
                    array.splice(index, 1)
                }
            } else {
                if (array.length < 15) {
                    array.push(i)
                } else {
                    setSuccess('Please unselect a number to select new one')
                    setGetNums((state) => {
                        return {
                            ...state,
                            [i]: !state[i],
                        };
                    });
                }
            }
        } else if (gameType === 'AGAINST') {
            setActiveNums((state) => {
                return {
                    ...state,
                    [i]: !state[i],
                };
            });

            if (array.includes(i)) {
                const index = array.indexOf(i)
                if (index > -1) {
                    array.splice(index, 1)
                }
            } else {
                const first_against = array.slice(0, array.indexOf(0))
                const second_against = array.slice(array.indexOf(0) + 1, array[array.length - 1])
                sec.push(second_against)
                if ((second_against.length + first_against.length) < 20) {
                    array.push(i)
                } else {
                    setSuccess('Please unselect a number to select new one')
                    setGetNums((state) => {
                        return {
                            ...state,
                            [i]: !state[i],
                        };
                    });
                }
            }
        } else {
            setSuccess('Please Choose a valid game type')
            return;
        }

    }


        useEffect(() => {
        var myHeaders = new Headers();
        myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");

        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };

        fetch(`${url}/api/v1/gamemaxamount`, requestOptions)
            .then(response => response.json())
            .then(result => {
                result.showMax.map((value) => {
                    if (value.type === 'Lotto Express') {
                        setExpressMax(value.value)
                    } else {
                        return;
                    }
                })
            })
            .catch(error => console.log('error', error));
        
        }, [])
    
    const handleInputChange = (e) => {
        e.preventDefault()
        let value = e.target.value
        setValue(value)
    }

        useEffect(() => {
        var myHeaders = new Headers();
        myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");

        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };

        fetch(`${url}/api/v1/gamemaxamount`, requestOptions)
            .then(response => response.json())
            .then(result => {
                result.showMax.map((value) => {
                    if (value.type === 'General') {
                        setGeneral(value.value)
                    } else {
                        return;
                    }
                })
            })
            .catch(error => console.log('error', error));
        
    }, [])


    useEffect(() => {
        if (window.innerWidth > 770) {
            setShowGameModal(false)
        }
    }, [])

    const handleInputSubmit = (e, data) => {
        e.preventDefault()
        setSubValue(value)
        data.amount = value * data.lines;
        data.amounts = parseInt(value)
        setValue(0)
        calculateTotalStake()
    }

    const handleChange = (e) => {
        e.preventDefault()
        setGetNums(false)
        setFirstAgainst([]);
        setArray([])
        setHideButton(false);
        setActiveNums(false)
        let value = e.target.value;
        if (value === 'AGAINST') {
            setFirstAgainst([]);
            setHideButton(false);
            setHideButton(false)
        }
        setGameType(value)
    }
  
    
    
    const handleRandom = e => {
        e.preventDefault()
        if (gameType === 'NAP 1' || gameType === '1 BANKER') {
            let ar = []
            if (showAll.length > 0) {
                let num = generateRandom(90, showAll);
                function generateRandom(max, except) {
                    const exceptSet = new Set(except);
                    let result;

                    do {
                        result = Math.floor(Math.random() * max) + 1;
                    } while (exceptSet.has(result));

                    return result;
                }
                setArray([num])
                setGetNums(true);
            } else {
                const number = Math.floor(Math.random() * 90) + 1
                setArray([number])
                setGetNums(true);
            }
        } else if (gameType === 'NAP 2') {
            let ar = []
            if (showAll.length > 0) {
                let num = generateRandom(90, showAll);
                let num1 = generateRandom(90, showAll);
                let num2 = generateRandom(90, showAll);
                function generateRandom(max, except) {
                    const exceptSet = new Set(except);
                    let result;

                    do {
                        result = Math.floor(Math.random() * max) + 1;
                    } while (exceptSet.has(result));

                    return result;
                }
                ar = [num, num1]
                let a = new Set(ar)
                let b = [...a]
                if (b.length !== 2) {
                    b.push(num2)
                    setArray(b)
                    setGetNums(true);
                } else {
                    setArray(b)
                    setGetNums(true);
                }
            } else {
                const number = Math.floor(Math.random() * 90) + 1
                const number1 = Math.floor(Math.random() * 90) + 1
                const number2 = Math.floor(Math.random() * 90) + 1
                ar = [number, number1]
                let a = new Set(ar)
                let b = [...a]
                if (b.length !== 2) {
                    b.push(number2)
                    setArray(b)
                    setGetNums(true);
                } else {
                    setArray(b)
                    setGetNums(true);
                }
            }
        } else if (gameType === 'NAP 3') {
            let ar = []
            if (showAll.length > 0) {
                let num = generateRandom(90, showAll);
                let num1 = generateRandom(90, showAll);
                let num2 = generateRandom(90, showAll);
                let num3 = generateRandom(90, showAll);
                function generateRandom(max, except) {
                    const exceptSet = new Set(except);
                    let result;

                    do {
                        result = Math.floor(Math.random() * max) + 1;
                    } while (exceptSet.has(result));

                    return result;
                }
                ar = [num, num1, num2]
                let a = new Set(ar)
                let b = [...a]
                if (b.length !== 3) {
                    b.push(num3)
                    setArray(b)
                    setGetNums(true);
                } else {
                    setArray(b)
                    setGetNums(true);
                }
            } else {
                const number = Math.floor(Math.random() * 90) + 1
                const number1 = Math.floor(Math.random() * 90) + 1
                const number2 = Math.floor(Math.random() * 90) + 1
                const number3 = Math.floor(Math.random() * 90) + 1
                ar = [number, number1, number2]
                let a = new Set(ar)
                let b = [...a]
                if (b.length !== 3) {
                    b.push(number3)
                    setArray(b)
                    setGetNums(true);
                } else {
                    setArray(b)
                    setGetNums(true);
                }
            }
        } else if (gameType === 'NAP 4') {
            let ar = []
            if (showAll.length > 0) {
                let num = generateRandom(90, showAll);
                let num1 = generateRandom(90, showAll);
                let num2 = generateRandom(90, showAll);
                let num3 = generateRandom(90, showAll);
                let num4 = generateRandom(90, showAll);

                function generateRandom(max, except) {
                    const exceptSet = new Set(except);
                    let result;

                    do {
                        result = Math.floor(Math.random() * max) + 1;
                    } while (exceptSet.has(result));

                    return result;
                }
                ar = [num, num1, num2, num3]
                let numberSet = new Set(ar)
                let a = [...numberSet]
                if (a.length !== 4) {
                    a.push(num4)
                    setArray(a)
                    setGetNums(true);
                } else {
                    setArray(a)
                    setGetNums(true);
                }
            } else {
                const number = Math.floor(Math.random() * 90) + 1
                const number1 = Math.floor(Math.random() * 90) + 1
                const number2 = Math.floor(Math.random() * 90) + 1
                const number3 = Math.floor(Math.random() * 90) + 1
                const number4 = Math.floor(Math.random() * 90) + 1
                ar = [number, number1, number2, number3]
                let numberSet = new Set(ar)
                let a = [...numberSet]
                if (a.length !== 4) {
                    a.push(number4)
                    setArray(a)
                    setGetNums(true);
                } else {
                    setArray(a)
                    setGetNums(true);
                }
            }
        } else if (gameType === 'NAP 5') {
            let ar = []
            if (showAll.length > 0) {
                let num = generateRandom(90, showAll);
                let num1 = generateRandom(90, showAll);
                let num2 = generateRandom(90, showAll);
                let num3 = generateRandom(90, showAll);
                let num4 = generateRandom(90, showAll);
                let num5 = generateRandom(90, showAll);

                function generateRandom(max, except) {
                    const exceptSet = new Set(except);
                    let result;

                    do {
                        result = Math.floor(Math.random() * max) + 1;
                    } while (exceptSet.has(result));

                    return result;
                }
                ar = [num, num1, num2, num3, num4]
                let numberSet = new Set(ar)
                let a = [...numberSet]
                if (a.length !== 5) {
                    a.push(num5)
                    setArray(a)
                    setGetNums(true);
                } else {
                    setArray(a)
                    setGetNums(true);
                }
            } else {
                const number = Math.floor(Math.random() * 90) + 1
                const number1 = Math.floor(Math.random() * 90) + 1
                const number2 = Math.floor(Math.random() * 90) + 1
                const number3 = Math.floor(Math.random() * 90) + 1
                const number4 = Math.floor(Math.random() * 90) + 1
                const num5 = Math.floor(Math.random() * 90) + 1
                ar = [number, number1, number2, number3, number4]
                let numberSet = new Set(ar)
                let a = [...numberSet]
                if (a.length !== 5) {
                    a.push(num5)
                    setArray(a)
                    setGetNums(true);
                } else {
                    setArray(a)
                    setGetNums(true);
                }
            }
        } else if (gameType === 'PERM 2' || gameType === 'PERM 3' || gameType === 'PERM 4') {
            let ar = []
            if (showAll.length > 0) {
                let num = generateRandom(90, showAll);
                let num1 = generateRandom(90, showAll);
                let num2 = generateRandom(90, showAll);
                let num3 = generateRandom(90, showAll);
                let num4 = generateRandom(90, showAll);
                let num5 = generateRandom(90, showAll);

                function generateRandom(max, except) {
                    const exceptSet = new Set(except);
                    let result;

                    do {
                        result = Math.floor(Math.random() * max) + 1;
                    } while (exceptSet.has(result));

                    return result;
                }
                ar = [num, num1, num2, num3, num4]
                let numberSet = new Set(ar)
                let a = [...numberSet]
                if (a.length !== 5) {
                    a.push(num5)
                    setArray(a)
                } else {
                    setArray(a)
                }
            } else {
                const number = Math.floor(Math.random() * 90) + 1
                const number1 = Math.floor(Math.random() * 90) + 1
                const number2 = Math.floor(Math.random() * 90) + 1
                const number3 = Math.floor(Math.random() * 90) + 1
                const number4 = Math.floor(Math.random() * 90) + 1
                const num5 = Math.floor(Math.random() * 90) + 1
                ar = [number, number1, number2, number3, number4]
                let numberSet = new Set(ar)
                let a = [...numberSet]
                if (a.length !== 5) {
                    a.push(num5)
                    setArray(a)
                } else {
                    setArray(a)
                }
            }
        } else if (gameType === 'PERM 5') {
                        let ar = []
            if (showAll.length > 0) {
                let num = generateRandom(90, showAll);
                let num1 = generateRandom(90, showAll);
                let num2 = generateRandom(90, showAll);
                let num3 = generateRandom(90, showAll);
                let num4 = generateRandom(90, showAll);
                let num5 = generateRandom(90, showAll);
                let num6 = generateRandom(90, showAll);

                function generateRandom(max, except) {
                    const exceptSet = new Set(except);
                    let result;

                    do {
                        result = Math.floor(Math.random() * max) + 1;
                    } while (exceptSet.has(result));

                    return result;
                }
                ar = [num, num1, num2, num3, num4, num5]
                let numberSet = new Set(ar)
                let a = [...numberSet]
                if (a.length !== 6) {
                    a.push(num6)
                    setArray(a)
                } else {
                    setArray(a)
                }
            } else {
                const number = Math.floor(Math.random() * 90) + 1
                const number1 = Math.floor(Math.random() * 90) + 1
                const number2 = Math.floor(Math.random() * 90) + 1
                const number3 = Math.floor(Math.random() * 90) + 1
                const number4 = Math.floor(Math.random() * 90) + 1
                const num5 = Math.floor(Math.random() * 90) + 1
                const num6 = Math.floor(Math.random() * 90) + 1
                ar = [number, number1, number2, number3, number4, num5]
                let numberSet = new Set(ar)
                let a = [...numberSet]
                if (a.length !== 6) {
                    a.push(num6)
                    setArray(a)
                } else {
                    setArray(a)
                }
            }
        } else {
            setSuccess('Please Pick games manually for this game type')
            return;
        }
    }

    const handleCat = (e, data) => {
        if(isNaN(e.target.value)){
            return;
        }else{
        e.target.value * data.lines >  parseInt(general) && setSuccess(`for games whose staking total amount is more than ${general} naira the odds would be made dynamic`)
        data.amounts = e.target.value;
        data.amount = e.target.value * data.lines;
        calculateTotalStake()
        }
    }

    let v = [];

    if (array.includes(0)) {
        v.push(array.slice(array.indexOf(0) + 1, array[array.length - 1]))
    }

    // const handleCouponBet = e => {
    //     e.preventDefault()
    //     const val = list.reduce((total, money) => {
    //         total += parseInt(money.amounts)
    //         return total;
    //     }, 0);

    //     if (user.balance > val) {
    //         if (val < 1) {
    //             setSuccess(`Kindly add an amount`)
    //             return;
    //         } else {
    //             var myHeaders = new Headers();
    //             myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
    //             myHeaders.append("Authorization", `Bearer ${get}`);
    //             myHeaders.append("Content-Type", "application/json");
    //             myHeaders.append("Cookie", "connect.sid=s%3Ae0Zu5esOyG3kUqFKLpyqNN4lka0d4o3F.WngMLxSIx3GfvGfOlQDWPwr%2BeLG2ABZ6sABSEo4Hwkc");
          
    //             list.filter((bet, i) => {
    //                 const { coupon, type, gameId, stakes, stakes1, stakes2, amount, amounts } = bet;
    //                 let stake1s = [stakes1];
    //                 let stake2s = [stakes2];

    //                 if (amount < 1) {
    //                     setSuccess(`cannot place bet for ticket number ${i + 1} please add an amount`)
    //                     return;
    //                 } else {
                            
    //                     var raw = JSON.stringify({
    //                         "totalStake": `${number}`,
    //                         "stakes": [
    //                             {
    //                                 "coupon": `${coupon}`,
    //                                 "amount": `${amount}`,
    //                                 "amounts": `${amounts}`,
    //                                 "type": `${type}`,
    //                                 "stakeList": `${stakes}`,
    //                                 "stakeList1": `${stakes1}`,
    //                                 "stakeList2": `${stakes2}`,
    //                                 "gameId": `${gameId}`,
    //                                 "game": "Regular Lotto"
    //                             },
    //                         ]
    //                     });

    //                     var requestOptions = {
    //                         method: 'POST',
    //                         headers: myHeaders,
    //                         body: raw,
    //                         redirect: 'follow'
    //                     };

    //                     fetch(`${url}/api/v1/placeStake`, requestOptions)
    //                         .then(response => response.json())
    //                         .then(result => {
    //                             if (result.result) {
    //                                 setId(Math.floor(Math.random() * 10000000000) + 1)
    //                                 let show = result.result.map((res) => res)
    //                                 if (show[0].type === 'AGAINST') {
    //                                     const { type, amount, odd, staked, coupon, date, amounts, possibleWinning, gameId, stakes1, stakes2 } = show[0];
    //                                     let response1 = stakes1.toString()
    //                                     let response2 = stakes2.toString()
    //                                     var myHeaders = new Headers();
    //                                     myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
    //                                     myHeaders.append("Authorization", `Bearer ${get}`);
    //                                     myHeaders.append("Content-Type", "application/json");

    //                                     var raw = JSON.stringify({
    //                                         "game": 'Regular Lotto',
    //                                         "amount": `${amount}`,
    //                                         "amounts": `${amounts}`,
    //                                         "type": `${type}`,
    //                                         "odd": `${odd}`,
    //                                         "possibleWinning": `${possibleWinning}`,
    //                                         "staked": `${amount}`,
    //                                         "stakes1": `${response1}`,
    //                                         "stakes2": `${response2}`,
    //                                         "date": `${date}`,
    //                                         "coupon": `${coupon}`,
    //                                         "gameId": `${gameId}`
    //                                     });

    //                                     var requestOptions = {
    //                                         method: 'POST',
    //                                         headers: myHeaders,
    //                                         body: raw,
    //                                         redirect: 'follow'
    //                                     };

    //                                     fetch(`${url}/api/v2/auth/betHistory`, requestOptions)
    //                                         .then(response => response.json())
    //                                         .then(result => {
    //                                             console.log(result)
    //                                             if (result.success) {
    //                                                 const { message } = result.success;
    //                                                 setMode(message)
    //                                                 setOpen(!open)
    //                                                 var myHeaders = new Headers();
    //                                                 myHeaders.append("signatures", "5a1131f2eb747be50714281ec3e68b759476c6dc9e1faf5fc5d91c552cf8c230");
    //                                                 myHeaders.append("Authorization", `Bearer ${get}`);

    //                                                 var requestOptions = {
    //                                                     method: 'GET',
    //                                                     headers: myHeaders,
    //                                                     redirect: 'follow'
    //                                                 };

    //                                                 fetch(`${url}/api/v2/auth/profile`, requestOptions)
    //                                                     .then(response => response.json())
    //                                                     .then(result => {
    //                                                         if (result.success) {
    //                                                             getUser(result.success.data)
    //                                                             const { data } = result.success;
    //                                                             showBoard(data)
    //                                                             //   localStorage.setItem('user', JSON.stringify(data))
    //                                                         } else {
    //                                                             return;
    //                                                         }
    //                                                     },
    //                                                         (error) => {
    //                                                             console.log(error)
    //                                                         });

    //                                             } else {
    //                                                 console.log(result)
    //                                             }
    //                                         })
    //                                         .catch(error => console.log('error', error));

                    
    //                                 } else if (show[0].type === '1 BANKER') {
    //                                     const { type, amount, coupon, odd, staked, amounts, date, possibleWinning, gameId, stakes } = show[0];
    //                                     let response = stakes.toString()
    //                                     var myHeaders = new Headers();
    //                                     myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
    //                                     myHeaders.append("Authorization", `Bearer ${get}`);
    //                                     myHeaders.append("Content-Type", "application/json");

    //                                     var raw = JSON.stringify({
    //                                         "game": 'Regular Lotto',
    //                                         "amount": `${amount}`,
    //                                         "amounts": `${amounts}`,
    //                                         "type": `${type}`,
    //                                         "odd": `${odd}`,
    //                                         "amounts": `${amount}`,
    //                                         "possibleWinning": `${possibleWinning}`,
    //                                         "staked": `${staked}`,
    //                                         "stakes": `${response}`,
    //                                         "date": `${date}`,
    //                                         "coupon": `${coupon}`,
    //                                         "gameId": `${gameId}`
    //                                     });

    //                                     var requestOptions = {
    //                                         method: 'POST',
    //                                         headers: myHeaders,
    //                                         body: raw,
    //                                         redirect: 'follow'
    //                                     };

    //                                     fetch(`${url}/api/v2/auth/betHistory`, requestOptions)
    //                                         .then(response => response.json())
    //                                         .then(result => {
    //                                             console.log(result)
    //                                             if (result.success) {
    //                                                 const { message } = result.success;
    //                                                 setMode(message)
    //                                                 setOpen(!open)
    //                                                 var myHeaders = new Headers();
    //                                                 myHeaders.append("signatures", "5a1131f2eb747be50714281ec3e68b759476c6dc9e1faf5fc5d91c552cf8c230");
    //                                                 myHeaders.append("Authorization", `Bearer ${get}`);

    //                                                 var requestOptions = {
    //                                                     method: 'GET',
    //                                                     headers: myHeaders,
    //                                                     redirect: 'follow'
    //                                                 };

    //                                                 fetch(`${url}/api/v2/auth/profile`, requestOptions)
    //                                                     .then(response => response.json())
    //                                                     .then(result => {
    //                                                         if (result.success) {
    //                                                             getUser(result.success.data)
    //                                                             const { data } = result.success;
    //                                                             showBoard(data)
    //                                                             //   localStorage.setItem('user', JSON.stringify(data))
    //                                                         } else {
    //                                                             return;
    //                                                         }
    //                                                     },
    //                                                         (error) => {
    //                                                             console.log(error)
    //                                                         });

    //                                             } else {
    //                                                 setSuccess('Check your internet, refresh and try again. Thanks.')
    //                                                 return;
    //                                             }
    //                                         })
    //                                         .catch(error => console.log('error', error));
    //                                 } else {
    //                                     const { type, amount, coupon, odd, staked, date, amounts, possibleWinning, stakes, gameId } = show[0];
    //                                     let response = stakes.toString()
    //                                     var myHeaders = new Headers();
    //                                     myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
    //                                     myHeaders.append("Authorization", `Bearer ${get}`);
    //                                     myHeaders.append("Content-Type", "application/json");

    //                                     var raw = JSON.stringify({
    //                                         "game": 'Regular Lotto',
    //                                         "amount": `${amount}`,
    //                                         "type": `${type}`,
    //                                         "odd": `${odd}`,
    //                                         "amounts": `${amounts}`,
    //                                         "possibleWinning": `${possibleWinning}`,
    //                                         "staked": `${staked}`,
    //                                         "stakes": `${response}`,
    //                                         "date": `${date}`,
    //                                         "coupon": `${coupon}`,
    //                                         "gameId": `${gameId}`
    //                                     });

    //                                     var requestOptions = {
    //                                         method: 'POST',
    //                                         headers: myHeaders,
    //                                         body: raw,
    //                                         redirect: 'follow'
    //                                     };

    //                                     fetch(`${url}/api/v2/auth/betHistory`, requestOptions)
    //                                         .then(response => response.json())
    //                                         .then(result => {
    //                                             console.log(result)
    //                                             if (result.success) {
    //                                                 const { message } = result.success;
    //                                                 setMode(message)
    //                                                 setOpen(!open)
    //                                                 var myHeaders = new Headers();
    //                                                 myHeaders.append("signatures", "5a1131f2eb747be50714281ec3e68b759476c6dc9e1faf5fc5d91c552cf8c230");
    //                                                 myHeaders.append("Authorization", `Bearer ${get}`);

    //                                                 var requestOptions = {
    //                                                     method: 'GET',
    //                                                     headers: myHeaders,
    //                                                     redirect: 'follow'
    //                                                 };

    //                                                 fetch(`${url}/api/v2/auth/profile`, requestOptions)
    //                                                     .then(response => response.json())
    //                                                     .then(result => {
    //                                                         if (result.success) {
    //                                                             getUser(result.success.data)
    //                                                             const { data } = result.success;
    //                                                             showBoard(data)
    //                                                             //   localStorage.setItem('user', JSON.stringify(data))
    //                                                         } else {
    //                                                             return;
    //                                                         }
    //                                                     },
    //                                                         (error) => {
    //                                                             console.log(error)
    //                                                         });

    //                                             } else {
    //                                                 setSuccess('Check your internet, refresh and try again. Thanks.')
    //                                                 return;
    //                                             }
    //                                         })
    //                                         .catch(error => console.log('error', error));
    //                                 }
    //                             } else {
    //                                 return
    //                             }
                
    //                         },
    //                             (error) => {
    //                                 console.log(error)
    //                             }
    //                         );
    //                 }
    //             })
    
    //         }
    //     } else {
    //         setSuccess('Please kindly note that you currently do not have sufficient balance to place this bet')
    //         return;
    //     }
    // }



    const handleCouponBet = e => {
        e.preventDefault()
        const val = list.reduce((total, money) => {
            total += parseInt(money.amounts)
            return total;
        }, 0);
        if (user.balance > val) {
            if (val < 1) {
                setSuccess(`Kindly add an amount`)
                return;
            } else {

                console.log('hello world')
                var myHeaders = new Headers();
                myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
                myHeaders.append("Authorization", `Bearer ${get}`);
                myHeaders.append("Content-Type", "application/json");
                myHeaders.append("Cookie", "connect.sid=s%3Ae0Zu5esOyG3kUqFKLpyqNN4lka0d4o3F.WngMLxSIx3GfvGfOlQDWPwr%2BeLG2ABZ6sABSEo4Hwkc");

                console.log(list)

                list.filter((bet, i) => {
                    const { coupon, type, gameId, stakes, stakes1, stakes2, amount, line, gameName, amounts, status } = bet;


                    if (amount < 1) {
                        setSuccess(`cannot place bet for ticket number ${i + 1} please add an amount`)
                        return;
                    } else if (status !== 'PENDING') {
                        setSuccess(`Cannot place bet on expired coupons`)
                        return;
                    } else {
                        if (amount > user.wallet && amount > user.withdrawable) {
                            setFundModal('kindly fund your account to play')
                            return;
                        } else {

                            var raw = JSON.stringify({
                                "totalStake": `${number}`,
                                "stakes": [
                                    {
                                        "coupon": `${id + 'lottoexpress'}`,
                                        "value": `${amount}`,
                                        "amounts": `${amounts}`,
                                        "type": `${type}`,
                                        "numbers1": `${stakes}`,
                                        "numbers2": `${stakes1}`,
                                    },
                                ]
                            });

                            var requestOptions = {
                                method: 'POST',
                                headers: myHeaders,
                                body: raw,
                                redirect: 'follow'
                            };

                            fetch(`${url}/api/v1/placeLottoExpressStake`, requestOptions)
                                .then(response => response.json())
                                .then(result => {
                                    console.log(result)
                                    if (result.result) {
                                        let show = result.result.map((res) => res)
                                        if (show[0].type === 'AGAINST') {
                                            const { type, amount, odd, line, staked, gameName, coupon, date, amounts, possibleWinning, gameId, stakes1, stakes2 } = show[0];
                                            let response1 = stakes1.toString()
                                            let response2 = stakes2.toString()
                                            var myHeaders = new Headers();
                                            myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
                                            myHeaders.append("Authorization", `Bearer ${get}`);
                                            myHeaders.append("Content-Type", "application/json");

                                            var raw = JSON.stringify({
                                                "game": 'Lotto Express',
                                                "amount": `${amount}`,
                                                "amounts": `${amounts}`,
                                                "type": `${type}`,
                                                "line": `${line}`,
                                                "gameName": `${gameName}`,
                                                "odd": `${odd}`,
                                                "possibleWinning": `${possibleWinning}`,
                                                "staked": `${amount}`,
                                                "stakes1": `${response1}`,
                                                "stakes2": `${response2}`,
                                                "coupon": `${coupon}`,
                                                "gameId": `${gameId}`
                                            });

                                            var requestOptions = {
                                                method: 'POST',
                                                headers: myHeaders,
                                                body: raw,
                                                redirect: 'follow'
                                            };

                                            fetch(`${url}/api/v2/auth/betHistory`, requestOptions)
                                                .then(response => response.json())
                                                .then(result => {
                                                    if (result.success) {
                                                        setId(Math.floor(Math.random() * 10000000000) + 1)
                                                        const { message } = result.success;
                                                        setSuccess(message)
                                                        var myHeaders = new Headers();
                                                        myHeaders.append("signatures", "5a1131f2eb747be50714281ec3e68b759476c6dc9e1faf5fc5d91c552cf8c230");
                                                        myHeaders.append("Authorization", `Bearer ${get}`);

                                                        var requestOptions = {
                                                            method: 'GET',
                                                            headers: myHeaders,
                                                            redirect: 'follow'
                                                        };

                                                        fetch(`${url}/api/v2/auth/profile`, requestOptions)
                                                            .then(response => response.json())
                                                            .then(result => {
                                                                if (result.success) {
                                                                    getUser(result.success.data)
                                                                    const { data } = result.success;
                                                                    showBoard(data)
                                                                    //   localStorage.setItem('user', JSON.stringify(data))
                                                                } else {
                                                                    return;
                                                                }
                                                            },
                                                                (error) => {
                                                                    console.log(error)
                                                                });

                                                    } else {
                                                        console.log(result)
                                                    }
                                                })
                                                .catch(error => console.log('error', error));

                    
                                        } else if (show[0].type === '1 BANKER') {
                                            const { type, amount, coupon, odd, gameName, line, staked, amounts, date, possibleWinning, gameId, stakes } = show[0];
                                            let response = stakes.toString()
                                            var myHeaders = new Headers();
                                            myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
                                            myHeaders.append("Authorization", `Bearer ${get}`);
                                            myHeaders.append("Content-Type", "application/json");

                                            var raw = JSON.stringify({
                                                "game": 'Lotto Express',
                                                "amount": `${amount}`,
                                                "amounts": `${amounts}`,
                                                "type": `${type}`,
                                                "odd": `${odd}`,
                                                "line": `${line}`,
                                                "amounts": `${amount}`,
                                                "gameName": `${gameName}`,
                                                "possibleWinning": `${possibleWinning}`,
                                                "staked": `${staked}`,
                                                "stakes": `${response}`,
                                                "coupon": `${coupon}`,
                                                "gameId": `${gameId}`
                                            });

                                            var requestOptions = {
                                                method: 'POST',
                                                headers: myHeaders,
                                                body: raw,
                                                redirect: 'follow'
                                            };

                                            fetch(`${url}/api/v2/auth/betHistory`, requestOptions)
                                                .then(response => response.json())
                                                .then(result => {
                                                    console.log(result)
                                                    if (result.success) {
                                                        setId(Math.floor(Math.random() * 10000000000) + 1)
                                                        const { message } = result.success;
                                                        setSuccess(message)
                                                        var myHeaders = new Headers();
                                                        myHeaders.append("signatures", "5a1131f2eb747be50714281ec3e68b759476c6dc9e1faf5fc5d91c552cf8c230");
                                                        myHeaders.append("Authorization", `Bearer ${get}`);

                                                        var requestOptions = {
                                                            method: 'GET',
                                                            headers: myHeaders,
                                                            redirect: 'follow'
                                                        };

                                                        fetch(`${url}/api/v2/auth/profile`, requestOptions)
                                                            .then(response => response.json())
                                                            .then(result => {
                                                                if (result.success) {
                                                                    getUser(result.success.data)
                                                                    const { data } = result.success;
                                                                    showBoard(data)
                                                                    //   localStorage.setItem('user', JSON.stringify(data))
                                                                } else {
                                                                    return;
                                                                }
                                                            },
                                                                (error) => {
                                                                    console.log(error)
                                                                });

                                                    } else {
                                                        setSuccess('refresh and try again. Thanks.')
                                                        return;
                                                    }
                                                })
                                                .catch(error => console.log('error', error));
                                        } else {
                                            const { type, amount, coupon, line, odd, gameName, staked, date, amounts, possibleWinning, stakes, gameId } = show[0];
                                            let response = stakes?.toString()
                                            var myHeaders = new Headers();
                                            myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
                                            myHeaders.append("Authorization", `Bearer ${get}`);
                                            myHeaders.append("Content-Type", "application/json");

                                            var raw = JSON.stringify({
                                                "game": 'Lotto Express',
                                                "amount": `${amount}`,
                                                "type": `${type}`,
                                                "odd": `${odd}`,
                                                "amounts": `${amounts}`,
                                                "possibleWinning": `${possibleWinning}`,
                                                "staked": `${staked}`,
                                                "stakes": `${response}`,
                                                "line": `${line}`,
                                                "gameName": `${gameName}`,
                                                "coupon": `${coupon}`,
                                                "gameId": `${gameId}`
                                            });

                                            var requestOptions = {
                                                method: 'POST',
                                                headers: myHeaders,
                                                body: raw,
                                                redirect: 'follow'
                                            };

                                            fetch(`${url}/api/v2/auth/betHistory`, requestOptions)
                                                .then(response => response.json())
                                                .then(result => {
                                                    console.log(result)
                                                    if (result.success) {
                                                        setId(Math.floor(Math.random() * 10000000000) + 1)
                                                        const { message } = result.success;
                                                        setSuccess(message)
                                                        var myHeaders = new Headers();
                                                        myHeaders.append("signatures", "5a1131f2eb747be50714281ec3e68b759476c6dc9e1faf5fc5d91c552cf8c230");
                                                        myHeaders.append("Authorization", `Bearer ${get}`);

                                                        var requestOptions = {
                                                            method: 'GET',
                                                            headers: myHeaders,
                                                            redirect: 'follow'
                                                        };

                                                        fetch(`${url}/api/v2/auth/profile`, requestOptions)
                                                            .then(response => response.json())
                                                            .then(result => {
                                                                if (result.success) {
                                                                    getUser(result.success.data)
                                                                    const { data } = result.success;
                                                                    showBoard(data)
                                                                    //   localStorage.setItem('user', JSON.stringify(data))
                                                                } else {
                                                                    return;
                                                                }
                                                            },
                                                                (error) => {
                                                                    console.log(error)
                                                                });

                                                    } else {
                                                        setSuccess('refresh and try again. Thanks.')
                                                        return;
                                                    }
                                                })
                                                .catch(error => console.log('error', error));
                                        }
                                    } else {
                                        return;
                                    }
                
                                },
                                    (error) => {
                                        console.log(error)
                                    }
                                );
                        }
                    }
                    // }
                })
    
            }
        } else {
            setSuccess('Please kindly note that you currently do not have sufficient balance to place this bet')
            return;
        }
    }


    const handleV = (e, items) => {
        e.preventDefault()
        if (isNaN(e.target.value) || e.target.value < 1) {
            return;
        } else {
            setListV(e.target.value)
            items.amounts = e.target.value
            items.amount = e.target.value * items.line
            items.staked = e.target.value * items.line
        }
    }

    // const handleAgainst = (e) => {
    //     e.preventDefault()
    //     array.push(0)
    //     setActiveNums(false)
    //     setHideButton(true)
    // }

    const handleAgainst = (e) => {
        e.preventDefault()
        array.push(0)
        const first_against = array.slice(0, array.indexOf(0))
        setFirstAgainst(first_against)
        setActiveNums(false)
        setHideButton(true)
    };

    useEffect(() => {
        let betSlip = JSON.parse(localStorage.getItem('LottoExpress'))
        console.log(betSlip)

        if (betSlip) {
            const val = betSlip.reduce((total, money) => {
                total += parseInt(money.amount)
                return total;
            }, 0);
            
            setNumber(val)
        }


    }, []);

        useEffect(() => {
        const timeInterval = setInterval(() => {
            if ((new Date().getMinutes()) < 30) {
                setTime(`${new Date().getHours()}:30`)
            }else if ((new Date().getMinutes()) >= 30 && (new Date().getMinutes()) < 59) {
                setTime(`${new Date().getHours() + 1}:00`)
            }
        }, 1000)

            return () => clearInterval(timeInterval)
            
                // (function () {
                //     var start = new Date;
                //     start.setHours(23, 0, 0); // 11pm

                //     function pad(num) {
                //         return ("0" + parseInt(num)).substr(-2);
                //     }

                //     function tick() {
                //         var now = new Date;
                //         if (now > start) { // too late, go to tomorrow
                //             start.setDate(start.getDate() + 1);
                //         }
                //         var remain = ((start - now) / 1000);
                //         var hh = pad((remain / 60 / 60) % 60);
                //         var mm = pad((remain / 60) % 60);
                //         var ss = pad(remain % 60);
                //         setTimes(hh + ":" + mm + ":" + ss);
                //         setTimeout(tick, 1000);
                //     }

                //    tick();
                // })();
    });


    return (
        <section className='black_bg'>
                        <div className='news pl-1 mt-2 game_head pl-lg-5 p_white d-flex justify-content-between'>
                <p className="timer pt-2">{timer}</p>
                <Link className='activated first pt-2' to='/lottoexpress'>Lotto Express</Link>
                <Link className='first game_links backgs pt-2' to='/games'>Original Lotto</Link>
                <Link className='game_links backgs first pt-2'  to='/promotion'>7/90</Link>
                <Link className='game_links backgs ml-3 pt-2' to='/softlotto'>Soft Lotto</Link>
                                                        <Link className='pt-2' to='#'>
                                          <p className='game_links d-none d-lg-inline backgs first'>How to Play</p>
                                        </Link>
                                        <Link className='pt-2' to='/fundwallet'>
                                           <p className='game_links backgs'>Deposit Fund</p>
                                        </Link>
                                        <Link className='pt-2' to='#'>
                                           <p className='game_links d-none d-lg-inline backgs'>First Bet</p>
                                        </Link>
                                                                    <div class=" pt-2 d-none d-lg-flex flex-right mr-2">
                                        <Link to='https://www.facebook.com'>
                                           <FontAwesomeIcon className=' backg backgs color3' size-mode='1x' icon={faFacebook} />
                                        </Link>
                                        <Link to='https://www.twitter.com'>
                                           <FontAwesomeIcon className='ml-1 mr-1 backgs backg color4' size-md='1x' icon={faTwitter} />
                                        </Link>
                                        <Link to='https://www.instagram.com'>
                                           <FontAwesomeIcon className='mr-1 backg backgs color5' size-md='1x' icon={faInstagram} />
                                        </Link>
                                        <Link to='https://www.youtube.com'>
                                           <FontAwesomeIcon className=' backg backgs color6'  size-md='1x' icon={faYoutube} />
                                        </Link>       
                </div>
            </div>
        <Container>
            <Row>
                <Col className='d-none d-lg-inline' lg={3}>
                    <section className='mt-2 ml-2 ml-lg-0 mb-2 mb-lg-0 pt-5'>
                        <img src={play} alt="" className='game_section_svg' />
                    </section>
                    
                </Col>
                <Col lg={5} className={`boxplay`}>
                                <div className='d-flex justify-content-center'>
                            <h5 className='yellow game_head'>Lotto Express</h5>
                                                                <Form.Control className='yellow choose mt-3 mb-3' style={{width: '150px', marginTop: '10px'}} as="select" required onChange={(e) => {history.push(e.target.value)}} custom>
                            <option name='lottoexpress' value='/lottoexpress'>Lotto Express (5/90)</option>
                            <option name='games' value='/games'>Original Lotto (5/90)</option>
                            <option name='softlotto' value='/softlotto'>Soft Lotto (3/10)</option>
                            <option name='promotions' value='/promotion'>Extra Lotto (7/90)</option>
                        </Form.Control>
                        </div>
                        
                        <div className='d-flex justify-content-center'>
                            {/* <h4 className='h4_font'>
                                            </h4> */}
                            {/* {gameShow.name && <p className='p_width pr-2' style={{ fontWeight: '800' }}> <span className='span_h4'>{gameShow.name}</span></p>} */}
                            <p className='p_width' style={{ fontWeight: '500' }}> <span className='span_h4'>Results will be drawn: {time} WAT</span></p>
                        </div>
                    {!hideButton && gameType === 'AGAINST' && <Button variant='danger' onClick={handleAgainst} className={`small_class ml-2 mb-2 ml-lg-0 ${array.length > 0 ? '' : 'disabled' }`}>Against</Button>}
                        <div className='d-flex justify-content-between d-lg-none'>
                            <Form className='form-select form-select-sm align-center select_game' onChange={handleChange}>
                                <Form.Group controlId="exampleForm.SelectCustom">
                                    <Form.Control as="select" custom>
                                        <option value='PERM 2'>PERM 2</option>
                                        <option value='NAP 1'>NAP 1</option>
                                        <option value='NAP 2'>NAP 2</option>
                                        <option value='NAP 3'>NAP 3</option>
                                        <option value='NAP 4'>NAP 4</option>
                                        <option value='NAP 5'>NAP 5</option>
                                        <option value='PERM 3'>PERM 3</option>
                                        <option value='PERM 4'>PERM 4</option>
                                        <option value='PERM 5'>PERM 5</option>
                                        <option value='1 BANKER'>1 BANKER</option>
                                        <option value='AGAINST'>AGAINST</option>
                                    </Form.Control>
                                </Form.Group>
                            </Form>
                            <div>
                                {gameType === 'NAP 1' && <p style={{color: '#fff', fontSize: '8px', marginLeft: '11px', marginTop: '7px'}}>Pick a single number And If They Are Drawn You Win Assigns Odds X Your Single Stake Amount.</p>}
                                {gameType === 'NAP 2' && <p style={{color: '#fff', fontSize: '8px', marginLeft: '11px', marginTop: '7px'}}>Pick 2 Numbers And If They Are Drawn You Win Assigns Odds X Your Single Stake Amount.</p>}
                                {gameType === 'PERM 2' && <p style={{ color: '#fff', fontSize: '8px', marginLeft: '11px', marginTop: '7px' }}>Perm Games Are Very Exciting, Please Read Our Guide On This.</p>}
                                {gameType === 'PERM 3' && <p style={{ color: '#fff', fontSize: '8px', marginLeft: '11px', marginTop: '7px' }}>Perm Games Are Very Exciting, Please Read Our Guide On This.</p>}
                                {gameType === 'PERM 4' && <p style={{ color: '#fff', fontSize: '8px', marginLeft: '11px', marginTop: '7px' }}>Perm Games Are Very Exciting, Please Read Our Guide On This.</p>}
                                {gameType === 'PERM 5' && <p style={{ color: '#fff', fontSize: '8px', marginLeft: '11px', marginTop: '7px' }}>Perm Games Are Very Exciting, Please Read Our Guide On This.</p>}
                                {gameType === 'NAP 3' && <p style={{color: '#fff', fontSize: '8px', marginLeft: '11px', marginTop: '7px'}}>Pick 3 Numbers And If They Are Drawn You Win Assigns Odds X Your Single Stake Amount.</p>}
                                {gameType === 'AGAINST' && <p style={{color: '#fff', fontSize: '8px', marginLeft: '11px', marginTop: '7px'}}> Permutations Of Numbers Against Each Other.</p>}
                                {gameType === 'NAP 4' && <p style={{color: '#fff', fontSize: '8px', marginLeft: '11px', marginTop: '7px'}}>Pick 4 Numbers And If They Are Drawn You Win Assigns Odds X Your Single Stake Amount.</p>}
                                {gameType === 'NAP 5' && <p style={{color: '#fff', fontSize: '8px', marginLeft: '11px', marginTop: '7px'}}>Pick 5 Numbers And If They Are Drawn You Win Assigns Odds X Your Single Stake Amount.</p>}
                                {gameType === '1 BANKER' && <p style={{color: '#fff', fontSize: '8px', marginLeft: '11px', marginTop: '7px'}}>1 BANKER: Pick A Number And If Its Drawn You Win Assigns Odds X Your Single Stake Amount.</p> }
                            </div>
                        </div>
                        <Form className='form-select form-select-sm game_head align-center ' onChange={handleChange}>
                            <Form.Group controlId="exampleForm.SelectCustom">
                                <Form.Control as="select" custom>
                                    <option value='PERM 2'>PERM 2</option>
                                    <option value='NAP 1'>NAP 1</option>
                                    <option value='NAP 2'>NAP 2</option>
                                    <option value='NAP 3'>NAP 3</option>
                                    <option value='NAP 4'>NAP 4</option>
                                    <option value='NAP 5'>NAP 5</option>
                                    <option value='PERM 3'>PERM 3</option>
                                    <option value='PERM 4'>PERM 4</option>
                                    <option value='PERM 5'>PERM 5</option>
                                    <option value='1 BANKER'>1 BANKER</option>
                                    <option value='AGAINST'>AGAINST</option>
                                </Form.Control>
                            </Form.Group>
                        </Form>
                        <div className='mt-2 text-md-center'>
                            {/* {nums.map((i) => {
                                return <Button key={i} name={!activeNums[i] && 'ready'} onClick={() => handleClass(i)} className={`${array.includes(i) ? 'game_clicked' : geteNums && 'red'} balx`} variant='outline-primary'>{i}</Button>
                            })} */}
                            {/* {nums.map((i) => {
                                return <button key={i} name={!activeNums[i] && 'ready'} onClick={(e) => handleClass(i)} className={`${array.includes(i) && !v[0]?.includes(i) ? 'game_clicked' : geteNums && !v[0]?.includes(i) && 'red'} balx ${v[0]?.includes(i) && 'games_b'}`} >{i}</button>
                            })} */}
                            {gameType !== 'AGAINST' && nums.map((i) => {
                                return <button key={i} name={!activeNums[i] && 'ready'} onClick={(e) => handleClass(i)} className={`${array.includes(i) && !v[0]?.includes(i) ? 'game_clicked' : geteNums && !v[0]?.includes(i) && 'red'} balx ${showAll.includes(i) && 'reds'} ${v[0]?.includes(i) && 'games_b'}`} variant={`${v[0]?.length > 0 ? 'outline-success' : 'outline-primary'}`}>{i}</button>
                            })}
                            {gameType === 'AGAINST' && nums.map((i) => {
                                return <button key={i} name={!activeNums[i] && 'ready'} onClick={(e) => handleClass(i)} className=
                                    {
                                    `${array.includes(i) && !v[0]?.includes(i) ? 'game_clicked' : geteNums && !v[0]?.includes(i) && 'red'}
                                     balx ${showAll.includes(i) && 'reds'}
                                    ${v[0]?.includes(i) && 'games_b'}
                                    ${firstAgainst.includes(i) && 'disabled'}`
                                } variant={`${v[0]?.length > 0 ? 'outline-success' : 'outline-primary'}`}>{ firstAgainst.includes(i) && <FontAwesomeIcon className='' size='1x' icon={faLock} /> || i}</button>
                            })}
                        </div>
                        <Row className="clear instant_pay mt-2">
                                <Button variant='outline-primary' className='thew' onClick={handleRandom}>Quick Pick</Button>
                            {
                                hideButton && gameType === "AGAINST" &&
                                    <Button variant='success' className='thew' onClick={handleBet}>Play</Button>
                            }
                            {
                                gameType !== "AGAINST" &&
                                    <Button variant='success' className='thew' onClick={handleBet}>Play</Button>
                            }
                            <Button variant='outline-danger' className='thew' onClick={() => { setArray([]); calculateTotalStake1([]); setHow(true); setSlip(false); setCount(0); setHideButton(false); setFirstAgainst([]); setShowGameModal(false)}}>Clear</Button>
                      </Row>
                </Col>
                <Col lg={3} className='show1'>
                        <Row className='mt-2'>
                            <div className={`col-md-6 col-sm-6 mbox1 text-center ${betSlip.length < 1 && 'disabled'}`} onClick={() => { setSlip(true); setHow(false); }}>BetSlip <span class="badge bg-secondary">{count}</span></div>
                            <div className='col-md-5 ml-1 col-sm-6 mbox1 text-center' onClick={() => { setSlip(false); setHow(true); }}><small>Search Coupon</small></div>
                        </Row>
                        <Row>
                        {
                            how ?
                            <div className={`${list.length > 0 && 'scroller'} bet_section col-sm-12`} id="howto">
                                <form className="d-flex flex-column" onSubmit={CouponSubmit}>
                                <input className="form-control m-2" type="text" name='coupon' placeholder="Coupon Code" onChange={handleCoupon}/>
                                <button className="btn size=sm btn-outline-success ml-2 mr-2 mb-2" type="submit" >Load Coupon</button>
          
                            </form>
                             {showCoupon && <div className='show_coupon' >
                                {list.map((items) => {
                                    var {amount, id, odd, results, stakes, line, type, stakes1, amounts, stakes2, status, staked, possibleWinning, game, gameId} = items;
                                    return (
                                       <>
                                        <main className='get_line'>
                                            <div className='d-flex justify-content-end'>
                                                <FaTimes onClick={() => {
                                                   removeItems(id)
                                                }}
                                                    className='cancel_game'
                                                />
                                            </div>
                                               <div className=''>
                                                {status !== 'PENDING' && <p className='redz'>Expired ticket</p>}
                                                <p className='p_type'>possibleWinning: &#x20A6;{possibleWinning}</p>
                                                    <p className='p_type'>Odd: {odd}</p>
                                                {user.wallet && <p className='p_type'>status: {status.toLowerCase()}</p>}
                                                <p className='p_type'>Game Name: {game}</p>
                                                <p className='p_type'>Lines: {line}</p>
                                                <p className='p_type'>Stake Amount: &#x20A6;{staked}</p>
                                                <p className='p_type'>Stake Amount per Line: &#x20A6;{amounts}</p>
                                                {stakes && <p className='p_type'>Bet Numbers: {stakes}</p>}
                                                {stakes1 && <p className='p_type'>Against first: {stakes1}</p>}
                                                {stakes2 && <p className='p_type'>Against second: {stakes2}</p>}
                                                {results && <p className='p_type'>Numbers Drawn {results}</p>}
                                                <p className='p_type'>game Type: {type}</p>
                                                    {status === 'PENDING' && <div>
                                                        <Form.Control className='form_input' pattern='[0-9]' onChange={(e) => handleV(e, items)} value={items.amounts} size='sm' placeholder={amounts} />
                                                        <div className='mt-2 d-flex justify-content-lg-between'>
                                                            <Button className='mr-2 mr-lg-0 games game' value='50' size='sm' onClick={() => {
                                                                setListV(50)
                                                                items.amounts = 50;
                                                                items.amount = 50 * items.line
                                                                items.staked = 50 * items.line
                                                            }}>50
                                                            </Button>
                                                            <Button className='mr-2 mr-lg-0 games game ' size='sm' value='100' size='sm' onClick={() => {
                                                                setListV(100)
                                                                items.amounts = 100;
                                                                items.amount = 100 * items.line
                                                                items.staked = 100 * items.line
                                                            }}>100</Button>
                                                            <Button className='mr-2 mr-lg-0 games game' size='sm' value='500' size='sm' onClick={() => {
                                                                setListV(500)
                                                                items.amounts = 500;
                                                                items.amount = 500 * items.line
                                                                items.staked = 500 * items.line
                                                            }}>500</Button>
                                                            <Button className='mr-2 mr-lg-0 games game' size='sm' value='1000' size='sm' onClick={() => {
                                                                setListV(1000)
                                                                items.amounts = 2500;
                                                                items.amount = 2500 * items.line
                                                                items.staked = 2500 * items.line
                                                            }}>1000</Button>
                                                            <Button className='mr-2 mr-lg-0 games game' size='sm' value='2500' size='sm' onClick={() => {
                                                                setListV(2500)
                                                                items.amounts = 2500;
                                                                items.amount = 2500 * items.line
                                                                items.staked = 2500 * items.line
                                                            }}>2500</Button>
                                                            <Button className='mr-2 mr-lg-0 games game' size='sm' value='5000' size='sm' onClick={() => {
                                                                setListV(5000)
                                                                items.amounts = 5000;
                                                                items.amount = 5000 * items.line
                                                                items.staked = 5000 * items.line
                                                            }}>5000</Button>
                                                        </div>
                                                    </div>
                                                    }
                                               </div>
                                            </main>
                                       </>
                                    )
                                })}
                                <Button size='sm' className={`align-item-center mb-2 game `} variant='success' onClick={handleCouponBet}>Bet Ticket</Button>
                            </div>}
                            </div> :
                                <section className={`${betSlip.length > 1 && 'scroller'} bet_section mt-2`}>
                             <div className='d-flex justify-content-between game_back'>
                            </div>
                            <div>
                                {betSlip.map((data) => {
                                    const { type, lines, id, gameId, stakeList, stakeList2, amount, amounts } = data;
                                    return (
                                        <main key={id} className='get_line'>
                                            <div className='d-flex justify-content-end'>
                                                <FaTimes onClick={() => {
                                                   removeItem(id)
                                                }}
                                                    className='cancel_game'
                                                />
                                            </div>
                                            <div>
                                               <p className='p_type'>Game Name: Lotto Express</p>
                                                <p className='p_type'>Lines: {lines}</p>
                                                <p className='p_type'>Type: {type}</p>
                                                {stakeList && stakeList2 && <p className='p_type'>Stakes: {`${stakeList.toString()} `}</p>}
                                                {type === 'AGAINST' && stakeList && <p className='p_type'>Against: {stakeList2.toString()} </p>}
                                                {gameType !== 'AGAINST' && stakeList && <p className='p_type'>Stakes: {stakeList.toString()} </p>}
                                                <p className='p_type'>Betslip total Amount: {amount}</p>
                                                <p className='p_type'>Amount per Line: {amounts}</p>
                                                <Form.Control className='form_input' onChange={(e) => handleCat(e, data)} value={data.amounts > 0 ? data.amounts : ''} size='sm' placeholder={data.amounts > 0 ? `${data.amounts}` : 'Amount'} />
                                                <div className='mt-2 d-flex justify-content-lg-between'>
                                                <Button className='mr-2 mr-lg-0 games game' value='50' size='sm' onClick={() => {
                                                        data.amount = 50 * lines;
                                                        data.amounts = 50
                                                        calculateTotalStake()
                                                }}>50</Button>
                                                <Button className='mr-2 mr-lg-0 games game 'size='sm' value='100' size='sm' onClick={() => {
                                                        data.amount = 100 * lines;
                                                        data.amounts = 100
                                                        calculateTotalStake()
                                                }}>100</Button>
                                                <Button className='mr-2 mr-lg-0 games game'size='sm' value='500' size='sm' onClick={() => {
                                                        data.amount = 500 * lines;
                                                        data.amounts = 500
                                                        calculateTotalStake()
                                                }}>500</Button>
                                                <Button className='mr-2 mr-lg-0 games game'size='sm' value='1000' size='sm' onClick={() => {
                                                        data.amount = 1000 * lines;
                                                        data.amounts = 1000
                                                        calculateTotalStake()
                                                }}>1000</Button>
                                                <Button className='mr-2 mr-lg-0 games game'size='sm' value='400' size='sm' onClick={() => {
                                                        data.amount = 2500 * lines;
                                                        data.amounts = 2500
                                                        calculateTotalStake()
                                                }}>2500</Button>
                                                <Button className='mr-2 mr-lg-0 games game' size='sm' value='500' size='sm' onClick={() => {
                                                        data.amount = 5000 * lines;
                                                        data.amounts = 5000
                                                        calculateTotalStake()
                                                }}>5000</Button>
                                            </div>
                                        
                                             </div>
                                        </main>
                                        
                                    )
                                })}
                            </div>
                                                                        <section className='mt-2'>
                                                <div className='d-flex justify-content-between'>
                                                   <p className='p_type'>Number of Bets: </p>
                                                   <p className='p_type'>{betSlip.length}</p>
                                                </div>
                                                <div className='d-flex justify-content-between'>
                                                    <p className='p_type'>Total Stake: </p>
                                                   <p className='p_type'>&#x20A6;{number}</p>
                                                </div>
                                                </section>
                                        <div className='d-flex justify-content-center'>
                                {!logedIn && <Button size='sm' autoFocus className={`align-item-center mb-2 game`} variant='success' onClick={() => { setShowModal(!showModal); showGameModal && setShowGameModal(false)}}>Login To Place Bet</Button> }
                                {logedIn &&
                                    <Button size='sm' autoFocus className={`align-item-center mb-2 game`} variant='success' onClick={handleSubmit}>Place Bet</Button>
                                }
                            </div>
                            
                            </section>    
                        }
                        </Row>
                    </Col>
                   
                        <div className={`${!showGameModal ? 'display' : 'c-sidebar --bet-slip is-open'} ${betSlip.length > 0 ? 'd_none scroll_game' : 'display'}`}>
                <section className={`${betSlip.length > 2 && 'scroller'} bet_section mt-2`}>
                    <div className='d-flex justify-content-between game_back'>
                                    <h6 className='game_h6'>
                                        Lotto Express
                                    </h6>
                                <p className='palce_bet' onClick={() =>  setShowGameModal(false)}>Place Another Bet</p>
                                <button className="game_slip_btn mb-1" onClick={() => { setBetSlip([]); calculateTotalStake1([]); setCount(0); setShowGameModal(false)}}>Clear Slip</button>
                            </div>
                            <div>
                                {betSlip.map((data) => {
                                    const { id, stakeList, type, amount, amounts, lines, stakeList2 } = data;
                                    return (
                                        <main key={id} className='get_line'>
                                            <div className='d-flex justify-content-end'>
                                                <FaTimes onClick={() => {
                                                   removeItem(id)
                                                }}
                                                    className='cancel_game'
                                                />
                                            </div>
                                            <div>
                                                <p className='p_type'>Game Name: Lotto Express</p>
                                                <p className='p_type'>Lines: {lines}</p>
                                                <p className='p_type'>Type: {type}</p>
                                                {stakeList && stakeList2 && <p className='p_type'>Stakes: {`${stakeList.toString()} `}</p>}
                                                {type === 'AGAINST' && stakeList && <p className='p_type'>Against: {stakeList2.toString()} </p>}
                                                {gameType !== 'AGAINST' && stakeList && <p className='p_type'>Stakes: {stakeList.toString()} </p>}
                                                <p className='p_type'>Betslip total Amount: {amount}</p>
                                                <p className='p_type'>Amount per Line: {amounts}</p>
                                                <Form.Control className='form_input' onChange={(e) => handleCat(e, data)} value={data.amounts > 0 ? data.amounts : ''} size='sm' placeholder={data.amounts > 0 ? `${data.amounts}` : 'Amount'} />
                                                <div className='mt-2 d-flex justify-content-lg-between'>
                                                <Button className='mr-1 mr-lg-0 games game' value='50' size='sm' onClick={() => {
                                                        data.amount = 50 * lines;
                                                        data.amounts = 50
                                                        calculateTotalStake()
                                                }}>50</Button>
                                                <Button className='mr-2 mr-lg-0 games game 'size='sm' value='100' size='sm' onClick={() => {
                                                        data.amount = 100 * lines;
                                                        data.amounts = 100
                                                        calculateTotalStake()
                                                }}>100</Button>
                                                <Button className='mr-2 mr-lg-0 games game'size='sm' value='500' size='sm' onClick={() => {
                                                        data.amount = 500 * lines;
                                                        data.amounts = 500
                                                        calculateTotalStake()
                                                }}>500</Button>
                                                <Button className='mr-1 mr-lg-0 games game'size='sm' value='1000' size='sm' onClick={() => {
                                                        data.amount = 1000 * lines;
                                                        data.amounts = 1000
                                                        calculateTotalStake()
                                                }}>1000</Button>
                                                <Button className='mr-2 mr-lg-0 games game'size='sm' value='400' size='sm' onClick={() => {
                                                        data.amount = 2500 * lines;
                                                        data.amounts = 2500
                                                        calculateTotalStake()
                                                }}>2500</Button>
                                                <Button className='mr-1 mr-lg-0 games game' size='sm' value='500' size='sm' onClick={() => {
                                                        data.amount = 5000 * lines;
                                                        data.amounts = 5000
                                                        calculateTotalStake()
                                                }}>5000</Button>
                                                <Button className='mr-2 mr-lg-0 games game' size='sm' value='500' size='sm' onClick={() => {
                                                        data.amount = 10000 * lines;
                                                        data.amounts = 10000
                                                        calculateTotalStake()
                                                }}>10000</Button>
                                            </div>
                                             </div>
                                        </main>
                                        
                                    )
                                })}
                            </div>
                                                                        <section className='mt-2'>
                                                <div className='d-flex justify-content-between'>
                                                   <p className='p_type'>Number of Bets: </p>
                                                   <p className='p_type'>{betSlip.length}</p>
                                                </div>
                                                <div className='d-flex justify-content-between'>
                                                    <p className='p_type'>Total Stake: </p>
                                                   <p className='p_type'>&#x20A6;{number}</p>
                                                </div>
                                                </section>
                                        <div className='d-flex justify-content-center'>
                                {!logedIn && <Button size='sm' autoFocus className={`align-item-center mb-2 game`} variant='success' onClick={() => { setShowModal(!showModal); showGameModal && setShowGameModal(false)}}>Login To Place Bet</Button> }
                                {logedIn &&
                                    <Button size='sm' autoFocus className={`align-item-center mb-2 game`} variant='success' onClick={handleSubmit}>Place Bet</Button>
                                }
                            </div>
                            
                </section>
            </div>
                </Row>
                {showMobile && <MobileCoupon value='lotto Express' showMobile={() => setShowMobile(false)} />}
                {showModal && <GetWhatsapp />}
            {fundModal && <FundModal currentUrl={currentUrl} setFundModal={setFundModal} mode={alerts}/>}
                {open && <Bet mode={mode} betSlip={setBetSlip} setOpen={() => {setOpen(false)}} type='lottoexpress' calculateTotalStake1={() => calculateTotalStake1([])} setMode={setMode} number={setNumber} count={setCount}/>}
            {success && <IntegrationNotistack success={`${success}`} />}
                        <section className='bottom'>
                <div className='d-flex'>
                    <div className='game_item' style={{background: '#122d4b', fontWeight: '800'}}>
                        {/* <Button variant='outline-primary' className='thew' onClick={handleRandom}>Quick Pick</Button> */}
                        <p onClick={handleRandom} className='pt-2 pb-2 mb-0 play_sec'>Quick Pick</p>
                    </div>
                    <div className='game_item' style={{background: '#007bff', fontWeight: '800'}}>
                                            {
                            hideButton && gameType === "AGAINST" && sec &&
                            <p onClick={handleBet} className='pt-2 pb-2 mb-0 play_sec' style={{paddingBottom: '10px', }}>Play</p>
                        // <Button variant='success' className='thew' onClick={handleBets}>Play</Button>
                    }
                    {
                        gameType !== "AGAINST" &&
                            <p onClick={handleBet} className='pt-2 pb-2 mb-0 play_sec'>Play</p>
                        // <Button variant='success' className='thew' onClick={handleBets}>Play</Button>
                    }
                    </div>
                    <div className='game_item' style={{background: '#6c757d', fontWeight: '800'}}>
                            <p onClick={() => { setArray([]); setHideButton(false); setFirstAgainst([]); setGetNums(false) }} className='pt-2 mb-0 pb-2 play_sec'>Clear</p>
                                            {/* <Button variant='outline-danger' className='thew' onClick={() => { setArr([]); setHideButton(false); setFirstAgainst([]); setGetNums(false) }}>Clear</Button> */}
                    </div>
                </div>
                <div className='d-flex smaller'>
                   
                    <div className='game_item' onClick={() => { showGameModal && setShowGameModal(false); history.push('/') }} style={{fontWeight: '700'}}>
                        <RiHome2Line className='select_icon' />
                        <span className='select_item'>Home</span>
                    </div>
                    <div className='game_item' onClick={() => { showGameModal && setShowGameModal(false); setShowMobile(!showMobile) }} style={{fontWeight: '700'}}>
                        <IoIosFootball className='select_icon' />
                        <span className='select_item'>Load Coupon</span>
                    </div>
                    <div className={`${betSlip.length > 0 && 'real'} game_item`} onClick={() => { setShowGameModal(!showGameModal) }} style={{fontWeight: '700'}}>
                        {/* <span className='bet_count'>{count}</span> */}
                        <IoOptionsOutline className='select_icon' />
                        <span className='select_item'>BetSlip <span className={`${betSlip.length > 0 && 'zoom-in-zoom-out'}`}>({count})</span> </span>
                    </div>
                    <div className={`game_item ${!logedIn && 'disabled getLogged'}`} onClick={() => { showGameModal && setShowGameModal(false); history.push('/profile') }} style={{fontWeight: '700'}}>
                        <IoMdHelpCircleOutline className='select_icon' />
                        <span className='select_item'>FAQ & Support</span>
                    </div>
                </div>
            </section>
            </Container>
                                <div class="row win">
                 <h1 className='instant_pay d-none d-lg-inline' style={{marginLeft: '26rem'}}>INSTANT PAYMENT, NO STORY</h1>
                        <Col className='pr-0'>
                  {gameType !== 'AGAINST' && <p style={{display: 'flex', marginBottom: '0px', flexWrap: 'wrap'}}>{array.map((a) => <span className='num_span'>{a }</span> )}</p>}
                  {gameType === 'AGAINST' && <p style={{display: 'flex', marginBottom: '0px', flexWrap: 'wrap'}}>{array.map((a) => firstAgainst.includes(a) && <span className='num_span'>{a }</span> || <span className='num_span1'>{a }</span>)}</p>}
                        </Col>
                  {/* <div class="col-md-12 col-sm-12 col-xs-12">
                      
                  </div> */}
              </div>
        </section>


    )
}

export default LottoExpress













































