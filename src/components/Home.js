import React, { useState, useEffect } from 'react';
import { Container, Carousel, Row, Col, Button } from 'react-bootstrap';
import { useHistory } from 'react-router';
import { Link } from 'react-router-dom';
import ball from '../static/assets/raffle-5870552_640.jpg'
import roles from '../static/assets/balls-6077901_640.jpg'
import img1 from '../static/assets/LOTTO.png'
import img2 from '../static/assets/5_9.jpeg'
import img3 from '../static/assets/playnow.png'
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import img4 from '../static/assets/Original.jpeg'
import Layout from '../Layout'
import img5 from '../static/assets/6.png'
import img6 from '../static/assets/img3.jpg'
import CouponModal from '../Fetch/CouponModal'
import { useGlobalContext } from '../store/context';
import moment from 'moment'
import GrandLotto from '../svg/GrandLotto.svg'
import IntegrationNotistack from '../Fetch/IntegrationNotistack';

const Home = () => {
  const {user, islogedIn} = useGlobalContext();
  const [timer, setTimer] = useState(null)
  const get = localStorage.getItem('token')
  const [arr, setArr] = useState([])
  const [value, setValue] = useState('')
  const url = 'https://api.grandlotto.ng/api/v1/gameresults'
  const [showCoupon, setShowCoupon] = useState(false)
  const [list, setList] = useState([])
  let history = useHistory()
  const [success, setSuccess] = useState('')
  const [loading, setLoading] = useState(true)


    // setLoading(true)
    if(window.innerWidth > 1001){
      history.push('/games')
    }
    // setLoading(false)

  // if(loading){
  //   return 
  // }


  return (
    <main className='home_bg'>
      <div className='img_home_div'>
        <div className='img_div'>
          <img src={img1} alt="" className='img_homes' onClick={() => history.push('/games')}/>
          <p className='lotto'>5/90</p>
        </div>
        <div className='img_div'>
          <img src={img2} alt="" className='img_homes' onClick={() => history.push('/promotion')}/>
        </div>
        <div className='img_div'>
         <img src={img3} alt=""  className='img_homes' onClick={() => history.push('/games')}/>
        </div>
      </div>
    </main>
  )
};

export default Home

