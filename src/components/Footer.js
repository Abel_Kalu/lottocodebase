import React from 'react'
import { Container, Row, Col } from 'react-bootstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faApple, faGooglePlay, faFacebook, faYoutube, faTwitter, faInstagram, faLinkedin, faWhatsapp } from '@fortawesome/free-brands-svg-icons'
import { faPhone, faHome, faPrint } from '@fortawesome/free-solid-svg-icons'
import { Link } from 'react-router-dom'
import logo from '../static/assets/GrandLotto.svg'
import flutterwave from '../static/assets/flutterwave.png'
import paystack from '../static/assets/paystack.png'
import mastercard from '../static/assets/mastercard.png'
import verve from '../static/assets/verve.png'
import visa from '../static/assets/visa.png'
import opay from '../static/assets/OPAY.png'
import gtb from '../static/assets/GTCO.png'
import bitcoin from '../static/assets/bitcoin.png'
import { faEnvelope } from '@fortawesome/free-regular-svg-icons'


const Footer = () => {

  let getYear = new Date().getFullYear()
    
  return (
    <footer className="text-center text-lg-start bg-dark text-muted" style={{position: 'static'}}>
      <section
        class="d-md-flex justify-content-center flex-row justify-content-lg-between"
      >
          <Col md={3} style={{width: '20%'}}>
            
            <div className="d-none d-lg-inline" style={{height: '48px', marginTop: '-45px'}}>
              <h6 className="text-uppercase fw-bold">
              <i><img src={logo} style={{width: '240px'}}/></i>
              </h6>
              {/* <p className=''>
                Instant Payment, No Story!
              </p> */}
            </div>
          </Col>
          <Col md={2}>

          </Col>
          <Col md={6}>
            <div className='d-flex pt-4'>
                                         <p>
                <a href="#" className="text-reset pr-5">How To Bet</a>
              </p>
              <p>
                <a href="#" className="text-reset pr-5">Agency Operator</a>
              </p>
              <p>
                <a href="#" className="text-reset pr-5">FAQs</a>
            </p>
                          <p>
                <a href="#" className="text-center text-reset">FirstBet</a>
              </p>
             </div>
          </Col>
          <Col md={1}>

          </Col>
      </section>
      <section className="" style={{background: '#282b2f'}}>
        <Col md={3}>
            {/* <h4 style={{color: '#fff'}}>About Us</h4> */}
         </Col>
         <Col md={2}></Col>
        <Col md={6} className='d-inline-block p-2'>
          <p style={{fontSize: '13px'}} className='d-none d-lg-inline'>This website is operated by Grandlotto (A Subsidiary of Lotgrand Limited). Lotgrand Limited is a company registered in Nigeria (RC1222087), and with it’s administrative office at Brick House 17, Yinusa Adeniji Street off Toyin IKEJA, 100212, Lagos.
Grandlotto as a brand was established by a group of visionaries and insightful people with ideas and mechanism for providing latest technologies to continuously revolutionize the gaming industry. Grandlotto provides online lotto systems, online gaming tools and API for all businesses interested in leveraging our experience, capacity and competence. Over the years, Grandlotto has built its reputation as a force to reckon with in the gaming industry. </p>
         </Col>
         <Col md={1}></Col>
      </section>
      <section>
        <Col md={3}></Col>
        <Col md={2}></Col>
        <Col md={6} className='d-inline-block p-2'>
          <div className='d-lg-inline'>
            <img src={bitcoin} alt="bitcoin logo" className='footer_pay'/>
            <img src={mastercard} alt="mastercard logo" className='footer_pay'/>
            <img src={opay} alt="opay logo" className='footer_pay'/>
            <img src={verve} alt="verve card logo" className='footer_pay'/>
            <img src={visa} alt="visa card logo" className='footer_pay'/>
            <img src={gtb} alt="gtb logo" className='footer_pay'/>
            <img src={paystack} alt="paystack logo" className='footer_pay' style={{width: '5rem'}}/>
            <img src={flutterwave} alt="flutterwave logo" className='footer_pay' style={{width: '5rem'}}/>
            {/* <img src={opay} alt="" /> */}
          </div>
        </Col>
        <Col md={1}></Col>
      </section>
      <div className="text-center p-4" style={{ backgroundColor: 'rgba(0, 0, 0, 0.05)' }}>
        © {getYear}
        <a className="text-reset fw-bold" href="/"> GrandLotto</a>
      </div>
    </footer>
  )
}

export default Footer
