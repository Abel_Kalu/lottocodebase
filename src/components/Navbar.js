import React, { useState, useEffect, useRef } from 'react';
import { useGlobalContext } from '../store/context';
import { Link } from 'react-router-dom';
import { Navbar, NavDropdown, Nav, Form, Button, Dropdown, DropdownButton } from 'react-bootstrap';
import { useHistory } from 'react-router';
import IntegrationNotistack from '../Fetch/IntegrationNotistack';
import logo from '../static/assets/GrandLotto.svg'
import { makeStyles } from "@material-ui/core";
import { Autorenew } from "@material-ui/icons";
import clsx from "clsx";
import GrandLotto from '../svg/GrandLotto.svg'
import GppMaybeIcon from '@mui/icons-material/GppMaybe';
import AddModeratorIcon from '@mui/icons-material/AddModerator';
import GppGoodIcon from '@mui/icons-material/GppGood';

//http://localhost:5016

const initialState = {
    firstName: '',
    lastName: '',
    dob: new Date(),
    gender: '',
    id_number: '',
    password: '',
    password2: '',
    sender_id: '',
    customer_id: '',
    id_number: '',
    id_url: '',
    amount: '',
    pin: '',
    pin2: ''
}

const useStyles = makeStyles((theme) => ({
	refresh: {
        marginTop: "2px",
        marginRight: '5px',
        cursor: "pointer",
        color: '#fff',
		"&.spin": {
			animation: "$spin 1s 1",
			pointerEvents:'none'
		}
	},
	"@keyframes spin": {
		"0%": {
			transform: "rotate(0deg)"
		},
		"100%": {
			transform: "rotate(360deg)"
		}
	}
}));


const Navigation = () => {
    let { giveAccess, giveAdminAccess, getUser, showBoard, logOut, logedIn, isLoggedIn, user } = useGlobalContext();
    const isMounted = useRef(true)
    let history = useHistory()
    const url = 'https://api.grandlotto.ng'
    const [success, setSuccess] = useState(false)
    const [spin, setSpin] = useState(false)
    const [userLogin, setUserLogin] = useState({ mobile: '', password: '' });
    let get = localStorage.getItem('token')
    const classes = useStyles();

    const refreshCanvas = async () => {
        setSpin(true);

        var myHeaders = new Headers();
        myHeaders.append("signatures", "3b55227b019105b2f8550792916ee41321b53fb2104fd0149e81c360811ef027");
        myHeaders.append("Authorization", `Bearer ${get}`);


        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };

        fetch(`https://lotto-api.firstbetng.com/api/v2/auth/profile`, requestOptions)
            .then(response => response.json())
            .then(result => {
                showBoard(result.success.data)
                getUser(result.success.data)
                giveAccess(isLoggedIn)
            })
            .catch(error => console.log('error', error));
        
        
        setTimeout(() => {
            setSpin(false);
        }, 1000);

    };

    const handleLogin = (e) => {
        e.preventDefault()
        e.stopPropagation()
        const name = e.target.name;
        const value = e.target.value;
        setUserLogin({ ...userLogin, [name]: value })
    };

    useEffect(() => {
        if (parseInt(user.dailybet_num) > 3 && user.activated < 1) {
            setSuccess('Hello Customer, This is to remind you to validate your account')
        }
    }, []);

    const handleSubmit = (e) => {
        e.preventDefault();
        e.stopPropagation()
        var myHeaders = new Headers();
        myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
        myHeaders.append("timestamps", "1614848109");
        myHeaders.append("Content-Type", "application/json");

        if (userLogin.mobile.includes('.com') || userLogin.mobile.includes('@')) {
            var raw = JSON.stringify({
                "email": `${userLogin.mobile}`,
                "password": `${userLogin.password}`
            });

            var requestOptions = {
                method: 'POST',
                headers: myHeaders,
                body: raw,
                redirect: 'follow'
            };

            fetch(`${url}/api/v1/login`, requestOptions)
                .then(response => response.json())
                .then(result => {
                    if (result.success) {
                        const { token } = result.success;
                        giveAccess(token)

                        var myHeaders = new Headers();
                        myHeaders.append("signatures", "5a1131f2eb747be50714281ec3e68b759476c6dc9e1faf5fc5d91c552cf8c230");
                        myHeaders.append("Authorization", `Bearer ${token}`);

                        var requestOptions = {
                            method: 'GET',
                            headers: myHeaders,
                            redirect: 'follow'
                        };

                        fetch(`${url}/api/v2/auth/profile`, requestOptions)
                            .then(response => response.json())
                            .then(result => {
                                if (result.success) {
                                    getUser(result.success.data)
                                    const { data } = result.success;
                                    showBoard(data)
                                    history.push('/games')
                                    refreshCanvas();
                                    //   localStorage.setItem('user', JSON.stringify(data))
                                } else {
                                    return;
                                }
                            },
                                (error) => {
                                    console.log(error)
                                });
                    } else {
                        setSuccess('Mobile number and password incorrect')
                        return;
                    }
                },
                    (error) => {
                        console.log(error)
                    }
                );

            userLogin.mobile = ''
            userLogin.password = ''
        } else {

            var raw = JSON.stringify({
                "mobile": `${userLogin.mobile}`,
                "password": `${userLogin.password}`
            });

            var requestOptions = {
                method: 'POST',
                headers: myHeaders,
                body: raw,
                redirect: 'follow'
            };

            fetch(`${url}/api/v1/login`, requestOptions)
                .then(response => response.json())
                .then(result => {
                    if (result.success) {
                        const { token } = result.success;
                        giveAccess(token)
                        localStorage.setItem('token', token)
                        var myHeaders = new Headers();
                        myHeaders.append("signatures", "5a1131f2eb747be50714281ec3e68b759476c6dc9e1faf5fc5d91c552cf8c230");
                        myHeaders.append("Authorization", `Bearer ${token}`);

                        var requestOptions = {
                            method: 'GET',
                            headers: myHeaders,
                            redirect: 'follow'
                        };

                        fetch(`${url}/api/v2/auth/profile`, requestOptions)
                            .then(response => response.json())
                            .then(result => {
                                if (result.success) {
                                    getUser(result.success.data)
                                    const { data } = result.success;
                                    showBoard(data)
                                    refreshCanvas();
                                    history.push('/games')
                                    //   localStorage.setItem('user', JSON.stringify(data))
                                } else {
                                    return;
                                }
                            },
                                (error) => {
                                    console.log(error)
                                });
                    } else {
                        setSuccess('Mobile number and password incorrect')
                        return;
                    }
                },
                    (error) => {
                        console.log(error)
                    }
                );

            userLogin.mobile = ''
            userLogin.password = ''
        }
    }



    const handleLogOut = (e) => {
        localStorage.removeItem('token')
        localStorage.removeItem('adminToken')
        getUser([])
        history.push('/games')
        logOut(true)
        console.log(isLoggedIn)
    }

    const handleBetHistory = (e) => {
        e.preventDefault()
        history.push('/profile/betHistory')
    }

    const handleSettings = (e) => {
        e.preventDefault()
        console.log(e.target)
    }

    const handleTransactionHistory = (e) => {
        e.preventDefault()
        history.push('/profile/transactions')
    }


    useEffect(() => {
        var myHeaders = new Headers();
        myHeaders.append("signatures", "3b55227b019105b2f8550792916ee41321b53fb2104fd0149e81c360811ef027");
        myHeaders.append("Authorization", `Bearer ${get}`);


        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };

        fetch(`${url}/api/v2/auth/profile`, requestOptions)
            .then(response => response.json())
            .then(result => {
                getUser(result.success.data)
                refreshCanvas()
                giveAccess(isLoggedIn)
            })
            .catch(error => console.log('error', error));
    }, []);

    useEffect(() => {
        const loggedInAdmin = localStorage.getItem('adminToken')
        if (loggedInAdmin) {
            giveAdminAccess(loggedInAdmin)
        }
        return () => {
            isMounted.current = false
        }
    }, [])

    return (
        <header>
            {success && <IntegrationNotistack success={`${success}`} />}
            <nav className="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
                <div className="container-fluid ms-2">
                    <a className="navbar-brand" onClick={() => history.push('/')} style={{ cursor: 'pointer' }}>
                        <img src={logo} alt=""  className="d-inline-block align-text-top nav_img" />
                    </a>
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
      
                    <div className="collapse navbar-collapse d-md-flex justify-content-end" id="navbarCollapse">
        
                        {!logedIn && <section className='d-flex flex-column flex-md-row'>
                            <form className="d-flex">
                                <input className="form-control form_control ml-2 mr-2" type="text" id="phone" name='mobile' onChange={handleLogin} value={userLogin.mobile} placeholder="Email or Phone" />
                                <input className="form-control form_control ml-2 mr-2" type="password" id="password" onChange={handleLogin} name='password' placeholder="Password" value={userLogin.password} />
                                <button className="btn btn-success ml-2 mr-2 game_head" type="submit" onClick={handleSubmit}>Login</button>
                                <Button className="ml-2 mr-2 choose" variant='success' size='sm' type="submit" onClick={handleSubmit}>Login</Button>
                            </form>
                            <a className='game_head ' onClick={() => history.push('/register')}><button className="btn btn-outline-primary fredbut me-2 mt-2 mt-md-0">Sign Up</button></a>
                            <div className='d-flex justify-content-between'>
                                
                            <a onClick={() => history.push('/register')} className='choose sign_up'>Sign Up Here</a>
                            <a onClick={() => history.push('/profile/passwordreset')} className='choose sign_up'>Forgot Password</a>
                            </div>
                        </section>
                        }
                        {
                            logedIn &&
                            <section className='d-flex justify-content-between flex-lg-end'>
                                {parseInt(user.mobile_verify) === 0 && parseInt(user.mobile_verify) === 0 && <span className='colors' onClick={() => history.push('/profile/Otp')}>Verify Now <GppMaybeIcon /></span>}
                                {!parseInt(user.kyc_status) >= 1 && parseInt(user.mobile_verify) === 1 && parseInt(user.mobile_verify) === 1 && <span className='colorUp' onClick={() => history.push('/profile/Otp')}>Update Kyc <AddModeratorIcon /></span>}
                                {parseInt(user.kyc_status) >= 1 && <span className='green mt-1'>Verificatiion 1 <GppGoodIcon /></span>}
                                <div className='d-flex'>
                                    {user.wallet && <p className='white mt-2 mr-2'>Balance: &#x20A6;{parseInt(user.wallet) + parseInt(user.withdrawable)}</p>}
                                    <Autorenew
                                        className={clsx({
                                            [classes.refresh]: true,
                                            spin: spin
                                        })}
                                        onClick={refreshCanvas}
                                        spin={720}
                                    />
                                </div>
                                <DropdownButton
                                    className='nav_dropdown check_drop'
                                    variant='secondary'
                                    size='sm'
                                    menuAlign="right"
                                    title="My Account"
                                    id="dropdown-menu-align-right"
                                >
                                    <Dropdown.Item onClick={() => history.push('/profile')}>View Profile</Dropdown.Item>
                                    <Dropdown.Item onClick={() => history.push('/fundwallet')}>Fund Wallet</Dropdown.Item>
                                    <Dropdown.Item onClick={handleTransactionHistory}>Transaction History</Dropdown.Item>
                                    <Dropdown.Item onClick={handleBetHistory}>Bet History</Dropdown.Item>
                                    <Dropdown.Item onClick={handleBetHistory}>Contact Support</Dropdown.Item>
                                    <Dropdown.Item onClick={() => {
                                        history.push('/profile/results')
                                    }}>Draw Results</Dropdown.Item>
                                    <Dropdown.Item onClick={handleLogOut}>Log out</Dropdown.Item>
                                </DropdownButton>
                   
                            </section>
                        
                        }
                    </div>
                </div>
            </nav>
        </header>
    )
}


export default Navigation

