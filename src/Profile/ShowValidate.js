import * as React from 'react';
import { styled } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import Slider from '@mui/material/Slider';
import MuiInput from '@mui/material/Input';
import ValidateOtp from './ValidateOtp'
import VolumeUp from '@mui/icons-material/VolumeUp';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import ListSubheader from '@mui/material/ListSubheader';
import Switch from '@mui/material/Switch';
import { useGlobalContext } from '../store/context';
import Layout from '../Layout'
import WifiIcon from '@mui/icons-material/Wifi';
import PhoneIcon from '@mui/icons-material/Phone';
import EmailIcon from '@mui/icons-material/Email';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import Fingerprint from '@mui/icons-material/Fingerprint';
import {useHistory} from 'react-router'
import IntegrationNotistack from '../Fetch/IntegrationNotistack';

const Input = styled(MuiInput)`
  width: 42px;
`;

export default function InputSlider() {
  let { user } = useGlobalContext();
  const [value, setValue] = React.useState(30);
  const [checked, setChecked] = React.useState(['email']);
  const [show, setShow] = React.useState(false)
  const [show1, setShow1] = React.useState(false)
  const [show3, setShow3] = React.useState(false)
    const url = 'https://api.grandlotto.ng'
  const [show4, setShow4] = React.useState(false)
  const [show2, setShow2] = React.useState(false)
  const [email, setEmail] = React.useState(null)
  const [mobile, setMobile] = React.useState(null)
  const [success, setSuccess] = React.useState(false)
  const get = localStorage.getItem('token')
  const history = useHistory()
  var myHeaders = new Headers();
  myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
  myHeaders.append("Authorization", `Bearer ${get}`);
  myHeaders.append("Content-Type", "application/json");

  const handleSliderChange = (event, newValue) => {
    setValue(newValue);
  };

  const handleInputChange = (event) => {
    setValue(event.target.value === '' ? '' : Number(event.target.value));
  };

  React.useEffect(() => {
        const timeout = setTimeout(() => {
            setSuccess(false)
        }, 3000)
        return () => clearTimeout(timeout)
  }, [success]);


  const handleBlur = () => {
    if (value < 0) {
      setValue(0);
    } else if (value > 100) {
      setValue(100);
    }
  };

  const handleClick = (e) => {
    e.preventDefault()
    if(mobile !== null){

        var raw = JSON.stringify({
        "mobile": `${mobile}`
        });

        var requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: raw,
        redirect: 'follow'
        };

        fetch(`${url}/api/v2/auth/validatephone`, requestOptions)
        .then(response => response.json())
        .then(result => {
            if(result.success.message){
                setSuccess(result.success.message)
                setShow3(true)
            } else {
              return;
            }
        })
        .catch(error => console.log('error', error));
    }else if(email !== null){

        var raw = JSON.stringify({       
            "email": `${email}`
        });

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };

        fetch(`${url}/api/v2/auth/validateemail`, requestOptions)
          .then(response => response.json())
          .then(result => {
            if(result.success.message){
                setSuccess(result.success.message)
                setShow3(!show3)
            }else{
                setSuccess(false)
            }
        })
          .catch(error => console.log('error', error));
    }else{
        setSuccess('Please add a validate email or phone number')
        setShow3(false)
        setShow4(false)
        return;
    }
    
  }


  const handleToggle = (value) => () => {
    const currentIndex = checked.indexOf(value);
    const newChecked = [...checked];

    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }

    setChecked(newChecked);
  };

  const handleChange = e => {
    e.preventDefault()
    let name = e.target.name
    let value = e.target.value
    if (name === 'email') {
      setEmail(value)
      localStorage.setItem('Email', value)
    } else if (name === 'mobile') {
      setMobile(value)
      localStorage.setItem('Mobile', value)
    } else {
      return
    }
  }

  const handleValMobile = e => {
    setMobile(user.mobile)
    localStorage.setItem('Mobile', user.mobile)
    var raw = JSON.stringify({
      "mobile": `${user.mobile}`
    });

    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: raw,
      redirect: 'follow'
    };

    fetch(`${url}/api/v2/auth/validatephone`, requestOptions)
      .then(response => response.json())
      .then(result => {
        console.log(result)
        if (result.success.message) {
          setSuccess(result.success.message)
          setShow3(!show3)
        } else {
          setSuccess('Kindly refresh your browser and retry!!!')
          return;
        }
      })
      .catch(error => console.log('error', error));
    return;
  }


  const handleValEmail = e => {
    setEmail(user.email)
    localStorage.setItem('Email', user.email)
        var raw = JSON.stringify({       
            "email": `${user.email}`
        });

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };

        fetch(`${url}/api/v2/auth/validateemail`, requestOptions)
          .then(response => response.json())
          .then(result => {
            console.log(result)
            if(result.success.message){
                setSuccess(result.success.message)
                setShow3(!show3)
            } else {
              setSuccess('Kindly refresh your browser and retry!!!')
              return;
            }
        })
          .catch(error => console.log('error', error));
    return;
  }


  return (
      <main>
    <section className='d-flex justify-content-center val_section' >
        <List
      sx={{ width: '100%', maxWidth: 660, bgcolor: 'background.paper', }}
      subheader={<ListSubheader>Verifications</ListSubheader>}
    >
      <ListItem>
        <ListItemIcon>
          <EmailIcon />
        </ListItemIcon>
        <ListItemText id="switch-list-label-email" primary="Email" />
          <p className='mr-5 mt-3'> {user.email && user.email }</p>
            {!user.email_verify == 1 && !user.email && <Button size='small' variant="contained" style={{fontSize: '8px'}} component="span" onClick={() => setShow1(!show1)}>Verify Email</Button>}
            {!user.email_verify == 1 && user.email && <Button size='small' style={{fontSize: '8px'}} variant="contained" component="span" onClick={handleValEmail}>Verify Email</Button>}
        <Switch
          edge="end"
          onChange={handleToggle('email')}
          checked={user.email !== 'null' && user.email_verify == 1  ? 'checked' : ''}
          inputProps={{
            'aria-labelledby': 'switch-list-label-email',
          }}
        />
          </ListItem>
          {
            show1 && !user.email &&
            <div className='d-flex justify-content-center'>
              <Box
                component="form"
                sx={{
                  '& > :not(style)': { m: 1, width: '25ch' },
                }}
                noValidate
                autoComplete="on"
              >
                <TextField
                  id="outlined-error-helper-text"
                  label="Email"
                  name='email'
                  onChange={handleChange}
                  value={email}
                  sx={{
                    '& > :not(style)': { width: '25ch', height: '5ch' },
                  }}
                  placeholder='Email'
                />
                <Button size='small' variant="contained" color="secondary" style={{fontSize: '8px'}} className='mt-3' onClick={handleClick}>Submit</Button>
                {/* {success && <TextField id="standard-basic" onChange={handleValidate} name='otp' label="Enter Otp" variant="standard" />}
      {show1 && !success && <TextField id="standard-basic" onChange={handleChange} name='email' label="Enter Email" variant="standard" />} */}
              </Box>
            
            </div>
          }
      <ListItem>
        <ListItemIcon>
          <PhoneIcon />
        </ListItemIcon>
        <ListItemText id="switch-list-label-phone" primary="Phone" />
        <p className='mr-5 mt-3'> {user.mobile && user.mobile }</p>
            {!user.mobile_verify == 1 && !user.mobile && <Button size='small' style={{fontSize: '8px'}} variant="contained" component="span" onClick={() => setShow2(!show2)}>Verify Phone</Button>}
            {!user.mobile_verify == 1 && user.mobile && <Button size='small' style={{fontSize: '8px'}} variant="contained" component="span" onClick={handleValMobile}>Verify Mobile</Button>}
               {/* {user.mobile && <Button size='small' color="secondary" disabled>Verified</Button>} */}
               {/* {show2 && !success && <Button size='small' variant="contained" color="secondary" onClick={handleClick}>Submit</Button>}
               {success && show3 && <Button size='small' variant="contained" color="secondary" onClick={handleValidate}>Validate Otp</Button>} */}

        <Switch
          edge="end"
          onChange={handleToggle('phone')}
          checked={user.mobile !== 'null' && user.mobile_verify == 1 ? 'checked' : ''}
          inputProps={{
            'aria-labelledby': 'switch-list-label-phone',
          }}
        />
          </ListItem>
          <div className='d-flex justify-content-center'>
            {
              show2 && !user.mobile &&
              <Box
                component="form"
                sx={{
                  '& > :not(style)': { m: 1, width: '25ch' },
                }}
                noValidate
                autoComplete="on"
              >
                <TextField
                  id="outlined-error-helper-text"
                  value={mobile}
                  onChange={handleChange}
                  name='mobile'
                  label="Mobile"
                  sx={{
                    '& > :not(style)': { width: '25ch', height: '5ch' },
                  }}
                  placeholder='Mobile'
                />
                <Button size='small' variant="contained" style={{fontSize: '8px'}} color="secondary" className='mt-3' onClick={handleClick}>Submit</Button>
                {/* {success && <TextField id="standard-basic" onChange={handleValidate} name='otp' label="Enter Otp" variant="standard" />}
      {show1 && !success && <TextField id="standard-basic" onChange={handleChange} name='email' label="Enter Email" variant="standard" />} */}
              </Box>
            }
         </div>
          <ListItem>
        <ListItemIcon>
          <Fingerprint />
        </ListItemIcon>
        <ListItemText id="switch-list-label-kyc" primary="KYC" />
               { !user.activated == 1 ? <Button size='small' variant="contained" component="span" style={{fontSize: '8px'}} onClick={() => history.push('/profile')}>
          KYC
        </Button> : <Button size='small' color="secondary" disabled>Verified</Button>}
        <Switch
          edge="end"
          onChange={handleToggle('kyc')}
          checked={!user.activated > 0 ? '' : 'checked'}
          inputProps={{
            'aria-labelledby': 'switch-list-label-kyc',
          }}
        />
      </ListItem>
    </List>
      </section>
      {show3 && <ValidateOtp setShow3={() => setShow3(false)}/>}
    {success && <IntegrationNotistack className='d' success={`${success}`} />}
    </main>
  )
}