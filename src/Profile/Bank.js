import React, { useState, useEffect } from 'react';
import { Container, Col, Row, Form, Button, Dropdown, DropdownButton } from 'react-bootstrap';
import IntegrationNotistack from '../Fetch/IntegrationNotistack';
import Layout from '../Layout'
import LoadingButton from '@mui/lab/LoadingButton';
import Stack from '@mui/material/Stack';

const initialState = {
    accountNum: '',
    accountName: '',
    bankName: '',
    bankCode: '',
}

const Bank = () => {
    const [users, setUsers] = useState(initialState)
    const [success, setSuccess] = useState('')
    const [showAlert, setShowAlert] = useState(false)
    const [arr, setArr] = useState([])
    const [show, setShow] = useState(false)
    const [details, setDetails] = useState([])
    const [code, setCode] = useState([])
    const url = 'https://api.grandlotto.ng'
    const secretKey = process.env.REACT_APP_PAYSTACK_SECRET_KEY
    const get = localStorage.getItem('token')

    useEffect(() => {
        var myHeaders = new Headers();
        myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
        myHeaders.append("Authorization", `Bearer ${get}`);
        myHeaders.append("Content-Type", "application/json");

        // var raw = JSON.stringify({
        //     "amount": "1000",
        //     "url": "https://firstbet.netlify.app"
        // });

        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };

        fetch(`${url}/api/v2/auth/opay/getBanks`, requestOptions)
            .then(response => response.json())
            .then(result => {
                if (result.success) {
                    const { data } = result.success;
                    setArr(data.data)
                }
            })
            .catch(error => console.log('error', error));
    }, []);

    useEffect(() => {
        let time = setTimeout(() => {
            setSuccess('')
        }, 3000)

        return () => clearTimeout(time)
    }, [success]);

    // console.log(arr)

    const handleSubmit = e => {
        e.preventDefault()

        console.log(code)
        console.log(users.accountNum)
        console.log(details)


        var myHeaders = new Headers(); 
        myHeaders.append("signatures", "signatures");
        myHeaders.append("Authorization", `Bearer ${get}`);
        myHeaders.append("Content-Type", "application/json");

        var raw = JSON.stringify({
            "bankName": `${code.name}`,
            "accountNumber": `${users.accountNum}`,
            "accountName": `${details.accountName}`,
            "bank_code": `${code.code}`
        });

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };

        fetch(`${url}/api/v2/auth/bankdetails`, requestOptions)
            .then(response => response.json())
            .then(result => {
                if (result.setSuccess) {
                    const { message } = result.success;
                    setSuccess(message)
                }
            })
            .catch(error => console.log('error', error));


    }


    useEffect(() => {
        let time = setTimeout(() => {
            setShowAlert(!showAlert)
            setSuccess('')
        }, 3000)

        return () => clearTimeout(time)
    }, [success]);


    const handleRegisterChange = e => {
        e.preventDefault()
        let name = e.target.name;
        let value = e.target.value;
        setUsers({ ...users, [name]: value })
    }
    
    const handleSelect = (e) => {
        e.preventDefault()
        // console.log(e.target.value)
        const v = arr.find((a) => a.name === e.target.value)
        setCode(v)
        setShow(true)
        var myHeaders = new Headers();
        myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
        myHeaders.append("Authorization", `Bearer ${get}`);
        myHeaders.append("Content-Type", "application/json");

        var raw = JSON.stringify({
            "bankAccountNo": `${users.accountNum}`,
            "bankCode": `${v.code}`
        });

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };

        fetch("https://api.grandlotto.ng/api/v2/auth/opay/resolveAccountNumber", requestOptions)
            .then(response => response.json())
            .then(result => {
                console.log(result)
                if (result.success.data) {
                    const { data } = result.success;
                    setDetails(data)
                    setUsers({...users, aacountNum: details.accountNo, accountName: details.accountName, bankCode: v.code})
                    setShow(false)
                } else {
                    setSuccess('Bank Account does not exist')
                    setShow(false)
                    return;
                }
            })
            .catch(error => console.log('error', error));
    }

    console.log(details)
    console.log(users)

    return (
        <>
            <section className='d-flex justify-content-center'>
                <h5>Update Bank Accounts</h5>
            </section>
            <section>
                <Form noValidate onSubmit={handleSubmit}>
                    <Form.Group as={Col} md="8" controlId="validationCustom01">
                        <Form.Label>Account Number</Form.Label>
                        <Form.Control
                            required
                            type="text"
                            name="accountNum"
                            value={users.accountNum}
                            onChange={handleRegisterChange}
                            placeholder="Account Number"
                            required
                        />
                        <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
                    </Form.Group>
                    <Form.Group as={Col} md="8" className='mt-3' controlId="validationCustom02">
                        <Form.Label>Bank Name</Form.Label>
                        {/* <Form.Control
                                required
                                type="text"
                                name="bankName"
                                value={users.bankName}
                                onChange={handleRegisterChange}
                                placeholder="bankName"
                        /> */}
                        <Form.Control as="select" onChange={handleSelect} className='mb-3' custom>
                            {arr.map((a, id) => {
                                const { name, code, type } = a;
                                return (
                                    <option key={id}>{name}</option>
                                )
                            })}
                            {/* <option name='' value=''>Select Gender</option>
                            <option name='Male' value='Male'>Male</option>
                            <option name='Female' value='Female'>Female</option> */}
                        </Form.Control>
                        <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
                    </Form.Group>
                    {
                        details.accountName && <Form.Group as={Col} md="8" className='mt-3' controlId="validationCustomUsername">
                            <Form.Label>Account Name</Form.Label>
                            <Form.Control
                                required
                                type="text"
                                name="accountName"
                                value={users.accountName}
                                onChange={handleRegisterChange}
                                placeholder={details.accountName}
                            />
                            <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
                            <Form.Control.Feedback type="invalid">
                                Please Enter A valid Date Of Birth.
                            </Form.Control.Feedback>
                        </Form.Group>
                    }
                    {
                        show && <Stack direction="row" spacing={2}>
                            <LoadingButton loading variant="outlined">
                                Submit
                            </LoadingButton>
                        </Stack>
                    }
                    {!show && <Button variant='success' className='mb-3 mt-3 ml-3' type="submit">Submit</Button>}
                </Form>
                        
            </section>
        
            {success && <IntegrationNotistack className='d' success={`${success}`} />}
        </>
    )
}

export default Bank
