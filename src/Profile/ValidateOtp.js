import React, { useState, useEffect } from 'react';
import Box from '@mui/material/Box';
import PhoneIcon from '@mui/icons-material/Phone';
import { faWhatsapp } from '@fortawesome/free-brands-svg-icons'
import TextField from '@mui/material/TextField';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import logo from '../static/assets/GrandLotto.svg'
import { Modal, Button, Form } from 'react-bootstrap';
import { useGlobalContext } from '../store/context';
import IntegrationNotistack from '../Fetch/IntegrationNotistack';


function MyVerticallyCenteredModal(props) {
    let { user } = useGlobalContext();
    const [value, setValue] = useState('')
    const [success, setSuccess] = useState('')
    const get = localStorage.getItem('token')
    let email = localStorage.getItem('Email')
    const url = 'https://api.grandlotto.ng'
    let mobile = localStorage.getItem('Mobile')
  var myHeaders = new Headers();
  myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
  myHeaders.append("Authorization", `Bearer ${get}`);
  myHeaders.append("Content-Type", "application/json");


    useEffect(() => {
        const timeout = setTimeout(() => {
            setSuccess(false)
        }, 3000)
        return () => clearTimeout(timeout)
    }, [success]);

    const handleValidate = e => {
        e.preventDefault()

        // console.log(props.mobile !== null && 'mobile')
        // console.log(props.email !== null && 'email')
        if (value) {
            if (mobile) {
                
                var myHeaders = new Headers();
                myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
                myHeaders.append("Authorization", `Bearer ${get}`);
                myHeaders.append("Content-Type", "application/json");

                var raw = JSON.stringify({
                    "otp": `${value}`
                });

                var requestOptions = {
                    method: 'POST',
                    headers: myHeaders,
                    body: raw,
                    redirect: 'follow'
                };

                fetch(`${url}/api/v2/auth/otpvalidate`, requestOptions)
                    .then(response => response.json())
                    .then(result => {
                        if (result.success) {
                            // localStorage.removeItem('Email')
                            // props.onHide();
                            // props.setShow3();
                            const { message } = result.success;
                            setSuccess(message)
                        }
                    })
                    .catch(error => console.log('error', error));
            } else if (email) {
                var myHeaders = new Headers();
                myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
                myHeaders.append("Authorization", `Bearer ${get}`);
                myHeaders.append("Content-Type", "application/json");

                var raw = JSON.stringify({
                    "email": `${email}`,
                    "otp": `${value}`
                });

                var requestOptions = {
                    method: 'POST',
                    headers: myHeaders,
                    body: raw,
                    redirect: 'follow'
                };

                fetch(`${url}{/api/v2/auth/otpvalidate`, requestOptions)
                    .then(response => response.json())
                    .then(result => {
                        console.log(result)
                        if (result.success) {
                            // localStorage.removeItem('Mobile')
                            // props.onHide();
                            // props.setShow3();
                            const { message } = result.success;
                            setSuccess(message)
                        } else {
                            const { message } = result.error;
                            setSuccess(message)
                        }
                    })
                    .catch(error => console.log('error', error));
            }
        }
    }

    const handlePhone = e => {
        e.preventDefault()
        var raw = JSON.stringify({
            "mobile": `${mobile}`
        });

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };

        fetch(`${url}/api/v2/auth/resend-voice`, requestOptions)
            .then(response => response.json())
            .then(result => {
                console.log(result)
                if (result.success.message) {
                    setSuccess(result.success.message)
                } else {
                    setSuccess('Kindly refresh your browser and retry!!!')
                    return;
                }
            })
            .catch(error => console.log('error', error));
        return;
    }

    const handleWhatsapp = e => {
        e.preventDefault()
        var raw = JSON.stringify({
            "mobile": `${mobile}`
        });

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };

        fetch(`${url}/api/v2/auth/resend-whatsapp`, requestOptions)
            .then(response => response.json())
            .then(result => {
                console.log(result)
                if (result.success.message) {
                    setSuccess(result.success.message)
                } else {
                    setSuccess('Kindly refresh your browser and retry!!!')
                    return;
                }
            })
            .catch(error => console.log('error', error));
        return;
    }


  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header>
        <Modal.Title id="contained-modal-title-vcenter" className='d-flex justify-content-center align-items-center'>
        <img src={logo} alt="" width="200px" height="60px" className="d-inline-block align-text-top nav_img" />          
        {success && <IntegrationNotistack className='d' success={`${success}`} /> }
        </Modal.Title>
      </Modal.Header>
        <Modal.Body>
            <div className='div'>
              <Box
                component="form"
                sx={{
                  '& > :not(style)': { m: 1, width: '25ch' },
                }}
                noValidate
                autoComplete="on"
              >
                <TextField
                  id="outlined-error-helper-text"
                  label="Otp"
                  name='otp'
                  onChange={(e) => setValue(e.target.value)}
                  sx={{
                    '& > :not(style)': { width: '25ch', height: '5ch' },
                  }}
                  placeholder='Otp'
                />
                {/* {success && <TextField id="standard-basic" onChange={handleValidate} name='otp' label="Enter Otp" variant="standard" />}
      {show1 && !success && <TextField id="standard-basic" onChange={handleChange} name='email' label="Enter Email" variant="standard" />} */}
                  </Box>
                  <Button size='small' variant="outline-secondary" color="secondary" className='mt-2' onClick={handleValidate}>Submit</Button>
            
              </div>
              
                  <p className='mt-3 ml-5'>Kindly use the any of the buttons below if otp is not recieved for mobile verification in 2 minutes</p>
                  <div className='d-flex flex-direction-column justify-content-around mt-2'>
                    <p className='verify' onClick={handlePhone}>Phone  <PhoneIcon /></p>
                    <p className='verify' onClick={handleWhatsapp}>Whatsapp <FontAwesomeIcon className='ml-2' size-md='2x' icon={faWhatsapp} /></p>
                
                  </div>
        </Modal.Body>
      <Modal.Footer>
              <Button onClick={ props.onHide, props.setShow3 }>Close</Button>
          </Modal.Footer>
    </Modal>
  );
}

function App(props) {
  const [modalShow, setModalShow] = useState(true);

  return (
    <>
      <MyVerticallyCenteredModal
        show={modalShow}
        onHide={() => setModalShow(false)}
        setModalShow={setModalShow}
        setShow3={props.setShow3}
      />
    </>
  );
}

export default App