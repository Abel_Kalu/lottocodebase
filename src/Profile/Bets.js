import { Category } from '@material-ui/icons';
import React, { useState, useEffect } from 'react';
import { Container, Col, Row, Form, Button, Dropdown, DropdownButton } from 'react-bootstrap';
import { useHistory } from 'react-router';
import { useGlobalContext } from '../store/context';
import IntegrationNotistack from '../Fetch/IntegrationNotistack';
import noBet from '../svg/notBet.svg'

    const items = [
        {all: 'WON'},
        {all: 'LOST'}
];
    
const allItems = items.map((item) => item.all);
// console.log(allItems)

const Bets = () => {
    const { bets } = useGlobalContext();
    // const [arc, setArc] = useState([])
    const [arr, setArr] = useState([])
    const url = "https://api.grandlotto.ng/api/v2/auth/betting-list"
    let history = useHistory()
    const [show, setShow] = useState(false)
    const [show1, setShow1] = useState(false)
    const [cat, setCat] = useState(['ALL'])
    const [change, setChange] = useState(false)
    const [success, setSuccess] = useState('')
    const [item, setItems] = useState(allItems)
    const [singleBet, setSingleBet] = useState([])
    
    var dt = new Date();
    
    let date = dt.getFullYear() + "-" + (dt.getMonth() + 1) + "-" + dt.getDate();
    let min = dt.getFullYear() + "-" + (dt.getMonth() + 1) + "-" + dt.getDate() - 7;

    const filterItem = (Category) => {
        setCat([Category])
        if (Category === 'OTHERS') {
            let newItem = bets.filter((item) => item.status !== 'PENDING')
            setArr(newItem)
            return;
        } else if (Category === 'ALL') {
            let newItem = bets.filter((item) => item.status !== 'LOST' && item.status !== 'WON')
            setArr(newItem)
        } else {
            let newItem = bets.filter((item) => item.status === Category)
            if (newItem.length > 0) {
                setArr(newItem)
            } else {
                setSuccess('List is empty')
                return;
            }
        }
    }

    useEffect(() => {

        let isCancelled = false
        const get = localStorage.getItem('token')

        const fetchData = async () => {
            var myHeaders = new Headers();
            myHeaders.append("signatures", "a0451a967f04a3ac7dc526086749599249a53b3d9e81b71afeb4f3efab8214d5");
            myHeaders.append("Authorization", `Bearer ${get}`);

            var requestOptions = {
                method: 'GET',
                headers: myHeaders,
                redirect: 'follow'
            };

            fetch(url, requestOptions)
                .then(response => response.json())
                .then(result => {
                    // console.log(result)
                    const data = result.success.result.user.filter((bets) => {
                        if (bets.status !== 'LOST' && bets.status !== 'WON') {
                            return [bets.id, bets.bet_id, bets.winningAmount, bets.gameName, bets.kind, bets.status, bets.amounts, bets.results, bets.amount, bets.odd, bets.possibleWinning, bets.coupon, bets.staked, bets.stakes, bets.stakes1, bets.stakes2, bets.date]
                        }
                    })
                    setArr(data)
                })
                .catch(error => console.log('error', error));
        }

        fetchData()

        return () => {
            isCancelled = true
        }
   
    }, [])

    const handleClick = (e) => {
        e.preventDefault()
        history.push('/games')
    }

    const handleClicks = (e, coupon) => {
        e.preventDefault()
        let v = arr.find((items) => items.coupon === coupon)
        setSingleBet(v)
        setShow1(true)
        setShow(true)
    }

    const filterItems2 = (e) => {
        let dates = new Date(e.replace('/', ''))
        let newItems = bets.filter((item) => new Date(item.date) < dates);
        setArr(newItems)
    }
    
    const handleSelect = e => {
        e.preventDefault()
        setCat([e.target.value])
    }

    return (
        <div className={`${arr.length < 2 && 'game_bottom'}`}>
            <div className=' ' style={{ marginTop: '7rem', borderBottom: '2px solid green', paddingBottom: '10px' }}>
                <div className='d-flex justify-content-around'>
                    <p className={`${cat.includes('ALL') && 'active_p'}`} onClick={() => { setChange(false); setCat(['ALL']); filterItem('ALL') }} style={{ cursor: 'pointer' }}>Open Bets</p>
                    <p style={{ cursor: 'pointer' }} onClick={() => { setCat(['OTHERS']); setChange(true); filterItem('OTHERS') }} className={`${ change && 'active_p'}`}>Settled Bets</p>
                </div>

             {  !cat.includes('ALL') && !show1 && <div className='d-flex justify-content-between align_all'>
                    <p className='ml-2 mt-3 mt-lg-2'>Filter Games</p>
                    <div className="d-flex justify-content-between get_flex">
                        <div className='d-none mt-2 mr-5 d-lg-flex '>
                            {item.map((it) => <p className={`pr-3 ${cat.includes(it) && 'active_p'}`} onClick={() => filterItem(it)} style={{ cursor: 'pointer' }}>{it}</p>)}
                        </div>

                            <Form.Control className='chooser mb-lg-3 d-inline d-lg-none' as="select" required onChange={handleSelect} custom>
                                <option name='WON' value='WON'>WON</option>
                                <option name='LOST' value='LOST'>LOST</option>
                            </Form.Control>

                        <Form.Control
                            required
                            type="date"
                            name="dob"
                            min={min}
                            max={date}
                            // value={users.dob}
                            onChange={(e) => filterItems2(e.target.value)}
                            placeholder={dt.getDate()}
                            className='date_selector'
                        />
                    </div>
                </div>}
            </div>
            {arr.length ?
            
                <section className='d-md-flex flex-md-wrap justify-content-md-center mt-5'>
                    {arr.map((a) => {
                        return (
                            <>
                                {!show && <Allbets key={a.id} a={a} handleClicks={handleClicks} />}
                            </>
                        )
                    })}
                    {show && <BetHistory singleBet={singleBet} />}
                </section>
                :
                <section className='bet_header_sections d-flex justify-content-center align-items-center flex-column'>
                    <img className='svg_img' src={noBet} alt="" />
                    <h1 className='bet_header'>Please Place a bet and try again... </h1>
                </section>
            }

            {!show && <Button size='sm' onClick={handleClick} className='bets_btn' variant='outline-success'>Play Game</Button>}
            {show && <Button size='sm' onClick={() => { setShow(false); setShow1(false) }} className='bets_btn' variant='outline-success' style={{marginBottom: '7rem'}}>Back to bet history</Button>}
            {success && <IntegrationNotistack className='d' success={`${success}`} />}
        </div>
    )
}


const Allbets = ({a, handleClicks}) => {
        let dates = new Date(a.date)
    let playTime = dates.toString().slice(0, 24)
    return (
        <main className=' ml-md-4 ml-lg-5 main_sec'>
            <section className='betHistory_section ml-2 ml-lg-4'>
                <p className='p_bets' >Coupon Code: <span className='bets_span'>{a.coupon}</span></p>
            <p className='p_bets'>Game Time: <span className='bets_span'>{playTime}</span></p>
            <p className='p_bets'>Status: <span className='bets_span'>{a.status}</span></p>
            <p className='p_bets' onClick={(e) => handleClicks(e, a.coupon)} style={{textDecoration: 'underline', cursor: 'pointer'}}><span className='bets_span'>View More</span></p>
            </section>
      </main>
  )
}

const BetHistory = ({ singleBet }) => {

    // console.log(props)
    const { amount, bet_id, winningAmount, coupon, type, odd, kind, amounts, gameName, status, possibleWinning, staked, stakes, stakes1, results, stakes2, date } = singleBet;
    console.log(coupon)

    let dates = new Date(date)
    let playTime = dates.toString().slice(0, 24)
    
    return (
        <main className=' ml-md-4 ml-lg-5 main_sec'>
            <section className='betHistory_section ml-2 ml-lg-4'>
                {/* hello */}
            <p className='p_bets'>Game Type: <span className='bets_span'>{type}</span></p>
            {kind && <p className='p_bets'>Soft Lotto: <span className='bets_span'>{kind}</span></p>}
            <p className='p_bets'>Game Time: <span className='bets_span'>{playTime}</span></p>
            <p className='p_bets'>Game ID: <span className='bets_span'>{gameName + " " + playTime}</span></p>
            <p className='p_bets'>Odd: <span className='bets_span'>{odd}</span></p>
            <p className='p_bets'>Possible Winning: <span className='bets_span'>&#x20A6;{possibleWinning}</span></p>
            <p className='p_bets'>Stake Amount per line: <span className='bets_span'>&#x20A6;{amounts}</span></p>
            {stakes && <p className='p_bets'>Numbers: <span className='bets_span'>{stakes}</span></p>}
            {stakes1 && <p className='p_bets'>Against 1: <span className='bets_span'>{stakes1}</span></p>}
            {stakes2 && <p className='p_bets'>Agaisnt 2: <span className='bets_span'>{stakes2}</span></p>}
            <p className='p_bets'>Amount: <span className='bets_span'>&#x20A6;{amount}</span></p>
            <p className='p_bets'>Status: <span className='bets_span'>{status}</span></p>
            <p className='p_bets'>Coupon Code: <span className='bets_span'>{coupon}</span></p>
            {results && <p className='p_bets'>Numbers Drawn: <span className='bets_span'>{results}</span></p>}
            {status === 'WON' && <p className='p_bets'>Winning Amount: <span className='bets_span'>&#x20A6;{winningAmount}</span></p>}    
            </section>
        </main>
        
   )
}

export default Bets
