import React, { useState, useEffect } from 'react';
import { Form, Button } from 'react-bootstrap';
import { PaystackButton } from "react-paystack"
import IntegrationNotistack from '../Fetch/IntegrationNotistack';
import { Redirect } from 'react-router-dom'
import Layout from '../Layout'
import { useHistory } from 'react-router';
import { useGlobalContext } from '../store/context';

const FundWallet = (props) => {
    let { giveAccess, isLoggedIn, urls, getUser, showBoard, user, refreshCanvas } = useGlobalContext();
    const [amount, setAmount] = useState(null)
    const [opay, setOpay] = useState(false)
    const [opayAmount, setOpayAmount] = useState('')
    const [paystack, setPaystack] = useState(true)
    const publicKey = process.env.REACT_APP_PAYSTACK_PUBLIC_KEY
    const [show, setShow] = useState(false)
    const history = useHistory()
    const get = localStorage.getItem('token')
    const url = 'https://api.grandlotto.ng'
    const [success, setSuccess] = useState('')
    const [showAlert, setShowAlert] = useState(false)
  
    // console.log(urls.indexOf('/'))

    // const getU = () => {
    //     return 
    // }
    // arr.slice(arr.indexOf(0) + 1, arr[arr.length - 1])

    const handleSubmit2 = e => {
        e.preventDefault()
        // console.log(opayAmount)

        var myHeaders = new Headers();
        myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
        myHeaders.append("Authorization", `Bearer ${get}`);
        myHeaders.append("Content-Type", "application/json");

        var raw = JSON.stringify({
            "amount": `${opayAmount}`,
            "url": `${'https://www.grandlotto.ng/fundwallet'}`
        });

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };

        fetch("https://api.grandlotto.ng/api/v2/auth/opay/initialise", requestOptions)
            .then(response => response.json())
            .then(result => {
                // console.log(result)
                if (result.success) {
                    const { data } = result.success.data;
                    window.location.href = data.cashierUrl
                    console.log(window.location.href)
                    // history.push(`/${data.cashierUrl}`)
                    // var myHeaders = new Headers();
                    // myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
                    // myHeaders.append("Authorization", `Bearer ${get}`);
                    // myHeaders.append("Content-Type", "application/json");

                    // var raw = JSON.stringify({
                    //     "reference": `${data.reference}`
                    // });

                    // var requestOptions = {
                    //     method: 'POST',
                    //     headers: myHeaders,
                    //     body: raw,
                    //     redirect: 'follow'
                    // };

                    // fetch("https://api.grandlotto.ng/api/v2/auth/opay/validate", requestOptions)
                    //     .then(response => response.json())
                    //     .then(result => {
                    //         console.log(result)
                    //         if (result.success) {
                    //             const { message } = result.success;
                    //             setSuccess(message)
                    //         } else {
                    //             setSuccess(result.error.message)
                    //         }
                    //     })
                    //     .catch(error => console.log('error', error));
                }
            })
            .catch(error => console.log('error', error));
    }

    const handleChange1 = e => {
        e.preventDefault()
        setOpayAmount(e.target.value)
    }



    let reference = new Date().getTime().toString()

    let componentProps;

    if (user) {
        
        let val = user.mobile + '@grandlotto.ng';
        
        componentProps = {
            email: user.email !== null ? user.email : val,
            reference,
            amount: parseInt(amount) * 100,
            metadata: {
                name: `${user.firstname} ${user.lastname}`,
                phone: user.mobile
            },
            publicKey,
            text: "Paystack",
            onSuccess: () => {
                var myHeaders = new Headers();
                myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
                myHeaders.append("Authorization", `Bearer ${get}`);
                myHeaders.append("Content-Type", "application/json");

                var raw = JSON.stringify({
                    "reference": `${reference}`
                });

                var requestOptions = {
                    method: 'POST',
                    headers: myHeaders,
                    body: raw,
                    redirect: 'follow'
                };

                //http://localhost:5016
                //https://lotto-api.firstbetng.com/                 

                fetch(`${url}/api/v2/auth/fund-wallet`, requestOptions)
                    .then(response => response.json())
                    .then(result => {
                        if (result.success) {
                            const { message } = result.success;
                            setSuccess(message)
                            var myHeaders = new Headers();
                            myHeaders.append("signatures", "5a1131f2eb747be50714281ec3e68b759476c6dc9e1faf5fc5d91c552cf8c230");
                            myHeaders.append("Authorization", `Bearer ${get}`);

                            var requestOptions = {
                                method: 'GET',
                                headers: myHeaders,
                                redirect: 'follow'
                            };

                            fetch(`${url}/api/v2/auth/profile`, requestOptions)
                                .then(response => response.json())
                                .then(result => {
                                    if (result.success) {
                                        getUser(result.success.data)
                                        const { data } = result.success;
                                        showBoard(data)
                                        return history.push(`${urls}`)
                                        //   localStorage.setItem('user', JSON.stringify(data))
                                    } else {
                                        return;
                                    }
                                },
                                    (error) => {
                                        console.log(error)
                                    });

                        } else {
                            if (result.error) {
                                setSuccess(result.error.message);
                            }
                        }
                    })
                    .catch(error => console.log('error', error));
                setAmount("")
                props.updateProfile()
                setSuccess(`Successfully Credited your account ${refreshCanvas()}`)
            },
            onClose: () => alert("Plase come back soon"),
        };
    }

    const handleSubmit = (e) => {
        e.preventDefault()
    }

    const handleChange = (e) => {
        e.preventDefault()
        setAmount(e.target.value)
    }

    useEffect(() => {
        setTimeout(() => {
            setShowAlert(!showAlert)
        }, 3000)
    }, [success])

    const handleClick = (e) => {
        e.preventDefault()
        history.push(`/games`)
    }

    return (
        <section>
   <h5 className='payment_h5'>Choose A Payment Method</h5>
        <section className='d-flex justify-content-start wide'>
            <div className='choose_pay' onClick={() => { setPaystack(true); setOpay(false)}}>
                <h5>Paystack</h5>
            </div>
            <div className='choose_pay' onClick={() => { setPaystack(false); setOpay(true)}}>
                <h5>Opay</h5>
            </div>
        </section>
        {
            paystack &&
            <section className='widths'>
                <span className='span_stack'>Payment Via Paystack</span>
               <Form onSubmit={handleSubmit}>
                    <Form.Label htmlFor="inputPassword5">Amount:</Form.Label>
                    <Form.Control
                      type="text"
                        name='amount'
                        value={amount}
                        placeholder='Amount'
                      className='input_widths'
                      onChange={handleChange}
                      id="inputPassword5"
                      aria-describedby="passwordHelpBlock"
                    />
                    </Form>
                {/* <Button className="paystack-button" variant='success' {...componentProps} type='submit'>paystack</Button> */}
                 <PaystackButton className="paystack-button" {...componentProps} />
        </section>
        }
        {
            opay &&
            <section className='widths'>
                <span className='span_stack'>Payment Via Opay</span>
               <Form onSubmit={handleSubmit2}>
                    <Form.Label htmlFor="inputPassword5">Amount:</Form.Label>
                    <Form.Control
                      type="text"
                        name='amount'
                        value={opayAmount}
                        placeholder='Amount'
                      className='input_widths'
                      onChange={handleChange1}
                      id="inputPassword5"
                      aria-describedby="passwordHelpBlock"
                    />
                <Button className="paystack-button" variant='success' type='submit'>Opay</Button>
                </Form>
                 {/* <PaystackButton className="paystack-button" {...componentProps} /> */}
        </section>
            }
            {success && <IntegrationNotistack className='d' success={`${success}`} />}
            </section>
    )
}

export default FundWallet


import React from 'react'
import { Container, Row, Col } from 'react-bootstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faApple, faGooglePlay, faFacebook, faYoutube, faTwitter, faInstagram, faLinkedin, faWhatsapp } from '@fortawesome/free-brands-svg-icons'
import { faPhone, faHome, faPrint } from '@fortawesome/free-solid-svg-icons'
import { Link } from 'react-router-dom'
import logo from '../static/assets/GrandLotto.svg'
import { faEnvelope } from '@fortawesome/free-regular-svg-icons'


const Footer = () => {

  let getYear = new Date().getFullYear()
    
  return (
    <footer className="text-center text-lg-start bg-dark text-muted">
      <section
        class="d-md-flex justify-content-center flex-row justify-content-lg-between p-4 border-bottom"
      >
        <div className="ml-5 mr-5  d-lg-block">
          <span>Get connected with us on social networks:</span>
        </div>
        <div>
          <div>
            <Link to='https://www.facebook.com'>
              <FontAwesomeIcon className='' size-mode='2x' icon={faFacebook} />
            </Link>
            <Link to='https://www.twitter.com'>
              <FontAwesomeIcon className='ml-5 mr-5' size-md='2x' icon={faTwitter} />
            </Link>
            <Link to='https://www.instagram.com'>
              <FontAwesomeIcon className='mr-5' size-md='2x' icon={faInstagram} />
            </Link>
            <Link to='https://www.youtube.com'>
              <FontAwesomeIcon className=' ' size-md='2x' icon={faYoutube} />
            </Link>
          </div>
   
      
        </div>
      </section>
      <section className="">
        <div className="container text-center text-md-start mt-5">
          <div className="row mt-3">
            <div className="d-none d-lg-inline col-md-3 col-lg-4 col-xl-3 mt-auto mb-4">
              <h6 className="text-uppercase fw-bold mb-4">
                <i><img src={logo} /></i>
              </h6>
              <p className=''>
                Instant Payment, No Story!
              </p>
            </div>
            <div className="d-none d-lg-inline col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">
              <h6 className="text-uppercase fw-bold mb-4">
                Products
              </h6>
              <p>
                <a href="#" className="text-center text-reset">FirstBet</a>
              </p>
          
            </div>
            <div className="d-none d-lg-inline col-md-3 col-lg-2 col-xl-2 mx-auto mb-4">
              <h6 className="text-uppercase fw-bold mb-4">
                Useful links
              </h6>
              <p>
                <a href="#" className="text-reset">How To Bet</a>
              </p>
              <p>
                <a href="#" className="text-reset">Agency Operator</a>
              </p>
              <p>
                <a href="#" className="text-reset">FAQs</a>
              </p>
         
            </div>
            <div className="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-2">
              <div className='col-md-12 mx-auto mb-md-0 mb-4'>
                <h6 class="d-none d-lg-flex justify-content-center text-uppercase fw-bold mb-4">Contact</h6>
                <div className='d-flex justify-content-between'>
                  <FontAwesomeIcon className='mr-3' size-mode='2x' icon={faPhone} />
                  <Link to=''>
                    <p className='text-muted'>   +234 903 000 3177</p>
                  </Link>
                </div>
                <div className='d-flex justify-content-between'>
                  <FontAwesomeIcon className='mr-3' size-md='2x' icon={faEnvelope} />
                  <Link to=''>
                    <p className='text-muted'>  info@grandlotto.com</p>
                  </Link>
                </div>
                {/* <div className='d-flex justify-content-between'>
                  <FontAwesomeIcon className='mr-3' size-md='2x' icon={faPrint} />
                  <Link to=''>
                    <p className='text-muted'>   + 234 234 567 88</p>
                  </Link>
                </div> */}
                                <div className='d-flex justify-content-between'>
                  <FontAwesomeIcon className='mr-5' size-md='2x' icon={faHome} />
                  <Link to='/about'>
                    <p className='text-muted'>About Us</p>
                  </Link>
                </div>

              </div>
                            
            </div>
          </div>
        </div>
      </section>
      <div className="text-center p-4" style={{ backgroundColor: 'rgba(0, 0, 0, 0.05)' }}>
        © {getYear}
        <a className="text-reset fw-bold" href="/"> GrandLotto</a>
      </div>
    </footer>
  )
}

export default Footer




// 1


