import React, { useState, useEffect } from 'react';
import { Form, Button, Row, Col, Container, InputGroup } from 'react-bootstrap';
import { Formik } from 'formik';
import IntegrationNotistack from '../Fetch/IntegrationNotistack';
import { useHistory } from 'react-router';
 import * as yup from 'yup';
import Layout from '../Layout'

const schema = yup.object().shape({
  token: yup.string().required(),
  password: yup.string().min(8, 'Must be 8 characters or more').required(),
  confPassword: yup.string().oneOf([yup.ref('password'), null], 'Password Must Match').required('Confirm password')
});



const PasswordUp = () => {
    const [error, setError] = useState(null)
    const url = 'https://api.grandlotto.ng'
    const [success, setSuccess] = useState(null);
    const [showAlert, setShowAlert] = useState(false)
    const history = useHistory()
    

    

    useEffect(() => {
        setTimeout(() => {
            setShowAlert(!showAlert)
            setSuccess('')
        }, 3000)
    }, [success])
        
    return (
        <section className='register_section d-flex justify-content-center'>
            <Container fluid='md'>
                <Row>
                    <Col className='mt-5' md={{ span: 12, offset: 1 }}>
                        <Formik
                            validationSchema={schema}
                            onSubmit={values => {
                                console.log(values.token)
                                var myHeaders = new Headers();
                                myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
                                myHeaders.append("Content-Type", "application/json");

                                var raw = JSON.stringify({
                                    "token": `${values.token}`
                                });

                                var requestOptions = {
                                    method: 'POST',
                                    headers: myHeaders,
                                    body: raw,
                                    redirect: 'follow'
                                };

                                fetch(`${url}/api/v1/validate-token`, requestOptions)
                                    .then(response => response.json())
                                    .then(result => {
                                        if (result.success) {
                                            var myHeaders = new Headers();
                                            myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
                                            myHeaders.append("Content-Type", "application/json");

                                            var raw = JSON.stringify({
                                                "token": `${values.token}`,
                                                "password": `${values.password}`
                                            });

                                            var requestOptions = {
                                                method: 'POST',
                                                headers: myHeaders,
                                                body: raw,
                                                redirect: 'follow'
                                            };

                                            fetch(`${url}/api/v1/update-password`, requestOptions)
                                                .then(response => response.json())
                                                .then(result => {
                                                    if (result.success) {
                                                        const { message } = result.success;
                                                        setSuccess(message)
                                                    } else if (result.error) {
                                                        const { message } = result.error;
                                                        setError(message)
                                                    } else {
                                                        return
                                                    }
                                                },
                                                    (error) => {
                                                        console.log(error)
                                                    });
                                        } else if (result.error) {
                                            const { message } = result.error;
                                            setError(message)
                                        } else {
                                            return;
                                        }
                                    },
                                        (error) => {
                                            console.log(error)
                                        });
                                history.push('/games')
                            }}
                            initialValues={{
                                token: '',
                                password: '',
                                confPassword: ''
                            }}
                        >
                            {({
                                handleSubmit,
                                handleChange,
                                handleBlur,
                                values,
                                touched,
                                isValid,
                                errors,
                            }) => (
                                <Form noValidate onSubmit={handleSubmit}>
                                    <Form.Group as={Col} md="10" controlId="validationFormik04">
                                        <Form.Label>Token</Form.Label>
                                        <Form.Control
                                            value={values.token}
                                            type="text"
                                            name="token"
                                            onChange={handleChange}
                                            placeholder="token"
                                            isInvalid={!!errors.token}
                                        />
                                        <Form.Control.Feedback type="invalid">
                                            {errors.token}
                                        </Form.Control.Feedback>
                                    </Form.Group>
                                    <Form.Group as={Col} md="10" className='mt-2' controlId="validationFormik05">
                                        <Form.Label>New Password</Form.Label>
                                        <Form.Control
                                            type='password'
                                            placeholder="password"
                                            name="password"
                                            value={values.password}
                                            onChange={handleChange}
                                            isInvalid={!!errors.password}
                                        />
                                            
                                        <Form.Control.Feedback className='d-inline-block' type="invalid">
                                            {errors.password}
                                        </Form.Control.Feedback>


                                    </Form.Group>
                                    <Form.Group as={Col} md="10" className='mt-2' controlId="validationFormik05">
                                        <Form.Label>Confirm Password</Form.Label>
                                        <Form.Control
                                            type='password'
                                            placeholder="confirm password"
                                            name="confPassword"
                                            value={values.confPassword}
                                            onChange={handleChange}
                                            isInvalid={!!errors.confPassword}
                                        />
                                        <Form.Control.Feedback type="invalid">
                                            {errors.confPassword}
                                        </Form.Control.Feedback>
                                    </Form.Group>
                                    <Button className='mt-2 ml-3 my-3' type='submit' variant="outline-success">Submit</Button>
                                </Form>
                            )}
                        </Formik>
                    </Col>
                </Row>
            </Container>
            {success && <IntegrationNotistack className='d' success={`${success}`} />}
        </section>
    )
}

export default PasswordUp


