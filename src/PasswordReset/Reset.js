import React, { useState, useEffect } from 'react';
import { Form, Button, Row, Container, Col, Tab, Tabs } from 'react-bootstrap';
import { useGlobalContext } from '../store/context';
import { Formik } from 'formik';
import Layout from '../Layout'
import { Link } from 'react-router-dom';
import { useHistory } from 'react-router';
import IntegrationNotistack from '../Fetch/IntegrationNotistack';
 import * as Yup from 'yup';

//  const schema2 = Yup.object().shape({
//   email: Yup.string()
//   .when('mobile', {
//     is: (mobile) => !mobile || mobile.length === 0,
//     then: Yup.string()
//       .required('At least one of the fields is required'),
//   }),
//   mobile: Yup.string()
//   .when('email', {
//     is: (email) => !email || email.length === 0,
//     then: Yup.string()
//       .required('At least one of the fields is required'),
//   }),
//   password: Yup.string().min(8, 'Must be 8 characters or more').required('Please provide a password'),
// }, ['email', 'mobile'])

const schema2 = Yup.object().shape({
  email: Yup.string().email('Email is required').required('Email is required'),
  // checked: Yup.string().email('Check box is required').required('Please accept out T/Cs'),
  // confPassword: Yup.string().oneOf([Yup.ref('password'), null], 'Password Must Match').required('Confirm password')
});

const schema3 = Yup.object().shape({
  // checked: Yup.string().email('Check box is required').required('Please accept out T/Cs'),
  mobile: Yup.string().min(11, 'Must be 11 characters long').required(),
  // confPassword: Yup.string().oneOf([Yup.ref('password'), null], 'Password Must Match').required('Confirm password')
});

// const schema = yup.object().shape({
//   mobile: yup.string().min(11, 'Must be 11 characters or more').required(),
//   password: yup.string().min(8, 'Must be 8 characters or more').required(),
// });

const Login = () => {
    const { giveAccess, showBoard, isLoggedIn } = useGlobalContext();
    let history = useHistory()
    const [success, setSuccess] = useState(false)
    const [show, setShow] = useState(false)
    const url = 'https://api.grandlotto.ng'
    const [showAlert, setShowAlert] = useState(false)

    useEffect(() => {
        let time = setTimeout(() => {
            setShowAlert(!showAlert)
            setSuccess('')
        }, 3000)

        return () => clearTimeout(time)
    }, [success]);
    
    return (
        <article>
            <div style={{ marginTop: '42px' }} className='d-flex justify-content-center'>
                <h5 className='green'>Password Reset</h5>
            </div>
        
            <Tabs defaultActiveKey="Phone" className='d-flex justify-content-center' style={{ marginTop: '20px', marginBottom: '70px' }} transition={false} id="noanim-tab-example">
                <Tab eventKey="Phone" title="Phone">
                    <section className='register_sections d-flex justify-content-center'>
                        <Container fluid='md'>
                            <Row>
                                <Col className='mt-5' md={{ span: 12, offset: 1 }}>
                                    <h5 className='ml-3 mb-1 green h'>Please enter either email or phone number</h5>
                                    <Formik
                                        validationSchema={schema3}
                                        onSubmit={values => {
                                            var myHeaders = new Headers();
                                            myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
                                            myHeaders.append("Content-Type", "application/json");


                                            var raw = JSON.stringify({
                                                "mobile": `${values.mobile}`
                                            });


                                            var requestOptions = {
                                                method: 'POST',
                                                headers: myHeaders,
                                                body: raw,
                                                redirect: 'follow'
                                            };

                                            // https://grandlotto.herokuapp.com/api/v2/auth/profile

                                            fetch(`${url}/api/v1/password-reset`, requestOptions)
                                                .then(response => response.json())
                                                .then(result => {
                                                    console.log(result)
                                                    if (result.success) {
                                                        const { message } = result.success;
                                                        setSuccess(message)
                                                        setShow(true)
                                                        // history.push('/profile/reset/password')
                                                    } else if (result.error) {
                                                        const { message } = result.error;
                                                        setSuccess(message)
                                                    } else {
                                                        return;
                                                    }
                                                },
                                                    (error) => {
                                                        console.log(error)
                                                    });

                                        }}
                                        initialValues={{
                                            mobile: '',
                                        }}
                                    >
                                        {({
                                            handleSubmit,
                                            handleChange,
                                            handleBlur,
                                            values,
                                            touched,
                                            isValid,
                                            errors,
                                        }) => (
                                            <Form noValidate onSubmit={handleSubmit}>
                                                <Form.Group as={Col} md="10" className='mt-2' controlId="validationFormik04">
                                                    <Form.Label>Mobile Number</Form.Label>
                                                    <Form.Control
                                                        type="text"
                                                        name="mobile"
                                                        onChange={handleChange}
                                                        placeholder="Mobile"
                                                        isInvalid={!!errors.mobile}
                                                        value={values.mobile}
                                                    />
                                                    <Form.Control.Feedback type="invalid">
                                                        {errors.mobile}
                                                    </Form.Control.Feedback>
                                                </Form.Group>
                                                <Button className='mt-2 ml-3 my-3' type='submit' size='sm' variant="success">Submit</Button>
                                                {show && <Button className='mt-2 ml-3 my-3' size='sm' variant="outline-success" onClick={() => history.push('/profile/reset/password')}>Verify Token</Button>}
                                                <div className='d-inline ml-3'>
                                                      
                                                    <p>Get reset code via <Link className='mt-3 rest_link mr-2' to='/validate/voice'>Voice Call</Link>  <Link className='mt-3 rest_link' to='/validate/whatsapp'>Whatsapp</Link></p>
                                                </div>
                                            </Form>
        
                                        )}
                                    </Formik>
                                </Col>
                            </Row>
                            {success && <IntegrationNotistack success={`${success}`} />}
                        </Container>
                    </section>
                </Tab>
                <Tab eventKey="Email" title="Email">
                    <section className='register_sections d-flex justify-content-center'  style={{ marginTop: '20px', marginBottom: '70px' }}>
                        <Container fluid='md'>
                            <Row>
                                <Col className='mt-5' md={{ span: 12, offset: 1 }}>
                                    <h5 className='ml-3 mb-1 green h'>Please enter either email or phone number</h5>
                                    <Formik
                                        validationSchema={schema2}
                                        onSubmit={values => {
                                            var myHeaders = new Headers();
                                            myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
                                            myHeaders.append("Content-Type", "application/json");

                                            var raw = JSON.stringify({
                                                "email": `${values.email}`,
                                            });


                                            var requestOptions = {
                                                method: 'POST',
                                                headers: myHeaders,
                                                body: raw,
                                                redirect: 'follow'
                                            };

                                            // https://grandlotto.herokuapp.com/api/v2/auth/profile

                                            fetch(`${url}/api/v1/password-reset`, requestOptions)
                                                .then(response => response.json())
                                                .then(result => {
                                                    if (result.success) {
                                                        const { message } = result.success;
                                                        setSuccess(message)
                                                        setShow(true)
                                                        // history.push('/profile/reset/password')
                                                    } else if (result.error) {
                                                        const { message } = result.error;
                                                        setSuccess(message)
                                                    } else {
                                                        return;
                                                    }
                                                },
                                                    (error) => {
                                                        console.log(error)
                                                    });

                                        }}
                                        initialValues={{
                                            email: '',
                                            // referer: '',
                                            // confPassword: '',
                                        }}
                                    >
                                        {({
                                            handleSubmit,
                                            handleChange,
                                            handleBlur,
                                            values,
                                            touched,
                                            isValid,
                                            errors,
                                        }) => (
                                            <Form noValidate onSubmit={handleSubmit}>
                                                <Form.Group as={Col} md="10" className='mt-2' controlId="validationFormik04">
                                                    <Form.Label>Email</Form.Label>
                                                    <Form.Control
                                                        value={values.email}
                                                        type="email"
                                                        name="email"
                                                        onChange={handleChange}
                                                        placeholder="Email"
                                                        isInvalid={!!errors.email}
                                                    />
                                                    <Form.Control.Feedback type="invalid">
                                                        {errors.email}
                                                    </Form.Control.Feedback>
                                                </Form.Group>
                                                <Button className='mt-2 ml-3 my-3' type='submit' size='sm' variant="success">Submit</Button>
                                                {show && <Button className='mt-2 ml-3 my-3' size='sm' variant="outline-success" onClick={() => history.push('/profile/reset/password')}>Verify Token</Button>}

                                                {/* <div className='d-inline ml-3'>
                                                      
                                                    <p>Get reset code via <Link className='mt-3 rest_link' to='/validate/voice'>Voice Call</Link>  <Link className='mt-3 rest_link' to='/validate/whatsapp'>Whatsapp</Link></p>
                                                </div> */}

                                            </Form>
        
                                        )}
                                    </Formik>
                                </Col>
                            </Row>
                            {success && <IntegrationNotistack success={`${success}`} />}
                        </Container>
                    </section>
                </Tab>
            </Tabs>
        </article>
    )
}

export default Login

