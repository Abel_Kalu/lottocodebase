import React, { useState, useEffect } from 'react';
import { Modal, Button, Form } from 'react-bootstrap';
import { useGlobalContext } from '../store/context';
import { useHistory } from 'react-router';
import IntegrationNotistack from '../Fetch/IntegrationNotistack';
import { FaSortAmountDown, FaTimes } from 'react-icons/fa';
import { Link } from 'react-router-dom';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import login from '../svg/login.svg'


function MyVerticallyCenteredModal(props) {
    let { user, getUser, showBoard } = useGlobalContext();
    const [success, setSuccess] = useState('')
    const [status, setStatus] = useState(null)
    const [submited, setSubmited] = useState(false)
    const get = localStorage.getItem('token')
    const [coupon, setCoupon] = useState('')
    const [id, setId] = useState(Math.floor(Math.random() * 10000000000) + 1)
    const [list, setList] = useState([])
    const [nap5Max, setNap5Max] = useState(null)
    const url = 'https://api.grandlotto.ng'
    const [listV, setListV] = useState(0)
    const [number, setNumber] = useState(0)
    const [mode, setMode] = useState('')
    const [open, setOpen] = useState(false)

  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header>
        <Modal.Title id="contained-modal-title-vcenter" className='d-flex justify-content-center align-items-center'>
        <img src="assets/brand/GrandLotto.svg" alt="" width="200px" height="60px" className="d-inline-block align-text-top nav_img" />          
        </Modal.Title>
      </Modal.Header>
          <Modal.Body style={{background: 'black'}}>
        {props.days.map((day, i) => {
          return (
            <section className=''>
              <div className='htext' key={i} onClick={(e) => props.handleCategory(e, i)}>
                <span className=''>{day}</span>
              </div>
              {
                <div className='game_types'>
                  {props.game.map((gam, index) => {
                    if (props.secArr.includes(i) && props.daysShow.includes(gam.day.charAt(0).toUpperCase() + gam.day.slice(1))) {
                      return <img src={gam.image} onClick={(e) => {
                        e.preventDefault()
                        props.clickeds(e, index)
                        props.setClicked()
                        props.setGameShow({ ...props.gameShow, name: gam.name, endTime: gam.endTime, day: gam.day, image: gam.image, start: gam.startTime, end: gam.endTime, id: gam.uuid })
                        props.showGameType()
                      }}
                        className={`${props.a.includes(index) ? 'actives' : 'types'} `} />
                    }
                  })}
                </div>
              }
            </section>
          )
        })}
        </Modal.Body>
      <Modal.Footer>
              <Button variant='success' onClick={() => { props.onHide(); props.showGameType() }}>Close</Button>
          </Modal.Footer>
    </Modal>
  );
}

function App(props) {
  const [modalShow, setModalShow] = useState(true);

  return (
    <>
      <MyVerticallyCenteredModal
            show={modalShow}
            onHide={() => setModalShow(false)}
            showGameType={props.showGameType}
            days={props.days}
            handleCategory={props.handleCategory}
            game={props.game}
            secArr={props.secArr}
            gameShow={props.gameShow}
            a={props.a}
            daysShow={props.daysShow}
            clickeds={props.clickeds}
            setGameShow={props.setGameShow}
            setClicked={props.setClicked}
          />
    </>
  );
}

export default App