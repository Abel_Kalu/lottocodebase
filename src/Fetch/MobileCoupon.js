import React, { useState, useEffect } from 'react';
import { Modal, Button, Form } from 'react-bootstrap';
import { useGlobalContext } from '../store/context';
import { useHistory } from 'react-router';
import IntegrationNotistack from '../Fetch/IntegrationNotistack';
import { FaSortAmountDown, FaTimes } from 'react-icons/fa';
import { Link } from 'react-router-dom';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import login from '../svg/login.svg'


function MyVerticallyCenteredModal(props) {
    let { user, getUser, showBoard } = useGlobalContext();
    const [success, setSuccess] = useState('')
    const [status, setStatus] = useState(null)
    const [submited, setSubmited] = useState(false)
    const get = localStorage.getItem('token')
    const [coupon, setCoupon] = useState('')
    const [id, setId] = useState(Math.floor(Math.random() * 10000000000) + 1)
    const [list, setList] = useState([])
    const [nap5Max, setNap5Max] = useState(null)
    const url = 'https://api.grandlotto.ng'
    const [listV, setListV] = useState(0)
    const [number, setNumber] = useState(0)
    const [mode, setMode] = useState('')
    const [open, setOpen] = useState(false)

    const handleCouponChange = e => {
        e.preventDefault();
        setCoupon(e.target.value)
    }

    const handleV = (e, items) => {
        e.preventDefault()
        if (isNaN(e.target.value) || e.target.value < 1) {
            return;
        } else {
            setListV(e.target.value)
            items.amounts = e.target.value
            items.amount = e.target.value * items.line
            items.staked = e.target.value * items.line
        }
    }

    useEffect(() => {
        let time = setTimeout(() => {
            setSuccess('')
        }, 3000)

        return () => clearTimeout(time)
    }, [success]);

    const handleCouponBet = e => {
        e.preventDefault()
        const val = list.reduce((total, money) => {
            total += parseInt(money.amounts)
            return total;
        }, 0);
        if (user.balance > val) {
            if (val < 1) {
                setSuccess(`Kindly add an amount`)
                return;
            } else {
                var myHeaders = new Headers();
                myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
                myHeaders.append("Authorization", `Bearer ${get}`);
                myHeaders.append("Content-Type", "application/json");
                myHeaders.append("Cookie", "connect.sid=s%3Ae0Zu5esOyG3kUqFKLpyqNN4lka0d4o3F.WngMLxSIx3GfvGfOlQDWPwr%2BeLG2ABZ6sABSEo4Hwkc");

                list.filter((bet, i) => {
                    const { coupon, type, gameId, stakes, stakes1, stakes2, amount, kind, line, gameName, amounts, status } = bet;


                    if (amount < 1) {
                        setSuccess(`cannot place bet for ticket number ${i + 1} please add an amount`)
                        return;
                    } else if (amount > user.wallet || amount > user.withdrawable) {
                        props.setShowCoupons()
                        return;
                    } else if (status !== 'PENDING') {
                        setSuccess(`Cannot place bet on expired coupons`)
                        return;
                    } else {
                        // console.log(listV)
                        // console.log(amount)
                        // console.log(amounts)
                        // console.log(coupon)
                        if (props.value.includes('regular lotto')) {
                            var myHeaders = new Headers();
                            myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
                            myHeaders.append("Authorization", `Bearer ${get}`);
                            myHeaders.append("Content-Type", "application/json");
                            myHeaders.append("Cookie", "connect.sid=s%3Ae0Zu5esOyG3kUqFKLpyqNN4lka0d4o3F.WngMLxSIx3GfvGfOlQDWPwr%2BeLG2ABZ6sABSEo4Hwkc");

                            if (amount > 1) {
                                if (amount > user.wallet && amount > user.withdrawable) {
                                    setSuccess('Kindly fund your account')
                                    return;
                                } else {
                                    var raw = JSON.stringify({
                                        "totalStake": `${number}`,
                                        "stakes": [
                                            {
                                                "coupon": `${id + 'regularlotto'}`,
                                                "amount": `${amount}`,
                                                "amounts": `${amounts}`,
                                                "type": `${type}`,
                                                "stakeList": `${stakes}`,
                                                "gameName": `${gameName}`,
                                                "stakeList1": `${stakes1}`,
                                                "stakeList2": `${stakes2}`,
                                                "gameId": `${gameId}`,
                                                "game": "Regular Lotto"
                                            },
                                        ]
                                    });

                                    var requestOptions = {
                                        method: 'POST',
                                        headers: myHeaders,
                                        body: raw,
                                        redirect: 'follow'
                                    };

                                    fetch(`${url}/api/v1/placeStake`, requestOptions)
                                        .then(response => response.json())
                                        .then(result => {
                                            if (result.result) {
                                                setSubmited(true)
                                                let show = result.result.map((res) => res)
                                                if (show[0].type === 'AGAINST') {
                                                    const { type, amount, odd, staked, line, gameName, coupon, date, amounts, possibleWinning, gameId, stakes1, stakes2 } = show[0];
                                                    let response1 = stakes1.toString()
                                                    let response2 = stakes2.toString()
                                                    var myHeaders = new Headers();
                                                    myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
                                                    myHeaders.append("Authorization", `Bearer ${get}`);
                                                    myHeaders.append("Content-Type", "application/json");

                                                    var raw = JSON.stringify({
                                                        "game": 'Regular Lotto',
                                                        "amount": `${amount}`,
                                                        "amounts": `${amounts}`,
                                                        "type": `${type}`,
                                                        "odd": `${odd}`,
                                                        "line": `${line}`,
                                                        "gameName": `${gameName}`,
                                                        "possibleWinning": `${possibleWinning}`,
                                                        "staked": `${amount}`,
                                                        "stakes1": `${response1}`,
                                                        "stakes2": `${response2}`,
                                                        "date": `${date}`,
                                                        "coupon": `${coupon}`,
                                                        "gameId": `${gameId}`
                                                    });

                                                    var requestOptions = {
                                                        method: 'POST',
                                                        headers: myHeaders,
                                                        body: raw,
                                                        redirect: 'follow'
                                                    };

                                                    fetch(`${url}/api/v2/auth/betHistory`, requestOptions)
                                                        .then(response => response.json())
                                                        .then(result => {
                                                            if (result.success) {
                                                                setId(Math.floor(Math.random() * 10000000000) + 1)
                                                                const { message } = result.success;
                                                                setSuccess(message)
                                                                var myHeaders = new Headers();
                                                                myHeaders.append("signatures", "5a1131f2eb747be50714281ec3e68b759476c6dc9e1faf5fc5d91c552cf8c230");
                                                                myHeaders.append("Authorization", `Bearer ${get}`);

                                                                var requestOptions = {
                                                                    method: 'GET',
                                                                    headers: myHeaders,
                                                                    redirect: 'follow'
                                                                };

                                                                fetch(`${url}/api/v2/auth/profile`, requestOptions)
                                                                    .then(response => response.json())
                                                                    .then(result => {
                                                                        if (result.success) {
                                                                            getUser(result.success.data)
                                                                            const { data } = result.success;
                                                                            showBoard(data)
                                                                            //   localStorage.setItem('user', JSON.stringify(data))
                                                                        } else {
                                                                            return;
                                                                        }
                                                                    },
                                                                        (error) => {
                                                                            console.log(error)
                                                                        });

                                                            } else {
                                                                console.log(result)
                                                            }
                                                        })
                                                        .catch(error => console.log('error', error));

                    
                                                } else if (show[0].type === '1 BANKER') {
                                                    const { type, amount, coupon, odd, staked, gameName, line, amounts, date, possibleWinning, gameId, stakes } = show[0];
                                                    let response = stakes.toString()
                                                    var myHeaders = new Headers();
                                                    myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
                                                    myHeaders.append("Authorization", `Bearer ${get}`);
                                                    myHeaders.append("Content-Type", "application/json");

                                                    var raw = JSON.stringify({
                                                        "game": 'Regular Lotto',
                                                        "amount": `${amount}`,
                                                        "amounts": `${amounts}`,
                                                        "type": `${type}`,
                                                        "odd": `${odd}`,
                                                        "line": `${line}`,
                                                        "gameName": `${gameName}`,
                                                        "amounts": `${amount}`,
                                                        "possibleWinning": `${possibleWinning}`,
                                                        "staked": `${staked}`,
                                                        "stakes": `${response}`,
                                                        "date": `${date}`,
                                                        "coupon": `${coupon}`,
                                                        "gameId": `${gameId}`
                                                    });

                                                    var requestOptions = {
                                                        method: 'POST',
                                                        headers: myHeaders,
                                                        body: raw,
                                                        redirect: 'follow'
                                                    };

                                                    fetch(`${url}/api/v2/auth/betHistory`, requestOptions)
                                                        .then(response => response.json())
                                                        .then(result => {
                                                            if (result.success) {
                                                                setId(Math.floor(Math.random() * 10000000000) + 1)
                                                                const { message } = result.success;
                                                                setSuccess(message)
                                                                var myHeaders = new Headers();
                                                                myHeaders.append("signatures", "5a1131f2eb747be50714281ec3e68b759476c6dc9e1faf5fc5d91c552cf8c230");
                                                                myHeaders.append("Authorization", `Bearer ${get}`);

                                                                var requestOptions = {
                                                                    method: 'GET',
                                                                    headers: myHeaders,
                                                                    redirect: 'follow'
                                                                };

                                                                fetch(`${url}/api/v2/auth/profile`, requestOptions)
                                                                    .then(response => response.json())
                                                                    .then(result => {
                                                                        if (result.success) {
                                                                            getUser(result.success.data)
                                                                            const { data } = result.success;
                                                                            showBoard(data)
                                                                            //   localStorage.setItem('user', JSON.stringify(data))
                                                                        } else {
                                                                            return;
                                                                        }
                                                                    },
                                                                        (error) => {
                                                                            console.log(error)
                                                                        });

                                                            } else {
                                                                setSuccess('refresh and try again. Thanks.')
                                                                return;
                                                            }
                                                        })
                                                        .catch(error => console.log('error', error));
                                                } else {
                                                    const { type, amount, coupon, odd, gameName, staked, line, date, amounts, possibleWinning, stakes, gameId } = show[0];
                                                    let response = stakes.toString()
                                                    var myHeaders = new Headers();
                                                    myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
                                                    myHeaders.append("Authorization", `Bearer ${get}`);
                                                    myHeaders.append("Content-Type", "application/json");

                                                    var raw = JSON.stringify({
                                                        "game": 'Regular Lotto',
                                                        "amount": `${amount}`,
                                                        "type": `${type}`,
                                                        "line": `${line}`,
                                                        "odd": `${odd}`,
                                                        "gameName": `${gameName}`,
                                                        "amounts": `${amounts}`,
                                                        "possibleWinning": `${possibleWinning}`,
                                                        "staked": `${staked}`,
                                                        "stakes": `${response}`,
                                                        "date": `${date}`,
                                                        "coupon": `${coupon}`,
                                                        "gameId": `${gameId}`
                                                    });

                                                    var requestOptions = {
                                                        method: 'POST',
                                                        headers: myHeaders,
                                                        body: raw,
                                                        redirect: 'follow'
                                                    };

                                                    fetch(`${url}/api/v2/auth/betHistory`, requestOptions)
                                                        .then(response => response.json())
                                                        .then(result => {
                                                            if (result.success) {
                                                                setId(Math.floor(Math.random() * 10000000000) + 1)
                                                                const { message } = result.success;
                                                                setSuccess(message)
                                                                var myHeaders = new Headers();
                                                                myHeaders.append("signatures", "5a1131f2eb747be50714281ec3e68b759476c6dc9e1faf5fc5d91c552cf8c230");
                                                                myHeaders.append("Authorization", `Bearer ${get}`);

                                                                var requestOptions = {
                                                                    method: 'GET',
                                                                    headers: myHeaders,
                                                                    redirect: 'follow'
                                                                };

                                                                fetch(`${url}/api/v2/auth/profile`, requestOptions)
                                                                    .then(response => response.json())
                                                                    .then(result => {
                                                                        if (result.success) {
                                                                            getUser(result.success.data)
                                                                            const { data } = result.success;
                                                                            showBoard(data)
                                                                            //   localStorage.setItem('user', JSON.stringify(data))
                                                                        } else {
                                                                            return;
                                                                        }
                                                                    },
                                                                        (error) => {
                                                                            console.log(error)
                                                                        });

                                                            } else {
                                                                setSuccess('refresh and try again. Thanks.')
                                                                return;
                                                            }
                                                        })
                                                        .catch(error => console.log('error', error));
                                                }
                                            } else {
                                                return;
                                            }
                
                                        },
                                            (error) => {
                                                console.log(error)
                                            }
                                        );
                                }
                            } else {
                                if (amount > user.wallet && amount > user.withdrawable) {
                                    setSuccess('kindly fund your account')
                                    return;
                                } else {
                                    var raw = JSON.stringify({
                                        "totalStake": `${number}`,
                                        "stakes": [
                                            {
                                                "coupon": `${id + 'regularlotto'}`,
                                                "amount": `${amount}`,
                                                "amounts": `${amounts}`,
                                                "type": `${type}`,
                                                "stakeList": `${stakes}`,
                                                "gameName": `${gameName}`,
                                                "stakeList1": `${stakes1}`,
                                                "stakeList2": `${stakes2}`,
                                                "gameId": `${gameId}`,
                                                "game": "Regular Lotto"
                                            },
                                        ]
                                    });

                                    var requestOptions = {
                                        method: 'POST',
                                        headers: myHeaders,
                                        body: raw,
                                        redirect: 'follow'
                                    };

                                    fetch(`${url}/api/v1/placeStake`, requestOptions)
                                        .then(response => response.json())
                                        .then(result => {
                                            if (result.result) {
                                                setSubmited(true)
                                                let show = result.result.map((res) => res)
                                                if (show[0].type === 'AGAINST') {
                                                    const { type, amount, odd, line, staked, gameName, coupon, date, amounts, possibleWinning, gameId, stakes1, stakes2 } = show[0];
                                                    let response1 = stakes1.toString()
                                                    let response2 = stakes2.toString()
                                                    var myHeaders = new Headers();
                                                    myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
                                                    myHeaders.append("Authorization", `Bearer ${get}`);
                                                    myHeaders.append("Content-Type", "application/json");

                                                    var raw = JSON.stringify({
                                                        "game": 'Regular Lotto',
                                                        "amount": `${amount}`,
                                                        "amounts": `${amounts}`,
                                                        "type": `${type}`,
                                                        "line": `${line}`,
                                                        "gameName": `${gameName}`,
                                                        "odd": `${odd}`,
                                                        "possibleWinning": `${possibleWinning}`,
                                                        "staked": `${amount}`,
                                                        "stakes1": `${response1}`,
                                                        "stakes2": `${response2}`,
                                                        "date": `${date}`,
                                                        "coupon": `${coupon}`,
                                                        "gameId": `${gameId}`
                                                    });

                                                    var requestOptions = {
                                                        method: 'POST',
                                                        headers: myHeaders,
                                                        body: raw,
                                                        redirect: 'follow'
                                                    };

                                                    fetch(`${url}/api/v2/auth/betHistory`, requestOptions)
                                                        .then(response => response.json())
                                                        .then(result => {
                                                            if (result.success) {
                                                                setId(Math.floor(Math.random() * 10000000000) + 1)
                                                                const { message } = result.success;
                                                                setSuccess(message)
                                                                var myHeaders = new Headers();
                                                                myHeaders.append("signatures", "5a1131f2eb747be50714281ec3e68b759476c6dc9e1faf5fc5d91c552cf8c230");
                                                                myHeaders.append("Authorization", `Bearer ${get}`);

                                                                var requestOptions = {
                                                                    method: 'GET',
                                                                    headers: myHeaders,
                                                                    redirect: 'follow'
                                                                };

                                                                fetch(`${url}/api/v2/auth/profile`, requestOptions)
                                                                    .then(response => response.json())
                                                                    .then(result => {
                                                                        if (result.success) {
                                                                            getUser(result.success.data)
                                                                            const { data } = result.success;
                                                                            showBoard(data)
                                                                            //   localStorage.setItem('user', JSON.stringify(data))
                                                                        } else {
                                                                            return;
                                                                        }
                                                                    },
                                                                        (error) => {
                                                                            console.log(error)
                                                                        });

                                                            } else {
                                                                console.log(result)
                                                            }
                                                        })
                                                        .catch(error => console.log('error', error));

                    
                                                } else if (show[0].type === '1 BANKER') {
                                                    const { type, amount, coupon, odd, gameName, line, staked, amounts, date, possibleWinning, gameId, stakes } = show[0];
                                                    let response = stakes.toString()
                                                    var myHeaders = new Headers();
                                                    myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
                                                    myHeaders.append("Authorization", `Bearer ${get}`);
                                                    myHeaders.append("Content-Type", "application/json");

                                                    var raw = JSON.stringify({
                                                        "game": 'Regular Lotto',
                                                        "amount": `${amount}`,
                                                        "amounts": `${amounts}`,
                                                        "type": `${type}`,
                                                        "odd": `${odd}`,
                                                        "line": `${line}`,
                                                        "amounts": `${amount}`,
                                                        "gameName": `${gameName}`,
                                                        "possibleWinning": `${possibleWinning}`,
                                                        "staked": `${staked}`,
                                                        "stakes": `${response}`,
                                                        "date": `${date}`,
                                                        "coupon": `${coupon}`,
                                                        "gameId": `${gameId}`
                                                    });

                                                    var requestOptions = {
                                                        method: 'POST',
                                                        headers: myHeaders,
                                                        body: raw,
                                                        redirect: 'follow'
                                                    };

                                                    fetch(`${url}/api/v2/auth/betHistory`, requestOptions)
                                                        .then(response => response.json())
                                                        .then(result => {
                                                            if (result.success) {
                                                                setId(Math.floor(Math.random() * 10000000000) + 1)
                                                                const { message } = result.success;
                                                                setSuccess(message)
                                                                var myHeaders = new Headers();
                                                                myHeaders.append("signatures", "5a1131f2eb747be50714281ec3e68b759476c6dc9e1faf5fc5d91c552cf8c230");
                                                                myHeaders.append("Authorization", `Bearer ${get}`);

                                                                var requestOptions = {
                                                                    method: 'GET',
                                                                    headers: myHeaders,
                                                                    redirect: 'follow'
                                                                };

                                                                fetch(`${url}/api/v2/auth/profile`, requestOptions)
                                                                    .then(response => response.json())
                                                                    .then(result => {
                                                                        if (result.success) {
                                                                            getUser(result.success.data)
                                                                            const { data } = result.success;
                                                                            showBoard(data)
                                                                            //   localStorage.setItem('user', JSON.stringify(data))
                                                                        } else {
                                                                            return;
                                                                        }
                                                                    },
                                                                        (error) => {
                                                                            console.log(error)
                                                                        });

                                                            } else {
                                                                setSuccess('refresh and try again. Thanks.')
                                                                return;
                                                            }
                                                        })
                                                        .catch(error => console.log('error', error));
                                                } else {
                                                    const { type, amount, coupon, line, odd, gameName, staked, date, amounts, possibleWinning, stakes, gameId } = show[0];
                                                    let response = stakes.toString()
                                                    var myHeaders = new Headers();
                                                    myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
                                                    myHeaders.append("Authorization", `Bearer ${get}`);
                                                    myHeaders.append("Content-Type", "application/json");

                                                    var raw = JSON.stringify({
                                                        "game": 'Regular Lotto',
                                                        "amount": `${amount}`,
                                                        "type": `${type}`,
                                                        "odd": `${odd}`,
                                                        "amounts": `${amounts}`,
                                                        "possibleWinning": `${possibleWinning}`,
                                                        "staked": `${staked}`,
                                                        "stakes": `${response}`,
                                                        "line": `${line}`,
                                                        "gameName": `${gameName}`,
                                                        "date": `${date}`,
                                                        "coupon": `${coupon}`,
                                                        "gameId": `${gameId}`
                                                    });

                                                    var requestOptions = {
                                                        method: 'POST',
                                                        headers: myHeaders,
                                                        body: raw,
                                                        redirect: 'follow'
                                                    };

                                                    fetch(`${url}/api/v2/auth/betHistory`, requestOptions)
                                                        .then(response => response.json())
                                                        .then(result => {
                                                            if (result.success) {
                                                                setId(Math.floor(Math.random() * 10000000000) + 1)
                                                                const { message } = result.success;
                                                                setSuccess(message)
                                                                var myHeaders = new Headers();
                                                                myHeaders.append("signatures", "5a1131f2eb747be50714281ec3e68b759476c6dc9e1faf5fc5d91c552cf8c230");
                                                                myHeaders.append("Authorization", `Bearer ${get}`);

                                                                var requestOptions = {
                                                                    method: 'GET',
                                                                    headers: myHeaders,
                                                                    redirect: 'follow'
                                                                };

                                                                fetch(`${url}/api/v2/auth/profile`, requestOptions)
                                                                    .then(response => response.json())
                                                                    .then(result => {
                                                                        if (result.success) {
                                                                            getUser(result.success.data)
                                                                            const { data } = result.success;
                                                                            showBoard(data)
                                                                            //   localStorage.setItem('user', JSON.stringify(data))
                                                                        } else {
                                                                            return;
                                                                        }
                                                                    },
                                                                        (error) => {
                                                                            console.log(error)
                                                                        });

                                                            } else {
                                                                setSuccess('refresh and try again. Thanks.')
                                                                return;
                                                            }
                                                        })
                                                        .catch(error => console.log('error', error));
                                                }
                                            } else {
                                                return;
                                            }
                
                                        },
                                            (error) => {
                                                console.log(error)
                                            }
                                        );
                                }
                            }
                    
                        } else if (props.value.includes('soft lotto')) {

                           var myHeaders = new Headers();
                            myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
                            myHeaders.append("Authorization", `Bearer ${get}`);
                            myHeaders.append("Content-Type", "application/json");
                            myHeaders.append("Cookie", "connect.sid=s%3Ae0Zu5esOyG3kUqFKLpyqNN4lka0d4o3F.WngMLxSIx3GfvGfOlQDWPwr%2BeLG2ABZ6sABSEo4Hwkc");
                            
                            var raw = JSON.stringify({
                                "stakes": [
                                    {
                                        "coupon": `${id + 'softlotto'}`,
                                        "value": `${amount}`,
                                        "amounts": `${amounts}`,
                                        "type": `${type}`,
                                        "kind": `${kind}`,
                                        "numbers": `${stakes}`,
                                        "game": "Soft Lotto"
                                    },
                                ]
                            });

                            var requestOptions = {
                                method: 'POST',
                                headers: myHeaders,
                                body: raw,
                                redirect: 'follow'
                            };

                            fetch(`${url}/api/v1/placeSoftLotto`, requestOptions)
                                .then(response => response.json())
                                .then(result => {
                                    console.log(result)
                                    if (result.result) {
                                        let show = result.result.map((res) => res)
                                        const { type, amount, odd, staked, coupon, date, amounts, kind, possibleWinning, gameId, stakes1, stakes2 } = show[0];
                                        let response1 = stakes.toString()
                                        var myHeaders = new Headers();
                                        myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
                                        myHeaders.append("Authorization", `Bearer ${get}`);
                                        myHeaders.append("Content-Type", "application/json");

                                        var raw = JSON.stringify({
                                            "game": 'Soft Lotto',
                                            "amount": `${amount}`,
                                            "amounts": `${amounts}`,
                                            "type": `${type}`,
                                            "kind": `${kind}`,
                                            "odd": `${odd}`,
                                            "possibleWinning": `${possibleWinning}`,
                                            "staked": `${amount}`,
                                            "stakes": `${response1}`,
                                            "line": `3`,
                                            "date": `${date}`,
                                            "coupon": `${coupon}`,
                                        });

                                        var requestOptions = {
                                            method: 'POST',
                                            headers: myHeaders,
                                            body: raw,
                                            redirect: 'follow'
                                        };

                                        fetch(`${url}/api/v2/auth/betHistory`, requestOptions)
                                            .then(response => response.json())
                                            .then(result => {
                                                if (result.success) {
                                                    setId(Math.floor(Math.random() * 10000000000) + 1)
                                                    const { message } = result.success;
                                                    setSuccess(message)
                                                    var myHeaders = new Headers();
                                                    myHeaders.append("signatures", "5a1131f2eb747be50714281ec3e68b759476c6dc9e1faf5fc5d91c552cf8c230");
                                                    myHeaders.append("Authorization", `Bearer ${get}`);

                                                    var requestOptions = {
                                                        method: 'GET',
                                                        headers: myHeaders,
                                                        redirect: 'follow'
                                                    };

                                                    fetch(`${url}/api/v2/auth/profile`, requestOptions)
                                                        .then(response => response.json())
                                                        .then(result => {
                                                            if (result.success) {
                                                                getUser(result.success.data)
                                                                const { data } = result.success;
                                                                showBoard(data)
                                                                //   localStorage.setItem('user', JSON.stringify(data))
                                                            } else {
                                                                return;
                                                            }
                                                        },
                                                            (error) => {
                                                                console.log(error)
                                                            });

                                                } else {
                                                    setSuccess('Check your internet, refresh and try again. Thanks.')
                                                    return;
                                                }
                                            })
                                            .catch(error => console.log('error', error));
 
                                    } else {
                                        return
                                    }
                
                                },
                                    (error) => {
                                        console.log(error)
                                    }
                                );
                        } else if (props.value.includes('lotto Express')) {
                            console.log('hello world')
                            var myHeaders = new Headers();
                            myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
                            myHeaders.append("Authorization", `Bearer ${get}`);
                            myHeaders.append("Content-Type", "application/json");
                            myHeaders.append("Cookie", "connect.sid=s%3Ae0Zu5esOyG3kUqFKLpyqNN4lka0d4o3F.WngMLxSIx3GfvGfOlQDWPwr%2BeLG2ABZ6sABSEo4Hwkc");

                            console.log(list)

                            list.filter((bet, i) => {
                                const { coupon, type, gameId, stakes, stakes1, stakes2, amount, line, gameName, amounts, status } = bet;


                                if (amount < 1) {
                                    setSuccess(`cannot place bet for ticket number ${i + 1} please add an amount`)
                                    return;
                                } else if (status !== 'PENDING') {
                                    setSuccess(`Cannot place bet on expired coupons`)
                                    return;
                                } else {
                                    if (amount > user.wallet && amount > user.withdrawable) {
                                        setSuccess('kindly fund your account to play')
                                        return;
                                    } else {
  
                                        var raw = JSON.stringify({
                                            "totalStake": `${number}`,
                                            "stakes": [
                                                {
                                                    "coupon": `${id + 'lottoexpress'}`,
                                                    "value": `${amount}`,
                                                    "amounts": `${amounts}`,
                                                    "type": `${type}`,
                                                    "numbers1": `${stakes}`,
                                                    "numbers2": `${stakes1}`,
                                                },
                                            ]
                                        });

                                        var requestOptions = {
                                            method: 'POST',
                                            headers: myHeaders,
                                            body: raw,
                                            redirect: 'follow'
                                        };

                                        fetch(`${url}/api/v1/placeLottoExpressStake`, requestOptions)
                                            .then(response => response.json())
                                            .then(result => {
                                                if (result.result) {
                                                    let show = result.result.map((res) => res)
                                                    if (show[0].type === 'AGAINST') {
                                                        const { type, amount, odd, line, staked, gameName, coupon, date, amounts, possibleWinning, gameId, stakes1, stakes2 } = show[0];
                                                        let response1 = stakes1.toString()
                                                        let response2 = stakes2.toString()
                                                        var myHeaders = new Headers();
                                                        myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
                                                        myHeaders.append("Authorization", `Bearer ${get}`);
                                                        myHeaders.append("Content-Type", "application/json");

                                                        var raw = JSON.stringify({
                                                            "game": 'Lotto Express',
                                                            "amount": `${amount}`,
                                                            "amounts": `${amounts}`,
                                                            "type": `${type}`,
                                                            "line": `${line}`,
                                                            "gameName": `${gameName}`,
                                                            "odd": `${odd}`,
                                                            "possibleWinning": `${possibleWinning}`,
                                                            "staked": `${amount}`,
                                                            "stakes1": `${response1}`,
                                                            "stakes2": `${response2}`,
                                                            "coupon": `${coupon}`,
                                                            "gameId": `${gameId}`
                                                        });

                                                        var requestOptions = {
                                                            method: 'POST',
                                                            headers: myHeaders,
                                                            body: raw,
                                                            redirect: 'follow'
                                                        };

                                                        fetch(`${url}/api/v2/auth/betHistory`, requestOptions)
                                                            .then(response => response.json())
                                                            .then(result => {
                                                                if (result.success) {
                                                                    setId(Math.floor(Math.random() * 10000000000) + 1)
                                                                    const { message } = result.success;
                                                                    setSuccess(message)
                                                                    var myHeaders = new Headers();
                                                                    myHeaders.append("signatures", "5a1131f2eb747be50714281ec3e68b759476c6dc9e1faf5fc5d91c552cf8c230");
                                                                    myHeaders.append("Authorization", `Bearer ${get}`);

                                                                    var requestOptions = {
                                                                        method: 'GET',
                                                                        headers: myHeaders,
                                                                        redirect: 'follow'
                                                                    };

                                                                    fetch(`${url}/api/v2/auth/profile`, requestOptions)
                                                                        .then(response => response.json())
                                                                        .then(result => {
                                                                            if (result.success) {
                                                                                getUser(result.success.data)
                                                                                const { data } = result.success;
                                                                                showBoard(data)
                                                                                //   localStorage.setItem('user', JSON.stringify(data))
                                                                            } else {
                                                                                return;
                                                                            }
                                                                        },
                                                                            (error) => {
                                                                                console.log(error)
                                                                            });

                                                                } else {
                                                                    console.log(result)
                                                                }
                                                            })
                                                            .catch(error => console.log('error', error));

                    
                                                    } else if (show[0].type === '1 BANKER') {
                                                        const { type, amount, coupon, odd, gameName, line, staked, amounts, date, possibleWinning, gameId, stakes } = show[0];
                                                        let response = stakes.toString()
                                                        var myHeaders = new Headers();
                                                        myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
                                                        myHeaders.append("Authorization", `Bearer ${get}`);
                                                        myHeaders.append("Content-Type", "application/json");

                                                        var raw = JSON.stringify({
                                                            "game": 'Lotto Express',
                                                            "amount": `${amount}`,
                                                            "amounts": `${amounts}`,
                                                            "type": `${type}`,
                                                            "odd": `${odd}`,
                                                            "line": `${line}`,
                                                            "amounts": `${amount}`,
                                                            "gameName": `${gameName}`,
                                                            "possibleWinning": `${possibleWinning}`,
                                                            "staked": `${staked}`,
                                                            "stakes": `${response}`,
                                                            "coupon": `${coupon}`,
                                                            "gameId": `${gameId}`
                                                        });

                                                        var requestOptions = {
                                                            method: 'POST',
                                                            headers: myHeaders,
                                                            body: raw,
                                                            redirect: 'follow'
                                                        };

                                                        fetch(`${url}/api/v2/auth/betHistory`, requestOptions)
                                                            .then(response => response.json())
                                                            .then(result => {
                                                                if (result.success) {
                                                                    setId(Math.floor(Math.random() * 10000000000) + 1)
                                                                    const { message } = result.success;
                                                                    setSuccess(message)
                                                                    var myHeaders = new Headers();
                                                                    myHeaders.append("signatures", "5a1131f2eb747be50714281ec3e68b759476c6dc9e1faf5fc5d91c552cf8c230");
                                                                    myHeaders.append("Authorization", `Bearer ${get}`);

                                                                    var requestOptions = {
                                                                        method: 'GET',
                                                                        headers: myHeaders,
                                                                        redirect: 'follow'
                                                                    };

                                                                    fetch(`${url}/api/v2/auth/profile`, requestOptions)
                                                                        .then(response => response.json())
                                                                        .then(result => {
                                                                            if (result.success) {
                                                                                getUser(result.success.data)
                                                                                const { data } = result.success;
                                                                                showBoard(data)
                                                                                //   localStorage.setItem('user', JSON.stringify(data))
                                                                            } else {
                                                                                return;
                                                                            }
                                                                        },
                                                                            (error) => {
                                                                                console.log(error)
                                                                            });

                                                                } else {
                                                                    setSuccess('refresh and try again. Thanks.')
                                                                    return;
                                                                }
                                                            })
                                                            .catch(error => console.log('error', error));
                                                    } else {
                                                        const { type, amount, coupon, line, odd, gameName, staked, date, amounts, possibleWinning, stakes, gameId } = show[0];
                                                        let response = stakes?.toString()
                                                        var myHeaders = new Headers();
                                                        myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
                                                        myHeaders.append("Authorization", `Bearer ${get}`);
                                                        myHeaders.append("Content-Type", "application/json");

                                                        var raw = JSON.stringify({
                                                            "game": 'Lotto Express',
                                                            "amount": `${amount}`,
                                                            "type": `${type}`,
                                                            "odd": `${odd}`,
                                                            "amounts": `${amounts}`,
                                                            "possibleWinning": `${possibleWinning}`,
                                                            "staked": `${staked}`,
                                                            "stakes": `${response}`,
                                                            "line": `${line}`,
                                                            "gameName": `${gameName}`,
                                                            "coupon": `${coupon}`,
                                                            "gameId": `${gameId}`
                                                        });

                                                        var requestOptions = {
                                                            method: 'POST',
                                                            headers: myHeaders,
                                                            body: raw,
                                                            redirect: 'follow'
                                                        };

                                                        fetch(`${url}/api/v2/auth/betHistory`, requestOptions)
                                                            .then(response => response.json())
                                                            .then(result => {
                                                                if (result.success) {
                                                                    setId(Math.floor(Math.random() * 10000000000) + 1)
                                                                    const { message } = result.success;
                                                                    setSuccess(message)
                                                                    var myHeaders = new Headers();
                                                                    myHeaders.append("signatures", "5a1131f2eb747be50714281ec3e68b759476c6dc9e1faf5fc5d91c552cf8c230");
                                                                    myHeaders.append("Authorization", `Bearer ${get}`);

                                                                    var requestOptions = {
                                                                        method: 'GET',
                                                                        headers: myHeaders,
                                                                        redirect: 'follow'
                                                                    };

                                                                    fetch(`${url}/api/v2/auth/profile`, requestOptions)
                                                                        .then(response => response.json())
                                                                        .then(result => {
                                                                            if (result.success) {
                                                                                getUser(result.success.data)
                                                                                const { data } = result.success;
                                                                                showBoard(data)
                                                                                //   localStorage.setItem('user', JSON.stringify(data))
                                                                            } else {
                                                                                return;
                                                                            }
                                                                        },
                                                                            (error) => {
                                                                                console.log(error)
                                                                            });

                                                                } else {
                                                                    setSuccess('refresh and try again. Thanks.')
                                                                    return;
                                                                }
                                                            })
                                                            .catch(error => console.log('error', error));
                                                    }
                                                } else {
                                                    return;
                                                }
                
                                            },
                                                (error) => {
                                                    console.log(error)
                                                }
                                            );
                                    }
                                }
                                // }
                            })
    
                        } else {
                            console.log('hello world')
                        }
                    }
                })
    
            }
        } else {
            setSuccess('Please kindly note that you currently do not have sufficient balance to place this bet')
            return;
        }
    }

    const CouponSubmit = e => {
        e.preventDefault()
        var myHeaders = new Headers();
        myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
        myHeaders.append("Authorization", `Bearer ${get}`);
        myHeaders.append("Content-Type", "application/json");

        var raw = JSON.stringify({
            "coupon": `${coupon}`
        });

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };

        fetch(`${url}/api/v1/coupons`, requestOptions)
            .then(response => response.json())
            .then(result => {
                console.log(result)
                if (result.betCoupons) {
                    setList(result.betCoupons)
                } else {
                    setSuccess(result.error.message)
                }
            })
            .catch(error => console.log('error', error));
    }

    useEffect(() => {
        const timeout = setTimeout(() => {
            setSuccess(false)
        }, 3000)
        return () => clearTimeout(timeout)
    }, [success]);

    const removeItems = id => {
        let newItems = list.filter((item) => item.id !== id)
        if (newItems.length < 1) {
            setList([])
            props.showMobile()
        } else {
            setList(newItems)
        }
    }

  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header>
        <Modal.Title id="contained-modal-title-vcenter" className='d-flex justify-content-center align-items-center'>
        <img src="assets/brand/GrandLotto.svg" alt="" width="200px" height="60px" className="d-inline-block align-text-top nav_img" />          
                  {success && <IntegrationNotistack className='d' success={`${success}`} /> }
        </Modal.Title>
      </Modal.Header>
          <Modal.Body>
              <section>
                  {list?.length < 1 &&
                      <div className='coupon_divs'>
                          <div>
                              <h1 className='search'>Search Coupons</h1>
                          </div>
                          <div>
                              <Box
                                  component="form"
                                  sx={{
                                      '& .MuiTextField-root': { m: 1, width: '25ch' },
                                  }}
                                  noValidate
                                  autoComplete="off"
                              >
                                  <TextField
                                      onChange={handleCouponChange}
                                      value={coupon}
                                      style={{ width: '100%' }}
                                      id="outlined-required"
                                      label="Coupons"
                                  />
                              </Box>
          
                              <Button className='ml-5' variant='success' onClick={CouponSubmit}>Load Coupon</Button>
                          </div>
                      </div>
                  }
                  
                  {
                      list?.length > 0 &&
                      <div className={`${list?.length > 1 && 'scrollers'}`} >
                          <div className='show_coupon' style={{ height: '50vh' }}>
                    
                              {list.map((items) => {
                                    var {amount, id, odd, results, stakes, line, type, stakes1, amounts, stakes2, status, staked, possibleWinning, game, gameId} = items;
                                    return (
                                       <>
                                        <main className='get_line'>
                                            <div className='d-flex justify-content-end'>
                                                <FaTimes onClick={() => {
                                                   removeItems(id)
                                                }}
                                                    className='cancel_game'
                                                />
                                            </div>
                                               <div className=''>
                                                {status !== 'PENDING' && <p className='redz'>Expired ticket</p>}
                                                <p className='p_type'>possibleWinning: &#x20A6;{possibleWinning}</p>
                                                    <p className='p_type'>Odd: {odd}</p>
                                                {user.wallet && <p className='p_type'>status: {status.toLowerCase()}</p>}
                                                <p className='p_type'>Game Name: {game}</p>
                                                <p className='p_type'>Lines: {line}</p>
                                                <p className='p_type'>Stake Amount: &#x20A6;{listV > 0 && listV * line || staked}</p>
                                                <p className='p_type'>Stake Amount per Line: &#x20A6;{listV > 0 && listV || amounts}</p>
                                                {stakes && <p className='p_type'>Bet Numbers: {stakes}</p>}
                                                {stakes1 && <p className='p_type'>Against first: {stakes1}</p>}
                                                {stakes2 && <p className='p_type'>Against second: {stakes2}</p>}
                                                {results && <p className='p_type'>Numbers Drawn {results}</p>}
                                                <p className='p_type'>game Type: {type}</p>
                                                    {status === 'PENDING' && <div>
                                                        <Form.Control className='form_input' pattern='[0-9]' onChange={(e) => handleV(e, items)} value={items.amounts} size='sm' placeholder={amounts} />
                                                        <div className='mt-2 d-flex justify-content-lg-between'>
                                                            <Button className='mr-2 mr-lg-0 games game' value='50' size='sm' onClick={() => {
                                                                setListV(50)
                                                                items.amounts = 50;
                                                                items.amount = 50 * items.line
                                                                items.staked = 50 * items.line
                                                            }}>50
                                                            </Button>
                                                            <Button className='mr-2 mr-lg-0 games game ' size='sm' value='100' size='sm' onClick={() => {
                                                                setListV(100)
                                                                items.amounts = 100;
                                                                items.amount = 100 * items.line
                                                                items.staked = 100 * items.line
                                                            }}>100</Button>
                                                            <Button className='mr-2 mr-lg-0 games game' size='sm' value='500' size='sm' onClick={() => {
                                                                setListV(500)
                                                                items.amounts = 500;
                                                                items.amount = 500 * items.line
                                                                items.staked = 500 * items.line
                                                            }}>500</Button>
                                                            <Button className='mr-2 mr-lg-0 games game' size='sm' value='1000' size='sm' onClick={() => {
                                                                setListV(1000)
                                                                items.amounts = 1000;
                                                                items.amount = 1000 * items.line
                                                                items.staked = 1000 * items.line
                                                            }}>1000</Button>
                                                            <Button className='mr-2 mr-lg-0 games game' size='sm' value='2500' size='sm' onClick={() => {
                                                                setListV(2500)
                                                                items.amounts = 2500;
                                                                items.amount = 2500 * items.line
                                                                items.staked = 2500 * items.line
                                                            }}>2500</Button>
                                                            <Button className='mr-2 mr-lg-0 games game' size='sm' value='5000' size='sm' onClick={() => {
                                                                setListV(5000)
                                                                items.amounts = 5000;
                                                                items.amount = 5000 * items.line
                                                                items.staked = 5000 * items.line
                                                            }}>5000</Button>
                                                        </div>
                                                    </div>
                                                    }
                                               </div>
                                            </main>
                                       </>
                                    )
                                })}
                              <Button size='sm' className={`align-item-center mb-5 game `} variant='success' onClick={handleCouponBet}>Bet Ticket</Button>
                          </div>
                      </div>
                  }
              </section>
      </Modal.Body>
      <Modal.Footer>
              <Button variant='success' onClick={() => { props.onHide(); props.showMobile() }}>Close</Button>
          </Modal.Footer>
    </Modal>
  );
}

function App(props) {
  const [modalShow, setModalShow] = useState(true);

  return (
    <>
      <MyVerticallyCenteredModal
        show={modalShow}
        onHide={() => setModalShow(false)}
        setModalShow={setModalShow}
        showMobile={props.showMobile}
        value={props.value}
          />
    </>
  );
}

export default App