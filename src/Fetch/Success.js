import React, { useState, useEffect } from 'react';
import { Form, Button } from 'react-bootstrap';
import { PaystackButton } from "react-paystack"
import IntegrationNotistack from '../Fetch/IntegrationNotistack';
import { Redirect } from 'react-router-dom'
import Layout from '../Layout'
import { useHistory } from 'react-router';
import { useGlobalContext } from '../store/context';

const FundWallet = (props) => {
    let { giveAccess, isLoggedIn, urls, getUser, showBoard, user, refreshCanvas } = useGlobalContext();
    const [amount, setAmount] = useState(null)
    const publicKey = process.env.REACT_APP_PAYSTACK_PUBLIC_KEY
    const [show, setShow] = useState(false)
    const history = useHistory()
    const get = localStorage.getItem('token')
    const url = 'https://api.grandlotto.ng'
    const [success, setSuccess] = useState('')
    const [showAlert, setShowAlert] = useState(false)
  
    // console.log(urls.indexOf('/'))

    // const getU = () => {
    //     return 
    // }
    // arr.slice(arr.indexOf(0) + 1, arr[arr.length - 1])

    let reference = new Date().getTime().toString()

    let componentProps;

    if (user) {
        
        let val = user.mobile + '@grandlotto.ng';
        
        componentProps = {
            email: user.email !== null ? user.email : val,
            reference,
            amount: parseInt(amount) * 100,
            metadata: {
                name: `${user.firstname} ${user.lastname}`,
                phone: user.mobile
            },
            publicKey,
            text: "Paystack",
            onSuccess: () => {
                var myHeaders = new Headers();
                myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
                myHeaders.append("Authorization", `Bearer ${get}`);
                myHeaders.append("Content-Type", "application/json");

                var raw = JSON.stringify({
                    "reference": `${reference}`
                });

                var requestOptions = {
                    method: 'POST',
                    headers: myHeaders,
                    body: raw,
                    redirect: 'follow'
                };

                //http://localhost:5016
                //https://lotto-api.firstbetng.com/                 

                fetch(`${url}/api/v2/auth/fund-wallet`, requestOptions)
                    .then(response => response.json())
                    .then(result => {
                        if (result.success) {
                            const { message } = result.success;
                            setSuccess(message)
                            var myHeaders = new Headers();
                            myHeaders.append("signatures", "5a1131f2eb747be50714281ec3e68b759476c6dc9e1faf5fc5d91c552cf8c230");
                            myHeaders.append("Authorization", `Bearer ${get}`);

                            var requestOptions = {
                                method: 'GET',
                                headers: myHeaders,
                                redirect: 'follow'
                            };

                            fetch(`${url}/api/v2/auth/profile`, requestOptions)
                                .then(response => response.json())
                                .then(result => {
                                    if (result.success) {
                                        getUser(result.success.data)
                                        const { data } = result.success;
                                        showBoard(data)
                                        return history.push(`${urls}`)
                                        //   localStorage.setItem('user', JSON.stringify(data))
                                    } else {
                                        return;
                                    }
                                },
                                    (error) => {
                                        console.log(error)
                                    });

                        } else {
                            if (result.error) {
                                setSuccess(result.error.message);
                            }
                        }
                    })
                    .catch(error => console.log('error', error));
                setAmount("")
                props.updateProfile()
                setSuccess(`Successfully Credited your account ${refreshCanvas()}`)
            },
            onClose: () => alert("Plase come back soon"),
        };
    }

    const handleSubmit = (e) => {
        e.preventDefault()
    }

    const handleChange = (e) => {
        e.preventDefault()
        setAmount(e.target.value)
    }

    useEffect(() => {
        setTimeout(() => {
            setShowAlert(!showAlert)
        }, 3000)
    }, [success])

    const handleClick = (e) => {
        e.preventDefault()
        history.push(`/games`)
    }

    return <Layout>
        <section className='widths'>
                <span className='span_stack'>Payment using paystack</span>
               <Form className='pt-4' onSubmit={handleSubmit}>
                    <Form.Label htmlFor="inputPassword5">Amount:</Form.Label>
                    <Form.Control
                      type="text"
                        name='amount'
                        value={amount}
                        placeholder='Amount'
                      className='input_widths'
                      onChange={handleChange}
                      id="inputPassword5"
                      aria-describedby="passwordHelpBlock"
                    />
                </Form>
            <PaystackButton className="paystack-button" {...componentProps} />
            
            <a onClick={() => history.push('/games')} style={{ color: 'green', cursor: 'pointer', display: 'block', textDecoration: 'underline'}}>Back to Games</a>
            {/* <Button size='sm' onClick={handleClick} className='ml-1 ml-lg-5 mt-2 mb-2 d-block' variant='outline-success'>Back to Game</Button> */}

        </section>
    </Layout>
}

export default FundWallet


import React, { useState, useEffect } from 'react';
import { Button } from 'react-bootstrap';
import Layout from '../Layout'
import { useHistory } from 'react-router';
import noBet from '../svg/notBet.svg'

const Bets = () => {
    const [arr, setArr] = useState([])
    const url = 'https://api.grandlotto.ng/api/v2/auth/betting-list'
    let history = useHistory()
    
    useEffect(() => {

        let isCancelled = false
        const get = localStorage.getItem('token')

        const fetchData = async () => {
            var myHeaders = new Headers();
            myHeaders.append("signatures", "a0451a967f04a3ac7dc526086749599249a53b3d9e81b71afeb4f3efab8214d5");
            myHeaders.append("Authorization", `Bearer ${get}`);

            var requestOptions = {
                method: 'GET',
                headers: myHeaders,
                redirect: 'follow'
            };

            fetch(url, requestOptions)
                .then(response => response.json())
                .then(result => {
                    const data = result.success.result.user.filter((bets) => {
                            return [ bets.id, bets.bet_id, bets.winningAmount, bets.gameName, bets.kind, bets.status, bets.amounts, bets.results, bets.amount, bets.odd, bets.possibleWinning, bets.coupon, bets.staked, bets.stakes, bets.stakes1, bets.stakes2, bets.date ]       
                    })
                     setArr(data)
                })
                .catch(error => console.log('error', error));
        }

        fetchData()

        return () => {
            isCancelled = true
        }
   
    }, [url])

    const handleClick = (e) => {
        e.preventDefault()
        history.push('/games')
    }

    return (
        <div>
            {arr.length ?
            
            <section className='bet_header_sections d-md-flex flex-md-wrap justify-content-md-center mt-5'>
                {arr.map((a) => {
                    return (
                        <BetHistory key={a.id} {...a} />
                    )
                })}
                </section>
                :
                <section className='bet_header_sections d-flex justify-content-center align-items-center flex-column'>
                    <img className='svg_img' src={noBet} alt="" />
                    <h1 className='bet_header'>Please Place a bet and try again... </h1>
                </section>
            }
            <Button size='sm' onClick={handleClick} className='bets_btn' variant='outline-success'>Play Game</Button>
        </div>
    )
}


const BetHistory = ({ amount, bet_id, winningAmount, coupon, type, odd, kind, amounts, gameName, status, possibleWinning, staked, stakes, stakes1, results, stakes2, date }) => {

    let dates = new Date(date)
    let playTime = dates.toString().slice(0, 24)
    
    return (
        <main className=' ml-md-4 ml-lg-5 main_sec'>
            <section className='betHistory_section ml-2 ml-lg-4'>
            <p className='p_bets'>Game Type: <span className='bets_span'>{type}</span></p>
            {kind && <p className='p_bets'>Soft Lotto: <span className='bets_span'>{kind}</span></p>}
            <p className='p_bets'>Game Time: <span className='bets_span'>{playTime}</span></p>
            <p className='p_bets'>Game ID: <span className='bets_span'>{gameName + " " + playTime}</span></p>
            <p className='p_bets'>Odd: <span className='bets_span'>{odd}</span></p>
            <p className='p_bets'>Possible Winning: <span className='bets_span'>&#x20A6;{possibleWinning}</span></p>
            <p className='p_bets'>Stake Amount per line: <span className='bets_span'>&#x20A6;{amounts}</span></p>
            {stakes && <p className='p_bets'>Numbers: <span className='bets_span'>{stakes}</span></p>}
            {stakes1 && <p className='p_bets'>Against 1: <span className='bets_span'>{stakes1}</span></p>}
            {stakes2 && <p className='p_bets'>Agaisnt 2: <span className='bets_span'>{stakes2}</span></p>}
            <p className='p_bets'>Amount: <span className='bets_span'>&#x20A6;{amount}</span></p>
            <p className='p_bets'>Status: <span className='bets_span'>{status}</span></p>
            <p className='p_bets'>Coupon Code: <span className='bets_span'>{coupon}</span></p>
            {results && <p className='p_bets'>Numbers Drawn: <span className='bets_span'>{results}</span></p>}
            {status === 'WON' && <p className='p_bets'>Winning Amount: <span className='bets_span'>&#x20A6;{winningAmount}</span></p>}    
            </section>
        </main>
        
   )
}

export default Bets



