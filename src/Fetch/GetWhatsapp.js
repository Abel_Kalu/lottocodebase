import React, { useState, useEffect } from 'react';
import { Modal, Button, Form } from 'react-bootstrap';
import { useGlobalContext } from '../store/context';
import { useHistory } from 'react-router';
import IntegrationNotistack from '../Fetch/IntegrationNotistack';
import { Link } from 'react-router-dom';
import login from '../svg/login.svg'


function MyVerticallyCenteredModal(props) {
    let { giveAccess, giveAdminAccess, showBoard, getUser, logOut, logedIn, isLoggedIn } = useGlobalContext();
    const [userLogin, setUserLogin] = useState({ mobile: '', password: '' });
    const [error, setError] = useState('')
    const [success, setSuccess] = useState('')
    const history = useHistory()
    const url = 'https://api.grandlotto.ng'

    useEffect(() => {
        setTimeout(() => {
            setSuccess('')
        }, 3000)
    }, [success]);

    const handleLogin = (e) => {
        e.preventDefault()
        const name = e.target.name
        const value = e.target.value
        setUserLogin({...userLogin, [name]: value})
    }
    const handleSubmit = (e) => {
        e.preventDefault();
        e.stopPropagation()
        var myHeaders = new Headers();
        myHeaders.append("signatures", "lWMVR8oHqcoW4RFuV3GZAD6Wv1X7EQs8y8ntHBsgkug=");
        myHeaders.append("timestamps", "1614848109");
        myHeaders.append("Content-Type", "application/json");

        if(userLogin.mobile.includes('.com') || userLogin.mobile.includes('@')){
            var raw = JSON.stringify({
                "email": `${userLogin.mobile}`,
                "password": `${userLogin.password}`
            });

            var requestOptions = {
                method: 'POST',
                headers: myHeaders,
                body: raw,
                redirect: 'follow'
            };

        fetch(`${url}/api/v1/login`, requestOptions)
            .then(response => response.json())
            .then(result => {
                if (result.success) {
                    const { token } = result.success;
                    giveAccess(token)
                        localStorage.setItem('token', token)
                    
                    var myHeaders = new Headers();
                    myHeaders.append("signatures", "5a1131f2eb747be50714281ec3e68b759476c6dc9e1faf5fc5d91c552cf8c230");
                    myHeaders.append("Authorization", `Bearer ${token}`);

                    var requestOptions = {
                        method: 'GET',
                        headers: myHeaders,
                        redirect: 'follow'
                    };

                    fetch(`${url}/api/v2/auth/profile`, requestOptions)
                        .then(response => response.json())
                        .then(result => {
                            if (result.success) {
                                getUser(result.success.data)
                                const { data } = result.success;
                                showBoard(data)
                                props.setModalShow(false)
                                //   localStorage.setItem('user', JSON.stringify(data))
                            } else {
                                return;
                            }
                        },
                            (error) => {
                                console.log(error)
                            });
                } else {
                    setSuccess('Mobile number and password incorrect')
                    return;
                }
            },
                (error) => {
                    console.log(error)
                }
            );

        userLogin.mobile = ''
        userLogin.password = ''
        }else{

        var raw = JSON.stringify({ 
            "mobile": `${userLogin.mobile}`,
            "password": `${userLogin.password}`
        });

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };

        fetch(`${url}/api/v1/login`, requestOptions)
            .then(response => response.json())
            .then(result => {
                if (result.success) {
                    const { token } = result.success;
                    giveAccess(token)
                    localStorage.setItem('token', token)
                    var myHeaders = new Headers();
                    myHeaders.append("signatures", "5a1131f2eb747be50714281ec3e68b759476c6dc9e1faf5fc5d91c552cf8c230");
                    myHeaders.append("Authorization", `Bearer ${token}`);

                    var requestOptions = {
                        method: 'GET',
                        headers: myHeaders,
                        redirect: 'follow'
                    };

                    fetch(`${url}/api/v2/auth/profile`, requestOptions)
                        .then(response => response.json())
                        .then(result => {
                            if (result.success) {
                                const { data } = result.success;
                                  getUser(result.success.data)
                                  showBoard(data)
                                  props.setModalShow(false)
                                //   localStorage.setItem('user', JSON.stringify(data))
                            } else {
                                return;
                            }
                        },
                            (error) => {
                                console.log(error)
                            });
                } else {
                    setSuccess('Mobile number and password incorrect')
                    return;
                }
            },
                (error) => {
                    console.log(error)
                }
            );

        userLogin.mobile = ''
        userLogin.password = ''
    }
    }

  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header>
        <Modal.Title id="contained-modal-title-vcenter" className='d-flex justify-content-center align-items-center'>
          <img src="assets/brand/GrandLotto.svg" alt="" width="200px" height="60px" className="d-inline-block align-text-top nav_img" />
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form onSubmit={handleSubmit} inline justify-content-center className="d-flex form_btn">
            <div className='d-inline-block'>
            <Form.Control name="mobile" variant="outline-success" onChange={handleLogin} type="text" placeholder="090xxxxxxxx" className="mr-2 modal_form" aria-label="Mobile" />
            <Form.Control onChange={handleLogin} variant="outline-success" type="password" name="password" placeholder="Password" className="mr-2 modal_form" aria-label="Password" />
            <Button size='sm' type='submit' className='mr-3' variant="success">Login</Button>
            </div>
        </Form>
        <div className='mt-3'>
            <p className='green' style={{marginBottom: '4px'}}>Forgot Password ?</p>
            <Button variant='outline-success' o className='mb-3' onClick={() => {
            history.push('/profile/passwordreset')      
                  }} >Reset</Button>
                  <br />
          <p>New Customer ? <Link className='mt-3 class' to='/register'>Register Here</Link></p>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="outline-success" onClick={props.onHide}>Close</Button>
      </Modal.Footer>
        {success && <IntegrationNotistack success={`${success}`} />}
    </Modal>
  );
}

function App() {
  const [modalShow, setModalShow] = useState(true);

  return (
    <>
      <MyVerticallyCenteredModal
        show={modalShow}
        onHide={() => setModalShow(false)}
        setModalShow={setModalShow}
      />
    </>
  );
}

export default App