import React, { useState, useEffect } from 'react';
import { Button, Modal } from 'react-bootstrap';
import { useHistory } from 'react-router';

const Bet = (props) => {
  const [show, setShow] = useState(false);
  const history = useHistory()

  useEffect(() => {
    setShow(true)
  }, [])

  const handleClear = e => {
    e.preventDefault()
    if (props.type === 'regular') {
      props.calculateTotalStake1()
    } else if (props.type === 'lottoexpress') {
      props.calculateTotalStake1()
    } else if (props.type === 'softlotto') {
      props.calculateTotalStake1()
    } else {
      return;
    }
  }

  return (
    <>
      <Modal
        show={show}
        onHide={() => setShow(false)}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header className='d-flex justify-content-center'>
          <Modal.Title className='green'>GrandLotto</Modal.Title>
        </Modal.Header>
        <Modal.Body className='d-flex justify-content-center yellow'>
          {props.mode}
        </Modal.Body>
        <Modal.Footer className='d-flex justify-content-between'>
          <Button size='sm' variant="outline-secondary" onClick={() => { setShow(false); props.setOpen() }}>
            Rebet
          </Button>
          <Button size='sm' variant="outline-secondary" onClick={() => { setShow(false); props.setOpen() }}>
            Close
          </Button>
          <Button size='sm' variant="outline-success" onClick={() => { history.push('/fundwallet'); props.setOpen() }}>
            Deposit
          </Button>
          <Button size='sm' variant="outline-primary" onClick={(e) => { setShow(false); handleClear(e); props.setOpen(); props.betSlip([]); props.number(0); props.count(0) }}>Clear </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}

export default Bet