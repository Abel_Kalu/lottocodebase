import React, { useState, useEffect } from 'react';
import { Button, Modal } from 'react-bootstrap';
import { useHistory } from 'react-router';
import { useGlobalContext } from '../store/context';

const Bet = (props) => {
  const { getUrl } = useGlobalContext();
  const [show, setShow] = useState(false);
  const history = useHistory()

  useEffect(() => {
    setShow(true)
  }, [])

  return (
    <>
      <Modal
        show={show}
        onHide={() => setShow(false)}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header className='d-flex justify-content-center'>
          <Modal.Title className='green'>GrandLotto</Modal.Title>
        </Modal.Header>
        <Modal.Body className='d-flex justify-content-center yellow align-center'>
          {props.mode}
        </Modal.Body>
        <Modal.Footer className='d-flex justify-content-between'>
        <Button size='sm' variant="outline-secondary" onClick={() => { setShow(false); props.setFundModal(false) }}>
            Close 
          </Button>
          <Button size='sm' variant="outline-success" onClick={() => { history.push('/fundwallet'); getUrl(props.currentUrl)}}>
            Deposit
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}

export default Bet